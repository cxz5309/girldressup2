﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public enum FriendType
{
    Char, BG, Object1, Object2, Animal, Bubble, Text
}

public class Friend : MonoBehaviour
{
    public GameObject goRender;
    public GameObject goCanvasPrefab;
    public GameObject goTools;
    public GameObject goFriendRoot;

    public GameObject prefabTools;
    public GameObject prefabTextTools;

    public CharSimpleObjectPool charSimpleObjectPool;
    public CharPartSetting charPartSetting;

    public MainCanvas goMainCanvas;
    public MakingCanvas goMakingCanvas;

    public Dictionary<int, CharPart> charPartsDic = new Dictionary<int, CharPart>();

    public int groupSeq;

    public FriendType friendType;

    //TransformInfo
    public int trSeq;
    public int friendSeq;
    public float positionX;
    public float positionY;
    public int sortingOrder;
    public float scale;
    public bool isReversed;

    //오브젝트
    public float objectRenderSizeX;
    public float objectRenderSizeY;

    public static float CHAR_REN_SIZE_X = 400;
    public static float CHAR_REN_SIZE_Y = 870;
    public static float TEXTOBJ_REN_SIZE_X = 112;
    public static float TEXTOBJ_REN_SIZE_Y = 112;
    public static float TEXT_REN_SIZE_X = 500;
    public static float TEXT_REN_SIZE_Y = 200;

    //텍스트 오브젝트
    public string text;
    public bool isText;

    public void SetTrInfo(CharTr charTr)
    {
        this.trSeq = int.Parse(charTr.TrSeq);
        this.friendSeq = int.Parse(charTr.UsedSeq);
        this.positionX = charTr.Position.x;
        this.positionY = charTr.Position.y;
        this.sortingOrder = charTr.Order;
        this.scale = charTr.Scale;
        this.isReversed = charTr.IsReversed;
    }

    public void SetTrInfo(int friendSeq, int trSeq, float positionX, float positionY, int sortingOrder, float scale, bool isReversed)
    {
        this.trSeq = trSeq;
        this.friendSeq = friendSeq;
        this.positionX = positionX;
        this.positionY = positionY;
        this.sortingOrder = sortingOrder;
        this.scale = scale;
        this.isReversed = isReversed;
    }

    public void SetGroup(int setGroupSeq)
    {
        groupSeq = setGroupSeq;
        switch (groupSeq)
        {
            case 0:
                friendType = FriendType.Char;
                break;
            case 1:
                friendType = FriendType.BG;
                break;
            case 2:
                friendType = FriendType.Object1;
                break;
            case 3:
                friendType = FriendType.Object2;
                break;
            case 4:
                friendType = FriendType.Animal;
                break;
            case 5:
                friendType = FriendType.Bubble;
                break;
            case 6:
                friendType = FriendType.Text;
                break;
        }
    }

    //Tr xml이 없을때 새로 생성하는 것 
    public void SetMainCharTr(CharTr charTr)
    {
        SetTrInfo(charTr);
        SaveTrDic(groupSeq, charTr);
        SetTransform();
        XMLManager.Instance.CreateOneMAINGROUPTr(groupSeq, charTr.TrSeq, charTr.UsedSeq, charTr);
    }


    public void SaveTrDic(int groupSeq, CharTr tmpTr)
    {
        InMainManager.Instance.allScreenChar[groupSeq].Add(trSeq, tmpTr);
    }

    //캐릭터 크기는 고정
    public void SetCharacterToolLine()
    {
        objectRenderSizeX = CHAR_REN_SIZE_X;
        objectRenderSizeY = CHAR_REN_SIZE_Y;
    }
    //텍스트 크기는 고정
    public void SetTextObjToolLine()
    {
        objectRenderSizeX = TEXTOBJ_REN_SIZE_X;
        objectRenderSizeY = TEXTOBJ_REN_SIZE_Y;
    }

    public void SetObj1Render(string imgName)
    {
        SpriteRenderer obj1Sprite = goRender.GetComponent<SpriteRenderer>();

        obj1Sprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllObject1Atlas, imgName);
        objectRenderSizeX = obj1Sprite.sprite.rect.width;
        objectRenderSizeY = obj1Sprite.sprite.rect.height;
        GetComponent<BoxCollider2D>().size = new Vector2(objectRenderSizeX * 0.01f, objectRenderSizeY * 0.01f);
    }
    public void SetObj2Render(string imgName)
    {
        SpriteRenderer obj2Sprite = goRender.GetComponent<SpriteRenderer>();

        obj2Sprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllObject2Atlas, imgName);
        objectRenderSizeX = obj2Sprite.sprite.rect.width;
        objectRenderSizeY = obj2Sprite.sprite.rect.height;
        GetComponent<BoxCollider2D>().size = new Vector2(objectRenderSizeX * 0.01f, objectRenderSizeY * 0.01f);
    }
    public void SetAnimalRender(string imgName)
    {
        SpriteRenderer animalSprite = goRender.GetComponent<SpriteRenderer>();

        animalSprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllAnimalAtlas, imgName);
        objectRenderSizeX = animalSprite.sprite.rect.width;
        objectRenderSizeY = animalSprite.sprite.rect.height;
        GetComponent<BoxCollider2D>().size = new Vector2(objectRenderSizeX/100, objectRenderSizeY * 0.01f);
    }
    public void SetTextObjRender(string imgName)
    {
        SpriteRenderer textSprite = goRender.GetComponent<SpriteRenderer>();

        textSprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllTextAtlas, imgName);
        //objectRenderSizeX = textSprite.sprite.rect.width;
        //objectRenderSizeY = textSprite.sprite.rect.height;
        SetTextObjToolLine();
        GetComponent<BoxCollider2D>().size = new Vector2(objectRenderSizeX * 0.01f, objectRenderSizeY * 0.01f);
    }
    public void SetBubbleObjRender(string imgName)
    {
        SpriteRenderer textSprite = goRender.GetComponent<SpriteRenderer>();

        textSprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllBubbleAtlas, imgName);
        objectRenderSizeX = textSprite.sprite.rect.width;
        objectRenderSizeY = textSprite.sprite.rect.height;
        GetComponent<BoxCollider2D>().size = new Vector2(objectRenderSizeX * 0.01f, objectRenderSizeY * 0.01f);

    }
    public void SetTextRender(GameObject text, int usedSeq)
    {
        RectTransform textRect = text.GetComponent<RectTransform>();
        TMPObject tmp = text.GetComponent<TMPObject>();

        textRect.sizeDelta = new Vector2(objectRenderSizeX * 0.1f, objectRenderSizeY * 0.1f - ObjectInfo.bubbleHeightDowns[usedSeq]);
        textRect.localPosition = new Vector3(textRect.localPosition.x, textRect.localPosition.y + ObjectInfo.bubblePosUps[usedSeq], 0.001f);
        textRect.sizeDelta = new Vector2(textRect.sizeDelta.x * ObjectInfo.bubbleWidthRatios[usedSeq], textRect.sizeDelta.y * ObjectInfo.bubbleHeightRatios[usedSeq]);
        //GetComponentInChildren<TMPObject>().GetComponent<RectTransform>().pivot = new Vector2(.5f, 1f);
        //GetComponentInChildren<TMPObject>().GetComponent<RectTransform>().sizeDelta = new Vector2(objectRenderSizeX / 10, GetComponentInChildren<TMPObject>().GetComponent<RectTransform>().sizeDelta.y - 20f);

        //여기서 텍스트 세팅?
        //GetComponent<TextMesh>().text = "꾹 누르기";
        //GetComponent<TextObj>().SetTextSetting();
        //SetTextToolLine();
        //GetComponent<BoxCollider2D>().size = new Vector2(objectRenderSizeX / 10, objectRenderSizeY / 10);
    }

    public void CreateCharacter(List<CharPart> list)
    {
        //1. 이전 오브젝트 제거
        foreach (GameObject goPart in charPartSetting.charPartsPrefabDic.Values)
        {
            charSimpleObjectPool.UnsetNowPart(goPart.GetComponent<CharPartUse>().charPart.charPartItem.kindInt);
            //Destroy(goPart.gameObject);
        }
        charPartSetting.charPartsPrefabDic.Clear();
        //2. 로드

        //여기까지 init
        //3. 생성
        for (int i = 0; i < list.Count; i++)
        {
            charPartSetting.CreatePartNotXMLChange(list[i], (KindType)0);
        }
    }
  
    public GameObject SetTools()
    {
        goTools = Instantiate(prefabTools, transform.position, Quaternion.identity, transform);
        goTools.transform.SetParent(transform.parent);
        goTools.GetComponent<Tools>().SetToolTrTop(transform.localPosition);
        goTools.transform.localScale = new Vector3(1f, 1f, 1f);
        this.GetComponent<Drag>().friend = this;
        return goTools;
    }

    public void SetTransform()
    {
        if (InMainManager.Instance.maxTrSeq < trSeq)
        {
            InMainManager.Instance.maxTrSeq = trSeq;
        }

        //순서에 따라 z축 위치 변경하여 콜라이더 올리기
        goFriendRoot.transform.localPosition = new Vector3(positionX, positionY, sortingOrder * -0.01f);
        transform.localPosition = Vector3.zero;

        goFriendRoot.transform.localScale = new Vector3(scale, scale, 1);
        GetComponent<SortingGroup>().sortingOrder = sortingOrder;
        if (InMainManager.Instance.sortingTop < sortingOrder)
        {
            InMainManager.Instance.sortingTop = sortingOrder;
        }
        //        if (isReversed)

        if (isReversed)
        {
            Quaternion newQuaternion = Quaternion.Euler(new Vector3(0, 180, 0));
            goRender.transform.rotation = goRender.transform.rotation * newQuaternion;
        }
        //{
        //    for (int i = 0; i < 2; i++)
        //    {
        //        if (isReversed)
        //        { i = 1; }
        //        Quaternion newQuaternion = Quaternion.Euler(new Vector3(0, 180, 0));
        //        goRender.transform.rotation = goRender.transform.rotation * newQuaternion;
        //    }
        //}
        //Debug.Log("툴 생성 바로위");
        SetTools();
    }

    public void SetTrValueXml(string att, string value)
    {
        XMLManager.Instance.SetOneMAINGROUPTrByTrSeq(groupSeq, trSeq, att, value);
    }

    public void RemoveThisTr()
    {
        XMLManager.Instance.RemoveOneMAINGROUPTrByTrSeq(groupSeq, trSeq);
        InMainManager.Instance.allScreenChar[groupSeq].Remove(trSeq);
    }

    public static int StringToIntFriendType(string nowButtonMenu)
    {
        return (int)(FriendType)Enum.Parse(typeof(FriendType), nowButtonMenu);
    }
}
