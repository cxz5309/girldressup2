﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Rendering;

public class CharPartSetting : MonoBehaviour
{
    public CharSimpleObjectPool charSimpleObjectPool;
    public Dictionary<int, GameObject> charPartsPrefabDic = new Dictionary<int, GameObject>();
    public Friend friend;

    StringBuilder stringBuilder = new StringBuilder();

    //xml이 이미 있는 상태에서 생성만 하기   
    public void CreatePartNotXMLChange(CharPart element, KindType parentKind)
    {
        GameObject obj;
        if (element.charPartItem.kind.kindType.Equals( KindType.FaceAc))
        {
            obj = charSimpleObjectPool.NowPartMultiple(element.charPartItem.kindInt, element.charPartItem.seq);
            if(obj == null)
            {
                //Debug.Log("널값 리턴됨");
                return;
            }
        }
        else
        {
            obj = charSimpleObjectPool.NowPart(element.charPartItem.kindInt);
        }
        CharPartUse useCharPart = obj.GetComponent<CharPartUse>();

        useCharPart.charPart = element;

        useCharPart.SetPosition();
        useCharPart.SetSprite();
        useCharPart.SetSortingOrder();

        if (charPartsPrefabDic.ContainsKey((int)element.charPartItem.kind.kindType))
        {
            charPartsPrefabDic[(int)element.charPartItem.kind.kindType] = obj;
        }
        else
        {
            charPartsPrefabDic.Add((int)element.charPartItem.kind.kindType, obj);
        }
    }

    //이미 동일 카테고리 파트가 있을 수도 있는경우 삭제하고 새로만들기
    public void ChangePart(CharPart element, KindType parentKind = 0)
    {
        GameObject obj;
        if (element.charPartItem.kind.kindType.Equals(KindType.FaceAc)) {
            obj = charSimpleObjectPool.NowPartMultiple(element.charPartItem.kindInt, element.charPartItem.seq);
            if (obj == null)
            {
                //Debug.Log("널값 리턴됨");
                return;
            }
        }
        else
        {
            XMLManager.Instance.RemovePartByPartKind(element.charPartItem.kindInt);

            obj = charSimpleObjectPool.NowPart(element.charPartItem.kindInt);
        }
        XMLManager.Instance.CreateOnePartMain_Negative(element.charPartItem.seq);

        CharPartUse useCharPart = obj.GetComponent<CharPartUse>();

        useCharPart.charPart = element;

        useCharPart.SetPosition();
        useCharPart.SetSprite();
        useCharPart.SetSortingOrder();

        if (charPartsPrefabDic.ContainsKey((int)element.charPartItem.kind.kindType))
        {
            charPartsPrefabDic[(int)element.charPartItem.kind.kindType] = obj;
        }
        else
        {
            charPartsPrefabDic.Add((int)element.charPartItem.kind.kindType, obj);
        }
    }
    //삭제하기 
    public void RemovePart(int kindNum)
    {
        if (XMLManager.Instance.isPartByPartKind(kindNum))
        {
            if (!charPartsPrefabDic.ContainsKey(kindNum))
            {
                //Debug.Log("why");
            }
            else
            {
                //xml에서 지우기
                XMLManager.Instance.RemovePartByPartKind(kindNum);
                //이미지 셋 지우기 
                charSimpleObjectPool.UnsetNowPart(kindNum);
                //딕셔너리에서 지우기
                charPartsPrefabDic.Remove(kindNum);
            }
        }
        else
        {
            //Debug.Log("삭제할 아이템이 없음 kindNum : " + kindNum);
        }
    }

    public bool CheckIsOn(KindType kindType)
    {
        if (charPartsPrefabDic.ContainsKey((int)kindType))
        {
            return true;
        }
        return false;
    }

    //파트 삭제할때 예외사항 체크하기
    public void CheckRemovePart(KindType kindNum)
    {
        CharPart newTmpPart;

        CharPart createCharPart;

        //삭제와 같은 의미
        RemovePart((int)kindNum);

        switch (kindNum)
        {
            //아우터일 경우
            case KindType.Outer:

                //팔있는 셔츠 추가
                if (CheckIsOn(KindType.Shirts))
                {
                    int spNum = XMLManager.Instance.SelectPartByPartKind((int)KindType.Shirts).charPartItem.spNum;
                    XMLManager.Instance.UsePartByPartOrder((int)OrderType.Shirts, spNum);

                    createCharPart = XMLManager.Instance.newTmpPart;
                    CheckCreatePart(createCharPart, (KindType)kindNum);
                }
                //아우터 백 삭제
                if (CheckIsOn(KindType.OuterBack))
                {
                    CheckRemovePart(KindType.OuterBack);
                    //Debug.Log("아우터 백이 있다-> 삭제");
                }
                //손 백 삭제
                if (CheckIsOn(KindType.HandBackO))
                {
                    CheckRemovePart(KindType.HandBackO);
                }
                //손 삭제
                if (CheckIsOn(KindType.HandO))
                {
                    CheckRemovePart(KindType.HandO);
                }
                break;
            case KindType.Shirts:
                //손 백 삭제
                if (CheckIsOn(KindType.HandBackS))
                {
                    CheckRemovePart(KindType.HandBackS);
                }
                //손 삭제
                if (CheckIsOn(KindType.HandS))
                {
                    CheckRemovePart(KindType.HandS);
                }
                break;
            case KindType.Dress:

                //손 백 삭제
                if (CheckIsOn(KindType.HandBackD))
                {
                    CheckRemovePart(KindType.HandBackD);
                }
                //손 삭제
                if (CheckIsOn(KindType.HandD))
                {
                    CheckRemovePart(KindType.HandD);
                }
                break;
            //슬립을 삭제할 경우
            case KindType.Slip:
                if (!CheckIsOn(KindType.Underwear))
                {
                    XMLManager.Instance.UsePartByPartOrder((int)OrderType.Underwear, 0);
                    newTmpPart = XMLManager.Instance.newTmpPart;
                    ChangePart(newTmpPart, (KindType)kindNum);
                }
                break;
        }
    }

    public void CheckCreatePart(CharPart charPart, KindType parentKind)
    {
        KindType thisKindType = charPart.charPartItem.kind.kindType;
        CharPart createCharPart;
        string tmpItemSeq;

        //파트 추가 및 삭제
        int handSeq = charPart.HandSeqToInt();
        int handType = charPart.handType;
        ChangePart(charPart, parentKind);

        //손@@@@@@@@@@@@@@@@@@@@@@@
        if (thisKindType.Equals(KindType.Shirts) || thisKindType.Equals(KindType.Outer) || thisKindType.Equals( KindType.Dress))
        {
            if (handSeq != -1)  
            {
                bool dontCreateHand = false;
                if (thisKindType.Equals( KindType.Shirts ))
                {
                    if (CheckIsOn(KindType.Outer))
                    {
                        if ( charPartsPrefabDic[(int)KindType.Outer].GetComponent<CharPartUse>().charPart.charPartItem.seq != "054115")
                        {
                            dontCreateHand = true;
                        }
                    }
                }
                if (!dontCreateHand)
                {
                    //인잇으로 대체
                    //HandMatch.MachingWithParent(thisKindType);
                    if (handType == 0)
                    {
                        createCharPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.MakeItemSeq((int)HandMatch.handOrder[HandMatch.kindNumToArrayNum(thisKindType)], handSeq));
                        //Debug.Log("현재 손이 있다");
                    }
                    else
                    {
                        createCharPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.MakeItemSeq((int)HandMatch.uHandOrder[HandMatch.kindNumToArrayNum(thisKindType)], handSeq));
                        //Debug.Log("현재 뒤손이 있다");
                    }
                    CheckCreatePart(createCharPart, thisKindType);
                }
            }
            else
            {
                CheckRemovePart(HandMatch.handKind[HandMatch.kindNumToArrayNum(thisKindType)]);
                CheckRemovePart(HandMatch.handBackKind[HandMatch.kindNumToArrayNum(thisKindType)]);

                friend.goMakingCanvas.isHand = false;
                //Debug.Log("이 의상에는 손이 없다");
            }
        }

        switch (thisKindType)
        {
            //드레스일경우
            case KindType.Dress:
                //옷, 바지 겉옷 삭제
                CheckRemovePart(KindType.Pants);
                CheckRemovePart(KindType.Shirts);
                CheckRemovePart(KindType.Slip);
                break;
            //아우터일경우
            case KindType.Outer:
                //옷 교체 Shirts -> WithoutArmsShirts
                
                if (CheckIsOn(KindType.Shirts))
                {
                    if (charPart.charPartItem.seq != "054115")
                    {
                        stringBuilder.Clear();
                        stringBuilder.Append(ParseItemSeq.numToD3((int)OrderType.WithoutArmsShirts)).Append(charPartsPrefabDic[(int)KindType.Shirts].GetComponent<CharPartUse>().charPart.charPartItem.spNum.ToString("D3"));
                        string withoutArmsSeq = stringBuilder.ToString();
                        //대응되는 팔없는 셔츠 아이템이 존재할 경우
                        if (XMLManager.Instance.IsCharPartItem(withoutArmsSeq))
                        {

                            createCharPart = XMLManager.Instance.UsePartByItemSeq(withoutArmsSeq);
                            CheckCreatePart(createCharPart, thisKindType);
                            //Debug.Log("셔츠는 팔이 없어진다");
                        }
                    }
                    else
                    {
                        stringBuilder.Clear();
                        stringBuilder.Append(ParseItemSeq.numToD3((int)OrderType.Shirts)).Append(charPartsPrefabDic[(int)KindType.Shirts].GetComponent<CharPartUse>().charPart.charPartItem.spNum.ToString("D3"));
                        string armsSeq = stringBuilder.ToString();
                        //대응되는 팔없는 셔츠 아이템이 존재할 경우

                        if (XMLManager.Instance.IsCharPartItem(armsSeq))
                        {
                            int spNum = XMLManager.Instance.SelectPartByPartKind((int)KindType.Shirts).charPartItem.spNum;
                            XMLManager.Instance.UsePartByPartOrder((int)OrderType.Shirts, spNum);

                            createCharPart = XMLManager.Instance.newTmpPart;
                            CheckCreatePart(createCharPart, thisKindType);
                            //Debug.Log("셔츠는 팔이 없어진다");
                        }
                    }
                    //}
                }
                else
                {
                    //Debug.Log("현재 옷이 없다");
                }
                //아우터 백이 있을 경우
                stringBuilder.Clear();
                stringBuilder.Append(ParseItemSeq.numToD3((int)OrderType.OuterBack)).Append(charPart.charPartItem.spNum.ToString("D3"));
                tmpItemSeq = stringBuilder.ToString();
                if (XMLManager.Instance.allPartItemDic.ContainsKey(tmpItemSeq))
                {
                    stringBuilder.Clear();
                    stringBuilder.Append(ParseItemSeq.numToD3((int)OrderType.OuterBack)).Append(charPart.charPartItem.spNum.ToString("D3"));

                    createCharPart = XMLManager.Instance.UsePartByItemSeq(stringBuilder.ToString());
                    CheckCreatePart(createCharPart, thisKindType);
                    //Debug.Log("이 의상에는 아우터 백이 있다");
                }
                else
                {
                    CheckRemovePart(KindType.OuterBack);

                    //Debug.Log("이 의상에는 아우터 백이 없다");
                }
                //슬립 삭제
                CheckRemovePart(KindType.Slip);
                break;
            //셔츠일경우
            case KindType.Shirts:
                CheckRemovePart(KindType.Dress);
                CheckRemovePart(KindType.Slip);

                //아우터를 이미 입고있을 경우
                if (charPartsPrefabDic.ContainsKey((int)KindType.Outer))
                {
                    Debug.Log(5);
                    if(charPartsPrefabDic[(int)KindType.Outer].GetComponent<CharPartUse>().charPart.charPartItem.seq == "054115")
                    {
                        //Debug.Log(6);
                        //stringBuilder.Clear();
                        //stringBuilder.Append(ParseItemSeq.numToD3((int)OrderType.Shirts)).Append(charPart.charPartItem.spNum.ToString("D3"));

                        //tmpItemSeq = stringBuilder.ToString();
                        //if (!XMLManager.Instance.allPartItemDic.ContainsKey(tmpItemSeq))
                        //{
                        //    Debug.Log(7);

                        //    return;
                        //}
                        //Debug.Log(8);

                        //XMLManager.Instance.UsePartByItemSeq((int)OrderType.Shirts, charPart.charPartItem.spNum);
                        //createCharPart = XMLManager.Instance.newTmpPart;
                        return;
                    }

                    //셔츠가 이미 팔이 없는 셔츠일경우
                    stringBuilder.Clear();
                    stringBuilder.Append(ParseItemSeq.numToD3((int)OrderType.WithoutArmsShirts)).Append(charPart.charPartItem.spNum.ToString("D3"));

                    tmpItemSeq = stringBuilder.ToString();
                    if (!XMLManager.Instance.allPartItemDic.ContainsKey(tmpItemSeq))
                    {
                        return;
                    }

                    //셔츠는 팔이 없어진다
                    XMLManager.Instance.UsePartByPartOrder((int)OrderType.WithoutArmsShirts, charPart.charPartItem.spNum);
                    createCharPart = XMLManager.Instance.newTmpPart;
                    //손은 없앨 필요가 없다 - > 어짜피 애초에 안나옴
                    ChangePart(createCharPart, thisKindType);
                    return;
                }

                break;
            case KindType.Pants:
                CheckRemovePart(KindType.Dress);
                CheckRemovePart(KindType.Slip);
                break;

            //손일경우
            case KindType.HandO:
            case KindType.HandD:
            case KindType.HandS:
                SetHandColor(thisKindType, parentKind);
                break;
            case KindType.Slip:
                CheckRemovePart(KindType.Pants);
                CheckRemovePart(KindType.Shirts);
                CheckRemovePart(KindType.Outer);
                CheckRemovePart(KindType.Dress);
                CheckRemovePart(HandMatch.handKind[0]);
                CheckRemovePart(HandMatch.handBackKind[0]);
                CheckRemovePart(HandMatch.handKind[1]);
                CheckRemovePart(HandMatch.handBackKind[1]);
                CheckRemovePart(HandMatch.handKind[2]);
                CheckRemovePart(HandMatch.handBackKind[2]);
                CheckRemovePart(KindType.Stocking);
                CheckRemovePart(KindType.OuterBack);
                CheckRemovePart(KindType.Underwear);
                break;
        }
    }

    //생성된 손을 바로 셋백하고 몸색깔로 바꾸어주면서 makingcanvas의 handRen도 이걸로 바꾸고 isHand도 true 해야함
    public void SetHandColor(KindType thisKind, KindType parentKind)
    {
        if (!XMLManager.Instance.isPartByPartKind((int)KindType.BodyBack))
        {
            return;
        }

        friend.goMakingCanvas.isHand = true;

        friend.goMakingCanvas.SetHandBack(parentKind);

        friend.goMakingCanvas.SetHandRenderObject();
        CharPart tmpPart = XMLManager.Instance.SelectPartByPartKind((int)KindType.BodyBack);

        for(int i = 0; i < 3; i++)
        {
            if (friend.goMakingCanvas.handRenDic.ContainsKey(HandMatch.kindNumToArrayNum(parentKind)))
            {
                friend.goMakingCanvas.SetRenColor(friend.goMakingCanvas.handRenDic[HandMatch.kindNumToArrayNum(parentKind)], tmpPart.color);
            }
        }
        XMLManager.Instance.ChangeColorByPartKind((int)HandMatch.handBackKind[HandMatch.kindNumToArrayNum(parentKind)], tmpPart.color);
    }

    //바지와 상의 sibling num 변경하기
    public void ChangePantsSibling()
    {
        if (charPartsPrefabDic.ContainsKey((int)KindType.Pants))
        {
            CharPartUse charPartUse = charPartsPrefabDic[(int)KindType.Pants].GetComponent<CharPartUse>();
            charPartUse.SetPantType(charPartUse.charPart.pantType.Equals(0) ? 1 : 0);
            XMLManager.Instance.ChangePantTypeByPartKind((int)KindType.Pants, charPartUse.charPart.pantType);
            charPartUse.SortingPant();
        }
        else
        {
            //Debug.Log("바지 없음");
        }
    }

}
