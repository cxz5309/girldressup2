﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class charPartUI : MonoBehaviour
{
    public Sprite overrideSprite;

    public int charPartSibling;

    private void Start()
    {
        overrideSprite = GetComponent<Image>().overrideSprite;

        RectTransform pos = GetComponent<RectTransform>();
        pos.sizeDelta = overrideSprite.rect.size;
    }

}
