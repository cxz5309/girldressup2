﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharSimpleObjectPool : MonoBehaviour
{
    public GameObject prefabPartObject;
    public GameObject prefabMultiLayer;

    public Dictionary<int, GameObject> indexPartDic = new Dictionary<int, GameObject>();
    public Dictionary<int, GameObject> multiContainerDic = new Dictionary<int, GameObject>();
    public Dictionary<string, GameObject> FaceAcDic = new Dictionary<string, GameObject>();

    public GameObject NowPart(int kind)
    {
        if (kind != (int)KindType.FaceAc) {
            if (indexPartDic.ContainsKey(kind))
            {
                indexPartDic[kind].SetActive(true);
                return indexPartDic[kind];
            }
            else
            {
                GameObject indexPart = Instantiate(prefabPartObject, transform);
                indexPart.name = CharPartKind.IntToString(kind);
                indexPartDic.Add(kind, indexPart);
                return indexPart;
            }
        }
        else
        {
            Debug.Log("멀티플로 바꿔야함");
            return null;
            //if (FaceAcDic.ContainsKey(j))
            //{
            //    return FaceAcDic[j++];
            //}
            //else
            //{
            //    return null;
            //}
        }
    }

    public GameObject NowPartMultiple(int kind, string itemSeq)
    {
        GameObject multiContainer = null;
        GameObject indexPart = null;

        //빈 컨테이너 제작
        if (multiContainerDic.ContainsKey(kind))
        {
            multiContainer = multiContainerDic[kind];
        }
        else
        {
            multiContainer = Instantiate(prefabMultiLayer, transform);
            multiContainerDic.Add(kind, multiContainer);
        }

        //컨테이너 내부에 오브젝트 제작
        if (kind == (int)KindType.FaceAc)
        {
            if (FaceAcDic.ContainsKey(itemSeq))
            {
                indexPart = FaceAcDic[itemSeq];
                indexPart.SetActive(true);
            }
            else
            {
                indexPart = Instantiate(prefabPartObject, multiContainer.transform);
                indexPart.name = CharPartKind.IntToString(kind) + itemSeq.ToString();
                FaceAcDic.Add(itemSeq, indexPart);
            }
        }
        return indexPart;
    }

    //public void SetNowPart(int kind)
    //{
    //    if (indexPartDic.ContainsKey(kind))
    //    {
    //        indexPartDic[kind].SetActive(true);
    //    }
    //}

    public void UnsetNowPart(int kind)
    {
        if(kind == (int)KindType.FaceAc)
        {
            foreach(KeyValuePair<string, GameObject> element in FaceAcDic)
            {
                element.Value.SetActive(false);
            }
        }
        if (indexPartDic.ContainsKey(kind))
        {
            indexPartDic[kind].SetActive(false);
        }
    }
    private void OnDisable()
    {
        indexPartDic.Clear(); 
    }
}
