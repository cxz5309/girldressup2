﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharButtonViewUI : MonoBehaviour {

    //캐릭터
    public GameObject prefabCharButton;
    public GameObject prefabCharCreateButton;
    public Transform charContents;

    public ScrollRect scrollRect;

    public AlertView alertView;

    public int groupSeq;

    private void Start()
    {
        scrollRect.verticalNormalizedPosition = 0f;
    }

    public void ClearViewContents()
    {
        for (int i = 0; i < charContents.childCount; i++)
        {
            Destroy(charContents.GetChild(i).gameObject);
        }
    }

    public void CreateCharButton()
    {
        //50개 버튼생성 제한 두기
        //List<int> Keys = new List<int>(tmpWear.Keys);
        //for(int i = 0; i < 50; i++)
        //{
        //    int idx = new int();
        //    idx = Keys[i];

        //}
        Dictionary<int, List<CharPart>> tmpWear = new Dictionary<int, List<CharPart>>();

        XMLManager.Instance.LoadAllMAIN_WEAR();
        tmpWear = XMLManager.Instance.listAllMainWEAR;

        GameObject createCharButton = Instantiate(prefabCharCreateButton, charContents);
        createCharButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            OnMakingSceneClick();
        });

        List<int> keys = new List<int>(tmpWear.Keys);

        for(int i = 0; i < keys.Count; i++)
        {
            int wearSeq = keys[i];

            GameObject obj = Instantiate(prefabCharButton, charContents);
            CharButtonUI charButtonUI = obj.GetComponent<CharButtonUI>();
            charButtonUI.charButtonViewUI = this;
            charButtonUI.wearSeq = wearSeq;

            charButtonUI.SetAlert(alertView);
            charButtonUI.SetNumText((i+1).ToString());
            charButtonUI.SetNameText(charButtonUI.GetCharName());
            charButtonUI.CreateMainCharImg();

            obj.GetComponent<Button>().onClick.AddListener(() =>
            {
                InMainManager.Instance.CreateChar(groupSeq, wearSeq);
            });
        }
    }

    public void OnMakingSceneClick()
    {
        SceneManager.LoadScene("MakingScene");
    }
}
