﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public enum ItemType
{
    nomalItem, storeItem, spItem
}

public class CharPart
{
    public CharPartItem charPartItem;
    public Color color;
    public float screenX;
    public float screenY;
    public string handSeq;//"-1" = 없음
    public int handType;//0 = 기본, 1 = U(옷뒤에)
    public int pantType;//0 = 앞, 1 = 뒤
    public ItemType itemType;//0 = 무료 아이템, 1 = 상점 아이템, 2 = 특수 아이템
    public int multiOrder; // -1 = 기본, 1 = 멀티

    public CharPart(CharPartItem charPartItem, Color color, string handSeq, int handType, int pantType, int itemType, float screenX = 0, float screenY = 0)
    {
        this.charPartItem = charPartItem;
        this.color = color;
        this.handSeq = handSeq;
        this.handType = handType;
        this.pantType = pantType;
        this.itemType = (ItemType)itemType;
        this.screenX = screenX;
        this.screenY = screenY;
        this.multiOrder = -1;
        if(charPartItem.order.orderType == OrderType.FaceAc)
        {
            multiOrder = 1;
        }
    }
    public void Set(CharPart charPart)
    {
        this.charPartItem = charPart.charPartItem;
        this.color = charPart.color;
        this.handSeq = charPart.handSeq;
        this.handType = charPart.handType;
        this.pantType = charPart.pantType;
        this.itemType = charPart.itemType;
        this.screenX = charPart.screenX;
        this.screenY = charPart.screenY;
        if (charPart.charPartItem.order.orderType == OrderType.FaceAc)
        {
            multiOrder = 1;
        }
    }
    public void Set(CharPartItem charPartItem, Color color, float screenX = 0, float screenY = 0)
    {
        this.charPartItem = charPartItem;
        this.color = color;
        this.screenX = screenX;
        this.screenY = screenY;
    }

    public int HandTypeToInt(string handtypeSt)
    {
        if (handtypeSt.Equals("U"))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public int HandSeqToInt()
    {
        string newHandSeq = handSeq;

        if (handType == 1)
        {
            newHandSeq.TrimEnd('U');
        }
        if (newHandSeq != "" && newHandSeq != null)
        {
            return int.Parse(newHandSeq);
        }
        else
        {
            return -1;
        }
    }
}

public class CharPartUse : MonoBehaviour
{
    public CharPart charPart;

    public float initPositionX;
    public float initPositionY;


    public void SetPosition()
    {
        initPositionX = (charPart.screenX - 540) * 0.01f;
        initPositionY = -((charPart.screenY - 960) * 0.01f);
       
        transform.localPosition = new Vector3(initPositionX, initPositionY, 0);
    }
    public void SetSprite()
    {
        //Debug.Log(charPart.charPartItem.sp.name);
        GetComponent<SpriteRenderer>().sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllPartsAtlas, charPart.charPartItem.spriteName);
        GetComponent<SpriteRenderer>().color = charPart.color;
    }

    public int SetSortingOrder()
    {
        SortingGroup sg = GetComponent<SortingGroup>();
        int siblingNum = (int)charPart.charPartItem.orderInt;
        if (charPart.charPartItem.kind.kindType == KindType.Pants )
        {
            if (charPart.pantType == 0)
            {
                siblingNum = (int)charPart.charPartItem.orderInt;
            }
            else
            {
                siblingNum = (int)charPart.charPartItem.orderInt - 10;
            }
        }
        sg.sortingOrder = siblingNum;
        return siblingNum;
    }

    public void SetImagePosition()
    {
        RectTransform pos = GetComponent<RectTransform>();
        initPositionX = (charPart.screenX - 540);
        initPositionY = -((charPart.screenY - 960));
        pos.localPosition = new Vector3(initPositionX, initPositionY , 0);
    }
    public void SetImageSprite()
    {
        Image image = GetComponent<Image>();

        image.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllPartsAtlas, charPart.charPartItem.spriteName);
        image.color = charPart.color;
    }

    public void SetPantType(int pantType)
    {
        charPart.pantType = pantType;
    }

    public void SortingPant()
    {
        SortingGroup sg = GetComponent<SortingGroup>();

        if (charPart.pantType == 0)
        {
            sg.sortingOrder = (int)charPart.charPartItem.orderInt;
        }
        else
        {
            sg.sortingOrder = (int)charPart.charPartItem.orderInt - 10;
        }
    }
}
