﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class CharButtonUI : MonoBehaviour
{
    public CharButtonViewUI charButtonViewUI;
    public Button namingButton;
    public GameObject contents;
    public GameObject BGObj;
    public Image BG;
    public GameObject alertPanel;
    public AlertView alertView;

    public GameObject prefabCharImgRoot;
    public GameObject prefabPartImg;
    public GameObject prefabMultiRootImg;
    public GameObject prefabImgObject;
    public GameObject selToggleImg;
    public GameObject waterMark;

    public Image slotNameImg;
    public Text nameText;
    public Text slotNumText;

    public int slotSeq;
    public int wearSeq;
    public string friendName;

    public List<int> listPartSibling;
    public List<int> listObjSibling;
    public Dictionary<int, GameObject> listPartObj = new Dictionary<int, GameObject>();
    public Dictionary<int, GameObject> listObj = new Dictionary<int, GameObject>();

    public StringBuilder stringBuilder = new StringBuilder();
    int middleFontsize;

    private void OnEnable()
    {
        InitWaterMark();
    }

    private void Start()
    {
        middleFontsize = LocalizationManager.Language == "Japanese" ? 48 : 52;
    }

    public void InitWaterMark()
    {
        if (XMLManager.Instance.LoadUserHave_RmWaterMark())
        {
            try
            {
                waterMark.gameObject.SetActive(false);
            }
            catch
            {

            }
        }
        else
        {
            try
            {
                waterMark.gameObject.SetActive(true);
            }
            catch
            {

            }
        }
    }
    public void SetAlert(AlertView alert)
    {
        alertView = alert;
        alertPanel = alert.transform.parent.gameObject;
    }
    //캐릭터 하나 이미지 생성
    public void CreateMainCharImg()
    {
        GameObject root = Instantiate(prefabCharImgRoot, transform);
        root.transform.SetAsFirstSibling();
        XMLManager.Instance.SetModifyWEAR(wearSeq.ToString());
        List<CharPart> listParts = XMLManager.Instance.listMakingPARTInit;
        for(int i = 0; i < listParts.Count; i++)
        {
            CreatePartImg(listParts[i], root);
        }
        SetAllSibling(listPartSibling, listPartObj);
    }

    //파트 하나 이미지 생성
    public void CreatePartImg(CharPart element, GameObject root)
    {
        if (element.multiOrder == -1)
        {
            GameObject obj = Instantiate(prefabPartImg, root.transform);
            CharPartUse tmpCharPart = obj.GetComponent<CharPartUse>();

            tmpCharPart.charPart = element;
            tmpCharPart.SetImageSprite();
            tmpCharPart.SetImagePosition();
            int siblingNum = tmpCharPart.SetSortingOrder();

            listPartSibling.Add(siblingNum);
            listPartObj.Add(siblingNum, obj);
        }
        else
        {
            //멀티루트 제작
            GameObject obj;
            int siblingNum = (int)element.charPartItem.order.orderType;
            if (!listPartObj.ContainsKey(siblingNum))
            {
                obj = Instantiate(prefabMultiRootImg, root.transform);
                listPartObj.Add(siblingNum, obj);
            }
            else
            {
                obj = listPartObj[siblingNum];
            }
            SortingGroup sg = obj.GetComponent<SortingGroup>();
            listPartSibling.Add(siblingNum);

            sg.sortingOrder = (int)element.charPartItem.order.orderType;

            //파트이미지 제작

            GameObject part = Instantiate(prefabPartImg, obj.transform);
            CharPartUse tmpCharPart = part.GetComponent<CharPartUse>();

            tmpCharPart.charPart = element;
            tmpCharPart.SetImageSprite();
            tmpCharPart.SetImagePosition();
        }
    }

    //캐릭터 이름짓기
    public void OnNamingButtonClick() {
        alertView.Alert(LocalizationManager.Localize(StringInfo.NameChar), true, XMLManager.Instance.LoadMAIN_WEARname(wearSeq.ToString()),
            new AlertViewOptions
            {
                cancelButtonDelegate = () =>
                {
                },
                okButtonDelegate = () =>
                {
                    Naming();
                }
            });
    }
    //슬롯이름짓기
    public void OnSlotNamingButtonClick()
    {
        if (XMLManager.Instance.isSelectSlot(slotSeq.ToString()))
        {
            alertView.Alert(LocalizationManager.Localize(StringInfo.CreSlot), true, XMLManager.Instance.LoadSlotName(slotSeq.ToString()),
                new AlertViewOptions
                {
                    cancelButtonDelegate = () =>
                    {
                    },
                    okButtonDelegate = () =>
                    {
                        NamingSlot();
                    }
                });
        }
        else
        {
            alertView.Alert(LocalizationManager.Localize(StringInfo.SLEmpty), middleFontsize);
        }
    }

    //슬롯 순서 적기
    public void SetNumText(string text)
    {
        slotNumText.text = text;
    }

    //이름 적기 
    public void SetNameText(string text) {
        nameText.text = text;
    }

    public string GetCharName() {
        if (XMLManager.Instance.IsOneMAINWEARByWearSeq(wearSeq.ToString())){
            friendName = XMLManager.Instance.LoadMAIN_WEARname(wearSeq.ToString());
        }
        else
        {
            friendName = "chibi";
        }
        return friendName;
    }

    public string GetSlotName()
    {
        if (XMLManager.Instance.isSelectSlot(slotSeq.ToString()))
        {
            friendName = XMLManager.Instance.LoadSlotName(slotSeq.ToString());
        }
        else
        {
            int slotSeqPlus = slotSeq + 1;
            stringBuilder.Clear();
            stringBuilder.Append("Slot").Append(slotSeqPlus);
            friendName = stringBuilder.ToString();
        }
        return friendName;
    }

    public void Naming()
    {
        friendName = alertView.inputField.text;
        nameText.text = friendName;
        XMLManager.Instance.SaveMAIN_WEARname(friendName, wearSeq.ToString());
    }

    public void NamingSlot()
    {
        string slotName = alertView.inputField.text;
        nameText.text = slotName;

        XMLManager.Instance.SaveSlotName(slotSeq.ToString(), slotName);
    }

    public void OnRemoveCharButtonClick()
    {
        alertView.Alert(LocalizationManager.Localize(StringInfo.DelChar),
           new AlertViewOptions
           {
               cancelButtonDelegate = () =>
               {
               },
               okButtonDelegate = () =>
               {
                   InMainManager.Instance.ClearOne(wearSeq);
                   XMLManager.Instance.RemoveOneMAIN_GROUP_WEAR(0, wearSeq.ToString());
                   charButtonViewUI.ClearViewContents();
                   charButtonViewUI.CreateCharButton();
               }
           });
    }

    public void OnModifyCharButtonClick()
    {
        alertView.Alert(LocalizationManager.Localize(StringInfo.ModifyChar),
           new AlertViewOptions
           {
               cancelButtonDelegate = () =>
               {
               },
               okButtonDelegate = () =>
               {
                   InGameManager.Instance.inModify = true;
                   InGameManager.Instance.modifyNum = wearSeq;
                   XMLManager.Instance.LoadseqWEARToWEARNegative(wearSeq.ToString(), "-1");
                   SceneManager.LoadScene("MakingScene");
               }
           });
    }

    public void RemoveSlotButtonClick()
    {
        if (XMLManager.Instance.isSelectSlot(slotSeq.ToString()))
        {
            alertView.Alert(LocalizationManager.Localize(StringInfo.DelSlot),
           new AlertViewOptions
           {
               cancelButtonDelegate = () =>
               {
               },
               okButtonDelegate = () =>
               {
                   ClearAllRen();
                   XMLManager.Instance.RemoveOneSlotXML(slotSeq.ToString());
                   GetSlotName();
               }
           });
        }
        else
        {
            alertView.Alert(LocalizationManager.Localize(StringInfo.SLEmpty), middleFontsize);
        }
    }

    //슬롯의 버튼 이미지 렌더링
    public void LoadAllSlotToggleRen(int slotNum)
    {
        //이전오브젝트들 삭제
        ClearAllRen();
        listObjSibling.Clear();

        LoadAllChar(slotNum);
        SetBG(slotNum);
        InitAllObj(slotNum, 2);
        InitAllObj(slotNum, 3);
        InitAllObj(slotNum, 5);
        InitAllObj(slotNum, 6);
        InitAllObj(slotNum, 4);

        SetAllSibling(listObjSibling, listObj);
    }
    //슬롯의 버튼 이미지 삭제
    public void ClearAllRen()
    {
        for(int i = 0; i < contents.transform.childCount; i++)
        {
            if (!contents.transform.GetChild(i).name.Equals("Button"))
            {
                Destroy(contents.transform.GetChild(i).gameObject);
            }
        }
        BGObj.SetActive(false);
        BG.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "save_item_listbox");
    }

    //모든 캐릭터 버튼 내부에 올리기 
    public void LoadAllChar(int slotNum)
    {
        //그룹seq 0 로드
        XMLManager.Instance.LoadAllSLOTGROUPTr(slotNum.ToString(), 0);
        List<CharTr> tmpTr = XMLManager.Instance.listMAIN_ObjTr;

        for (int i = 0; i < tmpTr.Count; i++)
        {
            GameObject root = Instantiate(prefabCharImgRoot, contents.transform);
            root.transform.SetAsFirstSibling();
            //캐릭터 자체의 오더
            listObjSibling.Add(tmpTr[i].Order);
            listObj.Add(tmpTr[i].Order, root);

            //0.6 올라가는 캐릭터와 같은 크기
            // /3 버튼의 비율
            root.transform.localPosition = new Vector3(tmpTr[i].Position.x, tmpTr[i].Position.y, 0) * 100 / 3;
            root.transform.localScale = new Vector3(tmpTr[i].Scale, tmpTr[i].Scale) * 0.6f / 3;

            if (tmpTr[i].IsReversed)
            {
                Quaternion newQuaternion = Quaternion.Euler(new Vector3(0, 180, 0));
                prefabCharImgRoot.transform.rotation = prefabCharImgRoot.transform.rotation * newQuaternion;
            }

            XMLManager.Instance.LoadOneSLOTCharWearXML(slotNum.ToString(), tmpTr[i].UsedSeq.ToString());
            List<CharPart> listParts = XMLManager.Instance.listTmpCharPart;

            listPartSibling.Clear();
            for (int j = 0; j < listParts.Count; j++)
            {
                CreatePartImg(listParts[j], root);
            }
            SetAllSibling(listPartSibling, listPartObj);
        }
    }

    //모든 오브젝트 버튼 내부에 올리기 
    public void InitAllObj(int slotSeq, int groupSeq)
    {
        Dictionary<int, ItemPart> tmpObj = new Dictionary<int, ItemPart>();

        XMLManager.Instance.LoadAllSLOTGROUPTr(slotSeq.ToString(), groupSeq);
        List<CharTr> tmpTr = XMLManager.Instance.listMAIN_ObjTr;

        tmpObj = XMLManager.Instance.listAllObjDic[groupSeq];

        float renderSizeX;
        float renderSizeY;
        float charRenSizeX = Friend.CHAR_REN_SIZE_X;
        float charRenSizeY = Friend.CHAR_REN_SIZE_Y;
        float ratioX;
        float ratioY;

        for (int i = 0; i < tmpTr.Count; i++)
        {
            GameObject root = null;

            switch (groupSeq)
            {
                case 2:
                    root = Instantiate(prefabImgObject, contents.transform);
                    SetObj1Render(tmpObj[int.Parse(tmpTr[i].UsedSeq)].typeSeq, root);
                    break;
                case 3:
                    root = Instantiate(prefabImgObject, contents.transform);
                    SetObj2Render(tmpObj[int.Parse(tmpTr[i].UsedSeq)].typeSeq, root);
                    break;
                case 4:
                    root = Instantiate(prefabImgObject, contents.transform);
                    SetAnimalRender(tmpObj[int.Parse(tmpTr[i].UsedSeq)].typeSeq, root);
                    break;
                case 5:
                    root = Instantiate(prefabImgObject, contents.transform);
                    SetBubbleRender(tmpObj[int.Parse(tmpTr[i].UsedSeq)].typeSeq, root);
                    break;
                case 6:
                    root = Instantiate(prefabImgObject, contents.transform);
                    SetTextRender(tmpObj[int.Parse(tmpTr[i].UsedSeq)].typeSeq, root);
                    break;
            }
            listObjSibling.Add(tmpTr[i].Order);
            listObj.Add(tmpTr[i].Order, root);
            if (tmpTr[i].IsReversed)
            {
                Quaternion newQuaternion = Quaternion.Euler(new Vector3(0, 180, 0));
                root.transform.rotation = root.transform.rotation * newQuaternion;
            }

            renderSizeX = root.GetComponent<Image>().sprite.rect.width;
            renderSizeY = root.GetComponent<Image>().sprite.rect.height;

            ratioX = renderSizeX / (charRenSizeX);
            ratioY = renderSizeY / (charRenSizeY);
            //보정
            float correction = 0.9f;

            // /3 버튼자체의 비율
            root.transform.localPosition = new Vector3(tmpTr[i].Position.x, tmpTr[i].Position.y, 0) * 100 / 3;

            // correction 오차값 눈대중 보정
            // ratio 고정된 캐릭터값에 대한 비율
            // ratioX 가로비율만 맞는 것이 보기에 더 좋아보임
            root.transform.localScale = new Vector3(tmpTr[i].Scale, tmpTr[i].Scale) * ratioX * correction / 3;
        }
    }

    public void SetObj1Render(string imgName, GameObject root)
    {
        Image objSprite = root.GetComponentInChildren<Image>();
        objSprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllObject1Atlas, imgName);
        //objectRenderSizeX = objSprite.sprite.rect.width;
        //objectRenderSizeY = objSprite.sprite.rect.height;
        //GetComponent<BoxCollider2D>().size = new Vector2(objectRenderSizeX / 100, objectRenderSizeY / 100);
    }
    public void SetObj2Render(string imgName, GameObject root)
    {
        Image objSprite = root.GetComponentInChildren<Image>();
        objSprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllObject2Atlas, imgName);
        //objectRenderSizeX = objSprite.sprite.rect.width;
        //objectRenderSizeY = objSprite.sprite.rect.height;
        //GetComponent<BoxCollider2D>().size = new Vector2(objectRenderSizeX / 100, objectRenderSizeY / 100);
    }
    public void SetAnimalRender(string imgName, GameObject root)
    {
        Image animalSprite = root.GetComponentInChildren<Image>();
        animalSprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllAnimalAtlas, imgName);
        //objectRenderSizeX = animalSprite.sprite.rect.width;
        //objectRenderSizeY = animalSprite.sprite.rect.height;
        //GetComponent<BoxCollider2D>().size = new Vector2(objectRenderSizeX / 100, objectRenderSizeY / 100);
    }
    public void SetTextRender(string imgName, GameObject root)
    {
        Image textSprite = root.GetComponentInChildren<Image>();
        textSprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllTextAtlas, imgName.ToString());
    }
    public void SetBubbleRender(string imgName, GameObject root)
    {
        Image bubbleSprite = root.GetComponentInChildren<Image>();
        bubbleSprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllBubbleAtlas, imgName.ToString());
    }

    public void SetBG(int slotNum)
    {
        if (XMLManager.Instance.IsOneSLOTGROUPTrByTrSeq(slotNum.ToString(), 1, 0))
        {
            XMLManager.Instance.LoadAllSLOTGROUPTr(slotNum.ToString(), 1);
            List<CharTr> tmpTr = XMLManager.Instance.listMAIN_ObjTr;
            BGObj.SetActive(true);
            BG.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllBGAtlas, tmpTr[0].UsedSeq.ToString());
        }
    }


    public void SetAllSibling(List<int> partSibling, Dictionary<int, GameObject> objSibling)
    {
        partSibling.Sort();

        for (int i = 0; i < partSibling.Count; i++)
        {
            objSibling[partSibling[i]].transform.SetAsLastSibling();
        }
        partSibling.Clear();
        objSibling.Clear();
    }

}
