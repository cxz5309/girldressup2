﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharPartItemPos
{
    public string itemSeq;
    public int x;
    public int y;
    public string hand;
    public string itemType;

    public CharPartItemPos(string itemSeq, int x, int y, string hand, string itemType)
    {
        this.itemSeq = itemSeq;
        this.x = x;
        this.y = y;
        this.hand = hand;
        this.itemType = itemType;
    }
}
