﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HorizontalViewUI : MonoBehaviour
{
    //캐릭터
    public GameObject prefabCharButton;
    public Transform HorizontalContentView;
    public InputField charBoxNumField;

    public AlertView alertView;

    public int groupSeq;
    public int charBoxCount;

    private void OnEnable()
    {
        //CreateCharButton();
        charBoxCount = InGameManager.Instance.modifyNum / 3;
        SetSlotNumText();
        SetInputField();
    }

    public void ClearViewContents()
    {
        for(int i = 0; i < HorizontalContentView.childCount; i++)
        {
            Destroy(HorizontalContentView.GetChild(i).gameObject);
        }
    }

    public void CreateCharButton()
    {
        //50개 버튼생성 제한 두기
        //List<int> Keys = new List<int>(tmpWear.Keys);
        //for(int i = 0; i < 50; i++)
        //{
        //    int idx = new int();
        //    idx = Keys[i];

        //}
        Dictionary<int, List<CharPart>> tmpWear = new Dictionary<int, List<CharPart>>();

        XMLManager.Instance.LoadAllMAIN_WEAR();
        tmpWear = XMLManager.Instance.listAllMainWEAR;

        for(int i = 0; i < 3; i++)
        {
            int charNum = new int();
            charNum = (charBoxCount * 3) + i;
            //if (!XMLManager.Instance.listAllMainWEAR.ContainsKey(charNum))
            //{
            //    continue;
            //}
            if (charNum == tmpWear.Count - 1)
            {
                break;
            }

            GameObject obj = Instantiate(prefabCharButton, HorizontalContentView);
            CharButtonUI charButtonUI = obj.GetComponent<CharButtonUI>();
            //charButtonUI.goHorizontalViewUI = this;
            charButtonUI.wearSeq = charNum;

            charButtonUI.SetAlert(alertView);
            charButtonUI.SetNumText((charNum + 1).ToString());
            charButtonUI.SetNameText(charButtonUI.GetCharName());
            charButtonUI.CreateMainCharImg();
            obj.GetComponent<Button>().onClick.AddListener(() =>
            {
                InMainManager.Instance.CreateChar(groupSeq, charNum);
            });
        }
    }

    //슬롯이동
    public void OnLeftArrowClick()
    {
        if (charBoxCount > 0)
        {
            ClearViewContents();

            charBoxCount--;
            CreateCharButton();
            SetSlotNumText();
        }
    }
    public void OnRightArrowClick()
    {
        if (charBoxCount < 32)
        {
            ClearViewContents();

            charBoxCount++;
            CreateCharButton();
            SetSlotNumText();
        }
    }


    //슬롯 인풋필드
    public void SetSlotNumText()
    {
        charBoxNumField.text = (charBoxCount + 1).ToString();
    }
    //인풋필드 입력
    public void SetInputField()
    {
        charBoxNumField.onEndEdit.AddListener((string text) =>
        {
            ClearViewContents();

            int textInt = int.Parse(text);
            if (textInt > 33)
            {
                charBoxNumField.text = StringInfo.MaxSlot;
                textInt = 33;
            }
            if(textInt < 1)
            {
                charBoxNumField.text = StringInfo.MinSlot;
                textInt = 1;
            }
            charBoxCount = textInt - 1;
            CreateCharButton();
        });
    }
}
