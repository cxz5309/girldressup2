﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteToggleInButton : MonoBehaviour
{
    private Image[] tmpImages;
    private Image thisImage;
    private string selString;
    private string tmpSpriteName;

    public string lastWord;
    public bool startButton;
    public bool dontSetImage;

    private Sprite nomalSp;
    private Sprite selectedSp;

    public ChildSpriteToggleUI spToggleUI;

    public void OnClickThis()
    {
        //spToggleUI.selBtn = transform;
        //spToggleUI.Refresh();
        if (!dontSetImage)
        {
            spToggleUI.SetImageToggle(transform.GetSiblingIndex());
        }
    }


    //public void SetToolBarImageToggle()
    //{
    //    if (!dontSetImage)
    //    {
    //        thisImage.sprite = selectedSp;
    //        for (int i = 0; i < transform.parent.childCount; i++)
    //        {
    //            GameObject tmpObj = transform.parent.GetChild(i).gameObject;

    //            if (tmpObj == gameObject)
    //            {
    //                continue;
    //            }
    //            tmpImages[i] = tmpObj.GetComponent<Image>();

    //            tmpImages[i].sprite = nomalSp;
    //        }
    //    }
    //}
    //public void SetToolBarImageToggle()
    //{
    //    if (!dontSetImage)
    //    {
    //        if (lastWord == "n")
    //        {
    //            selString = (thisImage.sprite.name).Replace("(Clone)", "");
    //            if (selString.Substring(selString.Length - 1) == "n")
    //            {
    //                tmpSpriteName = MakeAtlasStringUtil.setButtonStringToggleNP(selString);
    //                thisImage.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, tmpSpriteName);
    //            }
    //            for (int i = 0; i < transform.parent.childCount; i++)
    //            {
    //                GameObject tmpObj = transform.parent.GetChild(i).gameObject;
    //                if (tmpObj == gameObject)
    //                {
    //                    continue;
    //                }
    //                tmpImages[i] = tmpObj.GetComponent<Image>();

    //                string sourceImageName = tmpImages[i].sprite.name.Replace("(Clone)", "");
    //                if (sourceImageName.Substring(sourceImageName.Length - 1) == "p")
    //                {
    //                    string sourceImageNameToggle = MakeAtlasStringUtil.setButtonStringToggleNP(sourceImageName);

    //                    tmpImages[i].sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, sourceImageNameToggle);
    //                }
    //            }
    //        }
    //        else if (lastWord == "off")
    //        {
    //            tmpSpriteName = MakeAtlasStringUtil.setButtonStringToggleOnOff((thisImage.sprite.name).Replace("(Clone)", ""));

    //            thisImage.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, tmpSpriteName);

    //            for (int i = 0; i < transform.parent.childCount; i++)
    //            {
    //                GameObject tmpObj = transform.parent.GetChild(i).gameObject;
    //                if (tmpObj == gameObject)
    //                {
    //                    continue;
    //                }
    //                tmpImages[i] = tmpObj.GetComponent<Image>();

    //                string sourceImageName = tmpImages[i].sprite.name.Replace("(Clone)", "");
    //                if (sourceImageName.Substring(sourceImageName.Length - 2) == "on")
    //                {
    //                    string sourceImageNameToggle = MakeAtlasStringUtil.setButtonStringToggleOnOff(sourceImageName);

    //                    tmpImages[i].sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, sourceImageNameToggle);
    //                }
    //            }
    //        }
    //    }
    //}
}
