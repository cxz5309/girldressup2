﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemTypeButtonUI : MonoBehaviour
{
    public Image MaskImage;

    public GameObject itemTypeButtonBackUI;
    public Image buttonBackImage;

    public GameObject itemTypeButtonUI;
    public Image buttonImage;

    public GameObject itemLock;
    public Image itemLockImage;

    public void ChangeButtonImage(Sprite sprite)
    {
        buttonImage.sprite = sprite;
    }
    public void ChangeButtonColor(Color color)
    {
        buttonImage.color = color;
    }
    public void ChangeButtonSize(float n)
    {
        buttonImage.GetComponent<RectTransform>().sizeDelta = new Vector2(n, n);
    }

    public void ChangeButtonBackUIColor(Color color)
    {
        buttonBackImage.color = color;
    }

    public void ChangeButtonBackAlpha(float alpha)
    {
        buttonBackImage.color = new Color(buttonBackImage.color.r, buttonBackImage.color.g, buttonBackImage.color.b, alpha);
    }
    public void ChangeButtonAlpha(float alpha)
    {
        buttonImage.color = new Color(buttonImage.color.r, buttonImage.color.g, buttonImage.color.b, alpha);
    }

    public void ChangeButtonBackImage(Sprite sprite)
    {
        buttonBackImage.sprite = sprite;
    }
    public void UnsetButtonImage()
    {
        itemTypeButtonUI.SetActive(false);
    }
    public void SetButtonImage()
    {
        itemTypeButtonUI.SetActive(true);
    }
    public void UnsetbuttonBackImage()
    {
        itemTypeButtonBackUI.SetActive(false);
    }
    public void SetbuttonBackImage()
    {
        itemTypeButtonBackUI.SetActive(true);
    }


    public void ChangeItemTypeImage(ItemType type)
    {
        switch (type)
        {
            case ItemType.nomalItem:
                ChangeIsStore(false);
                break;

            case ItemType.spItem:
                ChangeIsStore(false);
                break;

            case ItemType.storeItem:
                ChangeIsStore(true);
                break;
        }
    }

    public void ChangeIsStore(bool isLock)
    {
        if (isLock)
        {
            itemLock.SetActive(true);
            itemLockImage.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "store_icon");
        }
        else
        {
            itemLock.SetActive(false);
        }
    }

    public void ChangeStoreColor(bool isLock)
    {
        if (isLock)
        {
            itemLockImage.color = ObjectInfo.seemsRed;
        }
        else
        {
            itemLockImage.color = ObjectInfo.seemsWhite;
        }
    }
}
