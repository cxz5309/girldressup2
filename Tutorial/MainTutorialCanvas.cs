﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainTutorialCanvas : MonoBehaviour
{
    public MainCanvas mainCanvas;
    public GameObject[] popUps;
    private int popUpIndex = 0;
    //public GameObject spawner;
    //public float waitTime = 2f;
    public Button[] buttons;

    public Transform bottomContents;
    public Transform slContents;

    public Transform charContents1;
    public Transform slContents4;
    public Transform AlertView7;

    private GameObject toggleObj;

    // 배너 광고로 위치 변경할 오브젝트 리스트
    public RectTransform[] bannerChangeTransforms;

    private void Start()
    {
        // 배너 광고 초기화
        AdsManager.OnLoadedBanner += OnLoadedBanner;
        AdsManager.Instance.RequestBanner();

        StartCoroutine(coWaitStart());
    }

    private void OnDestroy()
    {
        AdsManager.OnLoadedBanner -= OnLoadedBanner;
    }

    #region Actions-Events
    public void OnLoadedBanner(float bannerHeight)
    {
        if (bannerHeight <= 0)
        {
            bannerHeight = 0f;
        }

        for (int i = 0; i < bannerChangeTransforms.Length; i++)
        {
            Vector2 position = bannerChangeTransforms[i].offsetMax;
            position.y = -bannerHeight;
            bannerChangeTransforms[i].offsetMax = position;
        }
    }
    #endregion

    IEnumerator coWaitStart()
    {
        yield return null;

        transform.SetAsLastSibling();
        StartPopup();
        StartCoroutine(StopSecond(0.01f));
    }

    IEnumerator StopSecond(float a)
    {
        yield return new WaitForSeconds(a);
        PopupWithFunc();
    }

    public void PopupWithFunc()
    {
        switch (popUpIndex)
        {
            default:
                break;
            case 0:
                GameObject obj = Instantiate(bottomContents.GetChild(1).gameObject, charContents1);
                break;
            case 1:
                bottomContents.GetChild(1).GetComponent<Button>().onClick.Invoke();
                buttons[0].GetComponent<Image>().color = new Color(1, 1, 1, 0);
                break;
            case 2:
                buttons[0].onClick.Invoke();
                buttons[0].GetComponent<Image>().color = new Color(1, 1, 1, 1);
                toggleObj = Instantiate(slContents.GetChild(0).gameObject, slContents4);
                GameObject obj1 = Instantiate(slContents.GetChild(0).gameObject, slContents4);
                for(int i = 0; i < obj1.transform.childCount; i++)
                {
                    Destroy(obj1.transform.GetChild(i).gameObject);
                }
                GameObject obj2 = Instantiate(slContents.GetChild(0).gameObject, slContents4);
                for (int i = 0; i < obj2.transform.childCount; i++)
                {
                    Destroy(obj2.transform.GetChild(i).gameObject);
                }
                break;
            case 3:
                slContents.GetChild(0).GetComponent<Toggle>().onValueChanged.Invoke(true);
                toggleObj.GetComponent<Toggle>().onValueChanged.Invoke(true);
                break;
            case 4:
                buttons[1].onClick.Invoke();
                break;
            case 5:
                buttons[2].onClick.Invoke();
                break;
            case 6:
                buttons[3].onClick.Invoke();
                DoTweenUtil.DoPopupOpen(.2f, 1f, .4f, AlertView7);
                slContents.GetChild(0).GetComponent<Toggle>().onValueChanged.Invoke(false);
                break;
        }
        Popup();
    }


    public void StartPopup()
    {
        popUpIndex = 0;

        for (int i = 0; i < popUps.Length; i++)
        {
            if (i == popUpIndex)
            {
                popUps[i].SetActive(true);
            }
            else
            {
                popUps[i].SetActive(false);
            }
        }
    }


    public void EndPopup()
    {
        InGameManager.Instance.clearTutorial = true;
        
        mainCanvas.StartCoSetDaily();

        StartCoroutine(coDestroy());
    }


    IEnumerator coDestroy()
    {
        yield return new WaitForSeconds(5f);
        Destroy(this.gameObject);
    }

    public void Popup()
    {
        popUpIndex++;

        for (int i = 0; i < popUps.Length; i++)
        {
            if (i == popUpIndex)
            {
                popUps[i].SetActive(true);
            }
            else
            {
                popUps[i].SetActive(false);
            }
        }
        if(popUpIndex == popUps.Length)
        {
            EndPopup();
        }
    }

    public void PopupBack()
    {
        popUpIndex--;

        for (int i = 0; i < popUps.Length; i++)
        {
            if (i == popUpIndex)
            {
                popUps[i].SetActive(true);
            }
            else
            {
                popUps[i].SetActive(false);
            }
        }

    }

    private void OnDisable()
    {
        Destroy(gameObject);
    }
}
