﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TutorialCanvas : MonoBehaviour
{
    public MakingCanvas makingCanvas;
    public GameObject[] popUps;
    private int popUpIndex = 0;
    //public GameObject spawner;
    //public float waitTime = 2f;
    public Button[] buttons;

    public Transform tutoContents6;
    public Transform tutoContents15;

    public Transform makSelContents;
    public Transform buttomContents;

    public ScrollRect scrollRect6;
    public ScrollRect scrollRect15;

    // 배너 광고로 위치 변경할 오브젝트 리스트
    public RectTransform[] bannerChangeTransforms;

    private void Start()
    {
        // 배너 광고 초기화
        AdsManager.OnLoadedBanner += OnLoadedBanner;
        AdsManager.Instance.RequestBanner();

        StartCoroutine(CoWaitStart());
    }

    private void OnDestroy()
    {
        AdsManager.OnLoadedBanner -= OnLoadedBanner;
    }

    #region Actions-Events
    public void OnLoadedBanner(float bannerHeight)
    {
        if (bannerHeight <= 0)
        {
            bannerHeight = 0f;
        }

        for (int i = 0; i < bannerChangeTransforms.Length; i++)
        {
            Vector2 position = bannerChangeTransforms[i].offsetMax;
            position.y = -bannerHeight;
            bannerChangeTransforms[i].offsetMax = position;
        }
    }
    #endregion

    IEnumerator CoWaitStart()
    {
        yield return null;

        transform.SetAsLastSibling();
        StartPopup();
        StartCoroutine(StopSecond(.01f));
    }

    IEnumerator StopSecond(float a)
    {
        yield return new WaitForSeconds(a);
        PopupWithFunc();
    }

    public void PopupWithFunc()
    {
        switch (popUpIndex)
        {
            default:
                break;
            case 0:
                buttons[0].onClick.Invoke();
                break;
            case 1:
                buttomContents.Find("021000").GetComponent<Button>().onClick.Invoke();
                buttons[1].onClick.Invoke();
                break;
            case 2:
                buttomContents.Find("018000").GetComponent<Button>().onClick.Invoke();
                buttons[2].onClick.Invoke();
                break;
            case 3:
                buttomContents.Find("004000").GetComponent<Button>().onClick.Invoke();
                //startMove = false;
                makSelContents.localPosition = tutoContents6.localPosition = new Vector3(-862 - 540, -62.6f, 0);
                scrollRect6.content = null;
                //makSelContents.position = tutoContents6.position;
                buttons[3].onClick.Invoke();
                break;
            case 4:
                buttomContents.Find("013000").GetComponent<Button>().onClick.Invoke();
                buttons[4].onClick.Invoke();
                break;
            case 5:
                buttomContents.Find("014000").GetComponent<Button>().onClick.Invoke();
                buttons[5].onClick.Invoke();
                break;
            case 6:
                buttomContents.Find("015001").GetComponent<Button>().onClick.Invoke();
                buttons[6].onClick.Invoke();
                break;
            case 7:
                buttomContents.Find("016099").GetComponent<Button>().onClick.Invoke();

                //startMove = false;
                makSelContents.localPosition = tutoContents15.localPosition = new Vector3(-2409 - 540, -62.6f, 0);
                scrollRect15.content = null;
                //makSelContents.position = tutoContents15.position;
                buttons[7].onClick.Invoke();
                break;
            case 8:
                buttomContents.Find("035000").GetComponent<Button>().onClick.Invoke();
                buttons[8].onClick.Invoke();
                break;
            case 9:
                buttomContents.Find("040100").GetComponent<Button>().onClick.Invoke();
                buttons[9].GetComponent<Image>().color = new Color(0, 0, 0, 0);
                break;
            case 10:
                buttons[9].onClick.Invoke();
                buttons[9].GetComponent<Image>().color = new Color(1, 1, 1, 1);
                break;
            case 11:
                // false로 변경 시 튜토리얼 종료 후 true 상태 체크 불가능
                //InMakingManager.Instance.isTutorial = false;
                buttons[10].onClick.Invoke();
                break;
        }
        Popup();
    }


    public void StartPopup()
    {
        InMakingManager.Instance.isTutorial = true;
        popUpIndex = 0;

        for (int i = 0; i < popUps.Length; i++)
        {
            if (i == popUpIndex)
            {
                popUps[i].SetActive(true);
            }
            else
            {
                popUps[i].SetActive(false);
            }
        }
    }

    public void Popup()
    {
        popUpIndex++;

        for (int i = 0; i < popUps.Length; i++)
        {
            if (i == popUpIndex)
            {
                popUps[i].SetActive(true);
            }
            else
            {
                popUps[i].SetActive(false);
            }
        }

    }

    public void PopupBack()
    {
        popUpIndex--;

        for (int i = 0; i < popUps.Length; i++)
        {
            if (i == popUpIndex)
            {
                popUps[i].SetActive(true);
            }
            else
            {
                popUps[i].SetActive(false);
            }
        }
    }

    private void OnDisable()
    {
        Destroy(gameObject);
    }
}
