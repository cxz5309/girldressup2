﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorView : MonoBehaviour
{
    public MakingCanvas goMakingCanvas;
    public GameObject goColorButtonView;
    public GameObject goColorPicker;
    public GameObject button;

    public bool isColorPicker;
    public bool startColorPicker;
    private void OnEnable()
    {
        if (!startColorPicker)
        {
            ButtonInit();
        }
        else
        {
            ColorInit();
        }
    }

    public void ButtonInit()
    {
        isColorPicker = false;

        goMakingCanvas.StopPicker();
        goColorButtonView.SetActive(true);
        goColorPicker.SetActive(false);
        SetButtonString();
    }
    public void ColorInit()
    {
        isColorPicker = true;

        goMakingCanvas.StartPicker();

        goColorButtonView.SetActive(false);
        goColorPicker.SetActive(true);
        SetButtonString();
    }

    public void SetButtonString()
    {
        if (isColorPicker)
        {
            //button.transform.Find("Text").GetComponent<Text>().text = "칼라 버튼";
            button.GetComponent<Image>().sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_color02_n");
            SpriteState spriteState = new SpriteState();
            spriteState.pressedSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_color02_p");
            button.GetComponent<Button>().spriteState = spriteState;
        }
        else
        {
            //button.transform.Find("Text").GetComponent<Text>().text = "칼라 피커";
            button.GetComponent<Image>().sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_color01_n");
            SpriteState spriteState = new SpriteState();
            spriteState.pressedSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_color01_p");
            button.GetComponent<Button>().spriteState = spriteState;
        }
    }

   
    // 알림창을 닫는 메소드
    public void Dismiss()
    {
        startColorPicker = false;
        goMakingCanvas.UnSetPaintCharPosition();

        goMakingCanvas.StopPaint();
        gameObject.SetActive(false);
    }

    public void OnColorButtonClick()
    {
        goMakingCanvas.SetColorByButton();
    }

    public void OnColorPickerButtonClick()
    {
        goMakingCanvas.SetColorPickerToggle(isColorPicker);
        isColorPicker = !isColorPicker;
        SetButtonString();

        startColorPicker = !startColorPicker;
    }
}
