﻿using System.Collections;
using System.Collections.Generic;
using Assets.SimpleLocalization;
using UnityEngine;
using UnityEngine.UI;

public class RGBPointerView : MonoBehaviour
{
    public MakingCanvas goMakingCanvas;

    //public InputField[] rgbInputField = new InputField[3];
    //public Text[] rgbPointText = new Text[3];

    public InputField colorCodeInputField;
    public Text colorCodePointText;

    public Button ChangeColorButton;

    Color setColor = new Color();

    //public float[] rgbValue = new float[3];
    public string colorCode;

    private void Update()
    {
        if (goMakingCanvas.isPaint)
        {
            SetText();
        }
        
    }

    public void SetText()
    {
        if (goMakingCanvas.ren != null)
        {
            colorCodeInputField.text = colorCodeInputField.text.Replace("#", "");
            colorCodeInputField.text = "#" + ColorCodeUtil.ColorToHtmlString(goMakingCanvas.ren.color);
        }
    }
    public void SetTextNull()
    {
        if (goMakingCanvas.ren != null)
        {
            colorCodeInputField.text = "";
        }
    }

    public void OnChangeColorButtonClick()
    {
        if (goMakingCanvas.ren != null)
        {
            setColor = ColorCodeUtil.htmlStringToColorWithSharf(colorCode);
            goMakingCanvas.SetColor(setColor);
        }
    }

    public void CopyColorCode()
    {
        goMakingCanvas.alertView.Alert(LocalizationManager.Localize(StringInfo.CVCopy));
        UniClipboard.SetText(colorCodeInputField.text);
    }

    public void PasteColorCode()
    {
        goMakingCanvas.alertView.Alert(LocalizationManager.Localize(StringInfo.CVPaste));

        colorCodeInputField.text = UniClipboard.GetText();
        setColor = ColorCodeUtil.htmlStringToColorWithSharf(colorCodeInputField.text);
        goMakingCanvas.SetColor(setColor);
    }
}
