﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldUI : MonoBehaviour
{
    private TouchScreenKeyboard mobileKeys;
    public InputField inputField;
    public RGBPointerView rgbPointerView;

    public void OnEndEditCodeVal()
    {
        mobileKeys = inputField.touchScreenKeyboard;
        if (Application.platform == RuntimePlatform.Android)
        {
            if (mobileKeys.status == TouchScreenKeyboard.Status.Done)
            {
                rgbPointerView.colorCode = SetText(mobileKeys.text);
                rgbPointerView.OnChangeColorButtonClick();
            }
        }
    }

    public string SetText(string text)
    {
        string tmpTxt;
        tmpTxt = text.Replace("#", "");
        tmpTxt = tmpTxt.Replace(" ", "");
        if(text.Length > 6)
        {
            tmpTxt = tmpTxt.Substring(0, 6);
        }
        return "#" + tmpTxt + "FF";
    }
}
