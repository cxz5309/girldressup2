﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class ItemGetPopupUI : MonoBehaviour
{
    public GameObject textObj;
    public Text text;
    public GameObject mainImageObj;
    public Image mainImage;
    public Text buttonText;
    public GameObject CharRootObj;

    public Action startAction = null;
    public Action onClickAction = null;

    public GameObject prefabCharImgRoot;
    public GameObject prefabPartImg;
    public GameObject prefabMultiRootImg;
    public Dictionary<int, GameObject> listPartObj = new Dictionary<int, GameObject>();

    public List<int> listPartSibling;
    public List<int> listObjSibling;

    public Transform coinStartPos;

    private void Awake()
    {
        buttonText.text = LocalizationManager.Localize(StringInfo.Check);    
    }

    private void OnEnable()
    {
        if (startAction != null)
        {
            startAction.Invoke();
        }
        DoTweenUtil.DoPopupOpen(.2f, 2f, .4f, mainImageObj.transform);
    }

    public void SetStartAction(Action newAction)
    {
        startAction = newAction;
    }

    public void SetOnClickAction(Action newAction, bool effect = true)
    {
        onClickAction = newAction;
        if (effect)
        {
            AudioManager.Inst.StopAllSFX();
            AudioManager.Inst.PlaySFX("Item_get");
            InGameManager.Instance.StartEffect(0, new Vector3(0, 0, 0), 3f);
        }
    }

    public void SetButtonText(string textSt)
    {
        buttonText.text = textSt;
    }

    public void SetText(string textSt, int fontSize)
    {
        textObj.SetActive(true);
        text.text = textSt;
        text.fontSize = fontSize;
    }

    public void SetText(string textSt)
    {
        textObj.SetActive(true);
        text.text = textSt;
    }

    public void SetImage(Sprite sprite)
    {
        CharRootObj.SetActive(false);
        mainImageObj.SetActive(true);
        mainImage.sprite = sprite;
    }

    public void OnClickAny()
    {
        if (!DoTweenUtil.isTweening)
        {
            if (onClickAction != null)
            {
                onClickAction.Invoke();
            }
            DismissItemGetPopup();
        }
    }

    public void DismissItemGetPopup()
    {
        gameObject.SetActive(false);
    }

    public void SetChar(List<UserHave> itemSeqs)
    {
        CharRootObj.SetActive(true);
        mainImageObj.SetActive(false);
        CreateCharImg(itemSeqs);
    }


    //캐릭터 하나 이미지 생성
    public void CreateCharImg(List<UserHave> itemSeqs)
    {
        for (int i = 0; i < CharRootObj.transform.childCount; i++)
        {
            Destroy(CharRootObj.transform.GetChild(i).gameObject);
        }
        GameObject root = Instantiate(prefabCharImgRoot, CharRootObj.transform);
        root.transform.localScale = new Vector3(0.6f, 0.6f, 1);
        root.transform.SetAsFirstSibling();
        //XMLManager.Instance.use(wearSeq.ToString());
        CharPart tmpPart;
        for (int i = 0; i < itemSeqs.Count; i++)
        {
            tmpPart = XMLManager.Instance.UsePartByItemSeq(itemSeqs[i].itemSeq);
            CreatePartImg(tmpPart, root);
        }
        tmpPart = XMLManager.Instance.UsePartByItemSeq("007000");
        CreatePartImg(tmpPart, root);
        tmpPart = XMLManager.Instance.UsePartByItemSeq("009000");
        CreatePartImg(tmpPart, root);

        SetAllSibling(listPartSibling, listPartObj);
    }

    //파트 하나 이미지 생성
    public void CreatePartImg(CharPart element, GameObject root)
    {
        if (element.multiOrder == -1)
        {
            GameObject obj = Instantiate(prefabPartImg, root.transform);
            CharPartUse tmpCharPart = obj.GetComponent<CharPartUse>();

            tmpCharPart.charPart = element;
            tmpCharPart.SetImageSprite();
            tmpCharPart.SetImagePosition();
            int siblingNum = tmpCharPart.SetSortingOrder();

            listPartSibling.Add(siblingNum);
            listPartObj.Add(siblingNum, obj);
        }
        else
        {
            //멀티루트 제작
            GameObject obj;
            int siblingNum = (int)element.charPartItem.order.orderType;
            if (!listPartObj.ContainsKey(siblingNum))
            {
                obj = Instantiate(prefabMultiRootImg, root.transform);
                listPartObj.Add(siblingNum, obj);
            }
            else
            {
                obj = listPartObj[siblingNum];
            }
            SortingGroup sg = obj.GetComponent<SortingGroup>();
            listPartSibling.Add(siblingNum);

            sg.sortingOrder = (int)element.charPartItem.order.orderType;

            //파트이미지 제작

            GameObject part = Instantiate(prefabPartImg, obj.transform);
            CharPartUse tmpCharPart = part.GetComponent<CharPartUse>();

            tmpCharPart.charPart = element;
            tmpCharPart.SetImageSprite();
            tmpCharPart.SetImagePosition();
        }
    }
    //레이어
    public void SetAllSibling(List<int> partSibling, Dictionary<int, GameObject> objSibling)
    {
        partSibling.Sort();

        for (int i = 0; i < partSibling.Count; i++)
        {
            objSibling[partSibling[i]].transform.SetAsLastSibling();
        }
        partSibling.Clear();
        objSibling.Clear();
    }

}
