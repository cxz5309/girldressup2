﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SAlertViewUI : MonoBehaviour
{
    public Text thisText;

    public void SAlert(string text)
    {
        gameObject.SetActive(true);
        StartCoroutine(DoTweenUtil.DoFade(.6f, .2f, gameObject));
        StartCoroutine(DoTweenUtil.DoTextFade(1f, .2f, thisText.gameObject));
        thisText.text = text;
    }
}
