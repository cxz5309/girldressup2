﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class AlertViewOptions
{
    // 취소 버튼 눌렀을 때 실행되는 대리자
    public System.Action cancelButtonDelegate;
    // OK 버튼을 눌렀을 때 실행되는 대리자
    public System.Action okButtonDelegate;
    // Dismiss일 때 실행되는 대리자
    public System.Action dismissDelegate;
}

public class AlertView : MonoBehaviour
{
    public GameObject goPanel;

    public Transform nomalTextPosition;
    public Transform okTextPosition;
    public Transform inputTextPosition;

    public Text thisText;
    public InputField inputField;
    public Button okButton;
    public Text okButtonText;
    public Button noButton;
    public Text noButtonText;
    public Text chkButtonText;

    public GameObject checkObj;
    public GameObject oknoObj;

    const int SFontSize = 44;
    const int MFontSize = 48;
    const int LFontSize = 52;


    // 취소 버튼 눌렀을 때 실행되는 대리자
    private System.Action cancelButtonDelegate;
    // OK 버튼을 눌렀을 때 실행되는 대리자
    private System.Action okButtonDelegate;
    // Dismiss일 때 실행되는 대리자
    public System.Action dismissDelegate;

    private void OnEnable()
    {
        DoTweenUtil.DoPopupOpen(.4f, 1f, .3f, transform);
    }

    public void SetOkNoButtonText(string okText, string noText)
    {
        okButtonText.text = okText;
        noButtonText.text = noText;
    }

    public void SetCheckButtonText(string chkText)
    {
        chkButtonText.text = chkText;
    }

    public void Alert(string text)
    {
        goPanel.SetActive(true);
        thisText.text = text;
        thisText.fontSize = LFontSize;

        thisText.transform.position = nomalTextPosition.position;

        inputField.gameObject.SetActive(false);
        okButton.gameObject.SetActive(false);
        noButton.gameObject.SetActive(false);
        checkObj.SetActive(false);
        oknoObj.SetActive(true);
    }
    public void Alert(string text, int fontSize)
    {
        goPanel.SetActive(true);
        thisText.text = text;
        thisText.fontSize = fontSize;

        thisText.transform.position = nomalTextPosition.position;

        inputField.gameObject.SetActive(false);
        okButton.gameObject.SetActive(false);
        noButton.gameObject.SetActive(false);
        checkObj.SetActive(false);
        oknoObj.SetActive(true);
    }
    public void Alert(string text, AlertViewOptions options = null)
    {
        goPanel.SetActive(true);
        thisText.text = text;
        thisText.fontSize = MFontSize;

        if (options != null)
        {
            okButtonDelegate = options.okButtonDelegate;
            cancelButtonDelegate = options.cancelButtonDelegate;
            dismissDelegate = options.dismissDelegate;
        }
        else
        {
        }

        thisText.transform.position = okTextPosition.position;

        inputField.gameObject.SetActive(false);
        okButton.gameObject.SetActive(true);
        noButton.gameObject.SetActive(true);
        checkObj.SetActive(false);
        oknoObj.SetActive(true);
    }
    public void Alert(string text, int fontSize, AlertViewOptions options = null)
    {
        goPanel.SetActive(true);
        thisText.text = text;
        thisText.fontSize = fontSize;

        if (options != null)
        {
            okButtonDelegate = options.okButtonDelegate;
            cancelButtonDelegate = options.cancelButtonDelegate;
            dismissDelegate = options.dismissDelegate;
        }
        else
        {
        }

        thisText.transform.position = okTextPosition.position;

        inputField.gameObject.SetActive(false);
        okButton.gameObject.SetActive(true);
        noButton.gameObject.SetActive(true);
        checkObj.SetActive(false);
        oknoObj.SetActive(true);
    }
    public void Alert(string text, bool input, string inputText, AlertViewOptions options = null)
    {
        goPanel.SetActive(true);
        thisText.text = text;
        thisText.fontSize = SFontSize;

        if (options != null)
        {
            okButtonDelegate = options.okButtonDelegate;
            cancelButtonDelegate = options.cancelButtonDelegate;
            dismissDelegate = options.dismissDelegate;
        }
        if (input)
        {
            thisText.transform.position = inputTextPosition.position;

            inputField.gameObject.SetActive(true);
            inputField.text = inputText;
            okButton.gameObject.SetActive(true);
            noButton.gameObject.SetActive(true);
            checkObj.SetActive(false);
            oknoObj.SetActive(true);
        }
    }

    public void CheckAlert(string text, bool input, string inputText, AlertViewOptions options = null)
    {
        goPanel.SetActive(true);
        thisText.text = text;
        thisText.fontSize = SFontSize;

        if (options != null)
        {
            okButtonDelegate = options.okButtonDelegate;
            cancelButtonDelegate = options.cancelButtonDelegate;
            dismissDelegate = options.dismissDelegate;
        }
        if (input)
        {
            thisText.transform.position = inputTextPosition.position;

            inputField.gameObject.SetActive(true);
            inputField.text = inputText;
            checkObj.SetActive(true);
            oknoObj.SetActive(false);
        }
    }
    public void CheckAlert(string text, AlertViewOptions options = null)
    {
        goPanel.SetActive(true);
        thisText.text = text;
        thisText.fontSize = MFontSize;

        if (options != null)
        {
            okButtonDelegate = options.okButtonDelegate;
            cancelButtonDelegate = options.cancelButtonDelegate;
            dismissDelegate = options.dismissDelegate;
        }
        thisText.transform.position = okTextPosition.position;

        inputField.gameObject.SetActive(false);
        checkObj.SetActive(true);
        oknoObj.SetActive(false);
    }

    public void OnPressCancelButton()
    {
        if (!DoTweenUtil.isTweening)
        {
            if (cancelButtonDelegate != null)
            {
                cancelButtonDelegate.Invoke();
            }
            OnDismissButtonClick();
        }
    }

    // OK 버튼을 눌렀을 때 호출되는 메소드
    public void OnPressOKButton()
    {
        if (!DoTweenUtil.isTweening)
        {
            if (okButtonDelegate != null)
            {
                okButtonDelegate.Invoke();
            }
            OnDismissButtonClick();
        }
    }

    public void OnDismissButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            if (dismissDelegate != null)
            {
                dismissDelegate.Invoke();
            }
            goPanel.SetActive(false);
        }
    }

    public void OnCheckButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            if (okButtonDelegate != null)
            {
                okButtonDelegate.Invoke();
            }
        }
    }

}
