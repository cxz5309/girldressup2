﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroManager : MonoBehaviour
{
    public static IntroManager Instance;

    public IntroCanvas introCanvas;
    public AlertView alertView;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void WaitAlert(string text)
    {
        StartCoroutine(CoWaitAlert(text));
    }

    IEnumerator CoWaitAlert(string text)
    {
        yield return new WaitForSeconds(0.1f);
        alertView.Alert(text);
    }
}
