﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSFX : MonoBehaviour
{
    public string sfxName;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(StartSound);
    }

    void StartSound()
    {
        AudioManager.Inst.PlaySFX(sfxName);
    }
}
