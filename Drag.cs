﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Drag : MonoBehaviour
{
    public Friend friend;

    public GameObject target;

    Vector3 startPosition;
    float startX;
    float startY;
    float x;
    float y;
    float Threshold = 0.1f;
    bool isDontMove = false;
    public bool isMoving;
    public bool isHolding = false;

    public TMPObject tmpObj;

    public delegate void OnStartMove(Friend friend, float touchX, float touchY);
    public OnStartMove onStartMove;
    public delegate void OnMove(Friend friend, float touchX, float touchY, float moveX, float moveY);
    public OnMove onMove;
    public delegate void OnEndMove(Friend friend, float touchX, float touchY);
    public OnEndMove onEndMove;
    public UnityEvent onEventStartMove;
    //public UnityEvent onRemoveCheck;
    public UnityEvent onEventMove;
    public UnityEvent onEventEndMove;

    public enum TouchCase { notTouch, firstTouch, secondTouch };
    public TouchCase touchCase;

    public enum TouchActing { noActing, isMoving, isScaleChanging, isReversing, isSorting, isClearing }
    public TouchActing touchActing;

    public void OnSetMouseDrag(float moveX, float moveY)
    {
        if (isMoving)
        {
            return;
        }

        transform.position = new Vector3(transform.position.x + moveX, transform.position.y + moveY, transform.position.z);
        friend.positionX = transform.localPosition.x;
        friend.positionY = transform.localPosition.y;
    }

    void OnMouseDown()
    {
        isDontMove = false;
        isMoving = false;
        isHolding = true;

        if (InMainManager.Instance.canvasState == InMainManager.CanvasState.slCanvas||InMainManager.Instance.canvasState == InMainManager.CanvasState.captureCanvas)
        {
            isDontMove = true;
            return;
        }

        //if (EventSystem.current.IsPointerOverGameObject())
        //{
        //    isDontMove = true;
        //    return;
        //}

        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if (Input.GetMouseButtonDown(0))
        {
            if (results.Count > 0)
            {
                isDontMove = true;
                return;
            }
        }

        if (InMainManager.Instance.nowFriend != null)
        {
            if (InMainManager.Instance.nowFriend != friend)
            {
                InMainManager.Instance.nowFriend.goTools.SetActive(false);
                InMainManager.Instance.nowFriend = friend;
            }
        }
        else
        {
            InMainManager.Instance.nowFriend = friend;
        }
        if (friend.friendType == FriendType.Bubble)
        {
            tmpObj.cantOpen = false;
        }
        friend.goTools.SetActive(true);


        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z);
        startPosition = InMainManager.Instance.mainCamera.ScreenToWorldPoint(mousePosition);

        //if (friend.friendType == FriendType.Bubble)
        //{
        //    OnMouseHoldStart();
        //}
        if (onStartMove != null)
        {
            onStartMove.Invoke(friend, x, y);
        }
        if (onEventStartMove != null)
        {
            onEventStartMove.Invoke();
        }
    }


    void OnMouseDrag()
    {
        if (isDontMove)
        {
            return;
        }

        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z);
        Vector3 position = InMainManager.Instance.mainCamera.ScreenToWorldPoint(mousePosition);
        
        if (!isMoving)
        {
            var movePosition = position - startPosition;
            if (Mathf.Abs(movePosition.x) < Threshold &&
                Mathf.Abs(movePosition.y) < Threshold)
            {
                isMoving = false;
                return;
            }
            else
            {
                isMoving = true;
                x = position.x;
                y = position.y;
                if (friend.friendType == FriendType.Bubble)
                {
                    tmpObj.cantOpen = true;
                }
                InMainManager.Instance.positionChanged = true;
            }
        }
        float moveX = position.x - x;
        float moveY = position.y - y;
        friend.goFriendRoot.transform.position = new Vector3(friend.goFriendRoot.transform.position.x + moveX, friend.goFriendRoot.transform.position.y + moveY, friend.goFriendRoot.transform.position.z);
        x = position.x;
        y = position.y;
        // 위치값 업데이트
        friend.positionX = friend.goFriendRoot.transform.localPosition.x;
        friend.positionY = friend.goFriendRoot.transform.localPosition.y;

        SetMove(moveX, moveY);
    }

    void OnMouseUp()
    {
        EndMove();
        isMoving = false;
        isHolding = false;
        friend.SetTrValueXml("position_x", friend.positionX.ToString());
        friend.SetTrValueXml("position_y", friend.positionY.ToString());
    }

    public void OnMouseHoldStart() {
        StartCoroutine(coMouseHold());
    }

    IEnumerator coMouseHold() {
        float time = 0;
        while (time < .3f)
        {
            yield return new WaitForFixedUpdate();
            time += Time.fixedDeltaTime;

            if (isMoving || !isHolding)
            {
                yield break;
            }
        }
        Debug.Log("홀드 성공!");
        GetComponentInChildren<TMPObject>().OpenKeyBoard();
    }

    private void StartMove(float moveX, float moveY)
    {
        if (onMove != null)
        {
            onMove.Invoke(friend, x, y, moveX, moveY);
        }
    }

    private void SetMove(float moveX, float moveY)
    {
        if (onMove != null)
        {
            onMove.Invoke(friend, x, y, moveX, moveY);
        }
        if (onEventMove != null)
        {
            onEventMove.Invoke();
        }
    }

    private void EndMove()
    {
        if (onEndMove != null)
        {
            onEndMove.Invoke(friend, x, y);
        }
        if (onEventEndMove != null)
        {
            onEventEndMove.Invoke();
        }
    }
}
