﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyButtonUI : MonoBehaviour
{
    public Button thisButton;
    public Image thisImage;
    public GameObject checkObj;
    public Image rewardImage;
    public bool canReward;
    public int Day;

    public void SetCheckObj(bool check)
    {
        checkObj.SetActive(check);
    }
    public void SetThisButtonColor(Color color)
    {
        thisImage.color = color;
    }
    public void SetRewardImage(Sprite sprite)
    {
        rewardImage.sprite = sprite;
    }
}
