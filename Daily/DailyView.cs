﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class DailyView : MonoBehaviour
{
    public GameObject goDailyPanel;
    public GameObject goItmeGetPopup;
    public ItemGetPopupUI itemGetPopupUI;
    public Image spImage;
    public List<DailyButtonUI> listDailyButtonUI = new List<DailyButtonUI>(7);
    public Button goDailyButton;
    public Button goAddButton;
    public SAlertViewUI sAlertView;

    Sprite nomalCoin;
    Sprite smallCoins;
    Sprite manyCoins;
    Sprite mainImageSprite;

    public int today;
    public int todayRemainder;

    public int todayGetCoins;

    public string todaySpSeqName;
    public string todaySpSeq;

    public bool ad;

    private void OnEnable()
    {
        DoTweenUtil.DoPopupOpen(.2f, 1f, .4f, transform);
    }

    private void Start()
    {
        InitSpImage();
        InitDaily();
    }

    public void InitSpImage()
    {
        nomalCoin = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "coin_01");
        smallCoins = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "coin_02");
        manyCoins = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "coin_04");

        if (InGameManager.Instance.thisDailySeed >= ItemSeedInfo.listDaily.Count) {
            mainImageSprite = manyCoins;
        }
        else
        {
            string seqName = ItemSeedInfo.listDaily[InGameManager.Instance.thisDailySeed].seqName;
            string seq = ItemSeedInfo.listDaily[InGameManager.Instance.thisDailySeed].itemSeq;
            string spName = ParseItemSeq.ItemSeqToStringSpName(seq);

            todaySpSeqName = seqName;
            todaySpSeq = seq;
            mainImageSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, spName);
            spImage.sprite = mainImageSprite;
        }
    }

    public void InitDaily()
    {
        Button dailyButton;
        //헷갈려서 이 안에서는 그냥 day 0부터 시작

        //날짜변경은 이전 접속일과 비교하여 처음에 알아서 해줌
        today = XMLManager.Instance.LoadUserDayCount();
        todayRemainder = today % 7;//나머지 연산 1번만 하도록

        if (InGameManager.Instance.newToday)
        {
            listDailyButtonUI[todayRemainder].canReward = true;
            InGameManager.Instance.getDailyReward = false;
            InGameManager.Instance.getADCoins = false;
        }
        for(int i = 0; i < 7; i++)
        {
            DailyButtonUI dailyButtonUI = listDailyButtonUI[i];
            dailyButton = dailyButtonUI.thisButton;

            dailyButtonUI.SetRewardImage(SelCoinImg(i));

            //아이템을 받을 수 없는 경우
            if (!listDailyButtonUI[i].canReward)
            {
                dailyButton.interactable = false;
                dailyButtonUI.SetThisButtonColor(Color.gray);
                dailyButtonUI.SetCheckObj(false);
            }
            if(i == todayRemainder)
            {
                //아직 아이템을 받지 않은 경우(받을 수 있을 경우)
                if (InGameManager.Instance.getDailyReward == false)
                {
                    //체크 이미지 삭제
                    dailyButtonUI.SetCheckObj(false);
                }
                else
                {
                    SetDailyButtonImage(false);
                }
            }
            if (i < todayRemainder || (InGameManager.Instance.getDailyReward == true && i== todayRemainder))
            {
                dailyButtonUI.SetCheckObj(true);
                dailyButtonUI.SetThisButtonColor(Color.gray);
            }
        }
        if (InGameManager.Instance.thisDailySeed >= ItemSeedInfo.listDaily.Count && todayRemainder == 6)
        {
            todayGetCoins = 1000;
        }
        else
        {
            todayGetCoins = ObjectInfo.DAILY_REWARDS[todayRemainder];
        }
    }


    public void SetDailyButtonImage(bool on)
    {
        if (!on)
        {
            goAddButton.interactable = false;
            goDailyButton.interactable = false;

            goAddButton.GetComponent<Image>().color = Color.gray;
            goDailyButton.GetComponent<Image>().color = Color.gray;

            goAddButton.GetComponentInChildren<Text>().color = Color.gray;
            goDailyButton.GetComponentInChildren<Text>().color = Color.gray;
        }
        else
        {
            goAddButton.interactable = true;
            goDailyButton.interactable = true;

            goAddButton.GetComponent<Image>().color = Color.white;
            goDailyButton.GetComponent<Image>().color = Color.white;

            goAddButton.GetComponentInChildren<Text>().color = Color.white;
            goDailyButton.GetComponentInChildren<Text>().color = Color.white;
        }
    }

    Sprite SelCoinImg(int day)
    {
        switch (day)
        {
            default:
                return nomalCoin;
            case 2:
            case 5:
                return smallCoins;
            case 6:
                return mainImageSprite;
        }
    }

    //보상받는 버튼 클릭시
    public void OnDailyButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            SetItemGetPopup();
        }
    }

    //팝업이 뜬다
    public void SetItemGetPopup()
    {
        if (ad)
        {
            todayGetCoins = todayGetCoins * 2;
        }
        ad = false;

        itemGetPopupUI.SetStartAction(()=>
        {
            SetUI();
            InGameManager.Instance.StartEffect(5, itemGetPopupUI.coinStartPos.position);
        });
        itemGetPopupUI.SetOnClickAction(() =>
        {
            SetReward();
            DisMiss();
            AudioManager.Inst.PlaySFX("gaining-bonus-points-cut2");
            InGameManager.Instance.StartEffect(3, itemGetPopupUI.coinStartPos.position);
        });
        goItmeGetPopup.SetActive(true);
    }

    public void SetUI()
    {
        if (todayRemainder == 6)
        {
            itemGetPopupUI.SetImage(mainImageSprite);

            if (InGameManager.Instance.thisDailySeed >= ItemSeedInfo.listDaily.Count)
            {
                itemGetPopupUI.SetText(todayGetCoins + " " + LocalizationManager.Localize(StringInfo.COIN) + " " + LocalizationManager.Localize(StringInfo.GET_IT));//100 골드 획득!
            }
            else
            {
                itemGetPopupUI.SetText(todayGetCoins + " " + LocalizationManager.Localize(StringInfo.COIN) + ",  " + LocalizationManager.Localize(StringInfo.ITEM) + " " + LocalizationManager.Localize(StringInfo.GET_IT));//100 골드 + 아이템 획득!
            }
        }
        else
        {
            itemGetPopupUI.SetImage(SelCoinImg(todayRemainder));
            itemGetPopupUI.SetText(todayGetCoins + " " + LocalizationManager.Localize(StringInfo.COIN) + " " + LocalizationManager.Localize(StringInfo.GET_IT));
        }
    }

    public void SetReward()
    {
            if (todayRemainder == 6)
            {
                RewardSp();
            }
            else
            {
                Reward();
            }
            SetDaySetting();

            SetCoinSetting();
    }

    public void SetCoinSetting()
    {
        XMLManager.Instance.SaveUserCoinsAdd("0", todayGetCoins);
        InGameManager.Instance.GetCoinXmlAndShow();
    }

    public void Reward()
    {
        //text.text = getCoins + "획득!";
    }

    public void RewardSp()
    {
        //text.text = getCoins + " + " + spSeqName + " : " + spSeq + " 획득!";
        if (InGameManager.Instance.thisDailySeed >= ItemSeedInfo.listDaily.Count)
        {
            //SetCoinSetting에서 1000 골드 획득
        }
        else
        {
            InGameManager.Instance.AddItem(todaySpSeqName, todaySpSeq, ItemGetRoot.daily);//itemGetType 0 : 구매, 1 : 데일리, 2 : 룰렛,  3: 그 외
        }
        InGameManager.Instance.InitItemGet();
    }

    public void SetDaySetting()
    {
        listDailyButtonUI[todayRemainder].SetThisButtonColor(Color.gray);
        listDailyButtonUI[todayRemainder].thisButton.interactable = false;
        listDailyButtonUI[todayRemainder].SetCheckObj(true);

        SetDailyButtonImage(false);

        InGameManager.Instance.getDailyReward = true;

        XMLManager.Instance.SaveUserGetReward(true);
        InGameManager.Instance.newToday = false;
        InGameManager.Instance.rewardButtonUI.SetDailyCheck();
        InGameManager.Instance.rewardButtonUI.SetCheckImg3();
    }

    //광고 클릭
    public void OnADButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            AdsManager.Instance.WatchVideoWithAttendDouble((adsCallbackState) =>
            {
                switch (adsCallbackState)
                {
                    case AdsManager.AdsCallbackState.Success:
                        ad = true;
                        SetItemGetPopup();
                        break;
                    case AdsManager.AdsCallbackState.Loading:
                        sAlertView.SAlert(LocalizationManager.Localize(StringInfo.ADLoadAd));
                        break;
                }
            });
        }
    }

    public void DisMiss()
    {
        if (!DoTweenUtil.isTweening)
        {
            InGameManager.Instance.dismissDaily = true;
            goDailyPanel.SetActive(false);
            InGameManager.Instance.rewardButtonUI.SetDailyCheck();
            InGameManager.Instance.rewardButtonUI.SetCheckImg3();
        }
    }
}
