﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class SettingView : MonoBehaviour
{
    public GameObject goSettingPanel;
    public GameObject audioPanel;
    public GameObject localizePanel;
    public GameObject pushPanel;
    public CloudView cloudView;
    public TutorialView tutorialView;

    public AlertView alertView;
    public GameObject CheckPanel;
    public Button CheckButton;
    public Button restoreButton;
    public Text[] allButtonTexts;

    private void OnEnable()
    {
        DoTweenUtil.DoPopupOpen(.2f, 1f, .4f, transform);
        SetButtonInit();
        SetButtonTextInit();
    }

    public void SetButtonInit()
    {
        // iOS만 구매 복구 버튼 사용
        #if UNITY_IPHONE
            restoreButton.gameObject.SetActive(true);
        #endif
    }

    public void SetButtonTextInit()
    {
        for (int i = 0; i < allButtonTexts.Length; i++)
        {
            allButtonTexts[i].text = LocalizationManager.Localize(StringInfo.SettingButtons[i]);
        }
    }

    public void DisMiss()
    {
        if (!DoTweenUtil.isTweening)
        {
            goSettingPanel.SetActive(false);
        }
    }

    //사운드
    public void OnAudioButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            audioPanel.SetActive(true);
        }
    }

    //이용약관 
    public void OnTermsButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            OpenTermsOfService();
        }
    }

    //언어
    public void OnLocalizeButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            localizePanel.SetActive(true);
        }
    }

    //쿠폰코드입력(안쓸듯)
    public void OnClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            alertView.CheckAlert(StringInfo.UserCupon, true, "", new AlertViewOptions
            {
                okButtonDelegate = () =>
                {
                    CheckPanel.SetActive(true);
                    DoTweenUtil.DoPopupOpen(.2f, 1f, .4f, CheckButton.transform);
                    CheckButton.onClick.AddListener(() =>
                    {
                        CheckPanel.SetActive(false);
                    });
                }
            });
        }
    }

    // 구매 복구 (iOS)
    // Android는 메인 진입 시 자동 복구 로직
    public void OnRestoreClick()
    {
        IAPManager.Instance.RestorePurchases(
            () =>
            {
                // 성공
            },
            () =>
            {
                // 실패
            }
        );
    }

    //튜토리얼
    public void OnTutorialButtonClick()
    {
        tutorialView.InitTutorial();
    }

    //푸시알람
    public void OnPushMsgButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            pushPanel.SetActive(true);
        }
    }

    //클라우드
    public void OnCloudSaveButtonClick()
    {
        cloudView.PopupCloudView(true);
    }

    public void OnCloudLoadButtonClick()
    {
        cloudView.PopupCloudView(false);
    }

    //종료
    public void OnQuitButtonClick()
    {
        alertView.Alert(LocalizationManager.Localize(StringInfo.QuitGame), new AlertViewOptions
        {
            cancelButtonDelegate = () => { },
            okButtonDelegate = () => { Application.Quit(); }
        });
    }


    public void OpenTermsOfService()
    {
        Application.OpenURL("https://sites.google.com/view/catdressuppolicyprivacy");
    }
}
