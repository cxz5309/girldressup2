﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.SimpleLocalization;
using UnityEngine.UI;

public class LocalizeView : MonoBehaviour
{
    public SettingView settingView;
    public GameObject goLocalizePanel;
    public GameObject goCaseView;
    public Button[] caseButtons;
    public Text localButtonText;

    public string[] arrayLanguage;
    public string prevLanguage;
    public string nowLanguage;

    private void OnEnable()
    {
        Init();
        SetButton();
    }

    private void Start()
    {
        localButtonText.text = LocalizationManager.Localize(StringInfo.SetLocalButton1);
    }

    public void Init()
    {
        prevLanguage = nowLanguage = LocalizationManager.Language;
    }

    public void SetButton()
    {
        caseButtons = new Button[goCaseView.transform.childCount];
        for (int i = 0; i < goCaseView.transform.childCount; i++)
        {
            caseButtons[i] = goCaseView.transform.GetChild(i).GetComponent<Button>();
        }
    }

    public void OnOpenCaseButtonClick()
    {
        if (goCaseView.activeSelf)
        {
            goCaseView.SetActive(false);
        }
        else
        {
            goCaseView.SetActive(true);
        }
    }

    public void OnCaseButtonClick(string languageEach, string languageText)
    {
        nowLanguage = languageEach;
        localButtonText.text = languageText;

        LocalizationManager.Language = nowLanguage;
        settingView.SetButtonTextInit();

        goCaseView.SetActive(false);
    }

    public void OnSaveButtonClick() {
        LocalizationManager.Language = nowLanguage;
        PlayerPrefs.SetString(StringInfo.playerPrefsKeyList[7], nowLanguage);
        InGameManager.Instance.LoadAllSetting();
        Dismiss();
    }
    public void OnCancelButtonClick()
    {
        LocalizationManager.Language = prevLanguage;
        Dismiss();
    }
    public void Dismiss()
    {
        goLocalizePanel.SetActive(false);
    }
}
