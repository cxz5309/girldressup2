﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialView : MonoBehaviour
{
    public GameObject tutorialPanel;
    public GameObject settingViewObj;
    public GameObject SettingBackObj;
    public int index;

    public TutorialImgUI[] tutorialImgUI;
    // Start is called before the first frame update


    public void InitTutorial()
    {
        tutorialPanel.SetActive(true);
        settingViewObj.SetActive(false);
        SettingBackObj.SetActive(false);
        StartTutorial();
    }

    public void StartTutorial()
    {
        index = 0;
        SetIndexTutorialUI();
    }

    public void OnPrevButtonClick()
    {
        if (index > 0)
        {
            index--;
            SetIndexTutorialUI();
        }
    }
    public void OnNextButtonClick()
    {
        if (index < tutorialImgUI.Length - 1)
        {
            index++;
            SetIndexTutorialUI();
        }
    }

    public void SetIndexTutorialUI()
    {
        if (Assets.SimpleLocalization.LocalizationManager.Language == "Japanese")
        {
            tutorialImgUI[index].thisTitle.fontSize = 54;

            for (int i = 0; i < tutorialImgUI[index].thisText.Count; i++)
            {
                tutorialImgUI[index].thisText[i].fontSize = 36;
            }
            for (int i = 0; i < tutorialImgUI[index].thisAddText.Count; i++)
            {
                tutorialImgUI[index].thisAddText[i].fontSize = 28;
            }
        }
        else
        {
            tutorialImgUI[index].thisTitle.fontSize = 60;

            for (int i = 0; i < tutorialImgUI[index].thisText.Count; i++)
            {
                tutorialImgUI[index].thisText[i].fontSize = 48;
            }
            for (int i = 0; i < tutorialImgUI[index].thisAddText.Count; i++)
            {
                tutorialImgUI[index].thisAddText[i].fontSize = 32;
            }
        }
       
              
        for(int i = 0; i < tutorialImgUI.Length; i++)
        {
            if(index == i)
            {
                tutorialImgUI[i].tutorialObj.SetActive(true);
                tutorialImgUI[index].indexImg[i].color = Color.white;
            }
            else
            {
                tutorialImgUI[index].indexImg[i].color = Color.gray;
                tutorialImgUI[i].tutorialObj.SetActive(false);
            }
        }
    }

    public void Dismiss()
    {
        settingViewObj.SetActive(true);
        SettingBackObj.SetActive(true);
        tutorialPanel.SetActive(false);
    }
}
