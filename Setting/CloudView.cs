﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Messaging;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.SimpleLocalization;

public class CloudView : MonoBehaviour
{
    public GameObject goCloudPanel;
    public GameObject dontTouch;
    public AlertView alertView;
    public Button okButton;
    public Text infoText;
    public Text isConnectText;
    
    enum ViewState
    {
        CloudSave,CloudLoad
    };

    ViewState viewState;

    //OnEnable역할, save와 load구분해줌
    public void PopupCloudView(bool isSave)
    {
        goCloudPanel.SetActive(true);

        viewState = isSave ? ViewState.CloudSave : ViewState.CloudLoad;
        
        switch (viewState)
        {
            case ViewState.CloudSave:
                infoText.text = LocalizationManager.Localize(StringInfo.CloudSaveText);
                break;
            case ViewState.CloudLoad:
                infoText.text = LocalizationManager.Localize(StringInfo.CloudLoadText);
                break;
        }

        SetConnectUI();
    }

    //구글연동시 ui/x
    public void SetConnectUI()
    {
        if (FirebaseManager.Instance.signedIn && !FirebaseManager.Instance.user.IsAnonymous)
        {
            okButton.interactable = true;
            okButton.GetComponent<Image>().color = Color.white;

            isConnectText.text = "ON";
            isConnectText.color = ObjectInfo.seemsBlue;
        }
        else
        {
            okButton.interactable = false;
            okButton.GetComponent<Image>().color = Color.gray;

            isConnectText.text = "OFF";
            isConnectText.color = ObjectInfo.seemsRed;
        }
    }

    //시작시 자동 실행으로 옮겨짐(사용안함)
    public void OnGoogleConnectClick()
    {
        if (!FirebaseManager.Instance.signedIn)
        {
            FirebaseManager.Instance.GoogleLoginProcessing();
            StartCoroutine("CoIsUserAndAlert");
        }
        else
        {

            //do CloudSave / Load
        }
    }

    IEnumerator CoIsUserAndAlert()
    {
        float count = 0;
        while (true)
        {
            if (FirebaseManager.Instance.user != null)
            {
                //연결되고나서부터 버튼 사용 가능
                SetConnectUI();
                break;
            }
            count += 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
    }

    //기능
    public void OnCloudSLButtonClick()
    {
        switch (viewState)
        {
            case ViewState.CloudSave:
                dontTouch.SetActive(true);
                FirebaseManager.Instance.ChibiDataUpload()
                    .ContinueWith(uploadTask=>
                {
                    dontTouch.SetActive(false);

                    if (!uploadTask.IsFaulted && !uploadTask.IsCanceled)
                    {
                        alertView.Alert(LocalizationManager.Localize(StringInfo.SetCloudSave));
                    }
                    else
                    {
                        alertView.Alert(LocalizationManager.Localize(StringInfo.SetCloudSaveFail));
                    }
                    Dismiss();
                });
                break;
            case ViewState.CloudLoad:
                alertView.Alert(LocalizationManager.Localize(StringInfo.CloudMsg), 36, 
                    new AlertViewOptions
                    {
                        okButtonDelegate = () =>
                        {
                            dontTouch.SetActive(true);
                            StartCoroutine(FirebaseManager.Instance.ChibiDataDownload());
                            StartCoroutine("CoDownloadClearCallback");
                        },
                        cancelButtonDelegate = () =>
                        {
                        }
                    });

                break;
        }
    }

    IEnumerator CoDownloadClearCallback()
    {
        float count = 0;
        for (int i = 0; i < 30; i++)
        {
            if (FirebaseManager.Instance.startFailPopup)
            {
                alertView.Alert(LocalizationManager.Localize(StringInfo.SetCloudLoadFail));

                dontTouch.SetActive(false);
                FirebaseManager.Instance.startFailPopup = false;
                yield break;
            }
            count += 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
        dontTouch.SetActive(false);

        InGameManager.Instance.loadingPrevScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene("LoadingScene");
    }

    public void Dismiss()
    {
        goCloudPanel.SetActive(false);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
