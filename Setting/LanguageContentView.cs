﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageContentView : MonoBehaviour
{
    public GameObject languagePrefab;
    public LocalizeView localizeView;

    void OnEnable()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        GameObject go;
        for (int i = 0; i < localizeView.arrayLanguage.Length; i++)
        {
            go = Instantiate(languagePrefab, transform);

            string languageText = localizeView.arrayLanguage[i];
            string languageEach = StringInfo.LanguageEach[i];
            LanguageButtonUI languageButtonUI = go.GetComponent<LanguageButtonUI>();
            languageButtonUI.language = 
            languageButtonUI.text.text = languageText;

            if (languageEach.Equals(localizeView.nowLanguage))
            {
                languageButtonUI.text.color = Color.white;
                languageButtonUI.imageBg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "combo_sel");
            }
            else
            {
                languageButtonUI.text.color = Color.black;
                languageButtonUI.imageBg.sprite = null;
            }
            languageButtonUI.langegeButton.onClick.AddListener(() =>
            {
                localizeView.OnCaseButtonClick(languageEach, languageText);
                gameObject.SetActive(false);
            });
        }
    }
}
