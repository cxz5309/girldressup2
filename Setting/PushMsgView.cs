﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Messaging;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class PushMsgView : MonoBehaviour
{
    public GameObject goPushMsgPanel;

    public Text isPushText;
    public Text okButtonText;
    public Text noButtonText;

    public AlertView alertView;
    
    private bool pushChk;

    private void OnEnable()
    {
        pushChk = PlayerPrefs.GetInt(StringInfo.playerPrefsKeyList[6]) == 1 ? true : false;
        SetPushUI();
    }

    public void SetPushUI()
    {
        if (pushChk)
        {
            isPushText.color = ObjectInfo.seemsBlue;
            isPushText.text = "ON";
        }
        else
        {
            isPushText.color = ObjectInfo.seemsRed;
            isPushText.text = "OFF";
        }
    }

    public void OnPushOnClick()
    {
        if (pushChk)
        {
        }
        else
        {
            pushChk = true;
            if (FirebaseManager.Instance.user != null)
            {
                PlayerPrefs.SetInt(StringInfo.playerPrefsKeyList[6], 1);
                FirebaseManager.Instance.SubscribeMessage(pushChk);
            }
            else
            {
                PlayerPrefs.SetInt(StringInfo.playerPrefsKeyList[6], 0);
            }
            //FirebaseManager.Instance.MessageEnable(pushChk);
        }
        alertView.Alert(LocalizationManager.Localize(StringInfo.SetPush));
        SetPushUI();
    }//스트링값 옮기기

    public void OnPushOffClick()
    {
        if (pushChk)
        {
            pushChk = false;
            if (FirebaseManager.Instance.user != null)
            {
                PlayerPrefs.SetInt(StringInfo.playerPrefsKeyList[6], 0);
                FirebaseManager.Instance.SubscribeMessage(pushChk);
            }
            else
            {
                PlayerPrefs.SetInt(StringInfo.playerPrefsKeyList[6], 0);
            }
            //FirebaseManager.Instance.MessageEnable(pushChk);
        }
        else
        {
        }
        alertView.Alert(LocalizationManager.Localize(StringInfo.NSetPush));
        SetPushUI();
    }


    public void Dismiss()
    {
        goPushMsgPanel.SetActive(false);
    }
}
