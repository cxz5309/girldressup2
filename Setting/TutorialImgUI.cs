﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TutorialImgUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public TutorialView tutorialView;

    public GameObject tutorialObj;
    public Text thisTitle;
    public List<Text> thisText;
    public List<Text> thisAddText;
    public Image[] indexImg;

    //터치
    Vector3 startPos;
    Vector3 endPos;
    float minX_Distance = 100f;
    float maxY_Distance = 500f;
    float distX;
    float distY;


    public void OnPointerDown(PointerEventData eventData)
    {
        startPos = Input.mousePosition;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        endPos = Input.mousePosition;
        distX = endPos.x - startPos.x;
        distY = endPos.y - startPos.y;
        if (Mathf.Abs(distX) > minX_Distance && Mathf.Abs(distY) < maxY_Distance) {
            if (distX > 0)
            {
                tutorialView.OnPrevButtonClick();
            }
            else
            {
                tutorialView.OnNextButtonClick();
            }
        }
    }
}
