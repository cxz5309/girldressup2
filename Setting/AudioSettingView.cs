﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioSettingView : MonoBehaviour
{
    public GameObject AudioPanel;
    public Slider sliderBGM;
    public Slider sliderSFX;

    public float prevBGMVolume;
    public float prevSFXVolume;
    public float nowBGMVolume;
    public float nowSFXVolume;
    public float startBGMVolume;
    public float startSFXVolume;

    public bool prevIsOnBGM;
    public bool prevIsOnSFX;
    public bool nowIsOnBGM;
    public bool nowIsOnSFX;
    public bool startIsOnBGM;
    public bool startIsOnSFX;

    public Image BGMImg;
    public Image SFXImg;
    public Image BGMOnButtonImg;
    public Image SFXOnButtonImg;

    private void OnEnable()
    {
        Init();
    }

    public void Init()
    {
        InitSetting();
        InitUI();
    }

    public void InitSetting()
    {
        startIsOnBGM = prevIsOnBGM = nowIsOnBGM = AudioManager.Inst.IsMusicOn;
        startIsOnSFX = prevIsOnSFX = nowIsOnSFX =  AudioManager.Inst.IsSoundOn;

        startBGMVolume = nowBGMVolume = prevBGMVolume = AudioManager.Inst.MusicVolume;
        startSFXVolume = nowSFXVolume = prevSFXVolume = AudioManager.Inst.SoundVolume;
    }

    public void InitUI()
    {
        sliderBGM.interactable = startIsOnBGM;
        sliderSFX.interactable = startIsOnSFX;
        sliderBGM.value = startBGMVolume;
        sliderSFX.value = startSFXVolume;
        SetBGMStopImage(startIsOnBGM);
        SetSFXStopImage(startIsOnSFX);
    }

    public void OnBGMStopButtonClick()
    {
        nowIsOnBGM = !nowIsOnBGM;
        SetBGMStopImage(nowIsOnBGM);

        if (nowIsOnBGM) {
            //(세팅 변경만 해도 뮤트되는지 확인해봐야함) ==> 됨
            //AudioManager.Inst.ResumeAllSFX();

            sliderBGM.value = prevBGMVolume;
        }
        else
        {
            prevBGMVolume = sliderBGM.value;

            //AudioManager.Inst.PauseBGM();
            sliderBGM.value = 0;
        }
        //OnBGMSliderValChange();
        sliderBGM.interactable = nowIsOnBGM;
        AudioManager.Inst.IsMusicOn = nowIsOnBGM;
    }

    public void OnSFXStopButtonClick()
    {
        nowIsOnSFX = !nowIsOnSFX;
        SetSFXStopImage(nowIsOnSFX);
        if (nowIsOnSFX)
        {

            sliderSFX.value = prevSFXVolume;
        }
        else
        {
            prevSFXVolume = sliderSFX.value;

            sliderSFX.value = 0;
            //AudioManager.Inst.StopAllSFX();
        }
        //OnSFXSliderValChange();
        sliderSFX.interactable = nowIsOnSFX;
        AudioManager.Inst.IsSoundOn = nowIsOnSFX;
    }

    public void OnBGMSliderValChange()
    {
        nowBGMVolume = sliderBGM.value;
        AudioManager.Inst.MusicVolume = nowBGMVolume;
    }

    public void OnSFXSliderValChange()
    {
        nowSFXVolume = sliderSFX.value;
        AudioManager.Inst.SoundVolume = nowSFXVolume;
    }

    public void SetBGMStopImage(bool isOn)
    {
        if (isOn)
        {
            BGMOnButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "capture_togglebtn_on");
            BGMImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "audio_seticon01_p");
        }
        else
        {
            BGMOnButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "capture_togglebtn_off");
            BGMImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "audio_seticon01_n");
        }
    }

    public void SetSFXStopImage(bool isOn)
    {
        if (isOn)
        {
            SFXOnButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "capture_togglebtn_on");
            SFXImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "audio_seticon02_p");
        }
        else
        {
            SFXOnButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "capture_togglebtn_off");
            SFXImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "audio_seticon02_n");
        }
    }


    //저장하면 현재 세팅으로 변경
    public void OnOKButtonClick()
    {
        // : 아무것도 할게 없음
        Dismiss();
    }

    public void OnCancelButtonClick()
    {
        AudioManager.Inst.MusicVolume = startBGMVolume;
        AudioManager.Inst.SoundVolume = startSFXVolume;
        AudioManager.Inst.IsMusicOn = startIsOnBGM;
        AudioManager.Inst.IsSoundOn = startIsOnSFX;
        if (prevIsOnBGM)
        {
            //AudioManager.Inst.StopAllSFX();
        }
        if (prevIsOnSFX)
        {
            //AudioManager.Inst.PauseBGM();
        }
        else
        {
            //AudioManager.Inst.ResumeAllSFX();
        }
        Dismiss();
    }

    //그냥 나가면 이전 세팅으로 돌아감
    public void Dismiss()
    {
        AudioPanel.SetActive(false);
    }
}
