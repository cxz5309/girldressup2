﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserHave
{
    public string seqName;
    public string itemSeq;

    public UserHave(string seqName, string itemSeq)
    {
        this.seqName = seqName;
        this.itemSeq = itemSeq;
    }
}

public class ItemSeedInfo : MonoBehaviour
{
    public static List<UserHave> listDaily = new List<UserHave>();
    public static List<UserHave> listFortuneWheel = new List<UserHave>();
    public static int nowSeed1;//룰렛0번시드
    public static int nowSeed2;//룰렛4번시드

    public static void Init()
    {
        listDaily = XMLManager.Instance.LoadAllDailyItemSeed();
        listFortuneWheel = XMLManager.Instance.LoadAllWheelItemSeed();
        SetRandomWheelSeed();//가장 처음 실행 이후는 룰렛 돌릴때마다 변경
    }

    public static void SetRandomWheelSeed()//num : -1 = 전체 소유중, 0 ~ listFortuneWheel.Count-1 시드 숫자
    {
        int num1 = -1;
        int num2 = -1;
        int max = listFortuneWheel.Count;
        num1 = Random.Range(0, max);
        num2 = Random.Range(0, max);
        //랜덤 번호 아이템 가지고있으면 +1 반복
        for(int i = 0; i < max; i++)
        {
            num1 = (num1 + i) % max;

            if (XMLManager.Instance.IsUserHaveItem(listFortuneWheel[num1].seqName, listFortuneWheel[num1].itemSeq))
            {
                continue;
            }
            else
            {
                break;
            }
        }
        for (int i = 0; i < max; i++)
        {
            num2 = (num2 + i) % max;

            if (XMLManager.Instance.IsUserHaveItem(listFortuneWheel[num2].seqName, listFortuneWheel[num2].itemSeq))
            {
                continue;
            }
            else
            {
                break;
            }
        }

        //전부 가지고 있으면 -1
        bool allHave = true;
        for(int i=0; i < max; i++)
        {
            if (!XMLManager.Instance.IsUserHaveItem(listFortuneWheel[i].seqName, listFortuneWheel[i].itemSeq))
            {
                allHave = false;
                break;
            }
        }
        if(allHave == true)
        {
            num1 = -1;
            num2 = -1;
        }
        nowSeed1 = num1;//현재 시드 정보가 필요할 경우 사용
        nowSeed2 = num2;//현재 시드 정보가 필요할 경우 사용
    }
}
