﻿using System;
using UnityEngine;

public enum OrderType
{
    Prepare0 = 0,
    Tail = 1,
    Wing = 2,
    BackHairBack =3,
    BackHair=4,
    BackHairTrans=5,
    BodyBack=6,
    Body=7,
    BodyTrans=8,
    Underwear= 9,
    Prepare1 = 10,
    FaceAc = 11,
    Tattoo = 12,
    EyeLeft = 13,
    EyeRight = 14,
    Nose=15,
    Mouse=16,
    SideHairBack = 17,
    SideHair = 18,
    SideHairTrans = 19,
    FrontHairBack = 20,
    FrontHair = 21,
    FrontHairTrans = 22,
    EyebrowBack = 23,
    Eyebrow = 24,
    EyebrowTrans = 25,
    OuterBack = 26,
    Stocking=27,
    Shoes=28,
    UnderSkirt = 29,
    UnderPants=30,
    UHandBackS = 31,
    UHandS = 32,
    UHandTransS = 33,
    WithoutArmsShirts = 34,
    Shirts = 35,
    HandBackS = 36,
    HandS = 37,
    HandTransS = 38,
    Skirt = 39,
    Pants = 40,
    UHandBackD = 41,
    UHandD = 42,
    UHandTransD = 43,
    SailorSuit = 44,
    Costume = 45,
    Dress = 46,
    HandBackD = 47,
    HandD = 48,
    HandTransD = 49,
    Necklace = 50,
    UHandBackO = 51,
    UHandO = 52,
    UHandTransO = 53,
    Outer = 54,
    HandBackO = 55,
    HandO = 56,
    HandTransO = 57,
    Slip = 58,
    HeadDress=59,
    Mask=60,
    Glasses = 61,
    Necktie=62,
    Earring=63,
    Special = 100
}
public class CharPartOrder
{
    public OrderType orderType;

    public CharPartOrder(int orderNum)
    {
        this.orderType = (OrderType)orderNum;

        //InitNotDelete();
        //InitFixed();
        //InitMultiple();
    }

    public static int StringToIntOrder(string nowButtonMenu)
    {
        return (int)(OrderType)Enum.Parse(typeof(OrderType), nowButtonMenu);
    }
    public static OrderType StringToEnum(string nowButtonMenu)
    {
        return (OrderType)Enum.Parse(typeof(OrderType), nowButtonMenu);
    }

    public static string EnumToString(OrderType partOrderType)
    {
        return partOrderType.ToString();
    }

    public static string IntToString(int orderNum)
    {
        return EnumToString((OrderType)orderNum);
    }

    public static KindType OrderNumToKindNum(int orderNum)
    {
        switch (orderNum)
        {
            default:
                string thisOrder = IntToString(orderNum);
                return (KindType)CharPartKind.StringToIntKind(thisOrder);
            case 0:
            case 10:
                return 0;
            case 4:
            case 5:
                return (KindType)4;
            case 7:
            case 8:
                return (KindType)6;
            case 18:
            case 19:
                return (KindType)15;
            case 24:
            case 25:
                return (KindType)19;
            case 21:
            case 22:
                return (KindType)17;
            case 31:
            case 36:
                return (KindType)23;
            case 32:
            case 33:
            case 37:
            case 38:
                return (KindType)24;
            case 34:
            case 35:
                return (KindType)25;
            case 40:
            case 39:
            case 30:
            case 29:
                return (KindType)26;
            case 41:
            case 47:
                return (KindType)27;
            case 42:
            case 43:
            case 48:
            case 49:
                return (KindType)28;
            case 44:
            case 45:
            case 46:
                return (KindType)29;
            case 51:
            case 55:
                return (KindType)31;
            case 52:
            case 53:
            case 56:
            case 57:
                return (KindType)32;
        }
    }

    public static string MatchOrder(string orderType)
    {
        switch (orderType)
        {
            default:
                return orderType;
            case "UHandBack_S":
            case "UHandBack_D":
            case "UHandBack_O":
                return "UHandBack";
            case "UHand_S":
            case "UHand_D":
            case "UHand_O":
                return "UHand";
            case "UHandTrans_S":
            case "UHandTrans_D":
            case "UHandTrans_O":
                return "UHandTrans";
            case "HandBack_S":
            case "HandBack_D":
            case "HandBack_O":
                return "HandBack";
            case "Hand_S":
            case "Hand_D":
            case "Hand_O":
                return "Hand";
            case "HandTrans_S":
            case "HandTrans_D":
            case "HandTrans_O":
                return "HandTrans";
        }
    }

}