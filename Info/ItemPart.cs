﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPart
{
    public string itemSeq;

    public string seqName;
    public string typeSeq;
    public ItemType itemType;

    public ItemPart(string itemSeq, string seqName, string typeSeq, int itemType)
    {
        this.itemSeq = itemSeq;
        this.seqName = seqName;
        this.typeSeq = typeSeq;
        this.itemType = (ItemType)itemType;
    }
}
