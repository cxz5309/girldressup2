﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum KindType
{
    Prepare0 = 0,
    Tail = 1,
    Wing = 2,
    BackHairBack = 3,
    BackHair = 4,
    BodyBack = 5,
    Body = 6,
    Underwear = 7,
    FaceAc = 8,
    Tattoo = 9,
    EyeLeft = 10,
    EyeRight = 11,
    Nose = 12,
    Mouse = 13,
    SideHairBack = 14,
    SideHair = 15,
    FrontHairBack = 16,
    FrontHair = 17,
    EyebrowBack = 18,
    Eyebrow = 19,
    OuterBack = 20,
    Stocking = 21,
    Shoes = 22,
    HandBackS=23,
    HandS=24,
    Shirts = 25,
    Pants = 26,
    HandBackD=27,
    HandD=28,
    Dress = 29,
    Necklace=30,
    HandBackO=31,
    HandO=32,
    Outer = 33,
    Slip = 34,
    HeadDress = 35,
    Mask = 36,
    Glasses = 37,
    Necktie = 38,
    Earring = 39,
    Special = 100
}

public class CharPartKind
{
    //================================================================================
    // XXX Constants
    //================================================================================

    //================================================================================
    // XXX Variables
    //================================================================================
    // Lang
    public KindType kindType;

    public bool isNotDelete;
    public bool isFixed;
    public bool isMultiple;

    //================================================================================
    // XXX Enum
    //================================================================================
    
    //================================================================================
    // XXX Constructor
    //================================================================================
    public CharPartKind(int kindNum)
    {
        this.kindType = (KindType)kindNum;
        
        //InitNotDelete();
        //InitFixed();
        //InitMultiple();
    }
    public static string EnumToString(KindType partKindType)
    {
        return partKindType.ToString();
    }

    public static string IntToString(int kindNum)
    {
        return EnumToString((KindType)kindNum);
    }

    public static int StringToIntKind(string kind)
    {
        return (int)(KindType)Enum.Parse(typeof(KindType), kind);
    }

    public static KindType StringToEnum(string kind)
    {
        return (KindType)Enum.Parse(typeof(KindType), kind);
    }

    //private void InitNotDelete()
    //{
    //    switch (charPartsKindType)
    //    {
    //        case CharPartsKindType.Special:
    //        case CharPartsKindType.Cat:
    //        case CharPartsKindType.Eye:
    //        case CharPartsKindType.Mouth:
    //        case CharPartsKindType.Bg:
    //            isNotDelete = true;
    //            break;
    //        default:
    //            isNotDelete = false;
    //            break;
    //    }
    //}

    //private void InitFixed()
    //{
    //    switch (catPartsKindType)
    //    {
    //        case CharPartsKindType.Special:
    //        case CharPartsKindType.Cat:
    //        case CharPartsKindType.Top:
    //        case CharPartsKindType.Bottom:
    //        case CharPartsKindType.Outer:
    //        case CharPartsKindType.Shoes:
    //        case CharPartsKindType.Gloves:
    //        case CharPartsKindType.Necklace:
    //        case CharPartsKindType.Bg:
    //            isFixed = true;
    //            break;
    //        default:
    //            isFixed = false;
    //            break;
    //    }
    //}

    //private void InitMultiple()
    //{
    //    switch (catPartsKindType)
    //    {
    //        case CharPartsKindType.Face:
    //        case CharPartsKindType.Bodyac:
    //        case CharPartsKindType.Front:
    //        case CharPartsKindType.Rear:
    //        case CharPartsKindType.Text:
    //        case CharPartsKindType.Balloon:
    //        case CharPartsKindType.OverlayText:
    //            isMultiple = true;
    //            break;
    //        default:
    //            isMultiple = false;
    //            break;
    //    }
    //}
}
