﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class AtlasLoader : MonoBehaviour
{
    SpriteAtlas[] atlases = new SpriteAtlas[3];
    string[] atlasesPath =
    {
        "SpriteAtlas/AllUIAtlas0",//0
        "SpriteAtlas/AllUIAtlas1",//1
        "SpriteAtlas/AllCollectionAtlas0"//2
    };

    public void LoadAtlas(Action callback)
    {
        StartCoroutine(CoLoadAtlas(callback));
    }

    IEnumerator CoLoadAtlas(Action callback)
    {
        ResourceRequest resourceRequest;
        for (int i = 0; i < atlases.Length; i++)
        {
            resourceRequest = Resources.LoadAsync(atlasesPath[i]);
            while (!resourceRequest.isDone)
            {
                yield return null;
            }
            atlases[i] = resourceRequest.asset as SpriteAtlas;
        }

        AllocateAtlas();

        callback();
    }

    public void AllocateAtlas()
    {
        AtlasInfo.atlases = atlases;
    }

    private void OnDestroy()
    {
        atlases = null;
        atlasesPath = null;
    }
}
