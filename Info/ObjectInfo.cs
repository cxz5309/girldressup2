using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInfo : MonoBehaviour
{
    //playerprefs
    public const float defBGMVol = 0.5f;
    public const float defSFXVol = 0.5f;
    public const int defBGMMute = 1;
    public const int defSFXMute = 1;
    public const int defFontColor = 0;
    //public const int defFontSize = 0;
    public const int defColStage = 0;
    public const int isPushMsg = 1;//bool값 1 = true;

    //wheel cooltime
    public const int ITEM_COOL_TIME = 60;

    //reward coin
    public const int AD_REWARD = 200;
    public static int[] DAILY_REWARDS = { 100, 100, 300, 100, 100, 400, 700 };
    public static int[] MISSION_REWARDS_0 = { 50, 100, 150, 200, 250, 300, 350, 400, 450, 500 };

    //reward Item itemSet(보상이 하나가 아닌 여러개일 경우)
    public static string[][][] AllItemSet =
    {
        new string[][]//item_set 0
        {
            new string[]
            {
                "object1_seq", "Obj1_2"
            },
            new string[]
            {
                "object1_seq", "Obj1_4"
            }
        },
        new string[][]
        {
            new string[]
            {
                "eyes_seq", "017"
            },
            new string[]
            {
                "item_seq", "015011"
            },
            new string[]
            {
                "item_seq", "016102"
            }
            , new string[]
            {
                "item_seq", "024020"
            }
        },
        new string[][]
        {
            new string[]
            {
                "item_seq", "012002"
            },
            new string[]
            {
                "item_seq", "027112"
            }
        }
    };//[세트 순서(딕셔너리)][한 세트의 아이템 리스트][seqName과 itemSeq]

    //메이킹 워터마크 포지션
    public static Vector3 waterMarkPosMaking = new Vector3(0, -2.1f, -9);

    //괜찮은 색 모음
    public static Color mainCameraColor = new Color(1f, 0.844f, 0.966f);
    public static Color seemsRed = new Color(0.764f, 0.227f, 0.227f);
    public static Color seemsBlue = new Color(0.227f, 0.263f, 0.764f);
    public static Color seemsBlack = new Color(0.294f, 0.098f, 0.117f);
    public static Color seemsWhite = new Color(0.9f, 0.9f, 0.9f);

    //말풍선 텍스트 위치 세부 조정
    public const int bubbleLength = 6;
    public static float[] bubblePosUps = { 0, 0.15f, 0.262f, 0.376f, 0, 0.37f };
    public static float[] bubbleHeightDowns = { 0, 3f, 5.3f, 7.5f, 0, 7.4f };
    public static float[] bubbleWidthRatios = { 0.54f, 0.64f, 0.64f, 0.56f, 0.46f, 0.56f };
    public static float[] bubbleHeightRatios = { 0.54f, 0.64f, 0.64f, 0.56f, 0.46f, 0.56f };

    //말풍선 기본 크기
    public static int[] arrayTextSize = { 32, 48, 60 };
    //말풍선 색상
    public static Color[] arrayTextColor = {
        new Color32(0x4B, 0x19, 0x1E, 0xFF),
        new Color32(0xE6, 0xE6, 0xE6, 0xFF),//옅은 회색
        new Color32(0xD7, 0x44, 0x8c, 0xFF),
        new Color32(0xff, 0x54, 0x54, 0xFF),
        new Color32(0xff, 0x78, 0x19, 0xFF),
        new Color32(0xff, 0xCB, 0x17, 0xFF),
        new Color32(0xB6, 0xFA, 0x37, 0xFF),
        new Color32(0x50, 0xE1, 0x3c, 0xFF),
        new Color32(0x3C, 0xEB, 0xDC, 0xFF),
        new Color32(0x46, 0xC8, 0xFF, 0xFF),
        new Color32(0x6E, 0x78, 0xFA, 0xFF),
        new Color32(0xC8, 0x65, 0xF4, 0xFF)
    };

    public static Color[] listBodyColor =
    {
        Color.black,
        Color.gray,
        Color.magenta,
        Color.green,
        Color.blue,
        Color.cyan,
        Color.yellow,
        Color.red,
        new Color(.59f, .77f, .85f),
        new Color(.92f, .75f, .92f),
        new Color(.77f, .93f, .88f),
        new Color(.90f, .93f, .76f),
        new Color(.71f, .50f, .53f),
        new Color(0.96f, 0.78f, 0.78f),
        new Color(.92f, .81f, .76f),
        new Color(.99f, 0.89f, 0.84f),
        new Color(1, 0.89f, 0.89f),
        new Color(1, 0.95f, 0.95f),
        new Color(1f, 1f, 1f)
    };
}
