﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharTr
{
    private string trSeq;
    private string usedSeq;
    private Vector3 position;
    private float scale;
    private int order;
    private bool isReversed;

    public string UsedSeq { get => usedSeq; set => usedSeq = value; }
    public Vector3 Position { get => position; set => position = value; }
    public float Scale { get => scale; set => scale = value; }
    public int Order { get => order; set => order = value; }
    public bool IsReversed { get => isReversed; set => isReversed = value; }
    public string TrSeq { get => trSeq; set => trSeq = value; }

    public CharTr()
    {
        this.trSeq = null;
        this.UsedSeq = null;
        this.Position = Vector3.zero;
        this.Scale = 0;
        this.Order = 0;
        this.IsReversed = false;
    }

    public CharTr(string trSeq, string usedNum, Vector3 position, float scale, int order, bool isReversed)
    {
        this.trSeq = trSeq;
        this.UsedSeq = usedNum;
        this.Position = position;
        this.Scale = scale;
        this.Order = order;
        this.IsReversed = isReversed;
    }
}
