﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringInfo : MonoBehaviour
{
    //Playerprefs
    public static List<string> playerPrefsKeyList = new List<string>(new string[]
    {
        "BGMVol",//0
        "SFXVol",//1
        "BGMMute",//2
        "SFXMute",//3
        "FontColor",//4
        /*"FontSize", */
        "CollectionStage",//5
        "IsPush",//6
        "SettingLanguage",//7
        "IsNew"//8
    });

    //All
    public const string COIN = "Texts.Gold";
    public const string ITEM = "Texts.Item";
    public const string GET_IT = "Texts.Acquired";
    public const string QuitGame = "Texts.Exit";
    public const string OpenURL = "market://search?q=pub:OnDot";
    public const string GoBtn = "Texts.GO";
    public const string MClearBtn = "Texts.Done";
    public const string GET = "Texts.Get";

    //All 외부
    public const string Yes = "Texts.YES";
    public const string No = "Texts.NO";
    public const string Check = "Texts.OK";
    public const string NCheck = "Texts.Cancel";

    //Collection
    public const string collection_itembox_on = "collection_itembox_on";
    public const string collection_itembox_off = "collection_itembox_off";
    public const string collection_btn_get_off = "collection_btn_get_off";
    public const string collection_btn_get_on = "collection_btn_get_on";
    public const string COMMING_SOON = "Collection.Coming";
    public const string REWARD = "Collection.REWARD";
    public const string NIsOnInfo = "Collection.CanGetColRew";
    public const string IsOnInfo = "Collection.AcqCollRew";
    public const string BounsStage = "Collection.Bonus";
    public const string ReqInfo = "Collection.GetAllCol";
    public const string ColGetPopup = "Collection.StageN_N";
    public const string ColAllClGetPopup = "Collection.Cong";
    public const string ColText1 = "Collection.GetAtStore";
    public const string ColText2 = "Collection.ObtainFrom";

    //Collection 외부
    public const string ColButton1 = "Collection";
    public const string ColLabel1 = "STAGE CLEAR!";

    //MoreGame 외부
    public const string MoreGameTitle1 = "MORE GAMES";

    //AD외부
    public const string ADText1 = "Ad.WatchADTo";
    public const string ADButton1 = "Ad.WatchAD";
    public const string ADText2 = "Ad.TapWatchAd";
    public const string ADLoadAd = "Ad.LoadAd";

    //Store
    public const string shop_tab_shop_n = "shop_tab_shop_n";
    public const string shop_tab_shop_p = "shop_tab_shop_p";
    public const string StoreGold = "Gold";
    public const string Cash = "Texts.Cash";

    public const string RmWBtn = "워터마크 제거";
    public const string ConfInfo = "Store.Purchase";
    public const string NHaveCoinInfo = "Store.InsGold";
    public static string[] Bundle_Name =
    {
        "Store.RmAdAbove",
        "Store.RmWatermark",
        "Store.Bundle1",
        "Store.Bundle2",
        "Store.Bundle3",
        "Store.Bundle4",
        "Store.Bundle5",
        "Store.Bundle6",
        "Store.Bundle7",
        "Store.Bundle8",
        "Store.Bundle9",
        "Store.Bundle10",
    };
    public static string[] Bundle_Button_Name =
    {
        "0. remove top Ads",
        "1. remove waterMark",
        "2. Add All Dress Tab",
        "3. Add Costume Tab",
        "4. Add Glasses + Neckties Tab",
        "5. Add Mask + Earing Tab",
        "6. Add Wing + Tail Tab",
        "7. Add All Object2 Tab",
        "8. Add 21 Hair Style",
        "9. Add 55(Eye+Mouth+Nose)",
        "10. Add 34 Clothes",
        "11. Add Text + BG"
    };
    //Store 외부
    public const string StoreButton1 = "골드상점";
    public const string StoreButton2 = "일반상점";

    //Daily 외부
    public const string DailyTitle = "출석 보상";
    public const string DailyButton1 = "받기";
    public const string DailyButton2 = "두배 받기";


    //wheel
    public const string StartWheelButton = "FortuneWheel.SPIN";
    public const string WheelTimeFormat = "{0:D2} : {1:D2}";
    public const string WheelADInfo = "FortuneWheel.CanWatchMinute.";
    public const string GO = "GO!";

    //Mission
    public const string quest_list_quest_off = "quest_list_quest_off";
    public const string quest_btn_quest_off = "quest_btn_quest_off";
    public const string quest_list_quest_on = "quest_list_quest_on";
    public const string MSaveInfo = "Mission.4Info";
    public const string MExpInfo = "Mission.6Info";
    public static string[] Mission = {
        "Mission.1" ,
        "Mission.2",
        "Mission.3" ,
        "Mission.4",
        "Mission.5",
        "Mission.6",
        "Mission.7",
        "Mission.8",
        "Mission.9"
    };
    public static string[] M2ItemsSt = { "Mission.Shose", "Mission.glasses", "Mission.shirts" };
    public static string[] M2Items = { "Shoes", "Glasses", "Shirts" };

    //Mission 외부
    public const string MissionTitle1 = "Mission.Mission";

    //ColorView
    public const string CVCopy = "Scene.Making.CopyClipboard";
    public const string CVPaste = "Scene.Making.PasteText";

    //Making
    public const string TextViewColInfo = "Making.UnlockCol";
    public const string TextViewWheelInfo = "Making.UnlockWheel";
    public const string MakSave_Q = "Making.SaveQ";//뒷문장 사이즈 줄이기
    public const string MakClearInfo = "Scene.Making.ResetQ";

    //Making 외부
    public const string MultiItemText = "여러 개 추가되요";
    public const string ColorCode = "ColorCode : ";
    public const string CopyColorCode = "Copy\nColorCode";
    public const string PasteColorCode = "Paste\nColorCode";

    //Main
    public const string MainClearInfo = "Scene.Main.ResetQ";
    public const string DontMoreObj = "Scene.Main.CanNotCreate";

    //SL
    public const string SLSelSlotInfo = "Scene.Main.ChooseSlot";
    public const string SLCoverInfo = "Scene.Main.OverwriteSlot";
    public const string SLSaveInfo = "Scene.Main.SaveSlot";
    public const string SLLoadInfo = "Scene.Main.LoadSlot";
    public const string SLSaveAl = "Scene.Main.Saved";
    public const string SLLoadAl = "Scene.Main.ScreenLoaded";
    public const string SLEmpty = "Scene.Main.SlotEmpty";
    public const string SLName = "Slot {0}";
    public const string MaxSlot = "33";
    public const string MinSlot = "1";

    //Character
    public const string NameChar = "Scene.Main.NameChar";
    public const string CreSlot = "Scene.Main.CreSlot";
    public const string DelChar = "Scene.Main.DelChar";
    public const string ModifyChar = "Scene.Main.ModifyChar";
    public const string DelSlot = "Scene.Main.DelSlot";

    //Capture
    public const string CapPermissionInfo = "Scene.Main.Permissions";
    public const string CapPath = "Scene.Main.SavePath";

    //Setting 외부
    public static string[] SettingButtons =
        {
        "Setting.Language",
        "Setting.Sound",
        "Setting.Terms",
        "Setting.Tutorial",
        "Setting.RestorePurchase",
        "Setting.PurchaseRecord",
        "Setting.SaveCloud",
        "Setting.LoadCloud",
        "Setting.PushNotification",
        "Setting.ExitGame"
    };
    //Setting
    public const string UserCupon = "쿠폰 사용하기";
    //PushMassage
    public const string SetPush = "Setting.Push.PushON";
    public const string NSetPush = "Setting.Push.PushOFF";

    //PushMassage 외부
    public const string PushText = "푸시를{0}받으시겠습니까?";
    public const string PushText2 = "푸시 받기";
    public const string PushButton1 = "ON";
    public const string PushButton2 = "OFF";

    //Cloud Storage
    public const string SetCloudSave = "Setting.Cloud.Saved";
    public const string SetCloudSaveFail = "Setting.Cloud.SaveFail";
    public const string SetCloudLoad = "Setting.Cloud.Loaded";
    public const string SetCloudLoadFail = "Setting.Cloud.LoadFail";
    public const string CloudSaveText = "Setting.Cloud.SaveQ";
    public const string CloudLoadText = "Setting.Cloud.LoadQ";


    //Cloud Storage외부
    public const string CloudMsg = "Setting.Cloud.AllRemoved";
    public const string CloudText1 = "구글 계정 연동";
    public const string CloudButton1 = "계정 연동하기";

    //TMP
    public static string defaultText = "Texts.TOUCH";

    //Language 내부
    public const string SetLocalButton1 = "Setting.Lang.Language";
    //Language 내부
    public static string[] LanguageEach = { "Korean", "English", "Japanese" };

    //Audio 외부
    public const string SetAudioButton1 = "설정하기";

    //Tutorial 외부
    public const string TutorialClearText =
        "축하합니다! 튜토리얼을 모두 마치셨습니다!\n\n하단의 CREATE 버튼을 눌러 더 많은 캐릭터를 만들어보세요!";

    //firebase
    public const string LoginCanceled = "Texts.GoogleCanceled";
    public const string LoginFaulted = "Texts.GoogleFailed";

    // Review
    public const string RequestReviewGooglePlay = "Texts.RequestReviewGooglePlay";
    public const string RequestReviewAppStore = "Texts.RequestReviewAppStore";
}
