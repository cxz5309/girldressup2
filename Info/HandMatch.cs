﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandMatch
{
    public static OrderType[] handOrder = new OrderType[3];
    public static OrderType[] uHandOrder = new OrderType[3];
    public static OrderType[] handBackOrder = new OrderType[3];
    public static OrderType[] uHandBackOrder = new OrderType[3];
    public static OrderType[] handTransOrder = new OrderType[3];
    public static OrderType[] uHandTransOrder = new OrderType[3];
    public static KindType[] handKind = new KindType[3];
    public static KindType[] handBackKind = new KindType[3];
    public static bool[] isHandKind = new bool[3];

    public static int kindNumToArrayNum(KindType kindType)
    {
        switch (kindType)
        {
            default:
                return -1;
            case KindType.Shirts:
            case KindType.HandS:
            case KindType.HandBackS:
                return 0;
            case KindType.Outer:
            case KindType.HandO:
            case KindType.HandBackO:
                return 1;
            case KindType.Dress:
            case KindType.HandD:
            case KindType.HandBackD:
                return 2;
        }
    }

    public static void MachingWithParent(KindType parentKindType)
    {
        switch (parentKindType)
        {
            case KindType.Shirts:
                handOrder[0] = OrderType.HandS;
                uHandOrder[0] = OrderType.UHandS;
                handBackOrder[0] = OrderType.HandBackS;
                uHandBackOrder[0] = OrderType.UHandBackS;
                handTransOrder[0] = OrderType.HandTransS;
                uHandTransOrder[0] = OrderType.UHandTransS;
                handKind[0] = KindType.HandS;
                handBackKind[0] = KindType.HandBackS;
                isHandKind[0] = true;
                break;
            case KindType.Outer:
                handOrder[1] = OrderType.HandO;
                uHandOrder[1] = OrderType.UHandO;
                handBackOrder[1] = OrderType.HandBackO;
                uHandBackOrder[1] = OrderType.UHandBackO;
                handTransOrder[1] = OrderType.HandTransO;
                uHandTransOrder[1] = OrderType.UHandTransO;
                handKind[1] = KindType.HandO;
                handBackKind[1] = KindType.HandBackO;
                isHandKind[1] = true;
                break;
            case KindType.Dress:
                handOrder[2] = OrderType.HandD;
                uHandOrder[2] = OrderType.UHandD;
                handBackOrder[2] = OrderType.HandBackD;
                uHandBackOrder[2] = OrderType.UHandBackD;
                handTransOrder[2] = OrderType.HandTransD;
                uHandTransOrder[2] = OrderType.UHandTransD;
                handKind[2] = KindType.HandD;
                handBackKind[2] = KindType.HandBackD;
                isHandKind[2] = true;
                break;
        }
    }

    public static void InitHandMatch()
    {
        handOrder[0] = OrderType.HandS;
        uHandOrder[0] = OrderType.UHandS;
        handBackOrder[0] = OrderType.HandBackS;
        uHandBackOrder[0] = OrderType.UHandBackS;
        handTransOrder[0] = OrderType.HandTransS;
        uHandTransOrder[0] = OrderType.UHandTransS;
        handKind[0] = KindType.HandS;
        handBackKind[0] = KindType.HandBackS;
        handOrder[1] = OrderType.HandO;
        uHandOrder[1] = OrderType.UHandO;
        handBackOrder[1] = OrderType.HandBackO;
        uHandBackOrder[1] = OrderType.UHandBackO;
        handTransOrder[1] = OrderType.HandTransO;
        uHandTransOrder[1] = OrderType.UHandTransO;
        handKind[1] = KindType.HandO;
        handBackKind[1] = KindType.HandBackO;
        handOrder[2] = OrderType.HandD;
        uHandOrder[2] = OrderType.UHandD;
        handBackOrder[2] = OrderType.HandBackD;
        uHandBackOrder[2] = OrderType.UHandBackD;
        handTransOrder[2] = OrderType.HandTransD;
        uHandTransOrder[2] = OrderType.UHandTransD;
        handKind[2] = KindType.HandD;
        handBackKind[2] = KindType.HandBackD;
    }
    public static void RemoveIsHand(KindType parentKindType)
    {
        switch (parentKindType)
        {
            case KindType.Shirts:
                isHandKind[0] = true;
                break;
            case KindType.Outer:
                isHandKind[1] = false;
                break;
            case KindType.Dress:
                isHandKind[2] = false;
                break;
        }
    }
}
