﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.SimpleLocalization;
using UnityEngine;
using UnityEngine.Events;

public class Mission
{
    public int missionSeq;
    public bool isOn;
    public bool isClear;
    public int nowLevel;
    public int lastLevel;
    public string rewardType;
    public string reward;
    public float rate;
    public string description;
    public delegate float MissionContents();
    public MissionContents missionContents;

    public Mission(int missionSeq, bool isOn, bool isClear, int nowLevel, int allSeed, string rewardType, string reward)
    {
        this.rate = 0;
        this.missionSeq = missionSeq;
        this.isOn = isOn;
        this.isClear = isClear;
        this.nowLevel = nowLevel;
        this.lastLevel = allSeed;
        this.rewardType = rewardType;
        this.reward = reward;
        if (isClear)
        {
            this.rate = 1;
        }
        if (!isOn)
        {
            this.rate = 1;
        }
        this.description = null;
        this.missionContents = null;
    }
    public void SetDescription(string des)
    {
        this.description = des;
    }

    public void SetMissionContents(MissionContents content)
    {
        this.missionContents = content;
    }

    public void OffMission()
    {
        this.isOn = false;
        XMLManager.Instance.SaveMissionIsOn(missionSeq.ToString(), isOn);
    }

    public void CheckMissionContents()
    {
        this.rate = missionContents();

        if (this.rate == 1f)
        {
            this.isClear = true;
        }
        else
        {
            this.isClear = false;
        }
        XMLManager.Instance.SaveMissionIsClear(missionSeq.ToString(), isClear);
    }
}

public class MissionLevel
{
    public List<string> des = new List<string>();
    public List<int> count = new List<int>();
    public List<string> item = new List<string>();
}

public class MissionSeed : MonoBehaviour
{
    public static Dictionary<int, Mission> missionDic;
    public static Dictionary<int, MissionLevel> missionLevelDic = new Dictionary<int, MissionLevel>();

    public static void Init()
    {
        InitLevel();
        InitSeed();
    }

    public static void InitLevel()
    {
        MissionLevel missionLevel = new MissionLevel();
        missionLevelDic.Clear();

        string[] tmpDes = new string[50];
        int[] tmpCount;
        string[] tmpItem;

        if (missionLevelDic.ContainsKey(0))
        {
            return;
        }

        //0
        tmpDes[0] = (LocalizationManager.Localize(StringInfo.Mission[0]));
        missionLevel.des = new List<string>(tmpDes);
        missionLevelDic.Add(0, missionLevel);

        //1
        missionLevel = new MissionLevel();
        tmpCount = new int[] { 1, 5, 10, 20, 50 };
        for (int i = 0; i < 5; i++)
        {
            tmpDes[i] = string.Format(LocalizationManager.Localize(StringInfo.Mission[1]), tmpCount[i]);
        }
        missionLevel.count = new List<int>(tmpCount);
        missionLevel.des = new List<string>(tmpDes);
        missionLevelDic.Add(1, missionLevel);

        //2
        missionLevel = new MissionLevel();
        tmpCount = new int[] { (int)KindType.Shoes, (int)KindType.Glasses, (int)KindType.Shirts };

        tmpDes[0] = string.Format(LocalizationManager.Localize(StringInfo.Mission[2]), LocalizationManager.Localize(StringInfo.M2ItemsSt[0]));
        tmpDes[1] = string.Format(LocalizationManager.Localize(StringInfo.Mission[2]), LocalizationManager.Localize(StringInfo.M2ItemsSt[1]));
        tmpDes[2] = string.Format(LocalizationManager.Localize(StringInfo.Mission[2]), LocalizationManager.Localize(StringInfo.M2ItemsSt[2]));
        missionLevel.des = new List<string>(tmpDes);
        missionLevel.item = new List<string>(StringInfo.M2Items);
        missionLevel.count = new List<int>(tmpCount);
        missionLevelDic.Add(2, missionLevel);

        //3
        missionLevel = new MissionLevel();
        tmpCount = new int[] { 1, 3, 5, 10, 20, 30, 50, 100 };
        for (int i = 0; i < 8; i++)
        {
            tmpDes[i] = string.Format(LocalizationManager.Localize(StringInfo.Mission[3]), tmpCount[i]);
        }
        missionLevel.count = new List<int>(tmpCount);
        missionLevel.des = new List<string>(tmpDes);
        missionLevelDic.Add(3, missionLevel);

        //4
        missionLevel = new MissionLevel();
        tmpCount = new int[] { 1, 3, 5, 10, 20, 30, 50, 100 };
        for (int i = 0; i < 8; i++)
        {
            tmpDes[i] = string.Format(LocalizationManager.Localize(StringInfo.Mission[4]), tmpCount[i]);
        }
        missionLevel.count = new List<int>(tmpCount);
        missionLevel.des = new List<string>(tmpDes);
        missionLevelDic.Add(4, missionLevel);

        //5
        missionLevel = new MissionLevel();
        tmpCount = new int[] { 5, 10, 15, 20, 30, 50 };
        for (int i = 0; i < 6; i++)
        {
            tmpDes[i] = string.Format(LocalizationManager.Localize(StringInfo.Mission[5]), tmpCount[i]);
        }
        missionLevel.count = new List<int>(tmpCount);
        missionLevel.des = new List<string>(tmpDes);
        missionLevelDic.Add(5, missionLevel);

        //6
        missionLevel = new MissionLevel();
        tmpCount = new int[] { 1, 3, 5, 10, 20, 30 };
        for (int i = 0; i < 6; i++)
        {
            tmpDes[i] = string.Format(LocalizationManager.Localize(StringInfo.Mission[6]), tmpCount[i]);
        }
        missionLevel.count = new List<int>(tmpCount);
        missionLevel.des = new List<string>(tmpDes);
        missionLevelDic.Add(6, missionLevel);

        //7
        missionLevel = new MissionLevel();
        tmpCount = new int[] { 1000, 2000, 3000 };
        for (int i = 0; i < 3; i++)
        {
            tmpDes[i] = string.Format(LocalizationManager.Localize(StringInfo.Mission[7]), tmpCount[i]);
        }
        missionLevel.count = new List<int>(tmpCount);
        missionLevel.des = new List<string>(tmpDes);
        missionLevelDic.Add(7, missionLevel);

        //8
        missionLevel = new MissionLevel();
        tmpDes[0] = LocalizationManager.Localize(StringInfo.Mission[8]);
        missionLevel.des = new List<string>(tmpDes);
        missionLevelDic.Add(8, missionLevel);

    }
    public static void InitSeed()
    {
        missionDic = XMLManager.Instance.LoadAllMission();

        for(int i = 0; i < missionDic.Count; i++)
        {
            SetMission(missionDic[i].missionSeq);
        }
    }

    public static void SetMission(int seq)
    {
        SetDesEach(seq);
        SetConEach(seq);
    }

    public static void SetDesEach(int seq)
    {
        missionDic[seq].SetDescription(missionLevelDic[seq].des[missionDic[seq].nowLevel]);
    }
    public static void SetConEach(int seq)
    {
        switch (seq)
        {
            case 0:
                missionDic[seq].SetMissionContents(IsFirstStart);
                break;
            case 1:
                missionDic[seq].SetMissionContents(() => IsWheel(missionLevelDic[1].count[missionDic[1].nowLevel]));
                break;
            case 2:
                missionDic[seq].SetMissionContents(() => IsCharSaveWithKind((KindType)missionLevelDic[2].count[missionDic[2].nowLevel]));
                break;
            case 3:
                missionDic[seq].SetMissionContents(() => IsSaveSlot(missionLevelDic[3].count[missionDic[3].nowLevel]));
                break;
            case 4:
                missionDic[seq].SetMissionContents(() => IsShowAD(missionLevelDic[4].count[missionDic[4].nowLevel]));
                break;
            case 5:
                missionDic[seq].SetMissionContents(() => IsGetMissionReward(missionLevelDic[5].count[missionDic[5].nowLevel]));
                break;
            case 6:
                missionDic[seq].SetMissionContents(() => IsPerchase(missionLevelDic[6].count[missionDic[6].nowLevel]));
                break;
            case 7:
                missionDic[seq].SetMissionContents(() => IsCoin(missionLevelDic[7].count[missionDic[7].nowLevel]));
                break;
            case 8:
                missionDic[seq].SetMissionContents(IsEvaluation);
                break;
        }
    }

    public static Mission LevelUpMission(Mission prevMission)
    {
        int prevLevel = XMLManager.Instance.LoadNowLevel(prevMission.missionSeq.ToString());
        int lastLevel  = XMLManager.Instance.LoadLastLevel(prevMission.missionSeq.ToString());
        if(prevLevel == lastLevel)
        {
            prevMission.OffMission();
            XMLManager.Instance.SaveMissionIsOn(prevMission.missionSeq.ToString(), false);
        }
        else
        {
            XMLManager.Instance.SaveMissionNowLevel(prevMission.missionSeq.ToString(), prevLevel + 1);
            missionDic[prevMission.missionSeq] = XMLManager.Instance.LoadOneMission(prevMission.missionSeq.ToString());

            SetMission(prevMission.missionSeq);
        }
        return prevMission;
    }

    //하나라도 클리어&&보상받지 않은 상태라면 true
    public static bool CanGetMissionReward()
    {
        for (int i = 0; i < missionDic.Count; i++)
        {
            if (missionDic[i].isClear && missionDic[i].isOn)
            {
                return true;
            }
        }
        return false;
    }

    //전체 미션 클리어 여부
    public static void CheckAllMission()
    {
        for (int i = 0; i < missionDic.Count; i++)
        {
            if (missionDic[i].isOn)
            {
                missionDic[i].CheckMissionContents();
            }
        }
        if (InGameManager.Instance.rewardButtonUI != null)
        {
            InGameManager.Instance.rewardButtonUI.SetMissionCheck();
            InGameManager.Instance.rewardButtonUI.SetCheckImg3();
        }
        if (InGameManager.Instance.missionView != null)
        {
            InGameManager.Instance.missionView.Init();
        }
    }

    //미션 클리어
    public static void CheckMission(int i)
    {
        if (missionDic[i].isOn)
        {
            missionDic[i].CheckMissionContents();
            InGameManager.Instance.rewardButtonUI.SetMissionCheck();
            InGameManager.Instance.rewardButtonUI.SetCheckImg3();
            InGameManager.Instance.missionView.Init();
        }
    }

    #region 미션 내용

    //0
    private static float IsFirstStart()
    {
        return 1;
    }
    //1
    static float IsWheel(float fullTimes)
    {
        float tmp = XMLManager.Instance.LoadMissionValue(XMLManager.Instance.nodeWheelTimes) / fullTimes;
        if (tmp >= 1)
        {
            return 1;
        }
        return tmp;
    }

    //2
    static float IsCharSaveWithKind(KindType kind)
    {
        if ((int)XMLManager.Instance.LoadMissionValue(XMLManager.Instance.nodeSaveKind) == (int)kind)
        {
            return 1;
        }
        return 0;
    }

    //3
    static float IsSaveSlot(float fullTimes)
    {
        float tmp = XMLManager.Instance.LoadMissionValue(XMLManager.Instance.nodeSaveSlotTimes) / fullTimes;
        if (tmp>=1) { 
            return 1;
        }
        return tmp;
    }

    //4
    static float IsShowAD(float fullTimes)
    {
        float tmp = XMLManager.Instance.LoadMissionValue(XMLManager.Instance.nodeShowADTimes) / fullTimes;
        if (tmp>=1)
        {
            return 1;
        }
        return tmp;
    }

    //5
    static float IsGetMissionReward(float fullTimes)
    {
        float tmp = XMLManager.Instance.LoadMissionValue(XMLManager.Instance.nodeGetMissionRWTimes) / fullTimes;
        if (tmp>=1)
        {
            return 1;
        }
        return tmp;
    }

    //6
    static float IsPerchase(float fullTimes)
    {
        float tmp = XMLManager.Instance.LoadMissionValue(XMLManager.Instance.nodePerchaseTimes) / fullTimes;
        if (tmp>=1)
        {
            return 1;
        }
        return tmp;
    }

    //7
    static float IsCoin(float fullCoins)
    {
        float tmp = XMLManager.Instance.LoadUserCoins("0") / fullCoins;
        if (tmp >= 1f)
        {
            return 1;
        }
        return tmp;
    }

    //8
    private static float IsEvaluation()
    {
        if (XMLManager.Instance.LoadMissionValue(XMLManager.Instance.nodeEvaluation) != 0)
        {
            return 1;
        }
        return 0;
    }
    #endregion
}
