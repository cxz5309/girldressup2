﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class CharPartItem
{
    public string seq;
    public CharPartOrder order;
    public CharPartKind kind;
    public int kindInt;
    public int orderInt;

    //public Sprite sp;
    public int spNum;
    public string spriteName;

    public CharPartItem(string seq, int spNum, int charPartKindNum, int charPartOrderNum) {
        this.seq = seq;
        this.spNum = spNum;
        this.order = new CharPartOrder(charPartOrderNum);
        this.kind = new CharPartKind(charPartKindNum);
        this.kindInt = (int)kind.kindType;
        this.orderInt = (int)order.orderType;

        StringBuilder stringBuilder = new StringBuilder();
        spriteName = stringBuilder.Append(CharPartOrder.IntToString(charPartOrderNum)).Append("_").Append(spNum.ToString()).ToString();
    }


    //private void InitItemType()
    //{
    //    switch (catPartsKind.catPartsKindType)
    //    {
    //        case CatPartsKind.CatPartsKindType.Cat:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.CatHead);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Foot);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.CatUpper);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.CatLower);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.CatTail);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Eye:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Eye);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Eyebrow:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Eyebrow);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Mouth:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Mouth);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Cheek:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Cheek);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Top:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Top1);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Top2);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Bottom:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Bottom);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Outer:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Outer1);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Outer2);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Outer3);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Hat:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Hat1);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Hat2);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Shoes:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Shoes);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Gloves:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Gloves);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Tailac:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Tailac1);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Tailac2);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Glasses:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Glasses);
    //            break;
    //        case CatPartsKind.CatPartsKindType.EarLeft:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Earleft1);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Earleft2);
    //            break;
    //        case CatPartsKind.CatPartsKindType.EarRight:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Earright1);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Earright2);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Necklace:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Necklace1);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Necklace2);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Face:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Face1);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Face2);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Bodyac:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Bodyac1);
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Bodyac2);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Text:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Text);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Balloon:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Balloon);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Front:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Front);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Rear:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Rear);
    //            break;
    //        case CatPartsKind.CatPartsKindType.Bg:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.Bg);
    //            break;
    //        case CatPartsKind.CatPartsKindType.OverlayText:
    //            listCatPartsSubItemType.Add(CatPartsSubItemType.OverlayText);
    //            break;
    //    }
    //}
}
