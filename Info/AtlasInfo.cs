﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.U2D;

public class AtlasInfo : MonoBehaviour
{
    public static SpriteAtlas[] atlases;

    public static string AllPartsAtlas = "Parts1920";
    public static string AllObject1Atlas = "Object1";
    public static string AllObject2Atlas = "Object2";
    public static string AllAnimalAtlas = "Animal";
    public static string AllTextAtlas = "Text";
    public static string AllBubbleAtlas = "TextBox";
    public static string AllBGAtlas = "Background";
    public static string AllUIAtlas = "AllUIAtlas";
    public static string AllCollectionAtlas = "AllCollectionAtlas";

    public static Sprite tmpSprite;
    static string tmpFolderName;
    static string tmpHand;
    public static StringBuilder stringBuilder = new StringBuilder();

    public static Sprite GetAtlasNotNumbering(string atlasName, string spriteName)
    {
        switch (atlasName)
        {
            default:
                return null;
            case "Parts1920":
                tmpFolderName = spriteName.Split('_')[0];
                tmpHand = tmpFolderName.Substring(tmpFolderName.Length - 1);
                if (tmpHand == "D" || tmpHand == "O" || tmpHand == "S")
                {
                    tmpFolderName = tmpFolderName.Substring(0, tmpFolderName.Length - 1);
                }
                stringBuilder.Clear();
                stringBuilder.Append("Sprites/").Append(atlasName).Append("/").Append(tmpFolderName).Append("/").Append(spriteName);
                tmpSprite = Resources.Load<Sprite>(stringBuilder.ToString());
                if (tmpSprite != null) return tmpSprite;
                break;
            case "Object1":
                stringBuilder.Clear();
                stringBuilder.Append("Sprites/").Append(atlasName).Append("/").Append(spriteName);
                tmpSprite = Resources.Load<Sprite>(stringBuilder.ToString());
                if (tmpSprite != null) return tmpSprite;
                break;
            case "Object2":
                stringBuilder.Clear();
                stringBuilder.Append("Sprites/").Append(atlasName).Append("/").Append(spriteName);
                tmpSprite = Resources.Load<Sprite>(stringBuilder.ToString());
                if (tmpSprite != null) return tmpSprite;
                break;
            case "Animal":
                stringBuilder.Clear();
                stringBuilder.Append("Sprites/").Append(atlasName).Append("/").Append(spriteName);
                tmpSprite = Resources.Load<Sprite>(stringBuilder.ToString());
                if (tmpSprite != null) return tmpSprite;
                break;
            case "Text":
                stringBuilder.Clear();
                stringBuilder.Append("Sprites/").Append(atlasName).Append("/").Append(spriteName);
                tmpSprite = Resources.Load<Sprite>(stringBuilder.ToString());
                if (tmpSprite != null) return tmpSprite;
                break;
            case "TextBox":
                stringBuilder.Clear();
                stringBuilder.Append("Sprites/").Append(atlasName).Append("/").Append(spriteName);
                tmpSprite = Resources.Load<Sprite>(stringBuilder.ToString());
                if (tmpSprite != null) return tmpSprite;
                break;
            case "Background":
                stringBuilder.Clear();
                stringBuilder.Append("Sprites/").Append(atlasName).Append("/").Append(spriteName);
                tmpSprite = Resources.Load<Sprite>(stringBuilder.ToString());
                if (tmpSprite != null) return tmpSprite;
                break;
            case "AllUIAtlas":
                tmpSprite = atlases[0].GetSprite(spriteName);
                if (tmpSprite != null) return tmpSprite;
                tmpSprite = atlases[1].GetSprite(spriteName);
                if (tmpSprite != null) return tmpSprite;
                break;
            case "AllCollectionAtlas":
                tmpSprite = atlases[2].GetSprite(spriteName);
                if (tmpSprite != null) return tmpSprite;
                break;
        }
        return null;
    }
}
