﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class Bundle
{
    public int bundleSeq;
    public int coin;
    public int cash;
    public string productId;
    public bool rmWaterMark;
    public bool rmAD;

    public List<UserHave> listBundleItem = new List<UserHave>();
    public Bundle(int bundleSeq, int coin, int cash, string productId, List<UserHave> listUserHave, bool _rmWaterMark, bool _rmAD)
    {
        this.bundleSeq = bundleSeq;
        this.coin = coin;
        this.cash = cash;
        this.productId = productId;
        this.listBundleItem = listUserHave;
        this.rmWaterMark = _rmWaterMark;
        this.rmAD = _rmAD;
    }
}

public class StoreView : MonoBehaviour
{
    public GameObject goStorePanel;
    public Transform contents;
    public GameObject prefabBundleButton;

    public Image coinStoreImg;
    public Text coinStoreTxt;
    public Image nomalStoreImg;
    public Text nomalStoreTxt;
    public ScrollRect scrollRect;

    public AlertView alertView;
    public SAlertViewUI sAlertView;

    public bool isCoinStore;

    private void OnEnable()
    {
        DoTweenUtil.DoPopupOpen(.2f, 1f, .4f, transform);

        isCoinStore = true;
        InitItem();
        InitStoreUI(isCoinStore);
        scrollRect.verticalNormalizedPosition = 1;
    }

    public void DisMiss()
    {
        if (!DoTweenUtil.isTweening)
        {
            goStorePanel.SetActive(false);
        }
    }
    
    public void InitStoreUI(bool isCoinStore)
    {
        if (LocalizationManager.Language == "Japanese")
        {
            coinStoreTxt.fontSize = 40;
            nomalStoreTxt.fontSize = 40;
        }
        else
        {
            coinStoreTxt.fontSize = 48;
            nomalStoreTxt.fontSize = 48;
        }

        if (isCoinStore)
        {
            coinStoreImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.shop_tab_shop_p);
            coinStoreTxt.color = Color.white;

            nomalStoreImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.shop_tab_shop_n);
            //nomalStoreTxt.color = Color.gray;
        }
        else
        {
            nomalStoreImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.shop_tab_shop_p);
            nomalStoreTxt.color = Color.white;

            coinStoreImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.shop_tab_shop_n);
            //coinStoreTxt.color = Color.gray;
        }
    }

    //번들을 생성하기 위해 xml읽기
    public void InitItem()
    {
        for(int i = 0; i < contents.childCount; i++)
        {
            Destroy(contents.GetChild(i).gameObject);
        }
        //이미 구매한 아이템 뒤로 빼놓고 구별
        Queue<GameObject> queue = new Queue<GameObject>();
        for (int i = 0; i < XMLManager.Instance.SelectAllBundle().Count; i++)
        {
            List<UserHave> tmpList = XMLManager.Instance.LoadAllBundleItem(i.ToString());
            bool tmpBoolWatermark = XMLManager.Instance.LoadBundleRmWaterMark(i.ToString());
            bool tmpBoolAD = XMLManager.Instance.LoadBundleRmAd(i.ToString());
            int tmpCoin = XMLManager.Instance.LoadBundleCoin(i.ToString());
            int tmpCash = XMLManager.Instance.LoadBundleCash(i.ToString());
            string tmpProductId = XMLManager.Instance.LoadBundleProductId(i.ToString());

            //유저가 가진 번들아이템은 뒤로 빼놓기
            List<int> userHaveBundleList = XMLManager.Instance.LoadStoreBundleList();
            userHaveBundleList.Sort();

            GameObject bundleButton = CreateBundle(i, tmpCoin, tmpCash, tmpProductId, tmpList, tmpBoolWatermark, tmpBoolAD);
            if (IAPManager.Instance.HadPurchased(tmpProductId))
            {
                // 인앱 구매 내역 있음
                queue.Enqueue(bundleButton);
            }
            else
            {
                // 골드 구매 내역 있음
                for (int j = 0; j < userHaveBundleList.Count; j++)
                {
                    if (userHaveBundleList[j] == i)
                    {
                        queue.Enqueue(bundleButton);
                    }
                }
            }
            bundleButton.GetComponent<StoreBundleButtonUI>().deemObj.SetActive(false);
        }
        while (queue.Count > 0)
        {
            GameObject haveBundleButton = queue.Dequeue();
            haveBundleButton.transform.SetAsLastSibling();
            haveBundleButton.GetComponent<StoreBundleButtonUI>().deemObj.SetActive(true);
            haveBundleButton.GetComponent<Button>().interactable = false;
        }
    }

    //번들 생성
    public GameObject CreateBundle(int i, int coin, int cash, string productId, List<UserHave> userHavelist, bool isRMWaterMark, bool isRM_AD)
    {
        Bundle bundle = new Bundle(i, coin, cash, productId, userHavelist, isRMWaterMark, isRM_AD);

        GameObject bundleButton = Instantiate(prefabBundleButton, contents);
        StoreBundleButtonUI storeBundleButtonUI = bundleButton.GetComponent<StoreBundleButtonUI>();
        storeBundleButtonUI.SetListBundleItem(bundle);
        storeBundleButtonUI.alertView = alertView;
        storeBundleButtonUI.sAlertView = sAlertView;
        storeBundleButtonUI.storeView = this;
        storeBundleButtonUI.isCoinStore = isCoinStore;
        return bundleButton;
    }

    public void OnCoinStoreClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            if (!isCoinStore)
            {
                scrollRect.verticalNormalizedPosition = 1;
                isCoinStore = true;
                InitItem();
                InitStoreUI(isCoinStore);
            }
        }
    }

    public void OnNomalStoreClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            if (isCoinStore)
            {
                scrollRect.verticalNormalizedPosition = 1;
                isCoinStore = false;
                InitItem();
                InitStoreUI(isCoinStore);
            }
        }
    }
}
