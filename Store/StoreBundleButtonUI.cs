using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.SimpleLocalization;
using UnityEngine.Purchasing;

public class StoreBundleButtonUI : MonoBehaviour
{
    public StoreView storeView;
    public AlertView alertView;
    public SAlertViewUI sAlertView;
    public Text sideContextText;
    public Text bottomContextText;
    public Text priceText;
    public Text goldText;
    public GameObject deemObj;

    public StringBuilder stringBuilder = new StringBuilder();
    public Bundle bundle;

    public bool isCoinStore;

    public void SetListBundleItem(Bundle _bundle)
    {
        bundle = _bundle;
    }

    private void Start()
    {
        InitBundleButton();
        if(LocalizationManager.Language == "Japanese" || LocalizationManager.Language == "English")
        {
            sideContextText.fontSize = 40;
            bottomContextText.fontSize = 40;
        }
        else
        {
            sideContextText.fontSize = 48;
            bottomContextText.fontSize = 48;
        }
    }

    public void InitBundleButton()
    {
        string bundleButtonName = StringInfo.Bundle_Button_Name[bundle.bundleSeq];
        GetComponent<Image>().sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, bundleButtonName);
        switch (bundle.bundleSeq)
        {
            default:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                bottomContextText.gameObject.SetActive(true);
                sideContextText.gameObject.SetActive(false);
                bottomContextText.text = LocalizationManager.Localize(StringInfo.Bundle_Name[bundle.bundleSeq]);
                break;
            case 0:
            case 1:
                bottomContextText.gameObject.SetActive(false);
                sideContextText.gameObject.SetActive(true);
                sideContextText.text = LocalizationManager.Localize(StringInfo.Bundle_Name[bundle.bundleSeq]);
                break;
        }
        if (isCoinStore)
        {
            //코인일 경우 1000Gold
            //priceText.text = StringInfo.StoreGold;
            //goldText.text = bundle.coin.ToString();
            priceText.text = string.Format("{0} {1}", bundle.coin.ToString(), StringInfo.StoreGold);
        }
        else
        {
            //현금일 경우 ₩ 1000
            Product product = IAPManager.Instance.GetProduct(bundle.productId);
            if (product != null)
            {
                priceText.text = product.metadata.localizedPriceString;
            }
            else
            {
                priceText.text = null;
            }
            //goldText.text = LocalizationManager.Localize(StringInfo.Cash);
        }
    }

    public void OnBundleButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            if (isCoinStore)
            {
                alertView.Alert(LocalizationManager.Localize(StringInfo.ConfInfo), new AlertViewOptions
                {
                    cancelButtonDelegate = () =>
                    {
                    },
                    okButtonDelegate = () =>
                    {
                        Purchase();
                    }
                });
            }
            else
            {
                Purchase();
            }
        }
    }

    public void Purchase()
    {
        // 혹시라도 이미 구매한 물품일경우?
        for (int i = 0; i < XMLManager.Instance.LoadStoreBundleList().Count; i++)
        {
            if (XMLManager.Instance.LoadStoreBundleList()[i] == bundle.bundleSeq)
            {
                //sAlertView.SAlert(StringInfo.AlHaveItemInfo);
                return;
            }
        }

        if (isCoinStore)
        {
            bool lackOfBalance = ChargeCoin(bundle.coin);
            if (lackOfBalance)
            {
                sAlertView.SAlert(LocalizationManager.Localize(StringInfo.NHaveCoinInfo));
                return;
            }
            Purchased();
        }
        else
        {
            ChargeCash();
        }
    }

    private void Purchased()
    {
        //워터마크 삭제
        bool thisRMWaterMark = bundle.rmWaterMark;
        if (thisRMWaterMark == true)
        {
            XMLManager.Instance.SaveUserHave_RmWaterMark(thisRMWaterMark);
        }

        //상단광고 삭제
        bool thisRM_AD = bundle.rmAD;
        if (thisRM_AD == true)
        {
            XMLManager.Instance.SaveUserHave_RmAD(thisRM_AD);
            AdsManager.Instance.RefreshBanner();
        }

        //상점미션 수치 상승
        XMLManager.Instance.SaveMissionValueUp(XMLManager.Instance.nodePerchaseTimes);
        for (int i = 0; i < bundle.listBundleItem.Count; i++)
        {
            string seqName = bundle.listBundleItem[i].seqName;
            string itemSeq = bundle.listBundleItem[i].itemSeq;

            if (!XMLManager.Instance.CheckIsHave(seqName, itemSeq))
            {
                //이 안에서 미션 인잇
                InGameManager.Instance.AddItem(seqName, itemSeq, ItemGetRoot.store);
            }
        }
        AudioManager.Inst.PlaySFX("clearMission");

        //화면 초기화
        InGameManager.Instance.InitItemGet();
        if (isCoinStore)
        {
            XMLManager.Instance.SaveStoreBundleOne(bundle.bundleSeq.ToString());
        }
        storeView.InitItem();
        storeView.InitStoreUI(isCoinStore);
    }

    /// <summary>
    /// 골드 구매
    /// </summary>
    /// <param name="coins"></param>
    /// <returns></returns>
    public bool ChargeCoin(int coins)
    {
        if (XMLManager.Instance.LoadUserCoins("0") < coins)
        {
            return true;
        }
        InGameManager.Instance.ReduceCoins(coins);
        return false;
    }

    /// <summary>
    /// 인앱 결제
    /// </summary>
    public void ChargeCash()
    {
        IAPManager.Instance.BuyProductID(bundle.productId,
            (product) =>
            {
                Purchased();
            },
            () =>
            {

            }
        );
    }
}
