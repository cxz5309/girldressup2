﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetUIAnimator : MonoBehaviour
{
    Animator animator;
    public string aniTriggerName;
    public float aniCooldown;
    public bool notAni;
    public bool loopTrigger;

    private void OnEnable()
    {
        animator = GetComponent<Animator>();

        StartCoroutine("CoAnimation");
    }

    IEnumerator CoAnimation()
    {
        while (!notAni)
        {
            yield return new WaitForSeconds(aniCooldown);
            animator.SetTrigger(aniTriggerName);
            if (!loopTrigger)
            {
                break;
            }
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
