﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScaleUtil : MonoBehaviour
{
    public Friend friend;
    public Transform friendRoot;
    public Tools goTools;

    public SpriteRenderer spriteRenderer;
    public Sprite sp_p;
    public Sprite sp_n;

    private float charStartScale;
    private Vector2 prevScalePosition;
    private Vector2 nowScalePosition;
    private Vector2 zeroScalePosition;
    private float ratio;
    private float newRatio=1;

    public static float SCALE_MIN = .6f;
    public static float SCALE_MAX = 1.6f;
    public static float TEXT_SCALE_MIN = .8f;
    public static float TEXT_SCALE_MAX = 3f;

    Vector2 startPosition;

    void OnMouseDown()
    {

        OnScaleClickDown();
    }

    void OnMouseDrag()
    {
        OnScaleDrag();
    }

    void OnMouseUp()
    {
        OnScaleClickUp();
        //goTools.transform.SetParent(friendRoot.parent);
        //goTools.SetToolTrTop(friend.transform.localPosition);
    }


    #region 스케일Tool
    public void OnScaleClickDown()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        if (results.Count > 0)
        {
            return;
        }
        spriteRenderer.sprite = sp_p;

        Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        startPosition = InMainManager.Instance.mainCamera.ScreenToWorldPoint(mousePosition);

        charStartScale = friendRoot.transform.localScale.x;
        //InLobbyManager.Instance.touchActing = InLobbyManager.TouchActing.isScaleChanging;

        zeroScalePosition = friendRoot.transform.position;
        prevScalePosition = transform.position;
        nowScalePosition = startPosition;
    }
    public void OnScaleDrag()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        
        if (results.Count > 0)
        {
            return;
        }
        Vector2 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
        nowScalePosition = InMainManager.Instance.mainCamera.ScreenToWorldPoint(mousePosition);

        Vector2 ScaleFactor = prevScalePosition - zeroScalePosition;
        Vector2 ScalePosition = nowScalePosition - zeroScalePosition;

        float scaleMin;
        float scaleMax;
        switch (friend.friendType)
        {
            default:
                scaleMin = SCALE_MIN;
                scaleMax = SCALE_MAX;
                break;
            case FriendType.Text:
                scaleMin = TEXT_SCALE_MIN;
                scaleMax = TEXT_SCALE_MAX;
                break;
        }

        if (ScalePosition.x < 0 && ScalePosition.y < 0)
        {
            return;
        }
        else
        {
            ratio = ScalePosition.magnitude / ScaleFactor.magnitude;
            Vector3 newLocalScale = new Vector3(charStartScale * ratio, charStartScale * ratio, 1);

            if (friendRoot.transform.localScale.x < scaleMax && friendRoot.transform.localScale.x > scaleMin)
            {
                if (newLocalScale.x > scaleMin && newLocalScale.x < scaleMax)
                {
                    friendRoot.localScale = newLocalScale;
                }
                else if (newLocalScale.x <= scaleMin)
                {
                    Vector3 scaleMinVec = new Vector3(scaleMin, scaleMin, 1);
                    friendRoot.localScale = scaleMinVec;
                }
                else if (newLocalScale.x >= scaleMax)
                {
                    Vector3 scaleMaxVec = new Vector3(scaleMax, scaleMax, 1);
                    friendRoot.localScale = scaleMaxVec;
                }
            }
            else if (friendRoot.transform.localScale.x >= scaleMax)
            {
                if (newRatio < ratio)
                {
                    return;
                }
                else
                {
                    friendRoot.localScale = newLocalScale;
                }
            }
            else if (friendRoot.transform.localScale.x <= scaleMin)
            {
                if (newRatio > ratio)
                {
                    return;
                }
                else
                {
                    friendRoot.localScale = newLocalScale;
                }
            }
            newRatio = ratio;
        }
    }

    public void OnScaleClickUp()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if (Input.GetMouseButtonDown(0))
        {
            if (results.Count > 0)
            {
                return;
            }
        }
        spriteRenderer.sprite = sp_n;

        friend.scale = friendRoot.localScale.x;
        friend.SetTrValueXml("scale", friend.scale.ToString());
        //InGameManager.Instance.trList[friend.friendNum].Scale = friend.transform.localScale.x;
        //InLobbyManager.Instance.touchActing = InLobbyManager.TouchActing.noActing;
    }
    #endregion
}
