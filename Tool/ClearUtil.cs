﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClearUtil : MonoBehaviour
{
    public Friend friend;
    public Transform friendRoot;

    public SpriteRenderer spriteRenderer;
    public Sprite sp_p;
    public Sprite sp_n;

    void OnMouseDown()
    {

        OnClearCharacterClickDown();
    }

    #region 캐릭터 삭제 Tool
    public void OnClearCharacterClickDown()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if (Input.GetMouseButtonDown(0))
        {
            if (results.Count > 0)
            {
                return;
            }
        }
        spriteRenderer.sprite = sp_p;

        GetComponent<SpriteRenderer>().color = Color.gray;

        friend.RemoveThisTr();
        Destroy(friendRoot.gameObject);
    }
    #endregion
}
