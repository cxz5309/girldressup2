﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;

public class SortTopUtil : MonoBehaviour
{
    public Transform friendRoot;
    public Friend friend;
    SortingGroup sorting;
    public SpriteRenderer spriteRenderer;
    public Sprite sp_p;
    public Sprite sp_n;

    void OnMouseDown()
    {
        OnSortingTopClickDown();
    }
    void OnMouseUp()
    {
        OnSortingTopClickUp();
    }

#region 레이어 최상단 Tool
    public void OnSortingTopClickDown()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if (Input.GetMouseButtonDown(0))
        {
            if (results.Count > 0)
            {
                return;
            }
        }
        spriteRenderer.sprite = sp_p;

        //InLobbyManager.Instance.touchActing = InLobbyManager.TouchActing.isSorting;
        SetCharacterSortingOrder(true, friendRoot.gameObject);
    }
    public void OnSortingTopClickUp()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        
        if (results.Count > 0)
        {
            return;
        }
        spriteRenderer.sprite = sp_n;

        friend.sortingOrder = sorting.sortingOrder;
        friend.SetTrValueXml("tr_order", friend.sortingOrder.ToString());
        //InGameManager.Instance.trList[friend.friendNum].SortingOrder = friend.GetComponent<SortingGroup>().sortingOrder;
    }

    public void SetCharacterSortingOrder(bool charTop, GameObject obj = null)
    {
        sorting = friend.GetComponent<SortingGroup>();
        if (charTop)
        {
            InMainManager.Instance.sortingTop++;
            sorting.sortingOrder = InMainManager.Instance.sortingTop;
            friend.sortingOrder = sorting.sortingOrder;
        }
        else
        {
            sorting.sortingOrder = obj.GetComponent<Friend>().sortingOrder;
        }

        //if (InMainManager.Instance.sortingTop < sorting.sortingOrder)
        //{
        //    InMainManager.Instance.sortingTop = sorting.sortingOrder;
        //}
        //순서에 따라 z축 위치 변경하여 콜라이더 올리기
        friendRoot.localPosition = new Vector3(obj.transform.localPosition.x, obj.transform.localPosition.y, sorting.sortingOrder * -0.01f);
        friend.transform.localPosition = Vector3.zero;
    }
    #endregion
}
