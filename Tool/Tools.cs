﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tools : MonoBehaviour
{
    public SortTopUtil sortingTopButton;
    public ScaleUtil scaleButton;
    public ReverseUtil reverseButton;
    public ClearUtil clearButton;

    Friend friend;

    public LineRenderer[] lines = new LineRenderer[8]; 

    Transform CharactorRenObj;
    Transform friendRoot;
    public float resizeX;
    public float resizeY;

    public bool text;
    public Vector3[] resizePos = new Vector3[4];

    public float ratio;
    public static float LERF = 0.7f;//툴 크기 러프
    public static float CORRECTION = 0.7f;//최종 툴 스케일 보정

    private void Start()
    {
        friend = transform.parent.GetComponentInChildren<Friend>();
        friendRoot = friend.transform.parent;
        CharactorRenObj = friend.transform.Find("FriendRender");

        Allocate();
        Resize();
    }

    public void Allocate()
    {
        //if (friend.friendType != Friend.FriendType.Text)
        
            scaleButton.friend = friend;
            reverseButton.friend = friend;
            scaleButton.friendRoot = friendRoot;
            reverseButton.friendRoot = friendRoot;
            reverseButton.friendRenderer = CharactorRenObj;
        
        sortingTopButton.friend = friend;
        clearButton.friend = friend;

        sortingTopButton.friendRoot = friendRoot;
        clearButton.friendRoot = friendRoot;

    }
    

    public void SetToolTrTop(Vector3 friendPos)
    {
        transform.localPosition = friendPos + new Vector3(0, 0, -5);
    }

    public void Resize()
    {
        resizeX = friend.objectRenderSizeX/200;
        resizeY = friend.objectRenderSizeY/200;
        //꼭지점 정해주기
        resizePos[0] = new Vector2(-resizeX, resizeY);
        resizePos[1] = new Vector2(resizeX, resizeY);
        resizePos[2] = new Vector2(resizeX, -resizeY);
        resizePos[3] = new Vector2(-resizeX, -resizeY);
        //라인위치
        for (int i = 0; i < 4; i++)
        {
            lines[i].SetPosition(0, resizePos[(i) % 4]);
            lines[i].SetPosition(1, resizePos[(i + 1) % 4]);
            lines[i+4].SetPosition(0, resizePos[(i) % 4]);
            lines[i+4].SetPosition(1, resizePos[(i + 1) % 4]);
        }
        //점위치
        //if (friend.friendType != Friend.FriendType.Text)
        
            scaleButton.transform.localPosition = resizePos[1];
            reverseButton.transform.localPosition = resizePos[3];
        
        sortingTopButton.transform.localPosition = resizePos[0];
        clearButton.transform.localPosition = resizePos[2];

        //점 크기
        //비율은 화면상 전부 가로비율
        ratio = friend.objectRenderSizeX / Friend.CHAR_REN_SIZE_X;

        //캐릭터 크기를 1로 잡고 그것보다 작은 아이템은 툴 크기도 줄여야 하는데
        //너무 작은 경우 툴 크기가 과하게 작아서 러프하게 비율을 조정
        float newRatio = 1 - ((1 - ratio)* LERF);

        //CORRECTION : 캐릭터 포함 기본 툴 크기 조정
        newRatio *= CORRECTION;

        Vector3 ratioSize = new Vector3(newRatio, newRatio, 1);
        //if (friend.friendType != Friend.FriendType.Text)
        
            scaleButton.transform.localScale = ratioSize;
            reverseButton.transform.localScale = ratioSize;
        
        sortingTopButton.transform.localScale = ratioSize;
        clearButton.transform.localScale = ratioSize;
    }
}
