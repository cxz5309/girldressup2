﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ReverseUtil : MonoBehaviour
{
    public Friend friend;
    public Transform friendRoot;
    public Transform friendRenderer;

    public SpriteRenderer spriteRenderer;
    public Sprite sp_p;
    public Sprite sp_n;

    bool isReversed;

    void OnMouseDown()
    {
        OnReverseClickDown();
    }

    void OnMouseUp()
    {
        OnReverseClickUp();
    }

    #region 좌우반전 Tool
    public void OnReverseClickDown()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if (Input.GetMouseButtonDown(0))
        {
            if (results.Count > 0)
            {
                return;
            }
        }
        spriteRenderer.sprite = sp_p;

        //InLobbyManager.Instance.touchActing = InLobbyManager.TouchActing.isReversing;

        Quaternion newQuaternion = Quaternion.Euler(new Vector3(0, 180, 0));
        friendRenderer.rotation = friendRenderer.rotation * newQuaternion;
    }
    public void OnReverseClickUp()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if (Input.GetMouseButtonDown(0))
        {
            if (results.Count > 0)
            {
                return;
            }
        }
        spriteRenderer.sprite = sp_n;

        friend.isReversed = !friend.isReversed;
        friend.SetTrValueXml("is_reversed", friend.isReversed.ToString());
        //InGameManager.Instance.trList[friend.friendNum].IsReversed = isReversed;

        //InLobbyManager.Instance.touchActing = InLobbyManager.TouchActing.noActing;
    }
    #endregion
}
