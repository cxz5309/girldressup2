﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoreGameView : MonoBehaviour
{ 
    public GameObject goMoreGamePanel;

    private void OnEnable()
    {
        DoTweenUtil.DoPopupOpen(.2f, 1f, .4f, transform);
    }

    public void OnMoreGameButtonClick(string prjectName)
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.ondot." + prjectName);
    }

    public void DisMiss()
    {
        if(!DoTweenUtil.isTweening)
           goMoreGamePanel.SetActive(false);
    }
}
