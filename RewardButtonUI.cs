﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RewardButtonUI : MonoBehaviour
{
    public Image rewardButtonImage;
    public GameObject[] missionButton3 = new GameObject[3];
    public GameObject rewardCheckImg;
    public GameObject[] checkImg3 = new GameObject[3];//왼쪽부터 0:콜렉션, 1:미션, 2:데일리
    public bool[] check3 = new bool[3];
    public GameObject extendBox;

    public CollectionViewUI collectionViewUI;

    //일일보상
    public GameObject DailyPanel;

    //미션
    public GameObject missionPanel;

    //컬렉션
    public GameObject collectionPanel;

    bool isTweening;

    public GameObject currentObj;

    private void OnEnable()
    {
        if (InGameManager.Instance != null)
        {
            InGameManager.Instance.rewardButtonUI = this;
        }
        SetCollectionCheck();
        SetDailyCheck();
        SetMissionCheck();
        SetCheckImg3();
    }

    private void Update()
    {
        if (extendBox.activeSelf)
        {
            if (Input.GetMouseButtonUp(0))
            {
                if (EventSystem.current.currentSelectedGameObject != null)
                {
                    currentObj = EventSystem.current.currentSelectedGameObject;
                    if (!currentObj.CompareTag("RewardButtonGroup") &&
                        !currentObj.CompareTag("DismissButton"))
                    {
                        CloseRewardContainer();
                    }
                }
                else
                {
                    CloseRewardContainer();
                }
            }
        }
    }

    public void SetRewardImg3()
    {
        for(int i = 0; i < 3; i++)
        {
            if (check3[i])
            {
                rewardCheckImg.SetActive(true);
                return;
            }
        }
        rewardCheckImg.SetActive(false);
    }

    public void SetCheckImg3()
    {
        for(int i = 0; i < 3; i++)
        {
            if (check3[i])
            {
                checkImg3[i].SetActive(true);
            }
            else
            {
                checkImg3[i].SetActive(false);
            }
        }
        SetRewardImg3();
    }

    public void SetDailyCheck()
    {
        if (!InGameManager.Instance.getDailyReward)
        {
            check3[2] = true;
        }
        else
        {
            check3[2] = false;
        }
    }

    public void SetMissionCheck()
    {
        if (MissionSeed.CanGetMissionReward())
        {
            check3[1] = true;
        }
        else
        {
            check3[1] = false;
        }
    }

    public void SetCollectionCheck()
    {
        if (CollectionSeed.CheckAndSaveOneIsClear(CollectionSeed.collectionSetsDic[CollectionSeed.nowStage]))
        {
            check3[0] = true;
            return;
        }
        check3[0] = false;
        return;
    }

    public void OnRewardClick()
    {
        if (!isTweening)
        {
            if (!extendBox.activeSelf)
            {
                OpenRewardContainer();
            }
            else
            {
                CloseRewardContainer();
            }
        }
    }

    public void OpenRewardContainer()
    {
        if (!isTweening)
        {
            if (!extendBox.activeSelf)
            {
                isTweening = true;

                AudioManager.Inst.PlaySFX("BookPageFlip2");

                rewardButtonImage.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_mm_10_p");
                extendBox.SetActive(true);

                for (int i = 0; i < 3; i++)
                {
                    missionButton3[i].SetActive(true);
                    missionButton3[i].GetComponent<Image>().DOFade(1, .15f);
                    missionButton3[i].transform.DOLocalMove(missionButton3[i].transform.localPosition + new Vector3(141 * (i + 1), 0), .2f).OnComplete(() =>
                    {
                        isTweening = false;
                    });
                }
            }
        }
    }


    public void CloseRewardContainer()
    {
        if (!isTweening)
        {
            if (extendBox.activeSelf)
            {
                isTweening = false;

                rewardButtonImage.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_mm_10_n");
                extendBox.SetActive(false);

                for (int i = 0; i < 3; i++)
                {
                    int idx = new int();
                    idx = i;
                    missionButton3[i].GetComponent<Image>().DOFade(0, .15f);
                    missionButton3[i].transform.DOLocalMove(missionButton3[i].transform.localPosition - new Vector3(141 * (i + 1), 0), .2f).OnComplete(() =>
                    {
                        missionButton3[idx].SetActive(false);
                        isTweening = false;
                    });
                }
            }
        }
    }
    public void OnDailyButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            DailyPanel.SetActive(true);
        }
    }


    public void OnCollectionButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            collectionPanel.SetActive(true);
        }
    }

    public void OnMissionButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            missionPanel.SetActive(true);
        }
    }

}
