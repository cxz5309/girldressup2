﻿using System;
using UnityEngine;

public class DebugX
{
    public static void Log(string msg)
    {
#if UNITY_EDITOR
        Debug.Log(msg);
#endif
    }
}
