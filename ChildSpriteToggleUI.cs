﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ChildSpriteToggleUI : MonoBehaviour
{
    public List<Image> childImages;
    public List<string> spName;
    public List<Sprite> nomalSp;
    public List<Sprite> selectedSp;

    public Transform selBtn;

    public int prevSel = -1;
    public int nowSel;
    public List<int> exceptSel = new List<int>();
    public string lastWord;

    StringBuilder stringBuilder = new StringBuilder();

    private void Start()
    {
        //Debug.Log("이거먼저");
        prevSel = -1;
        childImages = new List<Image>();
        spName = new List<string>();
        nomalSp = new List<Sprite>();
        selectedSp = new List<Sprite>();
        for (int i = 0; i < transform.childCount; i++)
        {
            childImages.Add(transform.GetChild(i).GetComponent<Image>());


            if (lastWord.Equals("n"))
            {
                spName.Add(childImages[i].sprite.name.Replace("_n", "").Replace("(Clone)", ""));

                stringBuilder.Clear();
                stringBuilder.Append(spName[i]).Append("_n");
                nomalSp.Add(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, (stringBuilder.ToString())));

                stringBuilder.Clear();
                stringBuilder.Append(spName[i]).Append("_p");
                selectedSp.Add(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, (stringBuilder.ToString())));
            }
            else if(lastWord.Equals("off"))
            {
                spName.Add(childImages[i].sprite.name.Replace("_off", "").Replace("(Clone)", ""));

                stringBuilder.Clear();
                stringBuilder.Append(spName[i]).Append("_off");
                nomalSp.Add(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, (stringBuilder.ToString())));

                stringBuilder.Clear();
                stringBuilder.Append(spName[i]).Append("_on");
                selectedSp.Add(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, (stringBuilder.ToString())));
            }
        }
    }

    public void ExceptOne(int exceptNum)
    {
        exceptSel.Add(exceptNum);
    }


    public void SetImageToggle(int idx)
    {
        //Debug.Log("이게나중");

        if (idx == prevSel)
        {
            return;
        }
        nowSel = idx;

        if (!exceptSel.Contains(nowSel))
        {
            childImages[nowSel].sprite = selectedSp[nowSel];
        }
        if (!exceptSel.Contains(prevSel))
        {

            if (prevSel != -1)
            {
                childImages[prevSel].sprite = nomalSp[prevSel];
            }
        }
        prevSel = nowSel;
    }

    public void Refresh()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i) == selBtn)
            {
                childImages[i].sprite = selectedSp[i];
            }
            else
            {
                childImages[i].sprite = nomalSp[i];
            }
        }
    }
}
