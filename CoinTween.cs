﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CoinTween : MonoBehaviour
{
    public GameObject coin;
    public Vector3 startPos;
    public Vector3 target;

    public int coinCount;
    public float endTime;

    Transform coinImage;

    private void Start()
    {
        coinImage = InGameManager.Instance.coinImage;
        target = (Vector2)coinImage.position + new Vector2(0.2f, 0.2f);
        StartCoroutine(CoCreateCoin());
    }

    IEnumerator CoCreateCoin()
    {
        float time = 0;
        while (time < endTime)
        {
            GameObject coinObj1 = Instantiate(coin, startPos + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), transform.position.z), Quaternion.identity, this.transform);
            coinObj1.transform.DOLocalMove (target, .5f).SetEase(Ease.OutQuart);
            coinObj1.GetComponentInChildren<SpriteRenderer>().DOFade(0, 0.5f);

            GameObject coinObj2 = Instantiate(coin, startPos + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), transform.position.z), Quaternion.identity, this.transform);
            coinObj2.transform.DOLocalMove(target, .5f).SetEase(Ease.OutQuart);
            coinObj2.GetComponentInChildren<SpriteRenderer>().DOFade(0, 0.5f);

            GameObject coinObj3 = Instantiate(coin, startPos + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), transform.position.z), Quaternion.identity, this.transform);
            coinObj3.transform.DOLocalMove(target, .5f).SetEase(Ease.OutQuart);
            coinObj3.GetComponentInChildren<SpriteRenderer>().DOFade(0, 0.5f);

            time += endTime / coinCount;
            yield return new WaitForSeconds(endTime / coinCount);
        }
        yield return new WaitForSeconds(0.7f);
        Destroy(gameObject);
    }

}
