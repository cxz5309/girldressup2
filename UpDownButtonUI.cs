﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class UpDownButtonUI : MonoBehaviour
{
    public Transform charTrRoot;
    public Transform waterMark;
    public Image image;

    public bool isOn = true;
    public Transform BottomToolBar;

    public void OnUpDownButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            if (isOn)
            {
                image.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_up_n");

                SpriteState tmpSpriteState = new SpriteState();
                tmpSpriteState.pressedSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_up_p");
                image.GetComponent<Button>().spriteState = tmpSpriteState;

                DoTweenUtil.DoLocalMoveY(BottomToolBar.localPosition.y - BottomToolBar.GetComponent<RectTransform>().rect.height, 0.2f, BottomToolBar);
                isOn = !isOn;
            }
            else
            {
                image.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_down_n");

                SpriteState tmpSpriteState = new SpriteState();
                tmpSpriteState.pressedSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_down_p");
                image.GetComponent<Button>().spriteState = tmpSpriteState;

                DoTweenUtil.DoLocalMoveY(BottomToolBar.localPosition.y + BottomToolBar.GetComponent<RectTransform>().rect.height, 0.2f, BottomToolBar);
                isOn = !isOn;
            }
        }
    }
}
