﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinUpEffect : MonoBehaviour
{
    public GameObject coinObj;
    public int coinCount;
    public float endTime;

    private void OnEnable()
    {
        StartCoroutine("CoCoinCreate");
    }

    IEnumerator CoCoinCreate()
    {
        float time = 0;
        while (time < endTime)
        {
            GameObject thisCoinObj = Instantiate(coinObj, new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), 0), Quaternion.identity, this.transform);
            thisCoinObj.GetComponentInChildren<Animator>().SetTrigger("trigger");
            time += endTime / coinCount;
            yield return new WaitForSeconds(endTime / coinCount);
        }
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        StopCoroutine("CoCoinCreate");
        Destroy(this.gameObject);
    }
}
