﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParseItemSeq : MonoBehaviour
{
    public static string MakeItemSeq(int order, int itemSeq)
    {
        string fir = order.ToString("D3");

        string sec = itemSeq.ToString("D3");
        return fir + sec;
    }
    public static string SplitItemSeq1(string itemSeq)
    {
        string itemSeqSplit1 = itemSeq.Substring(0, 3);
        return itemSeqSplit1;
    }
    public static string SplitItemSeq2(string itemSeq)
    {
        string itemSeqSplit2 = itemSeq.Substring(3, 3);
        return itemSeqSplit2;
    }
    public static string numToD3(int num)
    {
        return num.ToString("D3");
    }
    public static string ItemSeqToStringSpName(string itemSeq)
    {
        string spName;
        int orderNum;
        int spNum;
        orderNum = int.Parse(SplitItemSeq1(itemSeq));
        spNum = int.Parse(SplitItemSeq2(itemSeq));

        spName = CharPartOrder.IntToString(orderNum) + "_" + spNum.ToString();

        return spName;
    }
}
