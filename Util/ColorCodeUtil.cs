﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ColorCodeUtil : MonoBehaviour
{
    public static string ColorToHtmlString(Color color)
    {
        return ColorUtility.ToHtmlStringRGBA(color);
    }

    //string -> Color
    public static Color htmlStringToColor(string input)
    {
        Color color;
        StringBuilder tmpString = new StringBuilder("#");
        tmpString.Append(input);
        ColorUtility.TryParseHtmlString(tmpString.ToString(), out color);
        return color;
    }

    public static Color htmlStringToColorWithSharf(string input)
    {
        Color color;
        ColorUtility.TryParseHtmlString(input.ToString(), out color);
        return color;
    }
}
