﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using UnityEngine;
using UnityEngine.U2D;

public class tmpParser : MonoBehaviour
{
    public SpriteAtlas[] spAt = new SpriteAtlas[3];

    Sprite[] sprite = new Sprite[1000];
    string[] spriteNameSplit = new string[5];
    string spriteFullName;

    string spriteName;
    string orderName;
    string spriteNumSt;

    int order;
    int kind;
    int spriteNum;
    StringBuilder stringBuilder;
    string itemSeq;

    private void Start()
    {
        //SwitchPosXmlToName();
        SaveXml();
    }

    public void SaveXml()
    {
        string xmlItemName = "test";
        XmlDocument doc = new XmlDocument();

        string xmlPosName = "testPos";
        XmlDocument posDoc = new XmlDocument();

        string xmlOutItem = "item";
        string xmlOutPos = "position";

        TextAsset txtAsset = (TextAsset)Resources.Load("Test/" + xmlItemName);
        doc.LoadXml(txtAsset.text);
        XmlNode itemRoot = doc.DocumentElement;

        TextAsset posAsset = (TextAsset)Resources.Load("Test/" + xmlPosName);
        posDoc.LoadXml(posAsset.text);
        XmlNode posRoot = posDoc.DocumentElement;

        itemRoot.RemoveAll();
        for (int j = 0; j < 3; j++)
        {
            for (int i = 0; i < spAt[j].spriteCount; i++)
            {
                spAt[j].GetSprites(sprite);
                spriteFullName = sprite[i].name;

                SetNameBySprite(spriteFullName);
                //아이템 xml생성
                XmlElement Item = (XmlElement)itemRoot.AppendChild(doc.CreateElement("row"));

                Item.SetAttribute("item_seq", itemSeq);
                Item.SetAttribute("sprite_num", spriteNum.ToString());
                Item.SetAttribute("part_kind", kind.ToString());
                Item.SetAttribute("part_order", order.ToString());
                Debug.Log(itemSeq);

                //pos xml 이름 item_seq로 바꾸기
                XmlNode tmpNode = SelectNode(posRoot, "row", "item_seq", spriteName);

                int x = int.Parse(tmpNode.Attributes["x"].Value);
                int sizeX = (int)sprite[i].textureRect.width / 2;
                int resizeX = x + sizeX;

                tmpNode.Attributes["x"].Value = resizeX.ToString();

                int y = int.Parse(tmpNode.Attributes["y"].Value);
                int sizeY = (int)sprite[i].textureRect.height / 2;
                int resizeY = y + sizeY;

                tmpNode.Attributes["y"].Value = resizeY.ToString();

                Debug.Log(tmpNode.Attributes["item_seq"].Value);
                Debug.Log(itemSeq);
                tmpNode.Attributes["item_seq"].Value = itemSeq;
            }
        }

        doc.Save("./Assets/Resources/Test/" + xmlOutItem + ".xml");
        posDoc.Save("./Assets/Resources/Test/" + xmlOutPos + ".xml");
    }

    public void SetNameBySprite(string SpriteFullName)
    {
        spriteName = SpriteFullName.Replace("(Clone)", "");
        spriteNameSplit = SpriteFullName.Split('_');

        orderName = spriteNameSplit[0];
        spriteNumSt = spriteNameSplit[1].Replace("(Clone)", "");
        Debug.Log(spriteName);
        order = CharPartOrder.StringToIntOrder(orderName);
        kind = (int)CharPartOrder.OrderNumToKindNum(order);
        spriteNum = int.Parse(spriteNumSt);

        stringBuilder = new StringBuilder();
        itemSeq = stringBuilder.Append(order.ToString("D3")).Append(spriteNum.ToString("D3")).ToString();
        stringBuilder.Clear();
    }


    public void SwitchPosXmlToName()
    {
        string xmlPosName = "testPos";
        XmlDocument posDoc = new XmlDocument();

        TextAsset posAsset = (TextAsset)Resources.Load("Test/" + xmlPosName);
        posDoc.LoadXml(posAsset.text);
        XmlNode posRoot = posDoc.DocumentElement;
        XmlNodeList listAllPos = posRoot.ChildNodes;
        for(int i = 0; i < listAllPos.Count; i++)
        {
            string itemSeq = listAllPos[i].Attributes["item_seq"].Value;
            listAllPos[i].Attributes["item_seq"].Value = itemSeqToSpriteName(itemSeq);

            //Debug.Log(itemSeq);
        }
        posDoc.Save("./Assets/Resources/Test/" + xmlPosName + ".xml");
    }

    public string itemSeqToSpriteName(string itemSeq)
    {
        string itemSeqSplit1 = itemSeq.Substring(0, 3);
        string itemSeqSplit2 = itemSeq.Substring(3, 3);

        string order = CharPartOrder.IntToString(int.Parse(itemSeqSplit1));
        int spNum = int.Parse(itemSeqSplit2);

        stringBuilder = new StringBuilder();
        string spName = stringBuilder.Append(order).Append("_").Append(spNum.ToString()).ToString();
        stringBuilder.Clear();
        return spName;
    }

    XmlNode SelectNode(XmlNode parent, string nodeName, string att, string seq)
    {
        //debug.GetComponent<Text>().text = "selectNode";
        //XmlNode tmpNode = parent.SelectSingleNode("row[@item_seq='SideHair_0']");
        XmlNode tmpNode = parent.SelectSingleNode(nodeName + "[@" + att + "='" + seq + "']");

        if (tmpNode == null)
        {
            //debug.GetComponent<Text>().text = "찾는 노드 없음";
        }
        return tmpNode;
    }
}
