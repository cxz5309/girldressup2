﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class MissionUI : MonoBehaviour
{
    public MissionView goMissionView;
    public Image missionUIImg;
    public Image rewardImg;
    public Button clearButton;
    public Image clearButtonImg;
    public Text clearButtonText;
    public Slider slider;
    public Text missionText;

    Mission thisMission;

    Sprite mainImageSprite;

    string rewardType;
    string reward;
    int rewardCoin;


    public void InitMission(Mission mission)
    {
        if(LocalizationManager.Language == "Japanese")
        {
            clearButtonText.fontSize = 24;
        }
        else
        {
            clearButtonText.fontSize = 32;
        }
        thisMission = mission;
        //이게 먼저
        SetMissionClear(thisMission.isClear);
        //보상받은 상태면 취소
        SetMissionOn(thisMission.isOn);
        SetSliderVal(thisMission.rate);
        SetText(thisMission.description);
        rewardType = thisMission.rewardType;
        reward = rewardCoinParse(thisMission.rewardType, thisMission.reward, thisMission.nowLevel);
        InitSpImage(rewardType, reward);
        SetButtonListener(thisMission, thisMission.isClear);
    }

    public void SetMissionOn(bool isOn)
    {
        if (!isOn)
        {
            clearButtonText.text = LocalizationManager.Localize(StringInfo.MClearBtn);
            clearButton.interactable = false;
            missionUIImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.quest_list_quest_off);
            clearButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.quest_btn_quest_off);
        }
    }

    public void SetMissionClear(bool isClear)
    {
        if (isClear)
        {
            clearButtonText.text = LocalizationManager.Localize(StringInfo.GET);
            clearButton.interactable = true;
            missionUIImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.quest_list_quest_on);
            clearButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "quest_btn_quest_02_n");
        }
        else
        {
            clearButtonText.text = LocalizationManager.Localize(StringInfo.GoBtn);
            clearButton.interactable = true;
            missionUIImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.quest_list_quest_on);
            clearButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "quest_btn_quest_01_n");
        }
    }

    public void SetButtonListener(Mission mission, bool isClear)
    {
        if (isClear)
        {
            clearButton.onClick.RemoveAllListeners();
            clearButton.onClick.AddListener(()=>OnGetRewardButtonClick(mission));
        }
        else
        {
            clearButton.onClick.RemoveAllListeners();
            clearButton.onClick.AddListener(() => OnGoButtonClick(mission));
        }
    }

    public void OnGetRewardButtonClick(Mission mission)
    {
        if (!DoTweenUtil.isTweening)
        {
            clearButtonText.text = LocalizationManager.Localize(StringInfo.MClearBtn);
            clearButton.interactable = false;
            missionUIImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.quest_list_quest_off);
            clearButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.quest_btn_quest_off);

            SetItemGetPopup();
        }
    }

    public void OnGoButtonClick(Mission mission)
    {
        switch (mission.missionSeq)
        {
            default:
                break;
            case 1:
                goMissionView.FortuneWheelPanel.SetActive(true);
                goMissionView.goMissionPanel.SetActive(false);
                break;
            case 2:
                SceneManager.LoadScene("MakingScene");
                break;
            case 3:
                goMissionView.alertView.Alert(LocalizationManager.Localize(StringInfo.MSaveInfo));
                break;
            case 4:
                goMissionView.ADPanel.SetActive(true);
                goMissionView.goMissionPanel.SetActive(false);
                break;
            case 5:
                goMissionView.alertView.Alert(string.Format(LocalizationManager.Localize(StringInfo.MExpInfo), MissionSeed.missionLevelDic[5].count[mission.nowLevel]));
                break;
            case 6:
                goMissionView.StorePanel.SetActive(true);
                goMissionView.goMissionPanel.SetActive(false);
                break;
            case 7:
                goMissionView.ADPanel.SetActive(true);
                goMissionView.goMissionPanel.SetActive(false);
                break;
            case 8:
                XMLManager.Instance.SaveMissionValue(XMLManager.Instance.nodeEvaluation, "value", "1");
                MissionSeed.CheckMission(8);
                #if UNITY_ANDROID
                    Application.OpenURL("market://details?id=com.ondot.girldressup2");
                #elif UNITY_IPHONE

                #endif
                break;
        }
    }

    public string rewardCoinParse(string rewardType, string reward, int level)
    {
        int coin = 0;
        if (rewardType.Equals("coin")) {
            switch (reward)
            {
                default:
                case "0":
                    coin = ObjectInfo.MISSION_REWARDS_0[level];
                    break;
            }
            reward = coin.ToString();
        }
        return reward;
    }

    public Sprite InitSpImage(string rewardType, string reward)
    {
        Sprite sprite;
        if (rewardType.Equals("coin"))
        {
            rewardCoin = int.Parse(reward);
            sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "coin_big");
        }
        else
        {
            string seq = reward;
            string spName = ParseItemSeq.ItemSeqToStringSpName(seq);

            sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllPartsAtlas, spName);
        }
        rewardImg.sprite = sprite;
        mainImageSprite = sprite;
        return sprite;
    }

    //팝업이 뜬다
    public void SetItemGetPopup()
    {
        goMissionView.itemGetPopupUI.SetStartAction(() =>
        {
            SetUI();
        });
        goMissionView.itemGetPopupUI.SetOnClickAction(() =>
        {
            SetReward();
        });
        goMissionView.goItmeGetPopup.SetActive(true);
    }

    public void SetUI()
    {
        if (rewardType.Equals("coin"))
        {
            goMissionView.itemGetPopupUI.SetImage(mainImageSprite);
            goMissionView.itemGetPopupUI.SetText(reward + " " + LocalizationManager.Localize(StringInfo.COIN) + " " + LocalizationManager.Localize(StringInfo.GET_IT));
            InGameManager.Instance.StartEffect(5, goMissionView.itemGetPopupUI.coinStartPos.position);
        }
        else
        {
            goMissionView.itemGetPopupUI.SetImage(mainImageSprite);
            goMissionView.itemGetPopupUI.SetText(LocalizationManager.Localize(StringInfo.ITEM) + " " + LocalizationManager.Localize(StringInfo.GET_IT));
        }
    }

    public void SetReward()
    {
        if (rewardType.Equals("coin"))
        {
            XMLManager.Instance.SaveUserCoinsAdd("0", rewardCoin);
            InGameManager.Instance.GetCoinXmlAndShow();
            AudioManager.Inst.PlaySFX("gaining-bonus-points-cut2");
            InGameManager.Instance.StartEffect(3, goMissionView.itemGetPopupUI.coinStartPos.position);
        }
        else
        {
            InGameManager.Instance.AddItem("item_seq", reward, ItemGetRoot.collection);//itemGetType 0 : 구매, 1 : 데일리, 2 : 룰렛,  3: 그 외
            InGameManager.Instance.InitItemGet();
        }
        MissionSeed.LevelUpMission(thisMission);

        XMLManager.Instance.SaveMissionValueUp(XMLManager.Instance.nodeGetMissionRWTimes);

        MissionSeed.CheckAllMission();
    }

    public void SetSliderVal(float val)
    {
        slider.value = val;
    }

    public void SetText(string text)
    {
        missionText.text = text;
    }
}
