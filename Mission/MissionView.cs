﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionView : MonoBehaviour
{
    public GameObject goMissionPanel;
    public Transform contents;
    public GameObject prefabMission;

    public GameObject goItmeGetPopup;
    public ItemGetPopupUI itemGetPopupUI;

    //바로가기
    public GameObject FortuneWheelPanel;
    public AlertView alertView;
    public GameObject ADPanel;
    public GameObject StorePanel;

    private void OnEnable()
    {
        DoTweenUtil.DoPopupOpen(.2f, 1f, .4f, transform);

        InGameManager.Instance.missionView = this;
        MissionSeed.CheckAllMission();
    }

    public void Init()
    {
        for(int i = 0; i < contents.childCount; i++)
        {
            Destroy(contents.GetChild(i).gameObject);
        }
        for (int i = 0; i < MissionSeed.missionDic.Count; i++)
        {
            GameObject missionObj = Instantiate(prefabMission, contents);
            MissionUI missionUI = missionObj.GetComponent<MissionUI>();
            missionUI.goMissionView = this;
            missionUI.InitMission(MissionSeed.missionDic[i]);
        }
    }
    public void DisMiss()
    {
        if (!DoTweenUtil.isTweening)
        {
            goMissionPanel.SetActive(false);
            InGameManager.Instance.rewardButtonUI.SetMissionCheck();
            InGameManager.Instance.rewardButtonUI.SetCheckImg3();
        }
    }
}
