﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransSliderUI : MonoBehaviour
{    
    public CaptureCanvas goCaptureCanvas;
    public Toggle toggle;
    public Image image;
    public int bgSeq;
    public int now;

    //이제 슬라이더x
    
    private void OnEnable()
    {
        if (XMLManager.Instance.IsOneMAINGROUPTrByTrSeq(1, 0))
        {
            bgSeq = int.Parse(XMLManager.Instance.SelectOneMAINGTOUPTrByTrSeq(1, 0).Attributes["used_seq"].Value);
        }
        else
        {
            bgSeq = -1;
        }
        OnTransValueButtonClick();
    }
    //public bool isOn;

    //슬라이더로 만들었을때(안씀)
    //public void ValueControl()
    //{
    //    if (slider.value < 0.5f)
    //    {
    //        slider.value = 0;
    //        goCaptureCanvas.transparency = false;
    //        if (bgSeq != -1)
    //        {
    //            InMainManager.Instance.SetBG(bgSeq);
    //        }
    //        else
    //        {
    //            InMainManager.Instance.UnSetBG();
    //        }
    //    }
    //    else
    //    {
    //        slider.value = 1;
    //        goCaptureCanvas.transparency = true;
    //        InMainManager.Instance.SetBG(-1);
    //    }
    //}

    public void OnTransValueButtonClick()
    {
        if (toggle.isOn)
        {
            image.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "capture_togglebtn_on");
            goCaptureCanvas.transparency = false;
            if (bgSeq != -1)
            {
                InMainManager.Instance.SetBG(bgSeq);
                now = bgSeq;
            }
            else
            {
                InMainManager.Instance.UnSetBG();
                now = -1;
            }
        }
        else
        {
            image.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "capture_togglebtn_off");
            goCaptureCanvas.transparency = true;
            InMainManager.Instance.SetBG(-1);
            now = -1;
        }
    }
}
