﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OrderTypeButtonUI : MonoBehaviour
{
    public ChildSpriteToggleUI spToggleUI;

    public Button orderTypeButton;
    public Image orderTypeButtonImage;
    public Image deemImage;
    public GameObject StoreImgObj;
    public Image StoreImg;
    public string nowButtonMenu;
    string seqName;
    string menuSeq;
    bool dontSetImage;
    public bool startButton;

    private void Awake()
    {
        //(이름의 숫자를 제거해우는 함수)(사용안함)
        //string nameParse = Regex.Replace(this.name, @"\d", "");
        nowButtonMenu = this.name;
        switch (SceneManager.GetActiveScene().name)
        {
            case "MakingScene":
                seqName = "order_seq";
                break;
            case "MainScene":
                seqName = "obj_type_seq";
                break;
        }
        menuSeq = ParseButtonNameToSeq(nowButtonMenu);
    }

    void OnEnable()
    {
        if(XMLManager.Instance.IsCostOrderButton(seqName, menuSeq))
        {
            SetStoreImg(true);

            if (!XMLManager.Instance.CheckIsHave(seqName, menuSeq))
            {
                SetDeem(true);
                SetStoreColor(true);
            }
            else
            {
                SetDeem(false);
                SetStoreColor(false);
            }
        }
        orderTypeButton.onClick.RemoveAllListeners();
        orderTypeButton.onClick.AddListener(
            () =>
            {
                OnClickToggle();
                if (!dontSetImage)
                {
                    spToggleUI.SetImageToggle(transform.GetSiblingIndex());
                }
                //GetComponent<SpriteToggleInButton>().OnClickThis();
            });
    }

    private void Start()
    {
        if (startButton)
        {
            OnClickToggle();
            if (!dontSetImage)
            {
                spToggleUI.SetImageToggle(transform.GetSiblingIndex());
            }
            //GetComponent<SpriteToggleInButton>().OnClickThis();
        }
    }

    public bool CheckCost()
    {
        bool isCost = false;
        switch (SceneManager.GetActiveScene().name)
        {
            case "MakingScene":
                if (XMLManager.Instance.IsCostOrderButton("order_seq", menuSeq))
                {
                    isCost = true;
                }
                break;
            case "MainScene":
                if (XMLManager.Instance.IsCostOrderButton("obj_type_seq", menuSeq))
                {
                    isCost = true;
                }
                break;
        }
        return isCost;
    }

    public bool CheckHave()
    {
        bool isHave = true;
        switch (SceneManager.GetActiveScene().name)
        {
            case "MakingScene":
                if (!XMLManager.Instance.IsUserHaveItem("order_seq", menuSeq))
                {
                    isHave = false;
                }
                break;
            case "MainScene":
                if (!XMLManager.Instance.IsUserHaveItem("obj_type_seq", menuSeq))
                {
                    isHave = false;
                }
                break;
        }
        return isHave;
    }

    public void SetDeem(bool deem)
    {
        if (deem)
        {
            deemImage.gameObject.SetActive(true);
        }
        else
        {
            deemImage.gameObject.SetActive(false);
        }
    }
    public void ChangeDeemColor(Color color)
    {
        deemImage.color = color;
    }

    public void SetStoreImg(bool isStore)
    {
        if (isStore) {
            StoreImgObj.SetActive(true);
        }
        else
        {
            StoreImgObj.SetActive(false);
        }
    }

    public void SetStoreColor(bool isLock)
    {
        if (isLock)
        {
            StoreImg.color = ObjectInfo.seemsRed;
        }
        else
        {
            StoreImg.color = ObjectInfo.seemsWhite;
        }
    }

    public void OnClickToggle()
    {
        if(!XMLManager.Instance.CheckIsHave(seqName, menuSeq))
        { 
            dontSetImage = true;
            GoStore(seqName, menuSeq);
            return;
        }
        else
        { 
            dontSetImage = false;
        }
        OnPartMenuClick();
    }

    public string ParseButtonNameToSeq(string nowButtonMenu_)
    {
        string returnSeq = null;
        switch (SceneManager.GetActiveScene().name)
        {
            case "MakingScene":
                returnSeq = CharPartOrder.StringToIntOrder(nowButtonMenu_).ToString("D3");
                break;
            case "MainScene":
                returnSeq = Friend.StringToIntFriendType(nowButtonMenu_).ToString();
                break;
        }
        return returnSeq;
    }

    public void OnPartMenuClick()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "MakingScene":
                InMakingManager.Instance.makingCanvas.OnPartMenuClick(nowButtonMenu);
                break;
            case "MainScene":
                InMainManager.Instance.mainCanvas.OnClickMenu(int.Parse(menuSeq));
                break;
        }
    }

    public void GoStore(string seqName, string itemSeq)
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "MakingScene":
                InMakingManager.Instance.makingCanvas.AlertAD(seqName, itemSeq);
                break;
            case "MainScene":
                InMainManager.Instance.mainCanvas.verticalViewUI.AlertAD(seqName, itemSeq);
                break;
        }
    }
}
