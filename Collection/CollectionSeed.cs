﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionSet
{
    public int collectionSeq;
    public bool isOn;
    public bool isClear;
    //public bool[] canGet;

    private int level;
    public int levelMax;

    public UserHave rewardSeq;
    public List<UserHave> colUHSeq; // 컬렉션 보상 아이템 목록
    public Dictionary<UserHave, List<UserHave>> requireUHSeq; //컬렉션 보상 아이템당 얻어야하는 아이템 목록 (key : 보상 아이템 seq, val : 얻어야하는 아이템 seq 리스트)

    public int Level { get => level; set => level = value; }

    public CollectionSet(int collectionSeq, bool isOn, bool isClear, int level)
    {
        this.collectionSeq = collectionSeq;
        this.isOn = isOn;
        this.isClear = isClear;
        this.Level = level;

        colUHSeq = new List<UserHave>();
        requireUHSeq = new Dictionary<UserHave, List<UserHave>>();
    }

    public bool CheckCanGetThisLevel()
    {
        if (level > levelMax)
        {
            return false;
        }
        for (int i = 0; i < requireUHSeq[colUHSeq[Level]].Count; i++)
        {
            if (!XMLManager.Instance.CheckIsHave(requireUHSeq[colUHSeq[Level]][i].seqName, requireUHSeq[colUHSeq[Level]][i].itemSeq))
            {
                return false;
            }
        }
        //canGet[Level] = true;
        return true;
    }

    //public void CheckCanGetThisItem(int itemLength)
    //{
    //    canGet = new bool[itemLength];
    //    for (int i = 0; i < itemLength; i++)
    //    {
    //        this.canGet[i] = this.isClear ? true : false;
    //    }
    //}

    public void SetRewardSeq(UserHave userHave)
    {
        this.rewardSeq = userHave;
    }

    public void SetColItemSeq(List<UserHave> itemSeqList)
    {
        this.colUHSeq = itemSeqList;
        //this.canGet = new bool[itemSeqList.Count];
        levelMax = colUHSeq.Count - 1;
    }

    public void SetReqItemSeq(Dictionary<UserHave, List<UserHave>> reqDic)
    {
        this.requireUHSeq = reqDic;
    }
}

public class CollectionSeed : MonoBehaviour
{
    public static Dictionary<int, CollectionSet> collectionSetsDic = new Dictionary<int, CollectionSet>();
    public static Dictionary<int, List<UserHave>> colItemSetUH = new Dictionary<int, List<UserHave>>();

    public static int collectionLimit = 1;
    public static int nowStage = 0;

    public static void Init()
    {
        nowStage = PlayerPrefs.GetInt("CollectionStage");
        collectionSetsDic = XMLManager.Instance.LoadAllCollections();
        InitItemSet();
    }

    public static bool CheckAndSaveOneIsClear(CollectionSet collectionSet)
    {
        if (collectionSet.collectionSeq < collectionLimit)
        {
            //collectionSet.isClear = CheckIsClear(collectionSet);
            if (collectionSet.CheckCanGetThisLevel())
            {
                return true;
            }
        }
        return false;
        //XMLManager.Instance.SaveCollectionIsClear(collectionSet.collectionSeq.ToString(), collectionSet.isClear);
    }

    public static bool CheckAndSaveAllIsClear()
    {
        for (int i = 0; i < 1; i++)
        {
            if (CheckAndSaveOneIsClear(collectionSetsDic[i]))
            {
                return true;
            }
        }
        return false;
    }

    //여러 아이템 묶음 info에서 받아 직렬화하기
    //컬렉션 이외에서 사용될 경우 옮기기
    public static void InitItemSet()
    {
        UserHave newUserHave;
        for (int i = 0; i < ObjectInfo.AllItemSet.Length; i++)
        {
            List<UserHave> newUserHaveList = new List<UserHave>();

            for (int j = 0; j < ObjectInfo.AllItemSet[i].Length; j++)
            {
                newUserHave = new UserHave(ObjectInfo.AllItemSet[i][j][0], ObjectInfo.AllItemSet[i][j][1]);
                newUserHaveList.Add(newUserHave);
            }
            if (!colItemSetUH.ContainsKey(i))
                colItemSetUH.Add(i, newUserHaveList);
        }
    }
}
