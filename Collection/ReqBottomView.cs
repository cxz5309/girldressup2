﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReqBottomView : MonoBehaviour
{
    public GameObject[] reqButtonsObj = new GameObject[20];

    public Button reqGetButton;

    public Text reqInfoText;
    public Text reqGetButtonText;
    public Text reqStageText;
    public GameObject clearLabel;

    public void SetButtonText(string text)
    {
        reqGetButtonText.text = text;
    }

    public void SetStageText(string text) {
        reqStageText.text = text;
    }

    public void SetInfoText(string text)
    {
        reqInfoText.text = text;
    }
}
