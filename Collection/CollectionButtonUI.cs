﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionButtonUI : MonoBehaviour
{
    public GameObject sel;
    public GameObject unSel;
    public GameObject deemImg;

    public Image image;
    public Text unSeltext;
    public Text selText;

    public bool startButton;

    private void Start()
    {
        if (startButton)
        {
            SetSel(true);
            SetDeem(false);
            SetImage();
        }
    }

    public void SetSel(bool on)
    {
        if (on)
        {
            sel.SetActive(true);
            unSel.SetActive(false);
        }
        else
        {
            sel.SetActive(false);
            unSel.SetActive(true);
        }
    }
    public void SetImage()
    {
        transform.parent.GetComponent<ChildSpriteToggleUI>().SetImageToggle(transform.GetSiblingIndex());
    }

    public void SetImage(Sprite sprite)
    {
        image.sprite = sprite;
    }

    public void SetSelText(string st)
    {
        selText.text = st;
    }

    public void SetUnselText(string st)
    {
        unSeltext.text = st;
    }

    public void SetDeem(bool on)
    {
        if (on)
        {
            deemImg.SetActive(true);
        }
        else
        {
            deemImg.SetActive(false);
        }
    }
}

