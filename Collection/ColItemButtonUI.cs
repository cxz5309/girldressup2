﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColItemButtonUI : MonoBehaviour
{
    public Button itemButton;
    public Image itemButtonImg;
    public Text stageText;
    public Image itemImg;
    public GameObject deemImage;
    public GameObject CompleteImage;
    public GameObject qMark;
}
