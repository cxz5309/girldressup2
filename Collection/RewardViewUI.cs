﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class RewardViewUI : MonoBehaviour
{
    public GameObject goRewardPanel;

    public GameObject prefabCharImgRoot;
    public GameObject prefabPartImg;
    public GameObject prefabMultiRootImg;
    public GameObject prefabImgObject;

    public Transform middleRect;

    public Dictionary<int, GameObject> listPartObj = new Dictionary<int, GameObject>();

    //원래 몸이랑 속옷이랑 최종보상만 해도 되는데 이제 기획을 믿을수가 없어서 최대한 확장 가능하도록 함
    public List<UserHave> RewardAllParts = new List<UserHave>();

    public List<int> listPartSibling;

    private void OnEnable()
    {
        DoTweenUtil.DoPopupOpen(.2f, 1f, .4f, transform);
        CreateCharImg();
    }

    public void Dismiss()
    {
        goRewardPanel.SetActive(false);
    }

    //컬렉션뷰에서 현재 최종보상을 가져온다 아이템 정보는 이거 하나로 컨트롤함
    public void Init(List<UserHave> RewardAllParts_)
    {
        RewardAllParts = RewardAllParts_;
    }


    #region 캐릭터 이미지 생성

    //캐릭터 하나 이미지 생성
    public void CreateCharImg()
    {
        GameObject root = Instantiate(prefabCharImgRoot, middleRect);
        root.transform.localScale = new Vector3(0.6f, 0.6f, 1);
        root.transform.SetAsFirstSibling();

        CharPart tmpPart;
        GameObject imageObj = null;

        //기본 세팅 이미지 생성
        //몸
        tmpPart = XMLManager.Instance.UsePartByItemSeq(XMLManager.Instance.bodyInitSeq);
        CreatePartImg(tmpPart, root);
        //속옷
        tmpPart = XMLManager.Instance.UsePartByItemSeq(XMLManager.Instance.underInitSeq);
        CreatePartImg(tmpPart, root);
        //눈1
        tmpPart = XMLManager.Instance.UsePartByItemSeq(XMLManager.Instance.eyeLInitSeq);
        CreatePartImg(tmpPart, root);
        //눈2
        tmpPart = XMLManager.Instance.UsePartByItemSeq(XMLManager.Instance.eyeRInitSeq);
        CreatePartImg(tmpPart, root);
        //코
        tmpPart = XMLManager.Instance.UsePartByItemSeq(XMLManager.Instance.noseInitSeq);
        CreatePartImg(tmpPart, root);
        //입
        tmpPart = XMLManager.Instance.UsePartByItemSeq(XMLManager.Instance.mouseInitSeq);
        CreatePartImg(tmpPart, root);

        //획득 아이템 이미지 생성(현재것까지 생성, 현재것은 딤처리)
        for (int i = 0; i < RewardAllParts.Count; i++)
        {
            switch (RewardAllParts[i].seqName)
            {
                default:
                    Debug.LogError("컬렉션 이미지 생성 오류");
                    break;
                case "item_seq":
                    tmpPart = XMLManager.Instance.UsePartByItemSeq(RewardAllParts[i].itemSeq);
                    imageObj = CreatePartImg(tmpPart, root);
                    int handSeq = tmpPart.HandSeqToInt();
                    int handType = tmpPart.handType;
                    if (handSeq != -1)
                    {
                        CharPart createCharPart;

                        if (handType == 0)
                        {
                            string createSeq = ParseItemSeq.MakeItemSeq((int)HandMatch.handOrder[HandMatch.kindNumToArrayNum(tmpPart.charPartItem.kind.kindType)], handSeq);
                            createCharPart = XMLManager.Instance.UsePartByItemSeq(createSeq);
                            CreatePartImg(createCharPart, root);
                        }
                        else
                        {
                            string createSeq = ParseItemSeq.MakeItemSeq((int)HandMatch.uHandOrder[HandMatch.kindNumToArrayNum(tmpPart.charPartItem.kind.kindType)], handSeq);
                            createCharPart = XMLManager.Instance.UsePartByItemSeq(createSeq);
                            CreatePartImg(createCharPart, root);
                        }
                    }
                    ItemEffect(i, true, imageObj);
                    break;
                case "hair_seq":
                    tmpPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.numToD3((int)OrderType.BackHair) + RewardAllParts[i].itemSeq);
                    imageObj = CreatePartImg(tmpPart, root);
                    ItemEffect(i, false, imageObj);

                    tmpPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.numToD3((int)OrderType.FrontHair) + RewardAllParts[i].itemSeq);
                    imageObj = CreatePartImg(tmpPart, root);
                    ItemEffect(i, true, imageObj);

                    tmpPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.numToD3((int)OrderType.SideHair) + RewardAllParts[i].itemSeq);
                    imageObj = CreatePartImg(tmpPart, root);
                    ItemEffect(i, false, imageObj);

                    break;
                case "eyes_seq":
                    tmpPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.numToD3((int)OrderType.EyeLeft) + RewardAllParts[i].itemSeq);
                    imageObj = CreatePartImg(tmpPart, root);
                    ItemEffect(i, true, imageObj);

                    tmpPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.numToD3((int)OrderType.EyeRight) + RewardAllParts[i].itemSeq);
                    imageObj = CreatePartImg(tmpPart, root);
                    ItemEffect(i, true, imageObj);

                    break;
                case "bg_seq":
                    imageObj = InitAllObj(RewardAllParts[i].seqName, RewardAllParts[i].itemSeq, FriendType.BG);
                    ItemEffect(i, true, imageObj);
                    break;
                case "object1_seq":
                    imageObj = InitAllObj(RewardAllParts[i].seqName, RewardAllParts[i].itemSeq, FriendType.Object1);
                    ItemEffect(i, true, imageObj);
                    break;
                case "object2_seq":
                    imageObj = InitAllObj(RewardAllParts[i].seqName, RewardAllParts[i].itemSeq, FriendType.Object2);
                    ItemEffect(i, true, imageObj);
                    break;
                case "animal_seq":
                    imageObj = InitAllObj(RewardAllParts[i].seqName, RewardAllParts[i].itemSeq, FriendType.Animal);
                    ItemEffect(i, true, imageObj);
                    break;
                case "bubble_seq":
                    imageObj = InitAllObj(RewardAllParts[i].seqName, RewardAllParts[i].itemSeq, FriendType.Bubble);
                    ItemEffect(i, true, imageObj);
                    break;
                case "text_seq":
                    imageObj = InitAllObj(RewardAllParts[i].seqName, RewardAllParts[i].itemSeq, FriendType.Text);
                    ItemEffect(i, true, imageObj);
                    break;
            }
            //레벨보다 크면 딤처리한다
        }
        SetAllSibling(listPartSibling, listPartObj);
    }

    public void ItemEffect(int i, bool isEffect, GameObject itemImage)
    {
        DoTweenUtil.DoFadeImage(0, 1, 2f, itemImage);
        DoTweenUtil.DoSizeImage(3, 1, 1f, itemImage);
        if (isEffect)
            InGameManager.Instance.StartEffect(2, itemImage.transform.position, 4f);
    }

    //파트 하나 이미지 생성
    public GameObject CreatePartImg(CharPart element, GameObject root)
    {
        if (element.multiOrder == -1)
        {
            GameObject obj = Instantiate(prefabPartImg, root.transform);
            CharPartUse tmpCharPart = obj.GetComponent<CharPartUse>();
            Image image = obj.GetComponent<Image>();

            tmpCharPart.charPart = element;
            image.sprite = SetSprite(false, "item_seq", element.charPartItem.seq);
            //tmpCharPart.SetImageSprite();
            tmpCharPart.SetImagePosition();

            int siblingNum = tmpCharPart.SetSortingOrder();

            listPartSibling.Add(siblingNum);
            listPartObj.Add(siblingNum, obj);

            return obj;
        }
        else
        {
            //멀티루트 제작
            GameObject obj;
            int siblingNum = (int)element.charPartItem.order.orderType;
            if (!listPartObj.ContainsKey(siblingNum))
            {
                obj = Instantiate(prefabMultiRootImg, root.transform);
                listPartObj.Add(siblingNum, obj);
            }
            else
            {
                obj = listPartObj[siblingNum];
            }
            SortingGroup sg = obj.GetComponent<SortingGroup>();
            listPartSibling.Add(siblingNum);

            sg.sortingOrder = (int)element.charPartItem.order.orderType;

            //파트이미지 제작
            GameObject part = Instantiate(prefabPartImg, obj.transform);
            CharPartUse tmpCharPart = part.GetComponent<CharPartUse>();
            Image image = obj.GetComponent<Image>();

            tmpCharPart.charPart = element;
            image.sprite = SetSprite(false, "item_seq", element.charPartItem.seq);
            //tmpCharPart.SetImageSprite();
            tmpCharPart.SetImagePosition();

            return part;
        }
    }


    //모든 오브젝트 버튼 내부에 올리기 
    public GameObject InitAllObj(string seqName, string typeSeq, FriendType friendType)
    {
        Dictionary<int, ItemPart> tmpObj = new Dictionary<int, ItemPart>();
        tmpObj = XMLManager.Instance.listAllObjDic[(int)friendType];

        //GameObject root = Instantiate(prefabCharImgRoot, middleRect);
        //root.transform.SetAsFirstSibling();

        GameObject obj = Instantiate(prefabImgObject, middleRect);
        SetObjRender(seqName, typeSeq, obj);

        //오브젝트가 올라올것 같지는 않지만 확장을 위해 남겨둠
        obj.transform.localPosition = new Vector3(0, 0, 0);
        return obj;
    }

    public void SetObjRender(string seqName, string imgName, GameObject root)
    {
        Image objSprite = root.GetComponentInChildren<Image>();
        objSprite.sprite = SetSprite(false, seqName, imgName);
    }

    //레이어
    public void SetAllSibling(List<int> partSibling, Dictionary<int, GameObject> objSibling)
    {
        partSibling.Sort();

        for (int i = 0; i < partSibling.Count; i++)
        {
            objSibling[partSibling[i]].transform.SetAsLastSibling();
        }
        partSibling.Clear();
        objSibling.Clear();
    }

    #endregion

    public Sprite SetSprite(bool btn, string seqName, string itemSeq)
    {
        //딤도 절대 안될거같지만 남겨둔다 최종보상 딤은 애초에 없다
        Sprite sprite = null;
        Sprite colSprite = null;
        string seq = null;
        string add = null;
        switch (seqName)
        {
            default:
                Debug.LogError("컬렉션 버튼이미지 오류");
                break;
            case "item_seq":
                seq = ParseItemSeq.ItemSeqToStringSpName(itemSeq);
                add = btn ? "_Btn" : "";

                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, seq + add);
                if (colSprite == null)
                {
                    if (btn)
                    {
                        sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, ParseItemSeq.ItemSeqToStringSpName(itemSeq));
                    }
                    else
                    {
                        sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllPartsAtlas, ParseItemSeq.ItemSeqToStringSpName(itemSeq));
                    }
                }
                else sprite = colSprite;
                break;
            case "hair_seq":
                seq = "Hairs_" + int.Parse(itemSeq);
                add = btn ? "_Btn" : "";

                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, seq + add);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "Hairs_" + int.Parse(itemSeq));
                }
                else sprite = colSprite;
                break;
            case "eyes_seq":
                seq = "Eyes_" + int.Parse(itemSeq);
                add = btn ? "_Btn" : "";
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, seq + add);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "Eyes_" + int.Parse(itemSeq));
                }
                else sprite = colSprite;
                break;
            case "bg_seq":
                seq = itemSeq;
                add = btn ? "_Btn" : "";
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, seq + add);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllBGAtlas, itemSeq);
                }
                else sprite = colSprite;
                break;
            case "object1_seq":
                seq = itemSeq;
                add = btn ? "_Btn" : "";
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, seq + add);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllObject1Atlas, itemSeq);
                }
                else sprite = colSprite;
                break;
            case "object2_seq":
                seq = itemSeq;
                add = btn ? "_Btn" : "";
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, seq + add);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllObject2Atlas, itemSeq);
                }
                else sprite = colSprite;
                break;
            case "animal_seq":
                seq = itemSeq;
                add = btn ? "_Btn" : "";
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, seq + add);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllAnimalAtlas, itemSeq);
                }
                else sprite = colSprite;
                break;
            case "bubble_seq":
                seq = itemSeq;
                add = btn ? "_Btn" : "";
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, seq + add);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllBubbleAtlas, itemSeq);
                }
                else sprite = colSprite;
                break;
            case "text_seq":
                seq = itemSeq;
                add = btn ? "_Btn" : "";
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, seq + add);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllTextAtlas, itemSeq);
                }
                else sprite = colSprite;
                break;
        }
        return sprite;
    }

    private void OnDisable()
    {
        for (int i = 0; i < middleRect.childCount; i++)
        {
            Destroy(middleRect.GetChild(i).gameObject);

            listPartSibling.Clear();
            listPartObj.Clear();
        }
    }
}
