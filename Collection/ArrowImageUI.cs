﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowImageUI : MonoBehaviour
{
    public Image[] images;

    public void SetImageColor(int i, Color color)
    {
        images[i].color = color;
    }
}
