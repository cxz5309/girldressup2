﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using Assets.SimpleLocalization;
using UnityEngine.SceneManagement;

public class CollectionViewUI : MonoBehaviour
{
    public GameObject collectionPanel;
    public GameObject prefabCharImgRoot;
    public GameObject prefabPartImg;
    public GameObject prefabMultiRootImg;
    public Transform middleRect;

    public GameObject[] itemButtonsObj = new GameObject[20];
    public GameObject[] arrowObj = new GameObject[20];

    public GameObject prefabImgObject;
    public ScrollRect colItemScrollRect;

    //요구아이템UI
    public ReqBottomView reqBottomView;

    //보상아이템UI(요구 아이템이 없는 상태)
    public RewBottomView rewBottomView;

    //최종보상 미리보기
    public RewardViewUI rewardViewUI;
    public GameObject rewardPanel;

    public CollectionButtonUI[] collectionMenuButtonsUI;
    public ChildSpriteToggleUI childSpriteToggleUI;
    public ItemGetPopupUI itemGetPopupUI;
    public ItemGetPopupUI itemInfoPopupUI;
    public SAlertViewUI sAlertView;
    public AlertView alertView;
    public GameObject DontTouch;

    //reqView 다른 화면 보내기
    public GameObject storePanel;
    public GameObject dailyPanel;
    public GameObject FortuneWheelPanel;

    //현재 컬렉션 정보 전체
    public CollectionSet thisCS;

    public int collectionNum = 0;

    public Dictionary<int, GameObject> listPartObj = new Dictionary<int, GameObject>();
    public List<int> listPartSibling;

    public List<Transform> objectTrList = new List<Transform>();
    public List<string> objectNameList = new List<string>();

    public int nowColIdx;

    CharPart tmpPart;
    GameObject imageObj = null;
    GameObject root = null;

    bool isFirst;

    private void OnEnable()
    {
        InGameManager.Instance.collectionViewUI = this;

        isFirst = true;
        InitCollectoinSets();
        InitTopToggle();
    }

    //위쪽 컬렉션 선택 토글 초기화
    public void InitTopToggle()
    {
        CollectionSeed.nowStage = 0;
        for (int i = 0; i < 3; i++)
        {
            if (CollectionSeed.collectionSetsDic[i].isClear)
            {
                CollectionSeed.nowStage++;
            }
        }
        //현재 스테이지 플레이어프렙스에 저장
        PlayerPrefs.SetInt(StringInfo.playerPrefsKeyList[5], CollectionSeed.nowStage);

        childSpriteToggleUI.exceptSel.Clear();
        for (int i = 0; i < collectionMenuButtonsUI.Length; i++)
        {
            // 컬렉션 한도, 게임 업데이트로 차차 해제
            if (i >= CollectionSeed.collectionLimit)
            {
                collectionMenuButtonsUI[i].SetSel(false);
                collectionMenuButtonsUI[i].SetImage(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.collection_btn_get_off));
                collectionMenuButtonsUI[i].SetUnselText(LocalizationManager.Localize(StringInfo.COMMING_SOON));
                collectionMenuButtonsUI[i].SetDeem(true);
                childSpriteToggleUI.ExceptOne(i);
                collectionMenuButtonsUI[i].GetComponent<Button>().interactable = false;
            }
            else
            {
                //컬렉션 진행도 현재 진행도보다 높은 컬렉션만 제한
                if (i > CollectionSeed.nowStage)
                {
                    collectionMenuButtonsUI[i].SetSel(false);
                    collectionMenuButtonsUI[i].SetImage(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.collection_btn_get_off));
                    collectionMenuButtonsUI[i].SetDeem(true);

                    childSpriteToggleUI.ExceptOne(i);
                    collectionMenuButtonsUI[i].GetComponent<Button>().interactable = false;
                    collectionMenuButtonsUI[i].SetUnselText("Collection " + (i + 1));
                }
                else
                {
                    //컬렉션 보상을 이미 받아 토글에서 제외..되지 않는다, 누르는 건 가능
                    if (!CollectionSeed.collectionSetsDic[i].isOn)
                    {
                        collectionMenuButtonsUI[i].GetComponent<Button>().interactable = true;
                        collectionMenuButtonsUI[i].SetDeem(false);
                    }
                    //컬렉션 토글 버튼
                    //지금은 자동으로 보상이 받아지기 때문에 이쪽으로 들어오지 않는다
                    else
                    {
                        collectionMenuButtonsUI[i].SetDeem(false);
                        collectionMenuButtonsUI[i].GetComponent<Button>().interactable = true;
                    }
                    collectionMenuButtonsUI[i].SetSel(true);
                    collectionMenuButtonsUI[i].SetSelText("Collection " + (i + 1));
                }
            }
        }
    }

    //위쪽 컬렉션 토글 클릭
    public void OnCollMenuClick(int selNum)
    {
        for (int i = 0; i < 3; i++)
        {
            if (i == selNum)
            {
                collectionNum = selNum;
            }
        }
        InitCollectoinSets();
    }


    //컬렉션 초기화
    public void InitCollectoinSets()
    {
        SetFirstInit();
        CreateButtonImg();
        OnCollectionPartClick(thisCS.Level, true);
        InitScrollPos();
    }

    public void SetFirstInit()
    {
        isFirst = true;
        CollectionSeed.Init();
        thisCS = CollectionSeed.collectionSetsDic[collectionNum];
    }

    //스크롤 현재 아이템 위치로 변경
    public void InitScrollPos()
    {
        float one = itemButtonsObj[thisCS.Level].GetComponent<RectTransform>().localPosition.x;
        float two = 540;
        float three = colItemScrollRect.content.rect.width;
        float four = colItemScrollRect.GetComponent<RectTransform>().rect.width;
        float nomalize = (one - two) / (three - four);
        if (nomalize < 0) nomalize = 0;
        if (nomalize > 1) nomalize = 1;
        colItemScrollRect.horizontalNormalizedPosition = nomalize;
    }

    #region 버튼 이미지 생성
    public void CreateButtonImg()
    {
        for(int i = 0; i < itemButtonsObj.Length; i++)
        {
            itemButtonsObj[i].SetActive(false);
        }
        for(int i=0;i< arrowObj.Length; i++)
        {
            arrowObj[i].SetActive(false);
        }
        ColItemButtonUI colItemButtonUI;

        int colCount = thisCS.colUHSeq.Count;
        //각 버튼상태
        for (int i = 0; i < colCount; i++)
        {
            int idx = new int();
            idx = i;
            itemButtonsObj[i].SetActive(true);
            arrowObj[i].SetActive(true);

            colItemButtonUI = itemButtonsObj[idx].GetComponent<ColItemButtonUI>();

            if (i <= thisCS.levelMax)
            {
                // 버튼 퀘스트 클리어시
                if (i < thisCS.Level)
                {//버튼누를수있음, 이미지 보임, q마크 삭제, 딤 삭제, 컴플리트 이미지 생성
                    colItemButtonUI.itemButton.interactable = true;
                    colItemButtonUI.itemButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.collection_itembox_on);
                    colItemButtonUI.deemImage.SetActive(false);
                    colItemButtonUI.qMark.SetActive(false);
                    colItemButtonUI.stageText.gameObject.SetActive(false);
                    colItemButtonUI.CompleteImage.SetActive(true);

                    for (int k = 0; k < 3; k++)
                    {
                        arrowObj[i].GetComponent<ArrowImageUI>().SetImageColor(k, ObjectInfo.seemsRed);
                    }
                    //위에 캐릭터 딤 해제
                }
                //현재 버튼 퀘스트
                else if (i == thisCS.Level)
                {//버튼누를수 있음, 보임, q마크 삭제, 딤 삭제, 컴플리트 이미지 삭제
                    colItemButtonUI.itemButton.interactable = true;
                    colItemButtonUI.itemButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.collection_itembox_off);
                    colItemButtonUI.deemImage.SetActive(false);
                    colItemButtonUI.qMark.SetActive(false);
                    colItemButtonUI.stageText.gameObject.SetActive(false);
                    colItemButtonUI.CompleteImage.SetActive(false);

                    for (int k = 0; k < 3; k++)
                    {
                        arrowObj[i].GetComponent<ArrowImageUI>().SetImageColor(k, ObjectInfo.seemsWhite);
                    }
                    //위에 캐릭터 딤 처리
                }
                //클리어 안됨
                else
                {//버튼누를수 없음, 이미지 안보임, 딤 생성, q마크 생성 
                    colItemButtonUI.itemButton.interactable = false;
                    colItemButtonUI.itemButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.collection_itembox_off);
                    colItemButtonUI.deemImage.SetActive(true);
                    colItemButtonUI.qMark.SetActive(true); colItemButtonUI.stageText.gameObject.SetActive(true);
                    colItemButtonUI.stageText.gameObject.SetActive(true);
                    colItemButtonUI.CompleteImage.SetActive(false);

                    for (int k = 0; k < 3; k++)
                    {
                        arrowObj[i].GetComponent<ArrowImageUI>().SetImageColor(k, ObjectInfo.seemsWhite);
                    }
                    //위에 캐릭터 딤 처리
                }
                colItemButtonUI.stageText.text = string.Format("{0}-{1}", thisCS.collectionSeq + 1, (idx+1));
                colItemButtonUI.itemImg.sprite = SetSprite(true, IsDeem(i, thisCS.Level), thisCS.colUHSeq[i].seqName, thisCS.colUHSeq[i].itemSeq);

                //버튼 딤처리
                //SetDeem(true, colItemButtonUI.itemImg, thisCS.colUHSeq[i].seqName, thisCS.colUHSeq[i].itemSeq);
               
                colItemButtonUI.itemButton.onClick.AddListener(() =>
                {
                    OnCollectionPartClick(idx, false);
                    //위에 캐릭터 바꾸는 함수 추가할 것
                });
            }
        }
        //마지막 보상 추가
        itemButtonsObj[colCount].SetActive(true);
        colItemButtonUI = itemButtonsObj[colCount].GetComponent<ColItemButtonUI>();

        colItemButtonUI.itemButton.interactable = true;
        colItemButtonUI.itemButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.collection_itembox_on);
        colItemButtonUI.deemImage.SetActive(false);
        colItemButtonUI.qMark.SetActive(false);
        colItemButtonUI.CompleteImage.SetActive(false);

        colItemButtonUI.stageText.text = LocalizationManager.Localize(StringInfo.REWARD);
        colItemButtonUI.stageText.fontSize = 32;
        colItemButtonUI.itemImg.sprite = SetSprite(true, false, thisCS.rewardSeq.seqName, thisCS.rewardSeq.itemSeq);

        colItemButtonUI.itemButton.onClick.AddListener(() =>
        {
            OnCollectionPartClick(colCount, false);
            //위에 캐릭터 바꾸는 함수 추가할 것
        });
    }

    public void OnCollectionPartClick(int colIndex, bool isFirst_)
    {
        isFirst = isFirst_;
        if (!DoTweenUtil.isTweening)
        {
            SettingUI(colIndex);
            //캐릭터 세팅도 바꾸어야함
            CreateCharImg();
        }
    }

    public void SettingUI(int colIndex)
    {
        nowColIdx = colIndex;
        UserHave colItemSeq;
        //최종보상 눌렀을 경우
        if (nowColIdx > thisCS.levelMax)
        {

            ClearLabelOnOff(false);
            colItemSeq = thisCS.rewardSeq;
            //실제 보상 획득시
            if (thisCS.Level > thisCS.levelMax)
            {
                reqBottomViewOn(false);
                GetButtonOnOff(false);

                rewBottomView.SetStageText(LocalizationManager.Localize(StringInfo.REWARD));
                rewBottomView.SetButtonText(LocalizationManager.Localize(StringInfo.GET));

                //전체 컬렉션 클리어시
                if (thisCS.isClear)
                {
                    rewBottomView.newImageObj.SetActive(true);

                    //컬렉션 보상 안받았을 시
                    if (thisCS.isOn)
                    {
                        rewBottomView.SetInfoText(LocalizationManager.Localize(StringInfo.NIsOnInfo));
                    }
                    //컬렉션 보상 받았을 시 (종료된 컬렉션)
                    else
                    {
                        rewBottomView.SetInfoText(LocalizationManager.Localize(StringInfo.IsOnInfo));
                        reqBottomView.reqGetButton.gameObject.SetActive(false);
                        rewBottomView.rewGetButton.gameObject.SetActive(false);
                        if (!isFirst)
                        {
                            SetAllClearItemGetPopupOnImage();
                            itemGetPopupUI.gameObject.SetActive(true);
                        }
                    }
                }
                else
                {
                    CollectionSeed.CheckAndSaveOneIsClear(thisCS);

                    rewBottomView.newImageObj.SetActive(false);
                }
            }
            else
            {
                //최종 보상 여러개 넣을 수 있음
                rewardViewUI.RewardAllParts.Clear();
                rewardViewUI.RewardAllParts.Add(colItemSeq);
                //최종 보상 미리보기
                rewardPanel.SetActive(true);
            }
        }
        else
        {
            colItemSeq = thisCS.colUHSeq[colIndex];
            rewBottomView.newImageObj.SetActive(false);

            //요구아이템 수에 따라 화면 변경
            int reqItemCount = thisCS.requireUHSeq[thisCS.colUHSeq[colIndex]].Count;

            if (reqItemCount == 0)
            {
                reqBottomViewOn(false);

                if (nowColIdx == thisCS.Level)
                {
                    ClearLabelOnOff(false);
                    GetButtonOnOff(true);
                }
                else
                {
                    ClearLabelOnOff(true);
                    GetButtonOnOff(false);
                }

                rewBottomView.SetStageText(string.Format("Stage {0}-{1}", thisCS.collectionSeq + 1, colIndex + 1));
                rewBottomView.SetButtonText(LocalizationManager.Localize(StringInfo.GET));
                rewBottomView.SetInfoText(string.Format(LocalizationManager.Localize(StringInfo.BounsStage), "\n"));
            }
            else
            {
                reqBottomViewOn(true);

                reqBottomView.SetStageText(string.Format("Stage {0}-{1}", thisCS.collectionSeq + 1, colIndex + 1));
                reqBottomView.SetInfoText(LocalizationManager.Localize(StringInfo.ReqInfo));
                //버튼 텍스트 아래에서 바꿔줌
                CreateRequireButtonImg(colIndex);
            }
        }
    }


    public void reqBottomViewOn(bool isReq)
    {
        if (isReq)
        {
            rewBottomView.gameObject.SetActive(false);
            reqBottomView.gameObject.SetActive(true);
        }
        else
        {
            rewBottomView.gameObject.SetActive(true);
            reqBottomView.gameObject.SetActive(false);
        }
    }

    //요구아이템 버튼 생성
    public void CreateRequireButtonImg(int colIndex)
    {
        int haveItemChk = 0;

        //요구아이템 순서대로 생성(3)
        for (int i = 0; i < thisCS.requireUHSeq[thisCS.colUHSeq[colIndex]].Count; i++)
        {
            int reqIdx = new int();
            reqIdx = i;
            ReqItemButtonUI reqItemButtonUI = reqBottomView.reqButtonsObj[i].GetComponent<ReqItemButtonUI>();
            reqItemButtonUI.itemImg.sprite = SetSprite(true, false, thisCS.requireUHSeq[thisCS.colUHSeq[colIndex]][i].seqName, thisCS.requireUHSeq[thisCS.colUHSeq[colIndex]][i].itemSeq);
            
            //가지고 있으면
            if (XMLManager.Instance.CheckIsHave(thisCS.requireUHSeq[thisCS.colUHSeq[colIndex]][i].seqName, thisCS.requireUHSeq[thisCS.colUHSeq[colIndex]][i].itemSeq))
            {
                reqItemButtonUI.itemButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.collection_itembox_on);
                reqItemButtonUI.itemButton.interactable = false;
                reqItemButtonUI.qMark.SetActive(false);
                reqItemButtonUI.deemImage.SetActive(false);
                //각 버튼에 리스너 달기?
                haveItemChk++;
            }
            else
            {
                reqItemButtonUI.itemButtonImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, StringInfo.collection_itembox_off);
                reqItemButtonUI.itemButton.interactable = true;
                reqItemButtonUI.qMark.SetActive(true);
                reqItemButtonUI.deemImage.SetActive(true);
                reqItemButtonUI.itemButton.onClick.RemoveAllListeners();
                reqItemButtonUI.itemButton.onClick.AddListener(() =>
                {
                    OnRequirePartClick(thisCS.requireUHSeq[thisCS.colUHSeq[colIndex]][reqIdx].seqName, thisCS.requireUHSeq[thisCS.colUHSeq[colIndex]][reqIdx].itemSeq);
                });
                //각 버튼에 리스너 달기
                //아이템 얻는 경로 설명
            }
        }
        //현재 레벨 아이템을 모두 가지고 있으면
        if (colIndex == thisCS.Level)
        {
            ClearLabelOnOff(false);

            if (haveItemChk == thisCS.requireUHSeq[thisCS.colUHSeq[colIndex]].Count)
            {
                reqBottomView.SetButtonText(LocalizationManager.Localize(StringInfo.GET));

                GetButtonOnOff(true);
            }
            else
            {
                reqBottomView.SetButtonText(string.Format("{0}/{1}", haveItemChk, thisCS.requireUHSeq[thisCS.colUHSeq[colIndex]].Count));

                GetButtonOnOff(false);
                //SetClearText(false);
            }
        }
        else
        {
            reqBottomView.SetButtonText(LocalizationManager.Localize(StringInfo.GET));
            ClearLabelOnOff(true);
            GetButtonOnOff(false);
        }
    }

    //요구 아이템 버튼 클릭
    public void OnRequirePartClick(string reqSeqName, string reqItemSeq)
    {
        if (!DoTweenUtil.isTweening)
        {
            //요구 아이템 확인 및 이동
            string reqText = null;
            int num = XMLManager.Instance.SelectBundleByItemSeq(reqSeqName, reqItemSeq);

            int state = -1;//0 상점보내기, 1 : 일일보상 보내기, 2 : 룰렛 보내기


            if (num != -1)
            {
                reqText = string.Format(LocalizationManager.Localize(StringInfo.ColText1), LocalizationManager.Localize(StringInfo.Bundle_Name[num]));
                state = 0;
            }
            if (XMLManager.Instance.IsDailyItemSeed(reqSeqName, reqItemSeq))
            {
                reqText = LocalizationManager.Localize(StringInfo.ColText2);
                state = 1;
            }
            if (XMLManager.Instance.IsWheelItemSeed(reqSeqName, reqItemSeq))
            {
                Debug.LogError("룰렛아이템은 원하는것을 얻기 힘드므로 뺀다");
                state = 2;
            }
            if (reqText == null)
            {
                Debug.LogError("확인되지 않는 아이템");
            }
            Debug.Log(state);

            Sprite sprite = SetSprite(true, false, reqSeqName, reqItemSeq);

            reqItemGetPopup(sprite, reqText, state);
            itemInfoPopupUI.gameObject.SetActive(true);
        }
    }

    public void reqItemGetPopup(Sprite sprite, string reqText, int state)
    {
        itemInfoPopupUI.SetStartAction(() =>
        {
            //itemInfoPopupUI.SetChar(thisCS.colUHSeq);
            itemInfoPopupUI.SetButtonText(LocalizationManager.Localize(StringInfo.GoBtn));//바로가기
            itemInfoPopupUI.SetImage(sprite);
            itemInfoPopupUI.SetText(reqText, 48);
        });
        itemInfoPopupUI.SetOnClickAction(() =>
        {
            //아이템 얻으러 가는 버튼 작업
            switch (state)
            {
                default:
                    Debug.LogError("위치 확인 실패");
                    break;
                case 0:
                    storePanel.SetActive(true);
                    InGameManager.Instance.EndEffect();
                    break;
                case 1:
                    dailyPanel.SetActive(true);
                    InGameManager.Instance.EndEffect();
                    break;
                case 2:
                    FortuneWheelPanel.SetActive(true);
                    InGameManager.Instance.EndEffect();
                    break;
            }
        }, false);
    }


    //획득상태 온오프
    public void GetButtonOnOff(bool on)
    {
        if (on)
        {
            rewBottomView.rewGetButton.interactable = true;
            rewBottomView.rewGetButton.GetComponent<Image>().color = Color.white;

            reqBottomView.reqGetButton.interactable = true;
            reqBottomView.reqGetButton.GetComponent<Image>().color = Color.white;
        }
        else
        {
            rewBottomView.rewGetButton.interactable = false;
            rewBottomView.rewGetButton.GetComponent<Image>().color = Color.gray;

            reqBottomView.reqGetButton.interactable = false;
            reqBottomView.reqGetButton.GetComponent<Image>().color = Color.gray;

        }
    }

    public void ClearLabelOnOff(bool on)
    {
        if (on)
        {
            reqBottomView.clearLabel.SetActive(true);
            rewBottomView.clearLabel.SetActive(true);
            reqBottomView.reqGetButton.gameObject.SetActive(false);
            rewBottomView.rewGetButton.gameObject.SetActive(false);
        }
        else
        {
            reqBottomView.clearLabel.SetActive(false);
            rewBottomView.clearLabel.SetActive(false);
            reqBottomView.reqGetButton.gameObject.SetActive(true);
            rewBottomView.rewGetButton.gameObject.SetActive(true);
        }
    }

    //획득 버튼 
    public void OnClickGetButton()
    {
        if (!DoTweenUtil.isTweening)
        {
            StartCoroutine(CoClearPopup());
        }
    }

    IEnumerator CoClearPopup()
    {
        if (reqBottomView.gameObject.activeSelf)
        {
            DontTouch.SetActive(true);
            arrowObj[thisCS.Level].GetComponent<ArrowImageUI>().SetImageColor(0, ObjectInfo.seemsRed);
            InGameManager.Instance.StartEffect(1, reqBottomView.reqButtonsObj[0].transform.position, 1f);
            AudioManager.Inst.PlaySFX("1");
            yield return new WaitForSeconds(0.5f);
            arrowObj[thisCS.Level].GetComponent<ArrowImageUI>().SetImageColor(1, ObjectInfo.seemsRed);
            InGameManager.Instance.StartEffect(1, reqBottomView.reqButtonsObj[1].transform.position, 1f);
            AudioManager.Inst.PlaySFX("3");
            yield return new WaitForSeconds(0.5f);
            arrowObj[thisCS.Level].GetComponent<ArrowImageUI>().SetImageColor(2, ObjectInfo.seemsRed);
            InGameManager.Instance.StartEffect(1, reqBottomView.reqButtonsObj[2].transform.position, 1f);
            AudioManager.Inst.PlaySFX("5");
            yield return new WaitForSeconds(0.7f);
            DontTouch.SetActive(false);
        }
        SetClearItemGetPopup();
        itemGetPopupUI.gameObject.SetActive(true);
        yield break;
    }

    //아이템 획득 
    public void SetClearItemGetPopup()
    {
        itemGetPopupUI.SetStartAction(() =>
        {
            //itemGetPopupUI.SetChar(thisCS.colUHSeq);
            Sprite sprite = SetSprite(true, false, thisCS.colUHSeq[thisCS.Level].seqName, thisCS.colUHSeq[thisCS.Level].itemSeq);
            
            itemGetPopupUI.SetImage(sprite);
            itemGetPopupUI.SetText(string.Format(LocalizationManager.Localize(StringInfo.ColGetPopup), thisCS.collectionSeq + 1, thisCS.Level + 1), 60);
        });
        itemGetPopupUI.SetOnClickAction(() =>
        {
            //컬렉션 외부에서 아이템 획득 반응
            InGameManager.Instance.AddItem(thisCS.colUHSeq[thisCS.Level].seqName, thisCS.colUHSeq[thisCS.Level].itemSeq, ItemGetRoot.collection);
            InGameManager.Instance.InitItemGet();
            //컬렉션 내에서 아이템 획득 반응
            XMLManager.Instance.SaveCollectionLevelUp(thisCS.collectionSeq.ToString());

            //컬렉션 레벨업
            if (thisCS.Level < thisCS.levelMax)
            {
            }

            //전체 컬렉션 클리어시 
            else if (thisCS.Level == thisCS.levelMax)
            {
                XMLManager.Instance.SaveCollectionIsClear(thisCS.collectionSeq.ToString(), true);
                //전체 컬렉션 클리어 반응 추가
                StartCoroutine(GetAllCollectionClearReward());
            }

            //바로 화면초기화
            InitCollectoinSets();
        });
    }

    //전체 클리어 
    //public void GetAllCollectionClearReward()
    //{
    //    SetAllClearItemGetPopup();
    //    itemGetPopupUI.gameObject.SetActive(true);
    //}
    //전체 클리어 
    IEnumerator GetAllCollectionClearReward()
    {
        yield return new WaitForSeconds(.5f);
        SetAllClearItemGetPopup();
        itemGetPopupUI.gameObject.SetActive(true);
    }

    //전체 클리어 팝업
    public void SetAllClearItemGetPopup()
    {
        Sprite rwSprite = SetSprite(true, false, thisCS.rewardSeq.seqName, thisCS.rewardSeq.itemSeq);

        itemGetPopupUI.SetStartAction(() =>
        {
            itemGetPopupUI.SetImage(rwSprite);
            itemGetPopupUI.SetText(LocalizationManager.Localize(StringInfo.ColAllClGetPopup), 48);
        });
        itemGetPopupUI.SetOnClickAction(() =>
        {
            //ingamemanager additem으로 특별 아이템 획득
            InGameManager.Instance.AddItem(thisCS.rewardSeq.seqName, thisCS.rewardSeq.itemSeq, ItemGetRoot.collection);
            InGameManager.Instance.InitItemGet();
            thisCS.isOn = false;
            XMLManager.Instance.SaveCollectionIsOn(thisCS.collectionSeq.ToString(), false);

            //화면초기화
            InitCollectoinSets();
            InitTopToggle();
        });
    }

    //클리어 이후 클리어 팝업 이미지만 보여주기
    public void SetAllClearItemGetPopupOnImage()
    {
        Sprite rwSprite = SetSprite(true, false, thisCS.rewardSeq.seqName, thisCS.rewardSeq.itemSeq);

        itemGetPopupUI.SetStartAction(() =>
        {
            itemGetPopupUI.SetImage(rwSprite);
            itemGetPopupUI.SetText(LocalizationManager.Localize(StringInfo.ColAllClGetPopup), 48);
        });
        itemGetPopupUI.SetOnClickAction(() =>
        {
        }, false);
    }
    #endregion


    #region 캐릭터 이미지 생성

    //캐릭터 하나 이미지 생성
    public void CreateCharImg()
    {
        for (int i = 0; i < middleRect.childCount; i++)
        {
            Destroy(middleRect.GetChild(i).gameObject);
        }
        root = Instantiate(prefabCharImgRoot, middleRect);
        root.transform.localScale = new Vector3(0.6f, 0.6f, 1);
        root.transform.SetAsFirstSibling();

        //기본 세팅 이미지 생성
        //몸
        tmpPart = XMLManager.Instance.UsePartByItemSeq(XMLManager.Instance.bodyInitSeq);
        CreatePartImg(false, tmpPart, root);
        //속옷
        tmpPart = XMLManager.Instance.UsePartByItemSeq(XMLManager.Instance.underInitSeq);
        CreatePartImg(false, tmpPart, root);

        //획득 아이템 이미지 생성(현재것까지 생성, 현재것은 딤처리)
        for (int i = 0; i <= thisCS.Level; i++)
        {
            if (i > thisCS.levelMax)
            {
                break;
            }
            SetOneColPart(i, thisCS.colUHSeq[i].seqName, thisCS.colUHSeq[i].itemSeq);
            //레벨보다 크면 딤처리한다
        }
        SetAllSibling(listPartSibling, listPartObj);
    }

    /// <summary>
    /// 파트 이미지 생성
    /// </summary>
    /// <param name="i">생성하는 파트의 단계</param>
    /// <param name="seqName"></param>
    /// <param name="itemSeq"></param>
    public void SetOneColPart(int i, string seqName, string itemSeq)
    {        
        switch (seqName)
        {
            default:
                Debug.LogError("컬렉션 이미지 생성 오류");
                break;
            case "item_seq":
                tmpPart = XMLManager.Instance.UsePartByItemSeq(itemSeq);
                imageObj = CreatePartImg(IsDeem(i, thisCS.Level), tmpPart, root);
                int handSeq = tmpPart.HandSeqToInt();
                int handType = tmpPart.handType;
                if (handSeq != -1)
                {
                    CharPart createCharPart;

                    if (handType == 0)
                    {
                        string createSeq = ParseItemSeq.MakeItemSeq((int)HandMatch.handOrder[HandMatch.kindNumToArrayNum(tmpPart.charPartItem.kind.kindType)], handSeq);
                        createCharPart = XMLManager.Instance.UsePartByItemSeq(createSeq);
                        CreatePartImg(IsDeem(i, thisCS.Level), createCharPart, root);
                    }
                    else
                    {
                        string createSeq = ParseItemSeq.MakeItemSeq((int)HandMatch.uHandOrder[HandMatch.kindNumToArrayNum(tmpPart.charPartItem.kind.kindType)], handSeq);
                        createCharPart = XMLManager.Instance.UsePartByItemSeq(createSeq);
                        CreatePartImg(IsDeem(i, thisCS.Level), createCharPart, root);
                    }
                }
                ItemEffect(i, true, imageObj);
                break;
            case "hair_seq":
                tmpPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.numToD3((int)OrderType.BackHair) + itemSeq);
                imageObj = CreatePartImg(IsDeem(i, thisCS.Level), tmpPart, root);

                ItemEffect(i, false, imageObj);

                tmpPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.numToD3((int)OrderType.FrontHair) + itemSeq);
                imageObj = CreatePartImg(IsDeem(i, thisCS.Level), tmpPart, root);

                ItemEffect(i, true, imageObj);

                tmpPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.numToD3((int)OrderType.SideHair) + itemSeq);
                imageObj = CreatePartImg(IsDeem(i, thisCS.Level), tmpPart, root);
                ItemEffect(i, false, imageObj);

                break;
            case "eyes_seq":
                tmpPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.numToD3((int)OrderType.EyeLeft) + itemSeq);
                imageObj = CreatePartImg(IsDeem(i, thisCS.Level), tmpPart, root);
                ItemEffect(i, true,imageObj);

                tmpPart = XMLManager.Instance.UsePartByItemSeq(ParseItemSeq.numToD3((int)OrderType.EyeRight) + itemSeq);
                imageObj = CreatePartImg(IsDeem(i, thisCS.Level), tmpPart, root);
                ItemEffect(i, true,imageObj);

                break;
            case "item_set":
                int num = int.Parse(itemSeq);

                for (int j = 0; j < CollectionSeed.colItemSetUH[num].Count; j++)
                {
                    string newSeqName = CollectionSeed.colItemSetUH[num][j].seqName;
                    string newItemSeq = CollectionSeed.colItemSetUH[num][j].itemSeq;
                    //재귀타서 아이템 입히기
                    Debug.Log(newSeqName + newItemSeq);
                    SetOneColPart(i, newSeqName, newItemSeq);
                }
                break;

            case "bg_seq":
                imageObj = InitAllObj(IsDeem(i, thisCS.Level), seqName, itemSeq, FriendType.BG);
                ItemEffect(i, true, imageObj);
                break;
            case "object1_seq":
                imageObj = InitAllObj(IsDeem(i, thisCS.Level), seqName, itemSeq, FriendType.Object1);
                ItemEffect(i, true, imageObj);
                break;
            case "object2_seq":
                imageObj = InitAllObj(IsDeem(i, thisCS.Level), seqName, itemSeq, FriendType.Object2);
                ItemEffect(i, true, imageObj);
                break;
            case "animal_seq":
                imageObj = InitAllObj(IsDeem(i, thisCS.Level), seqName, itemSeq, FriendType.Animal);
                ItemEffect(i, true, imageObj);
                break;
            case "bubble_seq":
                imageObj = InitAllObj(IsDeem(i, thisCS.Level), seqName, itemSeq, FriendType.Bubble);
                ItemEffect(i, true, imageObj);
                break;
            case "text_seq":
                imageObj = InitAllObj(IsDeem(i, thisCS.Level), seqName, itemSeq, FriendType.Text);
                ItemEffect(i, true, imageObj);
                break;
        }
    }

    public void ItemEffect(int i, bool isEffect, GameObject itemImage)
    {
        //클리어 상태에서는 다른 이펙트를 띄워준다
        if (!thisCS.isClear)
        {
            //현재 고른 것 이전 아이템, 현재 고른것이 현재 레벨일때(현재 레벨일때 아이템 딤처리, 바로 이전레벨이 가장 최근에 얻은것)
            if (isFirst)
            {
                //레벨 상승시 자동 선택
                if ((i == nowColIdx - 1 && nowColIdx == thisCS.Level))
                {
                    DoTweenUtil.DoFadeImage(0, 1, 2f, itemImage);
                    DoTweenUtil.DoSizeImage(3, 1, 1f, itemImage);
                    if (isEffect)
                        InGameManager.Instance.StartEffect(2, itemImage.transform.position, 4f);
                }
            }
            else {
                //현레벨에서 직접 선택
                if ((i == nowColIdx && nowColIdx == thisCS.Level))
                {
                    DoTweenUtil.DoFadeImage(0, 1, 2f, itemImage);
                    DoTweenUtil.DoSizeImage(3, 1, 1f, itemImage);
                    if (isEffect)
                        InGameManager.Instance.StartEffect(2, itemImage.transform.position, 4f);
                }
            }
        }
        else
        {
            if (thisCS.isOn)
            {
                if (i == nowColIdx-1 && thisCS.Level >= thisCS.levelMax)
                {
                    InGameManager.Instance.StartEffect(2, itemImage.transform.position, 4f);
                }
            }
        }
    }

    //파트 하나 이미지 생성
    public GameObject CreatePartImg(bool deem, CharPart element, GameObject root)
    {
        if (element.multiOrder == -1)
        {
            GameObject obj = Instantiate(prefabPartImg, root.transform);
            CharPartUse tmpCharPart = obj.GetComponent<CharPartUse>();
            Image image = obj.GetComponent<Image>();

            tmpCharPart.charPart = element;
            image.sprite = SetSprite(false, deem, "item_seq", element.charPartItem.seq);
            //tmpCharPart.SetImageSprite();
            tmpCharPart.SetImagePosition();

            int siblingNum = tmpCharPart.SetSortingOrder();

            listPartSibling.Add(siblingNum);
            listPartObj.Add(siblingNum, obj);

            return obj;
        }
        else
        {
            //멀티루트 제작
            GameObject obj;
            int siblingNum = (int)element.charPartItem.order.orderType;
            if (!listPartObj.ContainsKey(siblingNum))
            {
                obj = Instantiate(prefabMultiRootImg, root.transform);
                listPartObj.Add(siblingNum, obj);
            }
            else
            {
                obj = listPartObj[siblingNum];
            }
            SortingGroup sg = obj.GetComponent<SortingGroup>();
            listPartSibling.Add(siblingNum);

            sg.sortingOrder = (int)element.charPartItem.order.orderType;

            //파트이미지 제작
            GameObject part = Instantiate(prefabPartImg, obj.transform);
            CharPartUse tmpCharPart = part.GetComponent<CharPartUse>();
            Image image = obj.GetComponent<Image>();

            tmpCharPart.charPart = element;
            image.sprite = SetSprite(false, deem, "item_seq", element.charPartItem.seq);
            //tmpCharPart.SetImageSprite();
            tmpCharPart.SetImagePosition();

            return part;
        }
    }


    //모든 오브젝트 버튼 내부에 올리기 
    public GameObject InitAllObj(bool isDeem, string seqName, string typeSeq, FriendType friendType)
    {
        Dictionary<int, ItemPart> tmpObj = new Dictionary<int, ItemPart>();
        tmpObj = XMLManager.Instance.listAllObjDic[(int)friendType];

        //GameObject root = Instantiate(prefabCharImgRoot, middleRect);
        //root.transform.SetAsFirstSibling();

        GameObject obj = Instantiate(prefabImgObject, middleRect);
        SetObjRender(isDeem, seqName, typeSeq, obj);

        for(int k = 0; k < objectNameList.Count; k++)
        {
            if(typeSeq == objectNameList[k])
            {
                obj.transform.localPosition = objectTrList[k].localPosition;
            }
        }
        //obj.transform.localPosition = objectTrList[k].localPosition;
        return obj;
    }

    public void SetObjRender(bool isDeem, string seqName, string imgName, GameObject root)
    {
        Image objSprite = root.GetComponentInChildren<Image>();
        objSprite.sprite = SetSprite(false, isDeem, seqName, imgName);
    }

    //레이어
    public void SetAllSibling(List<int> partSibling, Dictionary<int, GameObject> objSibling)
    {
        partSibling.Sort();

        for (int i = 0; i < partSibling.Count; i++)
        {
            objSibling[partSibling[i]].transform.SetAsLastSibling();
        }
        partSibling.Clear();
        objSibling.Clear();
    }

    #endregion

    /// <summary>
    /// 이미지 생성, 딤처리 포함
    /// 이미지는 AllCollectionAtlas에서 먼저 찾고 없으면 기본 파트에서 찾는다
    /// </summary>
    /// <param name="isBtn">버튼이미지유무</param>
    /// <param name="isDeem">딤 처리유무</param>
    /// <param name="seqName"></param>
    /// <param name="itemSeq"></param>
    /// <returns></returns>
    public Sprite SetSprite(bool isBtn, bool isDeem, string seqName, string itemSeq)
    {
        Sprite sprite = null;
        Sprite colSprite = null;
        string spName = null;
        string add = "";
        string deem = "";

        add = isBtn ? "_Btn" : "";
        deem = isDeem ? "_Deem" : "";

        switch (seqName)
        {
            default:
                Debug.LogError("컬렉션 버튼이미지 오류");
                break;
            case "obj_type_seq":
                spName = "obj_type_seq_" + itemSeq;
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, spName + add + deem);
                if (colSprite == null)
                {
                    Debug.LogError("obj_type_seq 이미지 오류");
                }
                else sprite = colSprite;
                break;
            case "item_seq":
                spName = ParseItemSeq.ItemSeqToStringSpName(itemSeq);

                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, spName + add + deem);
                if (colSprite == null)
                {
                    if (isBtn)
                    {
                        sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, ParseItemSeq.ItemSeqToStringSpName(itemSeq));
                    }
                    else
                    {
                        sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllPartsAtlas, ParseItemSeq.ItemSeqToStringSpName(itemSeq));
                    }
                }
                else sprite = colSprite;
                break;
            case "hair_seq":
                spName = "Hair_" + int.Parse(itemSeq);

                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, spName + add + deem);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "Hair_" + int.Parse(itemSeq));
                }
                else sprite = colSprite;
                break;
            case "eyes_seq":
                spName = "Eyes_" + int.Parse(itemSeq);
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, spName + add + deem);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "Eyes_" + int.Parse(itemSeq));
                }
                else sprite = colSprite;
                break;

            case "item_set":
                spName = "ItemSet_" + int.Parse(itemSeq);
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, spName + add + deem);
                if (colSprite == null)
                {
                    Debug.LogError("아이템 여러개 주는 것은 항상 스프라이트 새로 만들기");
                    //굉장히 긴 코드를 추가해야하는 문제가 생김
                }
                else sprite = colSprite;
                break;
            case "bg_seq":
                spName = itemSeq;
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, spName + add + deem);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllBGAtlas, itemSeq);
                }
                else sprite = colSprite;
                break;
            case "object1_seq":
                spName = itemSeq;
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, spName + add + deem);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllObject1Atlas, itemSeq);
                }
                else sprite = colSprite;
                break;
            case "object2_seq":
                spName = itemSeq;
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, spName + add + deem);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllObject2Atlas, itemSeq);
                }
                else sprite = colSprite;
                break;
            case "animal_seq":
                spName = itemSeq;
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, spName + add + deem);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllAnimalAtlas, itemSeq);
                }
                else sprite = colSprite;
                break;
            case "bubble_seq":
                spName = itemSeq;
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, spName + add + deem);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllBubbleAtlas, itemSeq);
                }
                else sprite = colSprite;
                break;
            case "text_seq":
                spName = itemSeq;
                colSprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllCollectionAtlas, spName + add + deem);

                if (colSprite == null)
                {
                    sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllTextAtlas, itemSeq);
                }
                else sprite = colSprite;
                break;
        }
        return sprite;
    }

    public bool IsDeem(int startIdx, int endIdx)
    {
        if (startIdx < endIdx)
        {
            return false;
        }
        return true;
    }

    public void Dismiss()
    {
        if (!DoTweenUtil.isTweening)
        {
            collectionPanel.SetActive(false);
            InGameManager.Instance.rewardButtonUI.SetCollectionCheck();
            InGameManager.Instance.rewardButtonUI.SetCheckImg3();
            InGameManager.Instance.EndEffect();
        }
    }
}
