﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReqItemButtonUI : MonoBehaviour
{
    public Button itemButton;
    public Image itemButtonImg;
    public Image itemImg;
    public GameObject deemImage;
    public GameObject qMark;
}
