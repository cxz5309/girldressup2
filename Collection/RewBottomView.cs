﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewBottomView : MonoBehaviour
{
    public Button rewGetButton;
    public Text rewGetButtonText;
    public Text rewStageText;
    public GameObject newImageObj;
    public Text rewInfoText;
    public GameObject clearLabel;

    public void SetButtonText(string text)
    {
        rewGetButtonText.text = text;
    }

    public void SetStageText(string text)
    {
        rewStageText.text = text;
    }

    public void SetInfoText(string text)
    {
        rewInfoText.text = text;
    }
}