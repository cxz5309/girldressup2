﻿using System.Collections;
using System.Collections.Generic;
using Assets.SimpleLocalization;
using UnityEngine;
using UnityEngine.UI;

public class GoADView : MonoBehaviour
{
    public GameObject goADPanel;
    public GameObject StorePanel;
    //public Text itemText;
    //public GameObject itemTextLabelImg;

    //bundle
    public Image goStorePopupImg;

    public Text bottomContextText;
    public Text sideContextText;
    public Text priceText;
    public Text goldText;

    private void OnEnable()
    {
        DoTweenUtil.DoPopupOpen(.2f, 1f, .4f, transform);
    }

    //bundleNum default : -1(랜덤일때)
    public void PopupGoADPanel(bool isRand, int bundleNum)
    {
        goADPanel.SetActive(true);
        SetStoreUI(isRand, bundleNum);
    }

    public void OnGoStoreButtonClick()
    {
        if (!DoTweenUtil.isTweening)
        {
            StorePanel.SetActive(true);
            goADPanel.SetActive(false);
        }
    }

    //상점 ui 만들기
    public void SetStoreUI(bool isRand, int bundleNum)
    {
        string bundleName = null;
        string bundleButtonName = null;
        int bundleSeq;

        //UI 정보 초기화
        if (isRand)
        {
            bundleSeq = (int)Random.Range(0, StringInfo.Bundle_Button_Name.Length);
            bundleName = LocalizationManager.Localize(StringInfo.Bundle_Name[bundleSeq]);
            bundleButtonName = StringInfo.Bundle_Button_Name[bundleSeq];
            //itemTextLabelImg.SetActive(false);
        }
        else
        {
            if (bundleNum == -1)
            {
                bundleSeq = (int)Random.Range(0, StringInfo.Bundle_Button_Name.Length);
                bundleName = LocalizationManager.Localize(StringInfo.Bundle_Name[bundleSeq]);
                bundleButtonName = StringInfo.Bundle_Button_Name[bundleSeq];
            }
            else
            {
                bundleSeq = bundleNum;

                bundleName = LocalizationManager.Localize(StringInfo.Bundle_Name[bundleSeq]);
                bundleButtonName = StringInfo.Bundle_Button_Name[bundleSeq];
            }
            //itemTextLabelImg.SetActive(true);
            //itemText.text = bundleName;
        }
        if (bundleButtonName == "" || bundleButtonName == null)
        {
            Debug.LogError("상점이름 없음");
        }
        //실제 UI
        InitBundleButton(bundleName, bundleButtonName, bundleSeq);
    }


    public void InitBundleButton(string bundleName, string bundleButtonName, int bundleSeq)
    {
        goStorePopupImg.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, bundleButtonName);

        switch (bundleSeq)
        {
            default:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                bottomContextText.gameObject.SetActive(true);
                sideContextText.gameObject.SetActive(false);
                bottomContextText.text = bundleName;
                break;
            case 0:
            case 1:
                bottomContextText.gameObject.SetActive(false);
                sideContextText.gameObject.SetActive(true);
                sideContextText.text = bundleName;
                break;
        }

        priceText.text = string.Format("{0} {1}", XMLManager.Instance.LoadBundleCoin(bundleSeq.ToString()).ToString(), StringInfo.StoreGold);
        //goldText.text = StringInfo.StoreGold;
        //goldText.fontSize = 36;
    }

    public void DisMiss()
    {
        if (!DoTweenUtil.isTweening)
        {
            goADPanel.SetActive(false);
        }
    }
}
