﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class ADButtonUI : MonoBehaviour
{
    public GoADView goADView;
    public ItemGetPopupUI itemGetPopupUI;
    public SAlertViewUI sAlertView;
    //public Text secondText;

    //private void OnEnable()
    //{
    //    InGameManager.Instance.adTimerText = secondText;
    //}

    //private void Start()
    //{
    //    if (InGameManager.Instance.adUpdateTime >= ObjectInfo.ITEM_COOL_TIME)
    //    {
    //        InGameManager.Instance.adTimerText.text = StringInfo.GO;
    //        InGameManager.Instance.adUpdateTime = ObjectInfo.ITEM_COOL_TIME;
    //        InGameManager.Instance.canAD = true;
    //    }
    //    else
    //    {
    //        InGameManager.Instance.canAD = false;
    //    }
    //}

    //private void Update()
    //{
    //    if (InGameManager.Instance != null)
    //    {
    //        if (InGameManager.Instance.adTimerText != null)
    //        {
    //            if (!InGameManager.Instance.canAD)
    //            {
    //                if (InGameManager.Instance.adUpdateTime >= ObjectInfo.ITEM_COOL_TIME)
    //                {
    //                    InGameManager.Instance.adTimerText.text = StringInfo.GO;
    //                    InGameManager.Instance.adUpdateTime = ObjectInfo.ITEM_COOL_TIME;
    //                    InGameManager.Instance.canAD = true;
    //                    return;
    //                }

    //                InGameManager.Instance.adTimerText.text = string.Format(StringInfo.WheelTimeFormat, InGameManager.Instance.adMin, InGameManager.Instance.adSecond);
    //            }
    //        }
    //    }
    //}
    public void OnADViewButtonClick()
    {
        goADView.PopupGoADPanel(true, -1);
    }

    public void OnADButtonClick()
    {
        AdsManager.Instance.WatchVideoWithGetGold((adsCallbackState) =>
        {
            switch (adsCallbackState)
            {
                case AdsManager.AdsCallbackState.Success:
                    itemGetPopupUI.SetStartAction(() =>
                    {
                        itemGetPopupUI.SetText(ObjectInfo.AD_REWARD.ToString() + " " + LocalizationManager.Localize(StringInfo.COIN) + " " + LocalizationManager.Localize(StringInfo.GET_IT));
                        itemGetPopupUI.SetImage(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "coin_big"));
                        InGameManager.Instance.StartEffect(5, itemGetPopupUI.coinStartPos.position);
                    });
                    itemGetPopupUI.SetOnClickAction(() =>
                    {
                        InGameManager.Instance.AddCoins(ObjectInfo.AD_REWARD);
                        XMLManager.Instance.SaveMissionValueUp(XMLManager.Instance.nodeShowADTimes);
                        MissionSeed.CheckAllMission();
                        AudioManager.Inst.PlaySFX("gaining-bonus-points-cut2");
                        InGameManager.Instance.StartEffect(3, itemGetPopupUI.coinStartPos.position);
                    });
                    itemGetPopupUI.gameObject.SetActive(true);
                    break;
                case AdsManager.AdsCallbackState.Loading:
                    sAlertView.SAlert(LocalizationManager.Localize(StringInfo.ADLoadAd));
                    break;
            }
        });

        //InGameManager.Instance.canAD = true;//임시

        //if (InGameManager.Instance.canAD)
        //{
        //    itemGetPopupUI.SetStartAction(() =>
        //    {
        //        itemGetPopupUI.SetText(ObjectInfo.AD_REWARD.ToString() + " " + LocalizationManager.Localize(StringInfo.COIN) + " " + LocalizationManager.Localize(StringInfo.GET_IT));
        //        itemGetPopupUI.SetImage(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "coin_big"));
        //        InGameManager.Instance.StartEffect(5, itemGetPopupUI.coinStartPos.position);
        //    });
        //    itemGetPopupUI.SetOnClickAction(() =>
        //    {
        //        InGameManager.Instance.AddCoins(ObjectInfo.AD_REWARD);
        //        XMLManager.Instance.SaveMissionValueUp(XMLManager.Instance.nodeShowADTimes);
        //        MissionSeed.CheckAllMission();
        //        AudioManager.Inst.PlaySFX("gaining-bonus-points-cut2");
        //        InGameManager.Instance.StartEffect(3, itemGetPopupUI.coinStartPos.position);
        //    });
        //    itemGetPopupUI.gameObject.SetActive(true);
        //    //InitCooldownStart();
        //}
        //else
        //{
        //    //AlertCooldown();
        //}
    }

    //public void InitCooldownStart()
    //{
    //    InGameManager.Instance.canAD = false;
    //    InGameManager.Instance.adStartTime = Time.realtimeSinceStartup;
    //    InGameManager.Instance.adUpdateTime = 0;//업데이트 되자마자 조건 충족하여 canSlider true 되는 문제해결
    //}
    //public void AlertCooldown()
    //{
    //    sAlertView.SAlert(StringInfo.WheelADInfo);
    //}
}
