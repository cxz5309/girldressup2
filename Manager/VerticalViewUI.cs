using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class VerticalViewUI : MonoBehaviour
{
    public MainCanvas goMainCanvas;

    public Transform verticalContentView;

    //배경
    public GameObject prefabItemTypeButton;

    //텍스트
    public GameObject fontSettingPanel;
    public GameObject textSettingButton;

    public AlertView alertView;

    public int groupSeq;
    public int charBoxCount;


    //타입에 따른 해금
    Queue<ItemPart> itemPartType1Q = new Queue<ItemPart>();
    Queue<ItemPart> itemPartType2Q = new Queue<ItemPart>();
    Queue<ItemPart> storeQ = new Queue<ItemPart>();
    Queue<ItemPart> specialQ = new Queue<ItemPart>();

    StringBuilder stringBuilder = new StringBuilder();

    public void ClearViewContents()
    {
        for (int i = 0; i < verticalContentView.childCount; i++)
        {
            Destroy(verticalContentView.GetChild(i).gameObject);
        }
    }
    //메뉴에 맞는 모든 오브젝트 버튼 생성하기
    public void CreateAllObjButton(FriendType nowObjMenu)
    {
        Dictionary<int, ItemPart> tmpObj = new Dictionary<int, ItemPart>();

        groupSeq = (int)nowObjMenu;

        tmpObj = XMLManager.Instance.listAllObjDic[groupSeq];

        //배경 초기화 버튼
        if (nowObjMenu == FriendType.BG)
        {
            GameObject ClearBGButton = Instantiate(prefabItemTypeButton, verticalContentView);
            ItemTypeButtonUI itemTypeButtonUI = ClearBGButton.GetComponent<ItemTypeButtonUI>();

            Sprite sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_remove_n");
            itemTypeButtonUI.ChangeButtonBackImage(sprite);
            itemTypeButtonUI.UnsetButtonImage();
            itemTypeButtonUI.SetbuttonBackImage();
            ClearBGButton.GetComponent<Button>().onClick.AddListener(() =>
            {
                InMainManager.Instance.UnSetBG();
            });
        }
        //말풍선 세팅 버튼
        else if (nowObjMenu == FriendType.Bubble)
        {
            textSettingButton = Instantiate(prefabItemTypeButton, verticalContentView);
            ItemTypeButtonUI itemTypeButtonUI = textSettingButton.GetComponent<ItemTypeButtonUI>();

            Sprite sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "icon_cp");
            itemTypeButtonUI.ChangeButtonImage(sprite);
            itemTypeButtonUI.SetButtonImage();

            Color color = ObjectInfo.arrayTextColor[PlayerPrefs.GetInt(StringInfo.playerPrefsKeyList[4])];
            itemTypeButtonUI.ChangeButtonColor(color);
            itemTypeButtonUI.SetButtonImage();
            itemTypeButtonUI.UnsetbuttonBackImage();

            textSettingButton.GetComponent<Button>().onClick.AddListener(() =>
            {
                OntextSettingButtonClick();
            });

            //GameObject textButton = Instantiate(prefabObj, verticalContentView);
            //textButton.GetComponent<Button>().onClick.AddListener(() =>
            //{
            //    InMainManager.Instance.CreateText(groupSeq);
            //});
        }

        //상점, 스페셜아이템 뒤로 빼기
        itemPartType1Q.Clear();
        itemPartType2Q.Clear();
        storeQ.Clear();
        specialQ.Clear();

        List<int> keys = new List<int>(tmpObj.Keys);

        for (int i = 0; i < keys.Count; i++)
        {
            int seq = keys[i];

            if (tmpObj[seq].itemType == ItemType.storeItem)
            {
                itemPartType1Q.Enqueue(tmpObj[seq]);
                continue;
            }
            else if (tmpObj[seq].itemType == ItemType.spItem)
            {
                itemPartType2Q.Enqueue(tmpObj[seq]);
                continue;
            }
            switch (nowObjMenu)
            {
                case FriendType.BG:
                    if (seq == -1) continue;
                    CreateBGButton(tmpObj[seq], false);
                    break;
                case FriendType.Object1:
                    CreateObj1Button(tmpObj[seq], false);
                    break;
                case FriendType.Object2:
                    CreateObj2Button(tmpObj[seq], false);
                    break;
                case FriendType.Animal:
                    CreateAnimalButton(tmpObj[seq], false);
                    break;
                case FriendType.Text:
                    if (seq == 0) continue;
                    CreateTextButton(tmpObj[seq], false);
                    break;
                case FriendType.Bubble:
                    CreateBubbleButton(tmpObj[seq], false);
                    break;
            }
        }
        //구매아이템 생성, 보유하지 않은 아이템은 큐에넣기 
        while (itemPartType1Q.Count > 0)
        {
            ItemPart tmpPart = itemPartType1Q.Dequeue();
            if (XMLManager.Instance.CheckIsHave(tmpPart.seqName, tmpPart.typeSeq))
            {
                GameObject obj;
                switch (nowObjMenu)
                {
                    default:
                        obj = null;
                        Debug.LogError("ItemPart가 명시되지 않음");
                        break;
                    case FriendType.BG:
                        obj = CreateBGButton(tmpPart, false);
                        break;
                    case FriendType.Object1:
                        obj = CreateObj1Button(tmpPart, false);
                        break;
                    case FriendType.Object2:
                        obj = CreateObj2Button(tmpPart, false);
                        break;
                    case FriendType.Animal:
                        obj = CreateAnimalButton(tmpPart, false);
                        break;
                    case FriendType.Text:
                        obj = CreateTextButton(tmpPart, false);
                        break;
                    case FriendType.Bubble:
                        obj = CreateBubbleButton(tmpPart, false);
                        break;
                }
                ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();
                itemTypeButtonUI.ChangeItemTypeImage(ItemType.storeItem);
                if (nowObjMenu == FriendType.BG)
                {
                    itemTypeButtonUI.ChangeButtonBackUIColor(Color.white);
                }
                else
                {
                    itemTypeButtonUI.ChangeButtonColor(Color.white);
                }
                itemTypeButtonUI.ChangeStoreColor(false);
                obj.GetComponent<Image>().color = Color.white;
            }
            else
            {
                storeQ.Enqueue(tmpPart);
            }
        }
        //스페셜아이템 생성, 보유하지 않은 아이템은 큐에 넣기
        while (itemPartType2Q.Count > 0)
        {
            ItemPart tmpPart = itemPartType2Q.Dequeue();

            if (XMLManager.Instance.CheckIsHave(tmpPart.seqName, tmpPart.typeSeq))
            {
                GameObject obj;
                switch (nowObjMenu)
                {
                    default:
                        obj = null;
                        Debug.LogError("ItemPart가 명시되지 않음");
                        break;
                    case FriendType.BG:
                        obj = CreateBGButton(tmpPart, false);
                        break;
                    case FriendType.Object1:
                        obj = CreateObj1Button(tmpPart, false);
                        break;
                    case FriendType.Object2:
                        obj = CreateObj2Button(tmpPart, false);
                        break;
                    case FriendType.Animal:
                        obj = CreateAnimalButton(tmpPart, false);
                        break;
                    case FriendType.Text:
                        obj = CreateTextButton(tmpPart, false);
                        break;
                    case FriendType.Bubble:
                        obj = CreateBubbleButton(tmpPart, false);
                        break;
                }
                ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();
                itemTypeButtonUI.ChangeItemTypeImage(ItemType.spItem);
                if (nowObjMenu == FriendType.BG)
                {
                    itemTypeButtonUI.ChangeButtonBackUIColor(Color.white);
                }
                else
                {
                    itemTypeButtonUI.ChangeButtonColor(Color.white);
                }
                itemTypeButtonUI.ChangeStoreColor(false);
                obj.GetComponent<Image>().color = Color.white;
            }
            else
            {
                specialQ.Enqueue(tmpPart);
            }
        }
        //보유하지않은 구매아이템 생성, 스페셜은 안나타남/ 딤처리와 상점처리할것@@@@@@@@@@
        while (storeQ.Count > 0)
        {
            ItemPart tmpPart = storeQ.Dequeue();
            GameObject obj;
            switch (nowObjMenu)
            {
                default:
                    obj = null;
                    Debug.LogError("ItemPart가 명시되지 않음");
                    break;
                case FriendType.BG:
                    obj = CreateBGButton(tmpPart, true);
                    break;
                case FriendType.Object1:
                    obj = CreateObj1Button(tmpPart, true);
                    break;
                case FriendType.Object2:
                    obj = CreateObj2Button(tmpPart, true);
                    break;
                case FriendType.Animal:
                    obj = CreateAnimalButton(tmpPart, true);
                    break;
                case FriendType.Text:
                    obj = CreateTextButton(tmpPart, true);
                    break;
                case FriendType.Bubble:
                    obj = CreateBubbleButton(tmpPart, true);
                    break;
            }
            ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();
            itemTypeButtonUI.ChangeItemTypeImage(tmpPart.itemType);
            if (nowObjMenu == FriendType.BG)
            {
                itemTypeButtonUI.ChangeButtonBackUIColor(Color.gray);
            }
            else
            {
                itemTypeButtonUI.ChangeButtonColor(Color.gray);
            }
            itemTypeButtonUI.ChangeStoreColor(true);
            obj.GetComponent<Image>().color = Color.gray;
        }
    }

    //배경 버튼
    public GameObject CreateBGButton(ItemPart itemPart, bool isCost)
    {
        GameObject obj = Instantiate(prefabItemTypeButton, verticalContentView);
        ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

        Sprite sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, itemPart.typeSeq);
        itemTypeButtonUI.ChangeButtonBackImage(sprite);
        itemTypeButtonUI.UnsetButtonImage();
        itemTypeButtonUI.SetbuttonBackImage();
        obj.GetComponent<Image>().color = isCost == true ? Color.gray : Color.white;

        obj.GetComponent<Button>().onClick.AddListener(() =>
        {
            if (isCost)
            {
                AlertAD(itemPart.seqName, itemPart.typeSeq);
                return;
            }
            InMainManager.Instance.SetBG(int.Parse(itemPart.itemSeq));
        });
        return obj;
    }

    //obj1 버튼
    public GameObject CreateObj1Button(ItemPart itemPart, bool isCost)
    {
        GameObject obj = Instantiate(prefabItemTypeButton, verticalContentView);
        ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

        Sprite sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, itemPart.typeSeq);
        itemTypeButtonUI.ChangeButtonImage(sprite);
        itemTypeButtonUI.UnsetbuttonBackImage();
        obj.GetComponent<Image>().color = isCost == true ? Color.gray : Color.white;

        obj.GetComponent<Button>().onClick.AddListener(() =>
        {
            if (isCost)
            {
                AlertAD(itemPart.seqName, itemPart.typeSeq);
                return;
            }
            InMainManager.Instance.CreateObj1(groupSeq, int.Parse(itemPart.itemSeq), itemPart.typeSeq);
        });

        return obj;
    }

    //obj2버튼
    public GameObject CreateObj2Button(ItemPart itemPart, bool isCost)
    {
        GameObject obj = Instantiate(prefabItemTypeButton, verticalContentView);
        ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

        Sprite sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, itemPart.typeSeq);
        itemTypeButtonUI.ChangeButtonImage(sprite);
        itemTypeButtonUI.UnsetbuttonBackImage();
        obj.GetComponent<Image>().color = isCost == true ? Color.gray : Color.white;

        obj.GetComponent<Button>().onClick.AddListener(() =>
        {
            if (isCost)
            {
                AlertAD(itemPart.seqName, itemPart.typeSeq);
                return;
            }
            InMainManager.Instance.CreateObj2(groupSeq, int.Parse(itemPart.itemSeq), itemPart.typeSeq);
        });

        return obj;
    }

    //동물 버튼
    public GameObject CreateAnimalButton(ItemPart itemPart, bool isCost)
    {

        GameObject obj = Instantiate(prefabItemTypeButton, verticalContentView);
        ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

        Sprite sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, itemPart.typeSeq);
        itemTypeButtonUI.ChangeButtonImage(sprite);
        itemTypeButtonUI.UnsetbuttonBackImage();
        obj.GetComponent<Image>().color = isCost == true ? Color.gray : Color.white;

        obj.GetComponent<Button>().onClick.AddListener(() =>
        {
            if (isCost)
            {
                AlertAD(itemPart.seqName, itemPart.typeSeq);
                return;
            }
            InMainManager.Instance.CreateAnimal(groupSeq, int.Parse(itemPart.itemSeq), itemPart.typeSeq);
        });

        return obj;
    }

    //말풍선 버튼
    public GameObject CreateBubbleButton(ItemPart itemPart, bool isCost)
    {
        GameObject obj = Instantiate(prefabItemTypeButton, verticalContentView);
        ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

        Sprite sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, itemPart.typeSeq);
        itemTypeButtonUI.ChangeButtonImage(sprite);
        itemTypeButtonUI.buttonImage.transform.localScale = new Vector3(1.2f, 1.2f, 1f);
        itemTypeButtonUI.UnsetbuttonBackImage();
        obj.GetComponent<Image>().color = isCost == true ? Color.gray : Color.white;

        obj.GetComponent<Button>().onClick.AddListener(() =>
        {
            if (isCost)
            {
                AlertAD(itemPart.seqName, itemPart.typeSeq);
                return;
            }
            InMainManager.Instance.CreateBubble(groupSeq, int.Parse(itemPart.itemSeq), itemPart.typeSeq);
        });

        return obj;
    }


    //텍스트 버튼
    public GameObject CreateTextButton(ItemPart itemPart, bool isCost)
    {

        GameObject obj = Instantiate(prefabItemTypeButton, verticalContentView);
        ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

        Sprite sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllTextAtlas, itemPart.typeSeq);
        itemTypeButtonUI.ChangeButtonImage(sprite);
        itemTypeButtonUI.buttonImage.transform.localScale = new Vector3(0.8f, 0.8f, 1f);
        itemTypeButtonUI.UnsetbuttonBackImage();
        obj.GetComponent<Image>().color = isCost == true ? Color.gray : Color.white;

        obj.GetComponent<Button>().onClick.AddListener(() =>
        {
            if (isCost)
            {
                AlertAD(itemPart.seqName, itemPart.typeSeq);
                return;
            }
            InMainManager.Instance.CreateObjectText(groupSeq, int.Parse(itemPart.itemSeq), itemPart.typeSeq);
        });

        return obj;
    }

    //텍스트 세팅버튼
    public void OntextSettingButtonClick()
    {
        if (fontSettingPanel.activeSelf)
        {
            fontSettingPanel.SetActive(false);
        }
        else
        {
            fontSettingPanel.SetActive(true);
        }
    }

    public void AlertAD(string seqName, string itemSeq)
    {
        goMainCanvas.goADView.PopupGoADPanel(false, XMLManager.Instance.SelectBundleByItemSeq(seqName, itemSeq));
    }
}
