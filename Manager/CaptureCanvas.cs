﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.SimpleLocalization;
using UnityEngine;
using UnityEngine.Android;

public class CaptureCanvas : MonoBehaviour
{
    public GameObject mainCanvas;

    public AlertView alertView;

    //캡쳐
    public Camera mainCamera;
    public Camera tranRenderCamera;
    public Camera fullRenderCamera;
    Camera renderCamera;

    public GameObject goBG;
    public TransSliderUI transSliderUI;

    public GameObject dontTouch;

    private Texture2D[] captureTexs = new Texture2D[2];
    Texture2D captureTex = null;

    int resWidth;
    int resHeight;
    RenderTexture rt;
    Rect rect = new Rect();

    public bool transparency;

    private void Start()
    {
        resWidth = Screen.width;
        resHeight = Screen.height;
        captureTexs[0] = new Texture2D(resWidth, resHeight, TextureFormat.RGBA32, false);
        captureTexs[1] = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
    }

    private void OnEnable()
    {
        InMainManager.Instance.canvasState = InMainManager.CanvasState.captureCanvas;
        if (InMainManager.Instance.nowFriend != null)
        {
            InMainManager.Instance.nowFriend.goTools.SetActive(false);
        }
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        captureTex = null;
        Resources.UnloadUnusedAssets();
    }

    public void OnMainButtonClick()
    {
        if (transSliderUI.bgSeq != -1)
        {
            InMainManager.Instance.SetBG(transSliderUI.bgSeq);
        }
        else
        {
            InMainManager.Instance.UnSetBG();
        }
        ChangeCanvasToMain();
    }

    public void ChangeCanvasToMain()
    {
        gameObject.SetActive(false);
        mainCanvas.SetActive(true);
    }

    //캡쳐
    public void TakeScreenShot()
    {
        StartCoroutine(IRequestNativeGalleryPermission(transparency));
    }

    private IEnumerator IRequestNativeGalleryPermission(bool transparency)
    {
        yield return new WaitForEndOfFrame();
        if (Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite) == false)
        {
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
            yield return new WaitForSeconds(0.2f); // 0.2초의 딜레이 후 focus를 체크하자.
            yield return new WaitUntil(() => Application.isFocused == true);
            if (Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite) == false)
            {
                alertView.Alert(LocalizationManager.Localize(StringInfo.CapPermissionInfo),
                    new AlertViewOptions
                    {
                        cancelButtonDelegate = () =>
                        {
                        },
                        okButtonDelegate = () =>
                        {
                            OpenAppSetting();
                        }
                    });
            }
            yield break;
        }
        StartCoroutine(Capture(transparency));
    }

    private IEnumerator Capture(bool transparency)
    {
        dontTouch.SetActive(true);
        if (transparency)
        {
            goBG.SetActive(false);
            mainCamera.backgroundColor = Color.white;
            tranRenderCamera.gameObject.SetActive(true);
        }
        else
        {
            fullRenderCamera.gameObject.SetActive(true);
        }
        yield return null;

        rt = RenderTexture.GetTemporary(resWidth, resHeight, 24);

        if (transparency)
        {
            renderCamera = tranRenderCamera;

            renderCamera.targetTexture = rt;

            captureTex = captureTexs[0];
        }
        else
        {
            renderCamera = fullRenderCamera;

            renderCamera.targetTexture = rt;

            captureTex = captureTexs[1];
        }
        renderCamera.Render();
        RenderTexture.active = rt;
        rect.Set(0, 0, resWidth, resHeight);
        captureTex.ReadPixels(rect, 0, 0);
        captureTex.Apply();

        string fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".png"; ;

        string path = "ChibiFriends";
        yield return null;
        yield return null;
        if (transparency)
        {
            goBG.SetActive(true);
            mainCamera.backgroundColor = ObjectInfo.mainCameraColor;
            tranRenderCamera.gameObject.SetActive(false);
        }
        else
        {
            fullRenderCamera.gameObject.SetActive(false);
        }

        SaveTextureAsPNG(captureTex, path, fileName);

        RenderTexture.active = null;
        renderCamera.targetTexture = null;
        RenderTexture.ReleaseTemporary(rt);
        dontTouch.SetActive(false);
    }

    public void SaveTextureAsPNG(Texture2D texture, string path, string name)
    {
        byte[] bytes = texture.EncodeToPNG();

        //File.WriteAllBytes(path + name + ".png", bytes);
        NativeGallery.SaveImageToGallery(texture, path, name);

        alertView.Alert(string.Format(LocalizationManager.Localize(StringInfo.CapPath), path, name), 44);
        goBG.SetActive(true);
    }

    private void OpenAppSetting()
    {
        try
        {
#if UNITY_ANDROID
            using (var unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            using (AndroidJavaObject currentActivityObject = unityClass.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                string packageName = currentActivityObject.Call<string>("getPackageName");

                using (var uriClass = new AndroidJavaClass("android.net.Uri"))
                using (AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("fromParts", "package", packageName, null))
                using (var intentObject = new AndroidJavaObject("android.content.Intent", "android.settings.APPLICATION_DETAILS_SETTINGS", uriObject))
                {
                    intentObject.Call<AndroidJavaObject>("addCategory", "android.intent.category.DEFAULT");
                    intentObject.Call<AndroidJavaObject>("setFlags", 0x10000000);
                    currentActivityObject.Call("startActivity", intentObject);
                }
            }
#elif UNITY_IPHONE
                OpenAppSettingPlugin._OpenAppSettings();
#endif
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }
    }
}
