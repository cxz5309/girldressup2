﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Storage;
using Firebase.Messaging;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using Firebase.Auth;
using Google;
using System;
using Firebase.Extensions;
using Assets.SimpleLocalization;

public class FirebaseManager : MonoBehaviour
{
    public static FirebaseManager Instance = null;

    FirebaseAuth auth = null;
    public FirebaseUser user = null;

    FirebaseApp app;

    StorageReference root_ref;
    StorageReference userdata_ref;
    protected CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

    protected Task previousTask;

    public bool signedIn = false;
    public bool operationInProgress = false;
    public bool startFailPopup = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        SetMessaging();

        SetFirebase();

        StartCoroutine(CoLoginWait());
    }

    public void SetFirebase()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                app = FirebaseApp.DefaultInstance;
                Debug.Log("Firebase Ready");
                // Set a flag here to indicate whether Firebase is ready to use by your app.
            }
            else
            {
                Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        }).ContinueWith((authTask) => SetAuth())
          .ContinueWith((storageTask) =>SetStorage());
    }

    #region Auth
    public void SetAuth()
    {
        auth = FirebaseAuth.DefaultInstance;

        // 유저의 로그인 정보에 어떠한 변경점이 생기면 실행되게 이벤트를 걸어준다.
        auth.StateChanged += AuthStateChanged;
    }

    //public IEnumerator CoWaitAnonyLogin()
    //{
    //    yield return new WaitForSeconds(0.1f);
    //    AnonyLogin();
    //}

    //public void AnonyLogin()
    //{
    //    auth.SignInAnonymouslyAsync().ContinueWith(task => {
    //        if (task.IsCanceled)
    //        {
    //            Debug.LogError("SignInAnonymouslyAsync was canceled.");
    //            return;
    //        }
    //        if (task.IsFaulted)
    //        {
    //            Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
    //            return;
    //        }
    //        // 익명 로그인 연동 결과
    //        Firebase.Auth.FirebaseUser newUser = task.Result;
    //        Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);
    //    });
    //}
    public IEnumerator CoWaitCanStart()
    {
        yield return new WaitForSeconds(0.1f);
        IntroManager.Instance.introCanvas.canStart = true;
    }

    // 계정 로그인에 어떠한 변경점이 발생시 진입.
    public void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if(auth.CurrentUser != user)
        {
            signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if(!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                try
                {
                    IntroManager.Instance.introCanvas.canStart = true;
                }
                catch { }
                Debug.Log("Signed in " + user.UserId);
            }
        }
    }

    public IEnumerator CoLoginWait()
    {
        Debug.Log("진입");
        float time = 0;
        while (true)
        {
            if (signedIn && !user.IsAnonymous)
            {
                try
                {
                    Debug.Log("Already Login");
                    IntroManager.Instance.introCanvas.canStart = true;
                }
                catch
                {

                }
                yield break;
            }
            if(time > 1f)
            {
                break;
            }
            time += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
#if !UNITY_EDITOR && UNITY_ANDROID
        GoogleLoginProcessing().ContinueWith(task =>
        {
            Debug.Log("New Login");
            IntroManager.Instance.introCanvas.canStart = true;
        });
#else
        IntroManager.Instance.introCanvas.canStart = true;
#endif
    }

    ////////////// // 구글 로그인 // //////////////
    public Task GoogleLoginProcessing()
    {
        if (GoogleSignIn.Configuration == null)
        { // 설정
            GoogleSignIn.Configuration = new GoogleSignInConfiguration
            {
                RequestIdToken = true,
                // Copy this value from the google-service.json file.
                // oauth_client with type == 3
                WebClientId = "520074363534-jj2cltp7o1kskdm552v4eq1rd1einbvm.apps.googleusercontent.com"
            };
        }
        
        Task<GoogleSignInUser> signIn = GoogleSignIn.DefaultInstance.SignIn();

        TaskCompletionSource<FirebaseUser> signInCompleted = new TaskCompletionSource<FirebaseUser>();
        signIn.ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                try
                {
                    IntroManager.Instance.WaitAlert(LocalizationManager.Localize(StringInfo.LoginCanceled));
                }
                catch
                {

                }
                signInCompleted.SetCanceled();
                StartCoroutine(CoWaitCanStart());
            }
            else if (task.IsFaulted)
            {
                try
                {
                    IntroManager.Instance.WaitAlert(LocalizationManager.Localize(StringInfo.LoginFaulted));
                }
                catch
                {

                }
                Debug.Log(task.Exception);
                signInCompleted.SetException(task.Exception);
                StartCoroutine(CoWaitCanStart());
            }
            else
            {
                Credential credential = GoogleAuthProvider.GetCredential(task.Result.IdToken, null);
                Debug.Log(credential.Provider);

                auth.SignInWithCredentialAsync(credential).ContinueWith(authTask =>
                {
                    if (authTask.IsCanceled)
                    {
                        signInCompleted.SetCanceled();
                        Debug.Log("Google Login authTask.IsCanceled");
                        try
                        {
                            IntroManager.Instance.WaitAlert(LocalizationManager.Localize(StringInfo.LoginCanceled));
                        }
                        catch
                        {

                        }
                        StartCoroutine(CoWaitCanStart());
                    }
                    else if (authTask.IsFaulted)
                    {
                        signInCompleted.SetException(authTask.Exception);
                        Debug.Log("Google Login authTask.IsFaulted");
                        try
                        {
                            IntroManager.Instance.WaitAlert(LocalizationManager.Localize(StringInfo.LoginFaulted));
                        }
                        catch
                        {

                        }
                        StartCoroutine(CoWaitCanStart());
                    }
                    else
                    {
                        user = authTask.Result;
                        Debug.LogFormat("Google User signed in successfully: {0} ({1})", user.DisplayName, user.UserId);
                    }
                });
            }
        });
        return signIn;
    }
    // 연동 해제
    public void SignOut() {
        if (auth.CurrentUser != null) auth.SignOut();
    }
    // 연동 계정 삭제
    public void UserDelete() {
        if (auth.CurrentUser != null) auth.CurrentUser.DeleteAsync();
    }

#endregion

#region Storage
    public void SetStorage()
    {
        FirebaseStorage storage = FirebaseStorage.DefaultInstance;
        StorageReference storage_ref = storage.GetReferenceFromUrl("gs://girldressup2.appspot.com/");
        root_ref = storage_ref.Root;
        userdata_ref = root_ref.Child("UserData");
    }

    class WaitForTaskCompletion : CustomYieldInstruction
    {
        Task task;
        FirebaseManager firebaseManager;

        // Create an enumerator that waits for the specified task to complete.
        public WaitForTaskCompletion(FirebaseManager firebaseManager, Task task)
        {
            firebaseManager.previousTask = task;
            firebaseManager.operationInProgress = true;
            this.firebaseManager = firebaseManager;
            this.task = task;
        }

        // Wait for the task to complete.
        public override bool keepWaiting
        {
            get
            {
                if (task.IsCompleted)
                {
                    firebaseManager.operationInProgress = false;
                    firebaseManager.cancellationTokenSource = new CancellationTokenSource();
                    if (task.IsFaulted)
                    {
                        firebaseManager.startFailPopup = true;

                        firebaseManager.DisplayStorageException(task.Exception);
                    }
                    return false;
                }
                return true;
            }
        }
    }

    // Display a storage exception.
    protected void DisplayStorageException(Exception exception)
    {
        var storageException = exception as StorageException;
        if (storageException != null)
        {
            Debug.Log(String.Format("Error Code: {0}", storageException.ErrorCode));
            Debug.Log(String.Format("HTTP Result Code: {0}", storageException.HttpResultCode));
            Debug.Log(String.Format("Recoverable: {0}", storageException.IsRecoverableException));
            Debug.Log(storageException.ToString());
        }
        else
        {
            Debug.Log(exception.ToString());
        }
    }



    public Task ChibiDataUpload()
    {
        //string xmlUserName = "User";
        //string xmlUserHaveName = "UserHave";
        //string xmlFileName = "ChibiData";

        string xmlFilepath = Application.persistentDataPath + "/XML/" + XMLManager.Instance.xmlFileName + ".xml";
        string xmlUserpath = Application.persistentDataPath + "/XML/" + XMLManager.Instance.xmlUserName + ".xml";
        string xmlUHpath = Application.persistentDataPath + "/XML/" + XMLManager.Instance.xmlUserHaveName + ".xml";

        var filebytes = File.ReadAllBytes(xmlFilepath);
        var userbytes = File.ReadAllBytes(xmlUserpath);
        var uhbytes = File.ReadAllBytes(xmlUHpath);

        //Debug.Log("내부데이터" + XMLManager.Instance.xmlFileName + filebytes.Length);
        //Debug.Log("내부데이터" + XMLManager.Instance.xmlUserName + userbytes.Length);
        //Debug.Log("내부데이터" + XMLManager.Instance.xmlUserHaveName + uhbytes.Length);


        string filebaseFilepath = user.UserId + "/" + XMLManager.Instance.xmlFileName + ".xml";
        string firebaseUserpath = user.UserId + "/" + XMLManager.Instance.xmlUserName + ".xml";
        string firebaseUHpath = user.UserId + "/" + XMLManager.Instance.xmlUserHaveName + ".xml";

        //Debug.Log("파이어베이스경로" + filebaseFilepath);
        //Debug.Log("파이어베이스경로" + firebaseUserpath);
        //Debug.Log("파이어베이스경로" + firebaseUHpath);

        //파이어베이스 Root/유저 고유 아이디/파일이름 으로 파일 저장
        //게임데이터, 유저데이터, 유저 보유 아이템 데이터 순서대로 이전저장 종료시 저장
        //선행작업 SetAuth, SetStorage, 구글 로그인
        return CloudUpload(filebaseFilepath, filebytes)
            .ContinueWith(fileTask => CloudUpload(firebaseUserpath, userbytes)
            .ContinueWith(userTask => CloudUpload(firebaseUHpath, uhbytes)));
    }


    public Task CloudUpload(string firebasePath, byte[] data)
    {
        Debug.Log("Upload In " + firebasePath);

        return userdata_ref.Child(firebasePath).PutBytesAsync(data)
        .ContinueWith((Task<StorageMetadata> task) =>
        {
            if (task.IsFaulted || task.IsCanceled)
            {
                Debug.Log(task.Exception.ToString());
                // Uh-oh, an error occurred!
            }
            else
            {
                // Metadata contains file metadata such as size, content-type, and download URL.
                StorageMetadata metadata = task.Result;
                string download_url = metadata.Path;
                Debug.Log("Finished uploading...");
                Debug.Log("download url = " + download_url);
            }
        });
    }

    public IEnumerator ChibiDataDownload()
    {
        //string xmlUserName = "User";
        //string xmlUserHaveName = "UserHave";
        //string xmlFileName = "ChibiData";

        string filebaseFilepath = user.UserId + "/" + XMLManager.Instance.xmlFileName + ".xml";
        string filebaseUserpath = user.UserId + "/" + XMLManager.Instance.xmlUserName + ".xml";
        string filebaseUHpath = user.UserId + "/" + XMLManager.Instance.xmlUserHaveName + ".xml";

        //Debug.Log("파이어베이스경로" + filebaseFilepath);
        //Debug.Log("파이어베이스경로" + filebaseUserpath);
        //Debug.Log("파이어베이스경로" + filebaseUHpath);

        string xmlFilepath = Application.persistentDataPath + "/XML/" + XMLManager.Instance.xmlFileName + ".xml";
        string xmlUserpath = Application.persistentDataPath + "/XML/" + XMLManager.Instance.xmlUserName + ".xml";
        string xmlUHpath = Application.persistentDataPath + "/XML/" + XMLManager.Instance.xmlUserHaveName + ".xml";

        //Debug.Log("내부데이터 경로" + xmlFilepath);
        //Debug.Log("내부데이터 경로" + xmlUserpath);
        //Debug.Log("내부데이터 경로" + xmlUHpath);

        //StorageReference mydata_ref = userdata_ref.Child(user.UserId);
        //StorageReference file_ref = userdata_ref.Child(filebaseFilepath);
        //StorageReference user_ref = userdata_ref.Child(filebaseUserpath);
        //StorageReference userhave_ref = userdata_ref.Child(filebaseUHpath);

        //if (mydata_ref != null && file_ref != null && user_ref != null && userhave_ref != null)
        //{

        //파이어베이스 Root/유저 고유 아이디/파일이름 으로 파일 저장
        //게임데이터, 유저데이터, 유저 보유 아이템 데이터 순서대로 이전저장 종료시 저장
        //선행작업 SetAuth, SetStorage, 구글 로그인
        yield return StartCoroutine(CloudDownload(filebaseFilepath, xmlFilepath));
        yield return StartCoroutine(CloudDownload(filebaseUserpath, xmlUserpath));
        yield return StartCoroutine(CloudDownload(filebaseUHpath, xmlUHpath));
                        
        //}
    }


    public IEnumerator CloudDownload(string filebasePath, string inputPath)
    {
        Debug.Log("Download From " + filebasePath + ", In " + inputPath);

        var task = userdata_ref.Child(filebasePath).GetFileAsync(inputPath, null, cancellationTokenSource.Token);
        yield return new WaitForTaskCompletion(this, task);

        if (!task.IsFaulted && !task.IsCanceled)
        {
            Debug.Log("File downloaded.");
        }
        else
        {
            Debug.Log("File download fail!!");
            Debug.Log(task.Exception.ToString());
        }
    }

    //public void TmpDownload()
    //{
    //    string local_url = Application.streamingAssetsPath + "/XML/aa.xml";
    //    userdata_ref.GetFileAsync(local_url).ContinueWith(task =>
    //    {
    //        if (!task.IsFaulted && !task.IsCanceled)
    //        {
    //            Debug.Log("File downloaded.");
    //        }
    //    });
    //}

#endregion

#region Messaging

    //푸시왔을때 인게임 메시지 처리 방식(현재 아무것도 안함)
    public void SetMessaging()
    {
        FirebaseMessaging.TokenReceived += OnTokenReceived;
        FirebaseMessaging.MessageReceived += OnMessageReceived;
    }

    //푸시왔을때 인게임 메시지 처리 방식(현재 아무것도 안함)
    public void OnTokenReceived(object sender, TokenReceivedEventArgs token)
    {
        Debug.Log("Received Registration Token: " + token.Token);
    }

    //푸시왔을때 인게임 메시지 처리 방식(현재 아무것도 안함)
    public void OnMessageReceived(object sender, MessageReceivedEventArgs e)
    {
        Debug.Log("Received a new message from: " + e.Message.From);
    }


    //주제 구독설정으로 메세지 무시하기
    public void SubscribeMessage(bool on)
    {
        Debug.Log("Nomal 구독 상태" + on);
        if (user != null)
        {
            if (on)
            {
                FirebaseMessaging.SubscribeAsync("Nomal").ContinueWithOnMainThread(task =>
                {
                    LogTaskCompletion(task, "SubscribeAsync");
                });
            }
            else
            {
                FirebaseMessaging.UnsubscribeAsync("Nomal").ContinueWithOnMainThread(task =>
                {
                    LogTaskCompletion(task, "SubscribeAsync");
                });
            }
        }
    }

    protected bool LogTaskCompletion(Task task, string operation)
    {
        bool complete = false;
        if (task.IsCanceled)
        {
            Debug.Log(operation + " canceled.");
        }
        else if (task.IsFaulted)
        {
            Debug.Log(operation + " encounted an error.");
            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            {
                string errorCode = "";
                Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
                if (firebaseEx != null)
                {
                    errorCode = String.Format("Error.{0}: ",
                      ((Firebase.Messaging.Error)firebaseEx.ErrorCode).ToString());
                }
                Debug.Log(errorCode + exception.ToString());
            }
        }
        else if (task.IsCompleted)
        {
            Debug.Log(operation + " completed");
            complete = true;
        }
        return complete;
    }
#endregion
}
