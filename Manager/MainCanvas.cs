﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;
using Assets.SimpleLocalization;

public class MainCanvas : MonoBehaviour
{
    //컨텐츠뷰
    public Transform charButtonView;
    public CharButtonViewUI charButtonViewUI;

    public Transform VerticalView;
    public VerticalViewUI verticalViewUI;

    //버튼 
    public Image[] menuImages = new Image[6];

    //코인
    public Text CoinText;
    public GameObject dontTouchPanel;

    //sl캔버스
    public GameObject slCanvas;
    public GameObject captureCanvas;

    //패널(위에 있는 순서대로)
    public ItemGetPopupUI itemGetPopupUI;
    public AlertView alertView;
    public SettingView settingView;
    public CollectionViewUI collectionViewUI;
    public FortuneWheelView fortuneWheelView;
    public DailyView dailyView;
    public StoreView storeView;
    public MissionView missionView;
    public GoADView goADView;
    public MoreGameView moreGameView;

    public Transform menuContent;

    //버튼 숨기기
    public UpDownButtonUI upDown;
    public GameObject storeBtn;
    public GameObject rewardBtn;
    public GameObject fortuneWheelBtn;
    public GameObject settingBtn;
    public GameObject moreGameBtn;
    public GameObject clearBtn;
    public GameObject fontSettingPanel;

    public GameObject coinText;

    public int nowObjMenu;
    public int groupSeq;
    bool tmpIsFontSetting;

    // 배너 광고로 위치 변경할 오브젝트 리스트
    public RectTransform[] bannerChangeTransforms;

    private void OnEnable()
    {
        if (InMainManager.Instance != null)
        {
            InMainManager.Instance.canvasState = InMainManager.CanvasState.mainCanvas;
            if (InMainManager.Instance.nowFriend != null)
            {
                InMainManager.Instance.nowFriend.goTools.SetActive(false);
            }
        }
    }

    private void Start()
    {
        // 배너 광고 초기화
        AdsManager.OnLoadedBanner += OnLoadedBanner;
        AdsManager.Instance.RequestBanner();

        OnClickMenu((int)FriendType.Char);

        InGameManager.Instance.GetCoinText(coinText);
        InGameManager.Instance.GetCoinXmlAndShow();

        CheckReview();
    }

    private void OnDestroy()
    {
        AdsManager.OnLoadedBanner -= OnLoadedBanner;
    }

    #region Actions-Events
    public void OnLoadedBanner(float bannerHeight)
    {
        if (bannerHeight <= 0)
        {
            bannerHeight = 0f;
        }

        for (int i = 0; i < bannerChangeTransforms.Length; i++)
        {
            Vector2 position = bannerChangeTransforms[i].offsetMax;
            position.y = -bannerHeight;
            bannerChangeTransforms[i].offsetMax = position;
        }
    }
    #endregion

    /// <summary>
    /// 평점 요청 확인
    /// </summary>
    private void CheckReview()
    {
        int appOpenCount = PlayerPrefs.GetInt("AppOpenCount", 0);
        bool isWriteReview = PlayerPrefs.GetInt("isWriteReview", 0) == 1;
        if (!isWriteReview)
        {
            appOpenCount += 1;
            PlayerPrefs.SetInt("AppOpenCount", Math.Min(10, appOpenCount));
            if (appOpenCount == 10)
            {
                PlayerPrefs.SetInt("isWriteReview", 1);
            }
            if (appOpenCount == 5 ||
                appOpenCount == 10 ||
                appOpenCount == 20)
            {
                string message;
                #if PLATFORM_IOS
                    message = LocalizationManager.Localize(StringInfo.RequestReviewAppStore);
                #else
                    message = LocalizationManager.Localize(StringInfo.RequestReviewGooglePlay);
                #endif
                alertView.Alert(message, new AlertViewOptions
                {                    dismissDelegate = () =>
                    {
                        CheckCoSetDaily();                    },                    okButtonDelegate = () =>
                    {
                        PlayerPrefs.SetInt("isWriteReview", 1);
                        #if UNITY_ANDROID
                            Application.OpenURL("market://details?id=com.ondot.girldressup2");
                        #elif UNITY_IPHONE

                        #endif
                        CheckCoSetDaily();
                    }
                });
            }
            else
            {
                CheckCoSetDaily();
            }
        }
        else
        {
            CheckCoSetDaily();
        }
    }

    /// <summary>
    /// 출석 체크 확인
    /// </summary>
    private void CheckCoSetDaily()
    {
        if (!InGameManager.Instance.firstDay &&
            InGameManager.Instance.newToday &&
            !InGameManager.Instance.dismissDaily)
        {
            StartCoSetDaily();
        }
    }

    public void StartCoSetDaily()
    {
        StartCoroutine(SetDaliy());
    }

    public IEnumerator SetDaliy()
    {
        dontTouchPanel.SetActive(true);
        yield return new WaitForSeconds(.5f);
        OnDailyClick();
        dontTouchPanel.SetActive(false);
    }

    public void OnDailyClick()
    {
        dailyView.goDailyPanel.SetActive(true);
    }

    public void OnCollectionClick()
    {
        collectionViewUI.collectionPanel.SetActive(true);
    }

    public void OnClickMenu(int nowObjMenu_)
    {
        nowObjMenu = nowObjMenu_;
        SetBottomToolBar((FriendType)nowObjMenu);
    }
    public void OnStoreClick()
    {
        storeView.goStorePanel.SetActive(true);
    }
    public void OnMissionClick()
    {
        missionView.goMissionPanel.SetActive(true);
    }
    public void OnSettingClick()
    {
        settingView.goSettingPanel.SetActive(true);
    }
    public void OnMoreGameClick()
    {
        moreGameView.goMoreGamePanel.SetActive(true);
    }

    public void SetBottomToolBar(FriendType nowObjMenu)
    {
        charButtonViewUI.ClearViewContents();
        verticalViewUI.ClearViewContents();
        //SetToolBarImageToggle(nowObjMenuSt);

        switch (nowObjMenu)
        {
            default:
                SetContents(false);
                verticalViewUI.CreateAllObjButton(nowObjMenu);
                break;
            case FriendType.Char:
                SetContents(true);
                charButtonViewUI.groupSeq = 0;
                charButtonViewUI.CreateCharButton();
                break;
        }
    }

    public void SetContents(bool horizontal)
    {
        if (horizontal)
        {
            charButtonViewUI.gameObject.SetActive(true);
            VerticalView.gameObject.SetActive(false);
        }
        else
        {
            VerticalView.gameObject.SetActive(true);
            charButtonViewUI.gameObject.SetActive(false);
        }
    }

    public void SaveMainInSlot(int slot)
    {
        XMLManager.Instance.SaveMainToSlotXML(slot.ToString());
    }

    public void LoadMainBySlot(int slot)
    {
        //XMLManager.Instance.LoadSlotToMain(slot);
    }

    //저장불러오기 캔버스
    public void OnSaveLoadButtonClick()
    {
        ChangeCanvasToSL();
    }

    public void ChangeCanvasToSL()
    {
        slCanvas.SetActive(true);
        gameObject.SetActive(false);
    }

    //캡쳐 캔버스
    public void OnCaptureButtonClick()
    {
        ChangeCanvasToCapture();
    }

    public void ChangeCanvasToCapture()
    {
        captureCanvas.SetActive(true);
        gameObject.SetActive(false);
    }


    //화면클리어
    public void OnClearButtonClick()
    {
        alertView.Alert(LocalizationManager.Localize(StringInfo.MainClearInfo), new AlertViewOptions
        {            cancelButtonDelegate = () =>
            {            },            okButtonDelegate = () =>
            {
                InMainManager.Instance.positionChanged = true;
                InMainManager.Instance.ClearAll();
            }
        });
    }

    public void OnUpDownButtonClick()
    {
        if (upDown.isOn)
        {
            if (tmpIsFontSetting)
            {
                fontSettingPanel.SetActive(true);
                tmpIsFontSetting = false;
            }
            storeBtn.SetActive(true);
            rewardBtn.SetActive(true);
            fortuneWheelBtn.SetActive(true);
            settingBtn.SetActive(true);
            moreGameBtn.SetActive(true);
            clearBtn.SetActive(true);
        }
        else
        {
            if (fontSettingPanel.activeSelf)
            {
                fontSettingPanel.SetActive(false);
                tmpIsFontSetting = true;
            }
            storeBtn.SetActive(false);
            rewardBtn.SetActive(false);
            fortuneWheelBtn.SetActive(false);
            settingBtn.SetActive(false);
            moreGameBtn.SetActive(false);
            clearBtn.SetActive(false);
        }
    }
}
