﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.SimpleLocalization;

public class InMainManager : MonoBehaviour
{
    public static InMainManager Instance;

    public Camera mainCamera;
    public MainCanvas mainCanvas;
    public SLCanvas slCanvas;
    public CaptureCanvas captureCanvas;

    public Transform charTrRoot;
    public Transform waterMark;
    public GameObject prefabCharRoot;
    public GameObject prefabObjectRoot;
    public GameObject prefabTextRoot;
    public GameObject prefabTextObjRoot;

    public GameObject mainTutorialCanvas;

    //화면에 올라간 모든 오브젝트
    public Dictionary<int, Dictionary<int, CharTr>> allScreenChar = new Dictionary<int, Dictionary<int, CharTr>>();
    //public Dictionary<int, GameObject> allBubbleTextControl = new Dictionary<int, GameObject>();
    //현재 선택된 오브젝트
    public Friend nowFriend;

    static float DISTANCE = 10f;

    public int sortingTop = 0;
    public int maxTrSeq;

    //캐릭터 위치
    public bool positionChanged;
    Vector3 mousePositionWorld;

    int ClickCount = 0;

    StringBuilder stringBuilder = new StringBuilder();

    //화면에 올라간 캔버스
    public enum CanvasState
    {
        mainCanvas, slCanvas, captureCanvas
    }
    public CanvasState canvasState = CanvasState.mainCanvas;

    //배경
    public SpriteRenderer BG;

    //AlertView
    public AlertView alertView;
    public SAlertViewUI SAlertView;
    //텍스트
    //public GameObject prefabInputText;

    //위치
    Vector2 InitPosition = new Vector2(0, 4);
    Vector2 AddPosition = new Vector2(1, 1);
    Vector2 RandomPosition;
    
    //캐릭터 수 제한
    public static int CHAR_COUNT_MAX = 10;

    //화면에서 터치된 오브젝트
    public GameObject hitGameObject;

    bool isTutorial;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        Debug.Log("Main Start Log");
        StopAllCoroutines();
    }
    private void Start()
    {
        positionChanged = true;
        Init();

        if (InGameManager.Instance.firstDay && InGameManager.Instance.newToday)
        {
            if (!InGameManager.Instance.clearTutorial)
            {
                isTutorial = true;
                mainTutorialCanvas.SetActive(true);
            }
            else
            {
                isTutorial = false;
                mainTutorialCanvas.SetActive(false);
            }
        }
        else
        {
            isTutorial = false;
            mainTutorialCanvas.SetActive(false);
        }
        
        InitWaterMark(XMLManager.Instance.LoadUserHave_RmWaterMark());

    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ClickDown();
        }
        if (Input.GetMouseButtonUp(0))
        {
            ClickUp();
        }

        //back버튼 터치
#if UNITY_ANDROID
        if (!isTutorial)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (DisMissAll())
                {
                    return;
                }
                ClickCount++;

                if (!IsInvoking("DoubleClick"))
                    Invoke("DoubleClick", 0.5f);
            }
            else if (ClickCount == 2)
            {
                if (IsInvoking("DoubleClick"))
                {
                    QuitAlert();
                    ClickCount++;
                }
            }
        }
#else
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (DisMissAll())
                {
                    return;
                }
                ClickCount++;
                if (!IsInvoking("DoubleClick"))
                    Invoke("DoubleClick", 0.5f);
            }
            else if (ClickCount == 2)
            {
                if (IsInvoking("DoubleClick"))
                {
                    QuitAlert();
                    ClickCount++;
                }
            }
#endif
    }

    void DoubleClick()
    {
        ClickCount = 0;
    }


    void QuitAlert()
    {
        mainCanvas.alertView.Alert(LocalizationManager.Localize(StringInfo.QuitGame), new AlertViewOptions
        {
            cancelButtonDelegate = () =>
            {

            },
            okButtonDelegate = () =>
            {
                Application.Quit();
            }
        });
    }

    private void Init()
    {
        SetTrDic();
        InitAllChar();
        InitBG();
        InitAllObj((int)FriendType.Object1);
        InitAllObj((int)FriendType.Object2);
        InitAllObj((int)FriendType.Animal);
        InitAllObj((int)FriendType.Bubble);
        InitAllObj((int)FriendType.Text);
    }
    #region 클릭
    //클릭되는게 없으면 tool지우기
    private void ClickDown()
    {
        mousePositionWorld = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, DISTANCE));
        RaycastHit2D hit = Physics2D.Raycast(mousePositionWorld, -Vector2.zero);

        if (hit.collider == null)
        {
            if (nowFriend != null)
            {
                //if (nowFriend.isText)
                //{
                //    nowFriend.GetComponent<TextObj>().InputEnd();
                //}
                nowFriend.goTools.SetActive(false);
                nowFriend = null;
            }
            hitGameObject = null;
        }
        else
        {
            hitGameObject = hit.collider.gameObject;
        }
    }

    void ClickUp()
    {
        //HitColliderCallBack();
    }
#endregion

#region 오브젝트 생성
    //스크린 내의 모든 위치정보를 가지고 있는 allScreenCharSet 0으로 초기화
    public void SetTrDic()
    {
        allScreenChar.Clear();
        for (int i = 0; i < 7; i++)
        {
            CharTr charTr = new CharTr();
            Dictionary<int, CharTr> tmpDic = new Dictionary<int, CharTr>();
            allScreenChar.Add(i, tmpDic);
        }
    }

    //캐릭터 하나 생성, 오토트랜스폼
    public GameObject CreateChar(int groupSeq, int i)
    {
        if (allScreenChar.ContainsKey(groupSeq))
        {
            if (allScreenChar[groupSeq].Count >= CHAR_COUNT_MAX)
            {
                SAlertView.SAlert(string.Format(LocalizationManager.Localize(StringInfo.DontMoreObj), CHAR_COUNT_MAX));
                return null;
            }
        }
        GameObject obj = Instantiate(prefabCharRoot, Vector3.zero, Quaternion.identity, charTrRoot);
        Friend friend = obj.GetComponentInChildren<Friend>();

        LoadMainChar(friend, i);
        friend.SetCharacterToolLine();
        friend.SetGroup(groupSeq);
        friend.SetMainCharTr(AutoTransform(i));
        positionChanged = false;
        return obj;
    }
    //최초 캐릭터 생성
    public void InitAllChar()
    {
        XMLManager.Instance.LoadAllMAINGROUPTr(0);
        List<CharTr> tmpTr = XMLManager.Instance.listMAIN_ObjTr;

        for (int i = 0; i < tmpTr.Count; i++)
        {
            if (maxTrSeq < int.Parse(tmpTr[i].TrSeq))
            {
                maxTrSeq = int.Parse(tmpTr[i].TrSeq);
            }
            GameObject obj = Instantiate(prefabCharRoot, charTrRoot);
            Friend friend = obj.GetComponentInChildren<Friend>();

            LoadMainChar(friend, int.Parse(tmpTr[i].UsedSeq));
            friend.SetCharacterToolLine();
            friend.SetGroup(0);
            friend.SetTrInfo(tmpTr[i]);
            friend.SaveTrDic(0, tmpTr[i]);
            friend.SetTransform();
        }
    }

    public void LoadMainChar(Friend friend, int wearSeq)
    {
        XMLManager.Instance.LoadOneMAIN_WEAR(wearSeq.ToString());
        List<CharPart> list = XMLManager.Instance.listTmpCharPart;

        friend.CreateCharacter(list);
    }

    int i = 0;

    //자동tr조정
    public CharTr AutoTransform(int useqSeq)
    {
        CharTr charTr;
        Vector3 createPosition;
        int trSeq = maxTrSeq + 1;
        Vector2[] arrayVec = new Vector2[] { new Vector2(-4, -3), new Vector2(-3, -5), new Vector2(-2, -7), new Vector2(-4, -5), new Vector2(-3, -7)};
        if (positionChanged)
        {
            InitPosition = Vector3.zero;
            createPosition = InitPosition;
        }
        else
        {
            if (InitPosition.x > 3.5 || InitPosition.y > 6.5)
            {
                RandomPosition = arrayVec[i % 5];
                i++;
                InitPosition = RandomPosition;
            }
            InitPosition += AddPosition;
            createPosition = InitPosition;
        }
        sortingTop++;
        charTr = new CharTr(trSeq.ToString(), useqSeq.ToString(), createPosition, 1f, sortingTop, false);
        return charTr;
    }

    //배경
    public void InitBG()
    {
        //배경이 있으면 바꿔주고 없으면 그냥 놔둠
        if (XMLManager.Instance.IsOneMAINGROUPTrByTrSeq(1, 0))
        {
            int bgSeq = int.Parse(XMLManager.Instance.SelectOneMAINGTOUPTrByTrSeq(1, 0).Attributes["used_seq"].Value);
            if(bgSeq!= -1)
            {
                SetBG(bgSeq);
            }
        }
    }

    public void SetBG(int bgSeq)
    {
        //배경이 있으면 바꿔주고 없으면 새로 생성
        BG.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllBGAtlas, bgSeq.ToString());
        if (XMLManager.Instance.IsOneMAINGROUPTrByTrSeq(1, 0))
        {
            XMLManager.Instance.SetOneMAINGROUPTrByTrSeq(1, 0, "used_seq", bgSeq.ToString());
        }
        else
        {
            XMLManager.Instance.CreateOneMAINGROUPTr(1, "0", bgSeq.ToString(), new CharTr());
        }
    }
    //배경 없애기
    public void UnSetBG()
    {
        BG.sprite = null;
        XMLManager.Instance.RemoveOneMAINGROUPTrByTrSeq(1, 0);
    }

    //최초 오브젝트 생성
    public void InitAllObj(int groupSeq)
    {
        Dictionary<int, ItemPart> tmpObj = new Dictionary<int, ItemPart>();

        XMLManager.Instance.LoadAllMAINGROUPTr(groupSeq);
        List<CharTr> tmpTr = XMLManager.Instance.listMAIN_ObjTr;

        tmpObj = XMLManager.Instance.listAllObjDic[groupSeq];

        for (int i = 0; i < tmpTr.Count; i++)
        {
            if (maxTrSeq < int.Parse(tmpTr[i].TrSeq))
            {
                maxTrSeq = int.Parse(tmpTr[i].TrSeq);
            }
            GameObject obj = null;
            Friend firend = null;
            switch (groupSeq)
            {
                case 2:
                    obj = Instantiate(prefabObjectRoot, charTrRoot);
                    firend = obj.GetComponentInChildren<Friend>();

                    firend.SetObj1Render(tmpObj[int.Parse(tmpTr[i].UsedSeq)].typeSeq);
                    break;
                case 3:
                    obj = Instantiate(prefabObjectRoot, charTrRoot);
                    firend = obj.GetComponentInChildren<Friend>();

                    firend.SetObj2Render(tmpObj[int.Parse(tmpTr[i].UsedSeq)].typeSeq);
                    break;
                case 4:
                    obj = Instantiate(prefabObjectRoot, charTrRoot);
                    firend = obj.GetComponentInChildren<Friend>();

                    firend.SetAnimalRender(tmpObj[int.Parse(tmpTr[i].UsedSeq)].typeSeq);
                    break;
                case 6:
                    obj = Instantiate(prefabObjectRoot, charTrRoot);
                    firend = obj.GetComponentInChildren<Friend>();

                    //obj.GetComponentInChildren<TextObj>().SetPlayerPrefs(groupSeq, tmpTr[i].UsedSeq);
                    //obj.GetComponentInChildren<Friend>().isText = true;
                    firend.SetTextObjRender(tmpObj[int.Parse(tmpTr[i].UsedSeq)].typeSeq);
                    break;
                case 5:
                    obj = Instantiate(prefabObjectRoot, charTrRoot);
                    firend = obj.GetComponentInChildren<Friend>();
                    firend.SetBubbleObjRender(tmpObj[int.Parse(tmpTr[i].UsedSeq)].typeSeq);

                    //텍스트 컨트롤용, 원래 bool값만 입력해서 전체 텍스트 변경 막으려고 했는데 더 쓸일이 있을까봐 GameObject 넣음
                    //InMainManager.Instance.allBubbleTextControl.Add(int.Parse(tmpTr[i].TrSeq), obj);

                    //텍스트 박스 생성시 텍스트도 같이 생성하기
                    GameObject text = Instantiate(prefabTextRoot, obj.transform.GetChild(0));
                    firend.SetTextRender(text, int.Parse(tmpTr[i].UsedSeq));
                    TMPObject tmpObject = text.GetComponent<TMPObject>();

                    LoadText(tmpObject, tmpTr[i].UsedSeq, tmpTr[i].TrSeq);
                    break;
            }
            firend.SetGroup(groupSeq);
            firend.SetTrInfo(tmpTr[i]);
            firend.SaveTrDic(groupSeq, tmpTr[i]);
            firend.SetTransform();
        }
    }

    public void LoadText(TMPObject tmp, string usedSeq, string trSeq)
    {
        XMLManager.Instance.LoadOneMAIN_BUBBLE(usedSeq);
        BubbleText BT = XMLManager.Instance.dicTmpBubble[trSeq];
        tmp.bubbleText = BT;
        tmp.Init(false);
    }
    //최초 텍스트 생성
    public void InitAllText(int groupSeq)
    {
        XMLManager.Instance.LoadAllMAINGROUPTr(groupSeq);
        List<CharTr> tmpTr = XMLManager.Instance.listMAIN_ObjTr;
        for (int i = 0; i < tmpTr.Count; i++)
        {
            if (maxTrSeq < int.Parse(tmpTr[i].TrSeq))
            {
                maxTrSeq = int.Parse(tmpTr[i].TrSeq);
            }
            GameObject obj = Instantiate(prefabTextRoot, charTrRoot);

            obj.GetComponentInChildren<Friend>().SetGroup(groupSeq);
            obj.GetComponentInChildren<Friend>().SetTrInfo(tmpTr[i]);
            obj.GetComponentInChildren<Friend>().SaveTrDic(groupSeq, tmpTr[i]);
            //switch (groupSeq)
            //{
            //    case 2:
            //        obj.GetComponentInChildren<Friend>().SetObjRender(i);
            //        break;
            //    case 3:
            //        obj.GetComponentInChildren<Friend>().SetAnimalRender(i);
            //        break;
            //}
        }
    }
    //오브젝트1 생성
    public void CreateObj1(int groupSeq, int usedSeq, string seq)
    {
        if (allScreenChar.ContainsKey(groupSeq))
        {
            if (allScreenChar[groupSeq].Count >= CHAR_COUNT_MAX)
            {
                SAlertView.SAlert(string.Format(LocalizationManager.Localize(StringInfo.DontMoreObj), CHAR_COUNT_MAX));
                return;
            }
        }
        GameObject obj = Instantiate(prefabObjectRoot, charTrRoot);

        obj.GetComponentInChildren<Friend>().SetObj1Render(seq);
        obj.GetComponentInChildren<Friend>().SetGroup(groupSeq);
        obj.GetComponentInChildren<Friend>().SetMainCharTr(AutoTransform(usedSeq));
        positionChanged = false;
    }
    //오브젝트2 생성
    public void CreateObj2(int groupSeq, int usedSeq, string seq)
    {
        if (allScreenChar.ContainsKey(groupSeq))
        {
            if (allScreenChar[groupSeq].Count >= CHAR_COUNT_MAX)
            {
                SAlertView.SAlert(string.Format(LocalizationManager.Localize(StringInfo.DontMoreObj), CHAR_COUNT_MAX));
                return;
            }
        }
        GameObject obj = Instantiate(prefabObjectRoot, charTrRoot);

        obj.GetComponentInChildren<Friend>().SetObj2Render(seq);
        obj.GetComponentInChildren<Friend>().SetGroup(groupSeq);
        obj.GetComponentInChildren<Friend>().SetMainCharTr(AutoTransform(usedSeq));
        positionChanged = false;
    }
    //동물 생성
    public void CreateAnimal(int groupSeq, int usedSeq, string seq)
    {
        if (allScreenChar.ContainsKey(groupSeq))
        {
            if (allScreenChar[groupSeq].Count >= CHAR_COUNT_MAX)
            {
                SAlertView.SAlert(string.Format(LocalizationManager.Localize(StringInfo.DontMoreObj), CHAR_COUNT_MAX));
                return;
            }
        }
        GameObject obj = Instantiate(prefabObjectRoot, charTrRoot);

        obj.GetComponentInChildren<Friend>().SetAnimalRender(seq);
        obj.GetComponentInChildren<Friend>().SetGroup(groupSeq);
        obj.GetComponentInChildren<Friend>().SetMainCharTr(AutoTransform(usedSeq));
        positionChanged = false;
    }
    //텍스트 오브젝트 생서
    public void CreateObjectText(int groupSeq, int usedSeq, string imgName)
    {
        if (allScreenChar.ContainsKey(groupSeq))
        {
            if (allScreenChar[groupSeq].Count >= CHAR_COUNT_MAX * 3)
            {
                SAlertView.SAlert(string.Format(LocalizationManager.Localize(StringInfo.DontMoreObj), CHAR_COUNT_MAX * 3));
                return;
            }
        }
        GameObject obj = Instantiate(prefabObjectRoot, charTrRoot);
        obj.GetComponentInChildren<Friend>().SetTextObjRender(imgName);
        obj.GetComponentInChildren<Friend>().SetGroup(groupSeq);
        obj.GetComponentInChildren<Friend>().SetMainCharTr(AutoTransform(usedSeq));
        positionChanged = false;
    }

    //텍스트박스 생성
    public void CreateBubble(int groupSeq, int usedSeq, string imgName)
    {
        if (allScreenChar.ContainsKey(groupSeq))
        {
            if (allScreenChar[groupSeq].Count >= CHAR_COUNT_MAX)
            {
                SAlertView.SAlert(string.Format(LocalizationManager.Localize(StringInfo.DontMoreObj), CHAR_COUNT_MAX));
                return;
            }
        }
        GameObject obj = Instantiate(prefabObjectRoot, charTrRoot);
        Friend friend = obj.GetComponentInChildren<Friend>();

        friend.SetBubbleObjRender(imgName);
        friend.SetGroup(groupSeq);
        friend.SetMainCharTr(AutoTransform(usedSeq));

        //텍스트 박스 생성시 텍스트도 같이 생성하기
        GameObject text = Instantiate(prefabTextRoot, obj.transform.GetChild(0));
        friend.SetTextRender(text, usedSeq);
        TMPObject tmpObject = text.GetComponent<TMPObject>();

        tmpObject.newBubbleSeq = usedSeq.ToString();
        tmpObject.newTrSeq = friend.trSeq.ToString();

        XMLManager.Instance.CreateOneBubbleMAIN(usedSeq.ToString(), friend.trSeq.ToString(), LocalizationManager.Localize(StringInfo.defaultText));
        tmpObject.Init(true);
        positionChanged = false;
    }
    //텍스트 생성
    public void CreateText(int groupSeq)
    {
        if (allScreenChar.ContainsKey(groupSeq))
        {
            if (allScreenChar[groupSeq].Count >= CHAR_COUNT_MAX * 3)
            {
                SAlertView.SAlert(string.Format(LocalizationManager.Localize(StringInfo.DontMoreObj), CHAR_COUNT_MAX * 3));
                Debug.Log("오브젝트 최대 숫자는 30입니다. 라고 팝업띄우기");
                return;
            }
        }
        GameObject obj = Instantiate(prefabTextRoot, charTrRoot);
        Friend friend = obj.GetComponentInChildren<Friend>();
        friend.isText = true;
        //friend.SetTextRender();

        //TextObj textObj = obj.GetComponentInChildren<TextObj>();
        //textObj.SetTextSetting();
        //textObj.SaveTextSetting();
        friend.SetGroup(groupSeq);
        friend.SetMainCharTr(AutoTransform(0));

        positionChanged = false;

        //TextCanvas.gameObject.SetActive(true);

        //GameObject nowText = Instantiate(prefabInputText, TextCanvas.transform);
        //nowText.GetComponentInChildren<Friend>().SetGroup(groupSeq);
        //nowText.GetComponentInChildren<Friend>().SetMainCharTrText(AutoTransformText(i));
        //nowText.GetComponentInChildren<InputTextUI>().SetTextSetting();

        //positionChanged = false;
    }
#endregion

#region 오브젝트 삭제
    //화면클리어 기능 
    public void ClearAll()
    {
        GameObject[] tmpObj;

        for (int i = 0; i < 7; i++)
        {
            if (i == 1)
                continue;
            switch (i)
            {
                default:
                    tmpObj = null;
                    break;
                case 0:
                    tmpObj = GameObject.FindGameObjectsWithTag("CharObject");
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    tmpObj = GameObject.FindGameObjectsWithTag("Object");
                    break;
            }
            for (int j = 0; j < tmpObj.Length; j++)
            {
                allScreenChar[i].Clear();
                Destroy(tmpObj[j].transform.gameObject);
            }
        }
        for (int i = 0; i < 7; i++)
        {
            if (i == 1)
                continue;
            XMLManager.Instance.RemoveAllMAINGROUPTr(i);
        }
        UnSetBG();
    }
    //화면에 같은 캐릭터 종류만 삭제
    public void ClearOne(int wearSeq)
    {
        XMLManager.Instance.SelectSomeMAINGROUPTrByUsedSeq(0, wearSeq.ToString());
        GameObject[] tmpObj = GameObject.FindGameObjectsWithTag("CharObject");
        for (int i = 0; i < tmpObj.Length; i++)
        {
            for(int j = 0; j < XMLManager.Instance.listTmpTrSeq.Count; j++)
            {
                int selectedTrSeq = XMLManager.Instance.listTmpTrSeq[j];
                if (tmpObj[i].GetComponentInChildren<Friend>().trSeq == selectedTrSeq)
                {
                    allScreenChar[0].Remove(selectedTrSeq);
                    Destroy(tmpObj[i]);

                    XMLManager.Instance.RemoveOneMAINGROUPTrByTrSeq(0, selectedTrSeq);
                }
            }
        }
    }

    public void InitWaterMark(bool rmWaterMark)
    {
        //waterMark.position = ObjectInfo.waterMarkPos;
        if (rmWaterMark)
        {
            waterMark.gameObject.SetActive(false);
        }
        else
        {
            waterMark.gameObject.SetActive(true);
        }
    }
    #endregion


    //초기화
    public void MainSceneRefresh()
    {
        InitWaterMark(XMLManager.Instance.LoadUserHave_RmWaterMark());
        for (int i = 0; i < mainCanvas.menuContent.childCount; i++)
        {
            mainCanvas.menuContent.GetChild(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < mainCanvas.menuContent.childCount; i++)
        {
            mainCanvas.menuContent.GetChild(i).gameObject.SetActive(true);
        }

        mainCanvas.OnClickMenu(mainCanvas.nowObjMenu);
    }


    public bool DisMissAll()
    {
        //알림 있으면 알림부터 제거
        if (mainCanvas.alertView.goPanel.activeSelf)
        {
            mainCanvas.alertView.OnDismissButtonClick();
            return true;
        }
        if (slCanvas.alertView.goPanel.activeSelf)
        {
            slCanvas.alertView.OnDismissButtonClick();
            return true;

        }
        if (captureCanvas.alertView.goPanel.activeSelf)
        {
            captureCanvas.alertView.OnDismissButtonClick();
            return true;

        }

        //나머지는 한꺼번에 제거 하지 말고 그냥 위에순서부터 하나씩 제거
        if (mainCanvas.settingView.goSettingPanel.activeSelf)
        {
            mainCanvas.settingView.DisMiss();
            return true;
        }
        if (mainCanvas.collectionViewUI.collectionPanel.activeSelf)
        {
            mainCanvas.collectionViewUI.Dismiss();
            return true;
        }
        if (mainCanvas.fortuneWheelView.goFortuneWheelPanel.activeSelf)
        {
            mainCanvas.fortuneWheelView.DisMiss();
            return true;
        }
        if (mainCanvas.dailyView.goDailyPanel.activeSelf)
        {
            mainCanvas.dailyView.DisMiss();
            return true;
        }
        if (mainCanvas.storeView.goStorePanel.activeSelf)
        {
            mainCanvas.storeView.DisMiss();
            return true;
        }
        if (mainCanvas.missionView.goMissionPanel.activeSelf)
        {
            mainCanvas.missionView.DisMiss();
            return true;
        }
        if (mainCanvas.goADView.goADPanel.activeSelf)
        {
            mainCanvas.goADView.DisMiss();
            return true;
        }
        if (mainCanvas.moreGameView.goMoreGamePanel.activeSelf)
        {
            mainCanvas.moreGameView.DisMiss();
            return true;
        }
        return false;
    }
    
}
