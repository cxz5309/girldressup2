﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum ItemGetRoot
{
    //itemGetRoot 0 : 구매, 1 : 데일리, 2 : 룰렛,  3: 그 외(컬렉션)
    store, daily, wheel, collection
}

public class InGameManager : MonoBehaviour
{
    public static InGameManager Instance = null;

    public GameObject coinText;
    public bool clearTutorial;

    public RewardButtonUI rewardButtonUI;
    public MissionView missionView;
    public CollectionViewUI collectionViewUI;

    public string firstDateTime;
    public string startDateTime;
    public string prevDateTime;
    public int coins;
    public bool newToday = false;
    public bool firstDay = false;
    public bool getDailyReward = false;
    public bool getADCoins = false;

    //public bool firstDayCreate = false;

    //아이템 시드
    public int thisDailySeed;
    //public int thisFortuneWheelSeed;

    //메이킹씬 생성/수정 상태
    public bool inModify;
    public int modifyNum;

    //자동데일리 온
    public bool dismissDaily;

    //룰렛 타이머
    public float wheelStartTime;
    public float realTime;
    public float wheelUpdateTime;
    public int wheelFullSecond;
    public int wheelMin;
    public int wheelSecond;
    public bool canSlider;

    //기본 광고 타이머
    public Text adTimerText;
    public float adStartTime;
    public float adUpdateTime;
    public int adFullSecond;
    public int adMin;
    public int adSecond;
    public bool canAD;

    //이펙트
    public List<GameObject> prefabEffects;
    public Dictionary<int, List<Transform>> nowEffects = new Dictionary<int, List<Transform>>();

    //코인이미지
    public Transform coinImage;

    public string loadingPrevScene;

    StringBuilder stringBuilder = new StringBuilder();

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        // 60프레임 고정
        Application.targetFrameRate = 60;

        DontDestroyOnLoad(gameObject);
        ////테스트때는 InGameManager쪽에서 실행, 실제 구동때는 IntroCanvas에(InGameManager도 캔버스 이전에 실행되기 때문)
        //LoadAllSetting();
    }

    private void Start()
    {
        //InitEditor();
    }

    public void InitEditor()
    {
        InGameManagerStart();
    }

    //스크립트 내 기능들 초기화하기
    public void InGameManagerStart()
    {
        //받은 사용자와 미션 정보로 미션 클리어 체크
        MissionSeed.CheckAllMission();
        //손 관련 static할당
        HandMatch.InitHandMatch();
        //PlayerPrefs 설정(최초시작시에만 적용)
        SetAllPlayerPrefs();
        //푸시메세지 설정(PlayerPrefs 사용)
        SetPushMsg();
        //룰렛 최초 시간 설정
        wheelStartTime = -1 * (ObjectInfo.ITEM_COOL_TIME);
        //adStartTime = -1 * (ObjectInfo.ITEM_COOL_TIME);
    }


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            AudioManager.Inst.PlaySFX("button");
        }

        //룰렛 시간
        realTime = Time.realtimeSinceStartup;
        wheelUpdateTime = realTime - wheelStartTime;
        //adUpdateTime = realTime - adStartTime;
        //if (wheelUpdateTime >= ObjectInfo.ITEM_COOL_TIME)
        //{
        //    wheelUpdateTime = ObjectInfo.ITEM_COOL_TIME;
        //    return;
        //}
        wheelFullSecond = ((int)(ObjectInfo.ITEM_COOL_TIME - wheelUpdateTime));
        wheelMin = wheelFullSecond / 60;
        wheelSecond = wheelFullSecond % 60;//}

        //adFullSecond = ((int)(ObjectInfo.ITEM_COOL_TIME - adUpdateTime));
        //adMin = adFullSecond / 60;
        //adSecond = adFullSecond % 60;
    }

    
    //xml 관련 정보 초기화하기
    public void LoadAllSetting()
    {
        //날짜 설정
        SetDate();

        //데일리, 룰렛아이템시드 
        ItemSeedInfo.Init();
        //미션 시드
        MissionSeed.Init();
        //콜렉션 시드
        CollectionSeed.Init();

        //코인정보 받기 coinSeq0 은 내부재화
        coins = XMLManager.Instance.LoadUserCoins("0");
    }

    //날짜 설정
    public void SetDate()
    {
        //날짜정보 받기
        if (XMLManager.Instance.LoadUserDateTime("FirstDateTime") == "" || XMLManager.Instance.LoadUserDateTime("FirstDateTime") == null)
        {
            firstDateTime = DateTime.Now.ToString();
            XMLManager.Instance.SaveUserDateTime("FirstDateTime", firstDateTime);
            XMLManager.Instance.SaveUserDateTime("PrevDateTime", firstDateTime);
            XMLManager.Instance.SaveUserDateTime("StartDateTime", firstDateTime);
            newToday = true;
            firstDay = true;
            getDailyReward = false;
            thisDailySeed = 0;
            //thisFortuneWheelSeed = 0;

            XMLManager.Instance.SaveUserGetReward(getDailyReward);
            XMLManager.Instance.SaveUserDateDailySeed(thisDailySeed);
            //XMLManager.Instance.SaveUserDateWheelSeed(thisFortuneWheelSeed);
            XMLManager.Instance.SaveUserDayCount(0);

            clearTutorial = false;

            Debug.Log("xml에 저장된 첫 dateTime" + XMLManager.Instance.LoadUserDateTime("FirstDateTime"));
        }
        else//일반적인 경우
        {
            firstDateTime = XMLManager.Instance.LoadUserDateTime("FirstDateTime");
            //이전거는 로드해서 받기
            prevDateTime = XMLManager.Instance.LoadUserDateTime("PrevDateTime");
            DateTime tmpPrevDateTime = Convert.ToDateTime(prevDateTime);
            //현재거는 system이용하기 
            DateTime tmpStartDateTime = DateTime.Now;
            startDateTime = tmpStartDateTime.ToString();
            getDailyReward = XMLManager.Instance.LoadUserGetReward();
            thisDailySeed = XMLManager.Instance.LoadUserDateDailySeed();

            //보상을 받았다면 day증가
            if (getDailyReward)
            {
                //이전 접속 시간과 현재 접속 시간 비교하여 날짜 바뀐거 확인
                TimeSpan timeCal = tmpStartDateTime - tmpPrevDateTime;
                int dayCountCal = timeCal.Days;
                if (dayCountCal > 0)
                {
                    newToday = true;
                    int dayCount = XMLManager.Instance.LoadUserDayCount();
                    XMLManager.Instance.SaveUserGetReward(false);
                    XMLManager.Instance.SaveUserDayCount(dayCount + 1);
                }
                else
                {
                    newToday = false;
                }
            }
            //보상을 받지 않았다면 day증가하지 않고 이전 접속과 현재 접속 비교 필요 없이 newToday만 해준다
            else
            {
                newToday = true;
            }
            /////////////////////////////////////////////////////////
            getADCoins = false;

            XMLManager.Instance.SaveUserDateTime("StartDateTime", startDateTime);
            XMLManager.Instance.SaveUserDateTime("PrevDateTime", startDateTime);
        }
    }

    public void GetCoinText(GameObject coinText_)
    {
        coinText = coinText_;
    }
    //현재 코인 정보 받아오기 + 화면에 표시
    public void GetCoinXmlAndShow()
    {
        if (coinText != null)
        {
            coins = XMLManager.Instance.LoadUserCoins("0");
            coinText.GetComponent<Text>().text = coins.ToString();
        }
        else
        {
            switch (SceneManager.GetActiveScene().name)
            {
                case "MakingScene":
                    GetCoinText(InMakingManager.Instance.makingCanvas.coinText);
                    break;
                case "MainScene":
                    GetCoinText(InMainManager.Instance.mainCanvas.coinText);
                    break;
            }
        }
    }

    //코인 증가
    public void AddCoins(int addCoins)
    {
        XMLManager.Instance.SaveUserCoinsAdd("0", addCoins);
        GetCoinXmlAndShow();
    }
    //코인 감소
    public void ReduceCoins(int reduceCoins)
    {
        XMLManager.Instance.SaveUserCoinsReduce("0", reduceCoins);
        GetCoinXmlAndShow();
    }

    //소유 아이템 추가하기
    public void AddItem(string seqName, string itemSeq, ItemGetRoot itemGetRoot)
    {
        XMLManager.Instance.AddUserHaveItem(seqName, itemSeq, itemGetRoot);

        Debug.Log("워터마크" + XMLManager.Instance.LoadUserHave_RmWaterMark());
        //itemGetRoot 0 : 구매, 1 : 데일리, 2 : 룰렛,  3: 그 외(컬렉션)
        switch (itemGetRoot) {
            default:
            case ItemGetRoot.store:
                break;
            case ItemGetRoot.daily:
                thisDailySeed++;
                XMLManager.Instance.SaveUserDateDailySeed(thisDailySeed);
                break;
            case ItemGetRoot.wheel:
                //thisFortuneWheelSeed++;
                //XMLManager.Instance.SaveUserDateWheelSeed(thisFortuneWheelSeed);
                break;
        }
    }

    //AddItem과 같이 있어야함 원래 안에 있었는데 여러 아이템 획득시 포문 돌기 싫어서 뺀것
    public void InitItemGet()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "MakingScene":
                InMakingManager.Instance.MakingSceneRefresh();
                break;
            case "MainScene":
                InMainManager.Instance.MainSceneRefresh();
                break;
        }
        //CollectionSeed.CheckAndSaveAllIsClear();
        MissionSeed.CheckAllMission();

        if (collectionViewUI != null)
        {
            collectionViewUI.SetFirstInit();
            collectionViewUI.CreateButtonImg();
            collectionViewUI.SettingUI(collectionViewUI.thisCS.Level);
        }
        rewardButtonUI.SetCollectionCheck();
        rewardButtonUI.SetCheckImg3();
    }

    public void SetAllPlayerPrefs()
    {
        if (!PlayerPrefs.HasKey(StringInfo.playerPrefsKeyList[0]))
        {
            PlayerPrefs.SetFloat(StringInfo.playerPrefsKeyList[0], ObjectInfo.defBGMVol);
        }
        if (!PlayerPrefs.HasKey(StringInfo.playerPrefsKeyList[1]))
        {
            PlayerPrefs.SetFloat(StringInfo.playerPrefsKeyList[1], ObjectInfo.defSFXVol);
        }
        if (!PlayerPrefs.HasKey(StringInfo.playerPrefsKeyList[2]))
        {
            PlayerPrefs.SetFloat(StringInfo.playerPrefsKeyList[2], ObjectInfo.defBGMMute);
        }
        if (!PlayerPrefs.HasKey(StringInfo.playerPrefsKeyList[3]))
        {
            PlayerPrefs.SetFloat(StringInfo.playerPrefsKeyList[3], ObjectInfo.defSFXMute);
        }
        if (!PlayerPrefs.HasKey(StringInfo.playerPrefsKeyList[4]))
        {
            PlayerPrefs.SetFloat(StringInfo.playerPrefsKeyList[4], ObjectInfo.defFontColor);
        }
        //if (!PlayerPrefs.HasKey(StringInfo.playerPrefsKeyList[5]))
        //{
        //    PlayerPrefs.SetFloat(StringInfo.playerPrefsKeyList[5], ObjectInfo.defFontSize);
        //}
        if (!PlayerPrefs.HasKey(StringInfo.playerPrefsKeyList[5]))
        {
            PlayerPrefs.SetInt(StringInfo.playerPrefsKeyList[5], ObjectInfo.defColStage);
        }
        if (!PlayerPrefs.HasKey(StringInfo.playerPrefsKeyList[6]))
        {
            if (FirebaseManager.Instance.user != null)
            {
                PlayerPrefs.SetInt(StringInfo.playerPrefsKeyList[6], ObjectInfo.isPushMsg);
            }
            else
            {
                PlayerPrefs.SetInt(StringInfo.playerPrefsKeyList[6], 0);
            }
        }

        //playerPrefsKeyList[7] SettingLanguage 초기화 : IntroCanvas

        if (!PlayerPrefs.HasKey(StringInfo.playerPrefsKeyList[8]))
        {
            PlayerPrefs.SetInt(StringInfo.playerPrefsKeyList[8], 1);
        }
    }

    //푸시메세지 설정
    public void SetPushMsg()
    {
        if (PlayerPrefs.GetInt(StringInfo.playerPrefsKeyList[6]) == 1)
        {
            FirebaseManager.Instance.SubscribeMessage(true);
        }
        else
        {
            FirebaseManager.Instance.SubscribeMessage(false);
        }
    }

    /// <summary>
    /// 이펙트
    /// </summary>
    /// <param name="effectNum">번호</param>
    /// <param name="position">위치</param>
    /// <param name="destroyTime">삭제시간</param>
    public void StartEffect(int effectNum, Vector3 position, float destroyTime)
    {
        //num
        //0 : parts_stars_out1
        //1 : Star_A
        //2 : CFX4 Aura Bubble C_Change
        //3 : CoinSpin1tr
        if (prefabEffects != null)
        {
            Transform partT = Instantiate(prefabEffects[effectNum]).transform;
            partT.position = position;
            if (!nowEffects.ContainsKey(effectNum))
            {
                List<Transform> tmpTrs = new List<Transform>();
                tmpTrs.Add(partT);
                nowEffects.Add(effectNum, tmpTrs);
            }
            else
            {
                EndEffect(effectNum);
                nowEffects[effectNum].Add(partT);
            }
            StartCoroutine(DestroyAndClearDic(effectNum, destroyTime, partT));
        }
    }

    public void StartEffect(int effectNum, Vector3 position)
    {
        if (prefabEffects != null)
        {
            Transform partT = Instantiate(prefabEffects[effectNum]).transform;
            if (effectNum == 3)
            {
                partT.position = Vector3.zero;
                partT.GetComponent<CoinTween>().startPos = position;
            }
            else
            {
                partT.position = position;
            }
            if (!nowEffects.ContainsKey(effectNum))
            {
                List<Transform> tmpTrs = new List<Transform>();
                tmpTrs.Add(partT);
                nowEffects.Add(effectNum, tmpTrs);
            }
            else
            {
                EndEffect(effectNum);
                nowEffects[effectNum].Add(partT);
            }
        }
    }

    public IEnumerator DestroyAndClearDic(int effectNum, float time, Transform t)
    {
        yield return new WaitForSeconds(time);

        nowEffects[effectNum].Remove(t);
        if (t) Destroy(t.gameObject);
        yield break;
    }

    public void EndEffect(int effectNum)
    {
        if (nowEffects.ContainsKey(effectNum))
        {
            for (int i = 0; i < nowEffects[effectNum].Count; i++)
            {
                StartCoroutine(DestroyAndClearDic(effectNum, 0, nowEffects[effectNum][i]));
            }
        }
    }
    public void EndEffect()
    {
        foreach (KeyValuePair<int, List<Transform>> element in nowEffects)
        {
            for (int i = 0; i < element.Value.Count; i++)
            {
                StartCoroutine(DestroyAndClearDic(element.Key, 0, element.Value[i]));
            }
        }
    }
}

