﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Assets.SimpleLocalization;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;

public class SLCanvas : MonoBehaviour
{
    public GameObject mainCanvasObj;
    public MainCanvas mainCanvas;
    public Transform CharTrRoot;

    //save/load
    public Transform slContentsView;
    public InputField slotNumInputField;

    public List<GameObject> allToggles = new List<GameObject>();
    public GameObject prefabToggleButton;
    
    public bool isSel;
    public int selectedSlot;
    public int slotBoxCount = 0;

    int middleFontsize;

    //알림
    public AlertView alertView;

    private void Awake()
    {
        InitSelectNum0();
        MakeToggles();
        middleFontsize = LocalizationManager.Language == "Japanese" ? 48 : 52;
    }
    private void OnEnable()
    {
        InMainManager.Instance.canvasState = InMainManager.CanvasState.slCanvas;
        if (InMainManager.Instance.nowFriend != null)
        {
            InMainManager.Instance.nowFriend.goTools.SetActive(false);
        }
        InitToggles();
        SetInputField();
        isSel = false;
        UnsetToggleSel();
    }


    //메인화면
    public void OnMainButtonClick()
    {
        ChangeCanvasToMain();
    }

    public void ChangeCanvasToMain()
    {
        gameObject.SetActive(false);
        mainCanvasObj.SetActive(true);
    }

    //저장/불러오기
    public void OnSaveClick()
    {
        int idx = new int();
        idx = selectedSlot;
        if (!isSel)
        {            
            alertView.Alert(LocalizationManager.Localize(StringInfo.SLSelSlotInfo), middleFontsize);

            return;
        }
        if (XMLManager.Instance.isSelectSlot(selectedSlot.ToString()))
        {
            alertView.Alert(string.Format(LocalizationManager.Localize(StringInfo.SLCoverInfo), (selectedSlot + 1)), new AlertViewOptions
            {
                cancelButtonDelegate = () =>
                {
                },
                okButtonDelegate = () =>
                {
                    XMLManager.Instance.SaveMainToSlotXML(idx.ToString());
                    alertView.Alert(LocalizationManager.Localize(StringInfo.SLSaveAl), middleFontsize);
                    InitToggles();
                    if (XMLManager.Instance.nodeALL_SLOT.ChildNodes.Count > 0)
                    {
                        XMLManager.Instance.SaveMissionValueUp(XMLManager.Instance.nodeSaveSlotTimes);
                    }
                    MissionSeed.CheckAllMission();
                }
            });
        }
        else
        {
            alertView.Alert(string.Format(LocalizationManager.Localize(StringInfo.SLSaveInfo), (selectedSlot + 1)), new AlertViewOptions
            {
                cancelButtonDelegate = () =>
                {
                },
                okButtonDelegate = () =>
                {
                    XMLManager.Instance.SaveMainToSlotXML(idx.ToString());
                    alertView.Alert(LocalizationManager.Localize(StringInfo.SLSaveAl), middleFontsize);
                    InitToggles();
                    if (XMLManager.Instance.nodeALL_SLOT.ChildNodes.Count > 0)
                    {
                        XMLManager.Instance.SaveMissionValueUp(XMLManager.Instance.nodeSaveSlotTimes);
                    }
                    MissionSeed.CheckAllMission();
                }
            });
        }
    }

    public void OnLoadClick()
    {
        int idx = new int();
        idx = selectedSlot;
        if (!isSel)
        {
            alertView.Alert(LocalizationManager.Localize(StringInfo.SLSelSlotInfo), middleFontsize);
            return;
        }
        if (XMLManager.Instance.isSelectSlot(selectedSlot.ToString()))
        {
            alertView.Alert(string.Format(LocalizationManager.Localize(StringInfo.SLLoadInfo), (selectedSlot + 1)), new AlertViewOptions
            {
                cancelButtonDelegate = () =>
                {
                },
                okButtonDelegate = () =>
                {
                    InMainManager.Instance.ClearAll();

                    if (XMLManager.Instance.LoadSlotToMain(idx) == false)
                    {
                        return;
                    }
                    mainCanvas.SetBottomToolBar((FriendType)mainCanvas.nowObjMenu);
                    InMainManager.Instance.InitAllChar();
                    InMainManager.Instance.InitBG();
                    InMainManager.Instance.InitAllObj(2);
                    InMainManager.Instance.InitAllObj(3);
                    InMainManager.Instance.InitAllObj(4);
                    InMainManager.Instance.InitAllObj(5);
                    InMainManager.Instance.InitAllObj(6);
                    alertView.Alert(LocalizationManager.Localize(StringInfo.SLLoadAl), middleFontsize);
                    InitToggles();
                }
            });
        }
        else
        {
            alertView.Alert(LocalizationManager.Localize(StringInfo.SLEmpty), middleFontsize);
        }
    }

    public void InitSelectNum0()
    {
        selectedSlot = 0;
        isSel = false;
    }

    //토글 생성
    public void MakeToggles()
    {
        allToggles.Clear();

        for (int i = 0; i < 3; i++)
        {
            int idx = new int();
            idx = i;
            int slotNum = (slotBoxCount * 3) + idx;

            GameObject obj = Instantiate(prefabToggleButton, slContentsView);

            allToggles.Add(obj);
            obj.GetComponent<Toggle>().group = obj.transform.parent.GetComponent<ToggleGroup>();
        }
    }

    //토글 인잇
    public void InitToggles()
    {
        SetSlotNumText();

        for (int i = 0; i < 3; i++)
        {
            int idx = new int();
            idx = i;
            int slotNum = (slotBoxCount * 3) + idx;
            CharButtonUI charButtonUI = allToggles[idx].GetComponent<CharButtonUI>();

            allToggles[idx].GetComponent<Toggle>().onValueChanged.RemoveAllListeners();
            GameObject selImgObj = charButtonUI.selToggleImg;

            allToggles[idx].GetComponent<Toggle>().onValueChanged.AddListener((bool value) =>
            {
                OnToggleValueChange(selImgObj, value, slotNum);
            });

            charButtonUI.SetNumText((slotNum + 1).ToString());
            charButtonUI.SetAlert(alertView);
            charButtonUI.slotSeq = slotNum;
            if (XMLManager.Instance.isSelectSlot(slotNum.ToString()))
            {
                charButtonUI.SetNameText(charButtonUI.GetSlotName());
                charButtonUI.LoadAllSlotToggleRen(slotNum);
                charButtonUI.SetBG(slotNum);
            }
            else
            {
                int slotNumPlus = slotNum + 1;

                charButtonUI.SetNameText(string.Format(StringInfo.SLName, slotNumPlus));
                charButtonUI.ClearAllRen();
            }
        }
    }

    //토글선택해제
    public void UnsetToggleSel()
    {
        for (int i = 0; i < 3; i++) {
            allToggles[i].GetComponent<Toggle>().isOn = false; 
        }
    }
    //토글 선택
    public void OnToggleValueChange(GameObject selImgObj, bool sel, int num)
    {
        if (sel)
        {
            selImgObj.SetActive(true);
            isSel = true;
            selectedSlot = num;
        }
        else
        {
            selImgObj.SetActive(false);
            isSel = false;
        }
    }

    //슬롯이동
    public void OnLeftArrowClick()
    {
        UnsetToggleSel();
        if (slotBoxCount > 0)
        {
            slotBoxCount--;
            InitToggles();
            SetSlotNumText();
        }
    }
    public void OnRightArrowClick()
    {
        UnsetToggleSel();
        if (slotBoxCount < 32)
        {
            slotBoxCount++;
            InitToggles();
            SetSlotNumText();
        }
    }


    //슬롯 인풋필드
    public void SetSlotNumText()
    {
        slotNumInputField.text = (slotBoxCount+1).ToString();
    }
    //인풋필드 입력
    public void SetInputField()
    {
        slotNumInputField.onEndEdit.AddListener((string text) =>
        {
            int textInt = int.Parse(text);
            if (textInt > 33)
            {
                slotNumInputField.text = StringInfo.MaxSlot;
                textInt = 33;
            }
            if (textInt < 1)
            {
                slotNumInputField.text = StringInfo.MinSlot;
                textInt = 1;
            }
            slotBoxCount = textInt - 1;
            InitToggles();
            UnsetToggleSel();
        });
    }
}