﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour, IStoreListener
{
    public static IAPManager Instance = null;

    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;

    // 상품 구매 진행 중인지 여부
    private bool isBuyProgress;
    private string buyProductId;

    private Action<bool> initCallback;
    public delegate void PurchaseSuccess(Product product);
    PurchaseSuccess purchaseSuccess;
    public delegate void PurchaseFail();
    PurchaseFail purchaseFail;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    #region Actions-Events
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        DebugX.Log("IAPManager OnInitialized");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;

        foreach (Product product in m_StoreController.products.all)
        {
            DebugX.Log("IAPManager transactionID : " + product.transactionID);
            DebugX.Log("IAPManager receipt : " + product.receipt);
            DebugX.Log("IAPManager metadata isoCurrencyCode : " + product.metadata.isoCurrencyCode);
            DebugX.Log("IAPManager metadata localizedDescription : " + product.metadata.localizedDescription);
            DebugX.Log("IAPManager metadata localizedPrice : " + product.metadata.localizedPrice);
            DebugX.Log("IAPManager metadata localizedPriceString : " + product.metadata.localizedPriceString);
            DebugX.Log("IAPManager metadata localizedTitle : " + product.metadata.localizedTitle);
            DebugX.Log("IAPManager hasReceipt : " + product.hasReceipt);
            DebugX.Log("IAPManager availableToPurchase : " + product.availableToPurchase);
            DebugX.Log("IAPManager enabled : " + product.definition.enabled);
            DebugX.Log("IAPManager id : " + product.definition.id);
            DebugX.Log("IAPManager payout : " + product.definition.payout);
            DebugX.Log("IAPManager payouts : " + product.definition.payouts);
            DebugX.Log("IAPManager storeSpecificId : " + product.definition.storeSpecificId);
            DebugX.Log("IAPManager type : " + product.definition.type);

            if (Application.platform != RuntimePlatform.IPhonePlayer &&
                Application.platform != RuntimePlatform.OSXPlayer)
            {
                if (product != null &&
                    product.hasReceipt)
                {
                    PlayerPrefs.SetInt(product.definition.id, 1);
                }
                else
                {
                    PlayerPrefs.SetInt(product.definition.id, 0);
                }
            }
        }
        InitComplete(true);
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        DebugX.Log("IAPManager OnInitializeFailed // " + error);
        InitComplete(false);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        DebugX.Log("IAPManager ProcessPurchase");
        Product product = args.purchasedProduct;
        DebugX.Log("IAPManager buyProductId // " + buyProductId);
        DebugX.Log("IAPManager id // " + product.definition.id + ", buyProductId // " + buyProductId);
        DebugX.Log("IAPManager product // " + product);
        DebugX.Log("IAPManager product.hasReceipt // " + product.hasReceipt);
        PlayerPrefs.SetInt(product.definition.id, 1);
        if (product.definition.id.Equals(buyProductId))
        {
            purchaseFail = null;
            if (purchaseSuccess != null)
            {
                purchaseSuccess.Invoke(product);
                purchaseSuccess = null;
            }

            isBuyProgress = false;
        }
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        DebugX.Log("IAPManager OnPurchaseFailed // " + failureReason);
        purchaseSuccess = null;
        if (purchaseFail != null)
        {
            purchaseFail.Invoke();
            purchaseFail = null;
        }
        isBuyProgress = false;
    }
    #endregion

    /// <summary>
    /// 인앱 결제 초기화
    /// </summary>
    /// <param name="initCallback"></param>
    public void Init(Action<bool> initCallback)
    {
        this.initCallback = initCallback;

        InitializePurchasing();
    }

    public void InitializePurchasing()
    {
        if (IsInitialized())
        {
            InitComplete(true);
            return;
        }

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            InitComplete(false);
            return;
        }

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        for (int i = 0; i < XMLManager.Instance.SelectAllBundle().Count; i++)
        {
            string productId = XMLManager.Instance.LoadBundleProductId(i.ToString());
            builder.AddProduct(productId, ProductType.NonConsumable, new IDs()
            {
                { productId, GooglePlay.Name },
                { productId, AppleAppStore.Name },
            });
        }

        UnityPurchasing.Initialize(this, builder);
    }

    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    private void InitComplete(bool isSuccess)
    {
        XMLManager.Instance.SaveUserHave_RmWaterMark(false);
        XMLManager.Instance.SaveUserHave_RmAD(false);
        XMLManager.Instance.IsStoreHaveItem();

        // 골드 구매 상품
        List<int> haveBundles = XMLManager.Instance.LoadStoreBundleList();

        // 인앱 구매 상품
        for (int i = 0; i < XMLManager.Instance.SelectAllBundle().Count; i++)
        {
            string productId = XMLManager.Instance.LoadBundleProductId(i.ToString());
            bool isPurchased = IAPManager.Instance.HadPurchased(productId);
            if (isPurchased)
            {
                // 중복 제외
                if (!haveBundles.Contains(i))
                {
                    haveBundles.Add(i);
                }
            }
        }

        // 모든 구매 상품
        for (int i = 0; i < haveBundles.Count; i++)
        {
            if (XMLManager.Instance.LoadBundleRmWaterMark(haveBundles[i].ToString()))
            {
                XMLManager.Instance.SaveUserHave_RmWaterMark(true);
            }
            if (XMLManager.Instance.LoadBundleRmAd(haveBundles[i].ToString()))
            {
                XMLManager.Instance.SaveUserHave_RmAD(true);
            }
            List<UserHave> allBundleItems = XMLManager.Instance.LoadAllBundleItem(haveBundles[i].ToString());
            for (int j = 0; j < allBundleItems.Count; j++)
            {
                XMLManager.Instance.AddUserHaveItem(allBundleItems[j].seqName, allBundleItems[j].itemSeq, ItemGetRoot.store);
            }
        }

        initCallback?.Invoke(isSuccess);
    }

    /// <summary>
    /// 인앱 상품 구매
    /// </summary>
    /// <param name="productId"></param>
    /// <param name="purchaseSuccess"></param>
    /// <param name="purchaseFail"></param>
    public void BuyProductID(string productId, PurchaseSuccess purchaseSuccess, PurchaseFail purchaseFail)
    {
        DebugX.Log("IAPManager BuyProductID // isBuyProgress : " + isBuyProgress);
        if (isBuyProgress)
        {
            return;
        }
        isBuyProgress = true;

        this.buyProductId = productId;
        this.purchaseSuccess = purchaseSuccess;
        this.purchaseFail = purchaseFail;

        if (!IsInitialized())
        {
            Init(isSuccess =>
            {
                if (isSuccess &&
                    IsInitialized())
                {
                    DebugX.Log("IAPManager 초기화 성공");
                    RefreshScene();
                    ReqestPurchase(productId);
                }
                else
                {
                    DebugX.Log("IAPManager 초기화 실패");
                    purchaseSuccess = null;
                    if (purchaseFail != null)
                    {
                        purchaseFail.Invoke();
                        purchaseFail = null;
                    }
                    isBuyProgress = false;
                }
            });
        }
        else
        {
            ReqestPurchase(productId);
        }
    }

    /// <summary>
    /// 인앱 상품 결제 요청
    /// </summary>
    /// <param name="productId"></param>
    private void ReqestPurchase(string productId)
    {
        DebugX.Log("IAPManager ReqestPurchase");

        Product product = m_StoreController.products.WithID(productId);
        if (product != null && product.availableToPurchase)
        {
            DebugX.Log("IAPManager ReqestPurchase Start");
            m_StoreController.InitiatePurchase(product);
        }
        else
        {
            DebugX.Log("IAPManager ReqestPurchase Fail");
            purchaseSuccess = null;
            if (purchaseFail != null)
            {
                purchaseFail.Invoke();
                purchaseFail = null;
            }
            isBuyProgress = false;
        }
    }

    /// <summary>
    /// 복원
    /// </summary>
    /// <param name="restoreSuccess"></param>
    /// <param name="restoreFail"></param>
    public void RestorePurchases(Action restoreSuccess, Action restoreFail)
    {
        if (!IsInitialized())
        {
            if (restoreFail != null)
            {
                restoreFail.Invoke();
            }
            DebugX.Log("IAPManager RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            DebugX.Log("IAPManager RestorePurchases started ...");
            foreach (Product product in m_StoreController.products.all)
            {
                PlayerPrefs.SetInt(product.definition.id, 0);
            }

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                DebugX.Log("IAPManager RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                if (result)
                {
                    RefreshScene();
                    if (restoreSuccess != null)
                    {
                        restoreSuccess.Invoke();
                    }
                }
                else
                {
                    if (restoreFail != null)
                    {
                        restoreFail.Invoke();
                    }
                }
            });
        }
        else
        {
            DebugX.Log("IAPManager RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            if (restoreFail != null)
            {
                restoreFail.Invoke();
            }
        }
    }

    public Product GetProduct(string productId)
    {
        Product product = null;
        if (IsInitialized())
        {
            product = m_StoreController.products.WithID(productId);
        }
        return product;
    }

    public bool HadPurchased(string productId)
    {
        if (IsInitialized())
        {
            return PlayerPrefs.GetInt(productId, 0) == 1;
        }
        else
        {
            DebugX.Log("IAPManager HadPurchased FAIL. Not initialized.");
            return false;
        }
    }

    private void RefreshScene()
    {
        AdsManager.Instance.RefreshBanner();
        InGameManager.Instance.InitItemGet();
    }
}
