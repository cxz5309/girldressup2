using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;

public class AdsManager : MonoBehaviour
{
    public static AdsManager Instance = null;

    const bool IS_TEST = false;

    public float bannerHeight;

    private BannerView bannerView;
    private RewardedAd getGoldAd;
    private RewardedAd attendDoubleAd;
    private RewardedAd rouletteAd;
    private InterstitialAd exitMakingAd;

    private bool isLoadingBanner;
    private bool isLoadedBanner;
    private bool isLoadingGetGoldAd;
    private bool isCompleteGetGoldAd;
    private bool isLoadingAttendDoubleAd;
    private bool isCompleteAttendDoubleAd;
    private bool isLoadingRouletteAd;
    private bool isCompleteRouletteAd;
    private bool isLoadingExitMakingAd;

    public static event Action<float> OnLoadedBanner = delegate { };
    private Action<AdsCallbackState> callback;

    [Header("AD UNIT ID")]
    public string androidBannerId;
    public string iOSBannerId;
    public string androidGetGoldId;
    public string iOSGetGoldId;
    public string androidAttendDoubleId;
    public string iOSAttendDoubleId;
    public string androidRouletteId;
    public string iOSRouletteId;
    public string androidExitMakingId;
    public string iOSExitMakingId;

    public enum AdsCallbackState
    {
        Fail, // 광고 시청 실패
        Loading, // 광고 요청하고 로딩 중
        Success // 광고 시청 완료
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if (isCompleteGetGoldAd)
        {
            isCompleteGetGoldAd = false;
            Complete();
        }
        else if (isCompleteAttendDoubleAd)
        {
            isCompleteAttendDoubleAd = false;
            Complete();
        }
        else if (isCompleteRouletteAd)
        {
            isCompleteRouletteAd = false;
            Complete();
        }
    }

    private void Complete()
    {
        callback?.Invoke(AdsCallbackState.Success);
        callback = null;
    }

    /// <summary>
    /// 광고 초기화
    /// </summary>
    public void InitAds()
    {
        RequestGetGoldVideo();
        RequestAttendDoubleVideo();
        RequestRouletteVideo();
        //RequestExitMaking();
    }

    #region 배너
    /// <summary>    
    /// 배너 요청
    /// (배너는 메인 진입 완료 시 요청)
    /// </summary>
    public void RequestBanner()
    {
        if (isLoadedBanner)
        {
            OnLoadedBanner(bannerHeight);
            return;
        }
        if (isLoadingBanner)
        {
            OnLoadedBanner(bannerHeight);
            return;
        }
        isLoadingBanner = true;

        string adUnitId;
        if (IS_TEST)
        {
            #if UNITY_ANDROID
                adUnitId = "ca-app-pub-3940256099942544/6300978111";
            #elif UNITY_IPHONE
                adUnitId = "ca-app-pub-3940256099942544/2934735716";
            #else
		        adUnitId = "unexpected_platform";
            #endif
        }
        else
        {
            #if UNITY_ANDROID
                adUnitId = androidBannerId;
            #elif UNITY_IPHONE
                adUnitId = iOSBannerId;
            #else
		        adUnitId = "unexpected_platform";
            #endif
        }

        // 배너 삭제
        if (bannerView != null)
        {
            bannerView.Destroy();
            bannerView = null;
        }

        bannerHeight = 0;
        OnLoadedBanner(bannerHeight);

        // 광고 제거 구매 여부
        // 구매 시 이후 작업 안함
        if (XMLManager.Instance.LoadUserHave_RmAD())
        {
            isLoadingBanner = false;
            return;    
        }

        AdSize adaptiveSize = AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth);
        bannerView = new BannerView(adUnitId, adaptiveSize, AdPosition.Top);
        bannerView.OnAdLoaded += HandleOnAdLoaded;
        bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        bannerView.OnAdOpening += HandleOnAdOpened;
        bannerView.OnAdClosed += HandleOnAdClosed;
        bannerView.OnAdLeavingApplication += HandleOnAdLeavingApplication;

        if (IS_TEST)
        {
            AdRequest request = new AdRequest.Builder()
                .AddTestDevice("061E9CAB6528A6CDB67F6FF0AD652448")
                .Build();
            bannerView.LoadAd(request);
        }
        else
        {
            AdRequest request = new AdRequest.Builder()
                .Build();
            bannerView.LoadAd(request);
        }
    }

    public void RefreshBanner()
    {
        isLoadedBanner = false;
        RequestBanner();
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        bannerHeight = bannerView.GetHeightInPixels();
        isLoadedBanner = true;
        isLoadingBanner = false;
        OnLoadedBanner(bannerHeight);
        DebugX.Log(string.Format("Height: {0}, Width: {1}",
            bannerView.GetHeightInPixels(),
            bannerView.GetWidthInPixels()));
        DebugX.Log("HandleOnAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        isLoadingBanner = false;
        DebugX.Log("HandleOnAdFailedToLoad event received with message: " + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        DebugX.Log("HandleOnAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        DebugX.Log("HandleOnAdClosed event received");
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        DebugX.Log("HandleOnAdLeavingApplication event received");
    }
    #endregion

    #region 골드 획득
    /// <summary>
    /// 골드 획득 광고 요청
    /// </summary>
    private void RequestGetGoldVideo()
    {
        if (getGoldAd != null &&
            getGoldAd.IsLoaded())
        {
            return;
        }
        if (isLoadingGetGoldAd)
        {
            return;
        }
        isLoadingGetGoldAd = true;

        string adUnitId;
        if (IS_TEST)
        {
            #if UNITY_ANDROID
                adUnitId = "ca-app-pub-3940256099942544/5224354917";
            #elif UNITY_IPHONE
                adUnitId = "ca-app-pub-3940256099942544/1712485313";
            #else
                adUnitId = "unexpected_platform";
            #endif
        }
        else
        {
            #if UNITY_ANDROID
                adUnitId = androidGetGoldId;
            #elif UNITY_IPHONE
                adUnitId = iOSGetGoldId;
            #else
                adUnitId = "unexpected_platform";
            #endif
        }

        getGoldAd = new RewardedAd(adUnitId);
        getGoldAd.OnAdLoaded += HandleRewardedAdLoadedWithGetGold;
        getGoldAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoadWithGetGold;
        getGoldAd.OnAdOpening += HandleRewardedAdOpeningWithGetGold;
        getGoldAd.OnAdFailedToShow += HandleRewardedAdFailedToShowWithGetGold;
        getGoldAd.OnUserEarnedReward += HandleUserEarnedRewardWithGetGold;
        getGoldAd.OnAdClosed += HandleRewardedAdClosedWithGetGold;

        if (IS_TEST)
        {
            AdRequest request = new AdRequest.Builder()
                .AddTestDevice("061E9CAB6528A6CDB67F6FF0AD652448")
                .Build();
            getGoldAd.LoadAd(request);
        }
        else
        {
            AdRequest request = new AdRequest.Builder()
                .Build();
            getGoldAd.LoadAd(request);
        }
    }

    /// <summary>
    /// 광고 시청
    /// </summary>
    public void WatchVideoWithGetGold(Action<AdsCallbackState> callback)
    {
        if (getGoldAd != null &&
            getGoldAd.IsLoaded())
        {
            this.callback = callback;
            getGoldAd.Show();
        }
        else
        {
            // 광고 로딩 중 알림창 필요
            callback?.Invoke(AdsCallbackState.Loading);
            RequestGetGoldVideo();
        }
    }

	public void HandleRewardedAdLoadedWithGetGold(object sender, EventArgs args)
    {
        isLoadingGetGoldAd = false;
    }

    public void HandleRewardedAdFailedToLoadWithGetGold(object sender, AdErrorEventArgs args)
    {
        isLoadingGetGoldAd = false;
    }

    public void HandleRewardedAdOpeningWithGetGold(object sender, EventArgs args)
    {
        
    }

    public void HandleRewardedAdFailedToShowWithGetGold(object sender, AdErrorEventArgs args)
    {

    }

    public void HandleUserEarnedRewardWithGetGold(object sender, Reward args)
    {
		isCompleteGetGoldAd = true;
    }

    public void HandleRewardedAdClosedWithGetGold(object sender, EventArgs args)
    {
        RequestGetGoldVideo();
    }
    #endregion

    #region 출석 보상 두배 획득
    /// <summary>
    /// 출석 보상 두배 획득 광고 요청
    /// </summary>
    private void RequestAttendDoubleVideo()
    {
        if (attendDoubleAd != null &&
            attendDoubleAd.IsLoaded())
        {
            return;
        }
        if (isLoadingAttendDoubleAd)
        {
            return;
        }
        isLoadingAttendDoubleAd = true;

        string adUnitId;
        if (IS_TEST)
        {
            #if UNITY_ANDROID
                adUnitId = "ca-app-pub-3940256099942544/5224354917";
            #elif UNITY_IPHONE
                adUnitId = "ca-app-pub-3940256099942544/1712485313";
            #else
                adUnitId = "unexpected_platform";
            #endif
        }
        else
        {
            #if UNITY_ANDROID
                adUnitId = androidAttendDoubleId;
            #elif UNITY_IPHONE
                adUnitId = iOSAttendDoubleId;
            #else
                adUnitId = "unexpected_platform";
            #endif
        }

        attendDoubleAd = new RewardedAd(adUnitId);
        attendDoubleAd.OnAdLoaded += HandleRewardedAdLoadedWithAttendDouble;
        attendDoubleAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoadWithAttendDouble;
        attendDoubleAd.OnAdOpening += HandleRewardedAdOpeningWithAttendDouble;
        attendDoubleAd.OnAdFailedToShow += HandleRewardedAdFailedToShowWithAttendDouble;
        attendDoubleAd.OnUserEarnedReward += HandleUserEarnedRewardWithAttendDouble;
        attendDoubleAd.OnAdClosed += HandleRewardedAdClosedWithAttendDouble;

        if (IS_TEST)
        {
            AdRequest request = new AdRequest.Builder()
                .AddTestDevice("061E9CAB6528A6CDB67F6FF0AD652448")
                .Build();
            attendDoubleAd.LoadAd(request);
        }
        else
        {
            AdRequest request = new AdRequest.Builder()
                .Build();
            attendDoubleAd.LoadAd(request);
        }
    }

    /// <summary>
    /// 광고 시청
    /// </summary>
    public void WatchVideoWithAttendDouble(Action<AdsCallbackState> callback)
    {
        if (attendDoubleAd != null &&
            attendDoubleAd.IsLoaded())
        {
            this.callback = callback;
            attendDoubleAd.Show();
        }
        else
        {
            // 광고 로딩 중 알림창 필요
            callback?.Invoke(AdsCallbackState.Loading);
            RequestAttendDoubleVideo();
        }
    }

	public void HandleRewardedAdLoadedWithAttendDouble(object sender, EventArgs args)
    {
        isLoadingAttendDoubleAd = false;
    }

    public void HandleRewardedAdFailedToLoadWithAttendDouble(object sender, AdErrorEventArgs args)
    {
        isLoadingAttendDoubleAd = false;
    }

    public void HandleRewardedAdOpeningWithAttendDouble(object sender, EventArgs args)
    {
        
    }

    public void HandleRewardedAdFailedToShowWithAttendDouble(object sender, AdErrorEventArgs args)
    {

    }

    public void HandleUserEarnedRewardWithAttendDouble(object sender, Reward args)
    {
		isCompleteAttendDoubleAd = true;
    }

    public void HandleRewardedAdClosedWithAttendDouble(object sender, EventArgs args)
    {
        RequestAttendDoubleVideo();
    }
    #endregion

    #region 룰렛 돌리기
    /// <summary>
    /// 룰렛 돌리기 광고 요청
    /// </summary>
    private void RequestRouletteVideo()
    {
        if (rouletteAd != null &&
            rouletteAd.IsLoaded())
        {
            return;
        }
        if (isLoadingRouletteAd)
        {
            return;
        }
        isLoadingRouletteAd = true;

        string adUnitId;
        if (IS_TEST)
        {
            #if UNITY_ANDROID
                adUnitId = "ca-app-pub-3940256099942544/5224354917";
            #elif UNITY_IPHONE
                adUnitId = "ca-app-pub-3940256099942544/1712485313";
            #else
                adUnitId = "unexpected_platform";
            #endif
        }
        else
        {
            #if UNITY_ANDROID
                adUnitId = androidRouletteId;
            #elif UNITY_IPHONE
                adUnitId = iOSRouletteId;
            #else
                adUnitId = "unexpected_platform";
            #endif
        }

        rouletteAd = new RewardedAd(adUnitId);
        rouletteAd.OnAdLoaded += HandleRewardedAdLoadedWithRoulette;
        rouletteAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoadWithRoulette;
        rouletteAd.OnAdOpening += HandleRewardedAdOpeningWithRoulette;
        rouletteAd.OnAdFailedToShow += HandleRewardedAdFailedToShowWithRoulette;
        rouletteAd.OnUserEarnedReward += HandleUserEarnedRewardWithRoulette;
        rouletteAd.OnAdClosed += HandleRewardedAdClosedWithRoulette;

        if (IS_TEST)
        {
            AdRequest request = new AdRequest.Builder()
                .AddTestDevice("061E9CAB6528A6CDB67F6FF0AD652448")
                .Build();
            rouletteAd.LoadAd(request);
        }
        else
        {
            AdRequest request = new AdRequest.Builder()
                .Build();
            rouletteAd.LoadAd(request);
        }
    }

    /// <summary>
    /// 광고 시청
    /// </summary>
    public void WatchVideoWithRoulette(Action<AdsCallbackState> callback)
    {
        if (rouletteAd != null &&
            rouletteAd.IsLoaded())
        {
            this.callback = callback;
            rouletteAd.Show();
        }
        else
        {
            // 광고 로딩 중 알림창 필요
            callback?.Invoke(AdsCallbackState.Loading);
            RequestRouletteVideo();
        }
    }

	public void HandleRewardedAdLoadedWithRoulette(object sender, EventArgs args)
    {
        isLoadingRouletteAd = false;
    }

    public void HandleRewardedAdFailedToLoadWithRoulette(object sender, AdErrorEventArgs args)
    {
        isLoadingRouletteAd = false;
    }

    public void HandleRewardedAdOpeningWithRoulette(object sender, EventArgs args)
    {
        
    }

    public void HandleRewardedAdFailedToShowWithRoulette(object sender, AdErrorEventArgs args)
    {

    }

    public void HandleUserEarnedRewardWithRoulette(object sender, Reward args)
    {
		isCompleteRouletteAd = true;
    }

    public void HandleRewardedAdClosedWithRoulette(object sender, EventArgs args)
    {
        RequestRouletteVideo();
    }
    #endregion

    #region 꾸미기 종료
    /// <summary>
    /// 꾸미기 종료 광고 요청
    /// </summary>
    private void RequestExitMaking()
    {
        if (exitMakingAd != null &&
            exitMakingAd.IsLoaded())
        {
            return;
        }
        if (isLoadingExitMakingAd)
        {
            return;
        }
        isLoadingExitMakingAd = true;

        string adUnitId;
        if (IS_TEST)
        {
            #if UNITY_ANDROID
                adUnitId = "ca-app-pub-3940256099942544/1033173712";
            #elif UNITY_IPHONE
                adUnitId = "ca-app-pub-3940256099942544/4411468910";
            #else
                adUnitId = "unexpected_platform";
            #endif
        }
        else
        {
            #if UNITY_ANDROID
                adUnitId = androidExitMakingId;
            #elif UNITY_IPHONE
                adUnitId = iOSExitMakingId;
            #else
                adUnitId = "unexpected_platform";
            #endif
        }

        exitMakingAd = new InterstitialAd(adUnitId);
        exitMakingAd.OnAdLoaded += HandleOnAdLoadedWithExitMaking;
        exitMakingAd.OnAdFailedToLoad += HandleOnAdFailedToLoadWithExitMaking;
        exitMakingAd.OnAdOpening += HandleOnAdOpenedWithExitMaking;
        exitMakingAd.OnAdClosed += HandleOnAdClosedWithExitMaking;
        exitMakingAd.OnAdLeavingApplication += HandleOnAdLeavingApplicationWithExitMaking;

        if (IS_TEST)
        {
            AdRequest request = new AdRequest.Builder()
                .AddTestDevice("061E9CAB6528A6CDB67F6FF0AD652448")
                .Build();
            exitMakingAd.LoadAd(request);
        }
        else
        {
            AdRequest request = new AdRequest.Builder()
                .Build();
            exitMakingAd.LoadAd(request);
        }
    }

    public void HandleOnAdLoadedWithExitMaking(object sender, EventArgs args)
    {
        isLoadingExitMakingAd = false;
    }

    public void HandleOnAdFailedToLoadWithExitMaking(object sender, AdFailedToLoadEventArgs args)
    {
        isLoadingExitMakingAd = false;
    }

    public void HandleOnAdOpenedWithExitMaking(object sender, EventArgs args)
    {

    }

    public void HandleOnAdClosedWithExitMaking(object sender, EventArgs args)
    {
        RequestExitMaking();
    }

    public void HandleOnAdLeavingApplicationWithExitMaking(object sender, EventArgs args)
    {

    }

    /// <summary>
    /// 광고 시청
    /// </summary>
    public void WatchAdWithExitMaking()
    {
        if (exitMakingAd != null &&
            exitMakingAd.IsLoaded())
        {
            exitMakingAd.Show();
        }
        else
        {
            RequestExitMaking();
        }
    }
    #endregion
}
