﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class DebugManager : MonoBehaviour
{
    public static DebugManager Instance;
    public static Text thisText;
    private void OnEnable()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        //DontDestroyOnLoad(gameObject);

        try
        {
            thisText = GameObject.Find("Debug").GetComponent<Text>();
        }
        catch(Exception e)
        {
            Debug.Log(e);
        }
    }

    public static void ScreenDebug(string text)
    {
        if (thisText != null)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string prev = thisText.text;
            stringBuilder.Append(prev).Append("\n").Append(text);
            thisText.text = stringBuilder.ToString();
            if (thisText.text.Length > 1500)
            {
                thisText.text = "";
            }
        }
    }
}
