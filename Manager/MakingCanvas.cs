using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.U2D;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using Assets.SimpleLocalization;

public class MakingCanvas : MonoBehaviour
{
    public GameObject prefabPartButton;
    public GameObject prefabColorButton;

    public Transform bottomElementContentsRoot;
    public SimpleObjectPool ContentObjectPool;

    //캐릭터 파트 생성
    public CharPartSetting charPartSetting;

    //터치 막기
    public GameObject dontTouchPanel;

    //패널(위에 있는 순서대로)
    public AlertView alertView;
    public ItemGetPopupUI itemGetPopupUI;
    public SettingView settingView;
    public CollectionViewUI collectionView;
    public FortuneWheelView fortuneWheelView;
    public DailyView dailyView;
    public StoreView storeView;
    public MissionView missionView;
    public GoADView goADView;
    public MoreGameView moreGameView;

    public GameObject coinText;

    //멀티 설명
    public GameObject multiInfo;

    //상점
    public Transform menuContent;

    //바지 
    public GameObject PantChangeButton;

    //캐릭터
    public Friend makingFriend;

    //빈 하단 뷰 텍스트상태
    public GameObject charactorView;
    public GameObject textView;
    public Text textViewText;

    //칼라피커
    public GameObject colorView;
    public GameObject colorButtonView;
    public GameObject colorPicker;

    private ColorPickerTriangle CP;
    public SpriteRenderer ren;
    public Dictionary<int, SpriteRenderer> handRenDic = new Dictionary<int, SpriteRenderer>();
    private Color savedColor;

    //칼라피커 RGB입력
    public RGBPointerView rgbPointerView;

    public OrderType nowPartOrder;
    public KindType nowPartKind;
    public OrderType backPartOrder;
    public OrderType alterPartOrder;

    public bool isSp;
    public bool isBack;
    public bool isPaint;

    public bool isBodyBack;
    public bool isHand;
    public CharPart handPart;

    //타입에 따른 해금
    Queue<CharPart> charPartType1Q = new Queue<CharPart>();
    Queue<CharPart> charPartType2Q = new Queue<CharPart>();
    Queue<CharPart> storeQ = new Queue<CharPart>();
    Queue<CharPart> specialQ = new Queue<CharPart>();

    // 배너 광고로 위치 변경할 오브젝트 리스트
    public RectTransform[] bannerChangeTransforms;

    private void Start()
    {
        InGameManager.Instance.GetCoinText(coinText);
        InGameManager.Instance.GetCoinXmlAndShow();
        if (!InGameManager.Instance.firstDay)
        {
            if (InGameManager.Instance.newToday)
            {
                if (!InGameManager.Instance.dismissDaily)
                {
                    StartCoroutine(SetDaliy());
                }
            }
        }

        // 배너 광고 초기화
        AdsManager.OnLoadedBanner += OnLoadedBanner;
        AdsManager.Instance.RequestBanner();
    }

    private void OnDestroy()
    {
        AdsManager.OnLoadedBanner -= OnLoadedBanner;
    }

    #region Actions-Events
    public void OnLoadedBanner(float bannerHeight)
    {
        if (bannerHeight <= 0)
        {
            bannerHeight = 0f;
        }

        for (int i = 0; i < bannerChangeTransforms.Length; i++)
        {
            Vector2 position = bannerChangeTransforms[i].offsetMax;
            position.y = -bannerHeight;
            bannerChangeTransforms[i].offsetMax = position;
        }
    }
    #endregion

    //1초 후 데일리뷰
    IEnumerator SetDaliy()
    {
        dontTouchPanel.SetActive(true);
        yield return new WaitForSeconds(.5f);
        OnDailyClick();
        dontTouchPanel.SetActive(false);
        yield break;
    }

    public void OnSettingClick()
    {
        settingView.goSettingPanel.SetActive(true);
    }

    public void OnDailyClick()
    {
        dailyView.goDailyPanel.SetActive(true);
    }

    public void OnMoreGameClick()
    {
        moreGameView.goMoreGamePanel.SetActive(true);
    }

    public void OnPartMenuClick(string nowButtonMenu)
    {
        rgbPointerView.SetTextNull();
        CreateEachPartButton(nowButtonMenu);
    }

    public void InitColorBack()
    {
        ren = null;
        //if (isHand)
        //{
        //    handRenDic.Clear();
        //    //handRen[0] = null;
        //    //handRen[1] = null;
        //    //handRen[2] = null;
        //}
        isPaint = false;
        isBack = false;
        isSp = false;
    }

    //현재 올라와 있는 파트의 정보를 받아서 칼라가 변경 가능한 상태인지 확인
    public void FindThisPart()
    {
        isSp = false;
        isBack = false;

        if (nowPartKind == KindType.Body || nowPartKind == KindType.BackHair || nowPartKind == KindType.FrontHair || nowPartKind == KindType.SideHair || nowPartKind == KindType.Eyebrow)
        {
            if (XMLManager.Instance.isPartByPartKind((int)nowPartKind))
            {
                isSp = true;
                //SetColorBack(nowButtonMenu);
                if (XMLManager.Instance.isPartByPartKind((int)nowPartKind - 1))
                {
                    isBack = true;
                    SetPartName();
                    SetRenderObject();
                }
            }
            else
            {
                InitColorBack();
            }
        }
        else
        {
            InitColorBack();
        }
    }

    //각각 버튼 만들기
    public void CreateEachPartButton(string nowButtonMenu)
    {
        nowPartOrder = CharPartOrder.StringToEnum(nowButtonMenu);
        nowPartKind = CharPartOrder.OrderNumToKindNum((int)nowPartOrder);

        Dictionary<int, CharPart> thispartsDic = XMLManager.Instance.listAllPartDic[(int)nowPartOrder];
        List<int> thispartsDicKeys = XMLManager.Instance.listAllPartKeysDic[(int)nowPartOrder];
        //현재 올라와 있는 파트의 정보를 받아서 칼라가 변경 가능한 상태인지 확인
        FindThisPart();
        //하단 버튼 삭제
        for (int i = 0; i < bottomElementContentsRoot.childCount; i++)
        {
            if (bottomElementContentsRoot.GetChild(i).CompareTag("OtherButton"))
            {
                Destroy(bottomElementContentsRoot.GetChild(i).gameObject);
            }
            else
            {
                bottomElementContentsRoot.GetChild(i).gameObject.SetActive(false);
            }
        }

        //페인트 켜고 캐릭터 이동시키는 작업
        if (nowPartOrder != OrderType.BackHair && nowPartOrder != OrderType.SideHair && nowPartOrder != OrderType.FrontHair && nowPartOrder != OrderType.Eyebrow || !isSp)
        {
            UnSetPaintCharPosition();
        }
        else
        {
            SetPaintCharPosition();
        }

        //바지 레이어변경 버튼 생성 작업
        if (nowPartOrder == OrderType.Skirt || nowPartOrder == OrderType.Pants || nowPartOrder == OrderType.Shirts)
        {
            PantChangeButton.SetActive(true);
        }
        else
        {
            PantChangeButton.SetActive(false);
        }

        //멀티 알림 생성 작업
        if (nowPartOrder == OrderType.FaceAc)
        {
            multiInfo.SetActive(true);
        }
        else
        {
            multiInfo.SetActive(false);
        }

        //맨앞 지우기 버튼 또는 칼라피커
        if (nowPartOrder == OrderType.Body)
        {
            if (textView.activeSelf)
            {
                charactorView.SetActive(true);
                textView.SetActive(false);
            }
            isSp = true;
            //손이 있을 경우
            if (XMLManager.Instance.isPartByPartKind((int)HandMatch.handKind[0]) || XMLManager.Instance.isPartByPartKind((int)HandMatch.handKind[1]) || XMLManager.Instance.isPartByPartKind((int)HandMatch.handKind[2]))
            {
                isHand = true;
            }

            UnSetPaintCharPosition();

            for (int i = 0; i < ObjectInfo.listBodyColor.Length; i++)
            {
                int idx = new int();
                idx = i;
                {
                    CreateColorBodyButton(idx, thispartsDic[0]);
                }
            }
            CreateNomalBodyButton(thispartsDic[0]);

            CreateColorPickerButton();
        }
        //스페셜 아이템 생성하는 작업
        else if (nowPartOrder == OrderType.Special)
        {
            CreateSPClearButton();
            bool isHave = false;
            string seqName;
            string itemSeq;
            for (int i = 0; i < CollectionSeed.collectionLimit; i++)
            {
                seqName = CollectionSeed.collectionSetsDic[i].rewardSeq.seqName;
                itemSeq = CollectionSeed.collectionSetsDic[i].rewardSeq.itemSeq;
                CharPart tmpPart;
                //옷일 경우
                if (seqName == "item_seq")
                {
                    tmpPart = XMLManager.Instance.UsePartByItemSeq(itemSeq);
                    if (XMLManager.Instance.CheckIsHave("item_seq", tmpPart.charPartItem.seq))
                    {
                        GameObject obj = CreatePartButton(nowButtonMenu, tmpPart, false);
                        ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

                        itemTypeButtonUI.ChangeItemTypeImage(ItemType.spItem);
                        itemTypeButtonUI.ChangeButtonColor(Color.white);
                        obj.GetComponent<Image>().color = Color.white;
                        //하나라도 있으면 텍스트 삭제
                        isHave = true;
                    }
                }
                else
                {
                    //노답
                }
            }
            if (!isHave)
            {
                textViewText.text = LocalizationManager.Localize(StringInfo.TextViewColInfo);

                charactorView.SetActive(false);
                textView.SetActive(true);
                //없으면 생성안됨 = 컬렉션 클리어시에만 생성
            }
            else
            {
                textViewText.text = "";

                charactorView.SetActive(true);
                textView.SetActive(false);
            }
        }
        //룰렛 아이템 생성
        else if (nowPartOrder == OrderType.SailorSuit)
        {
            CreateClearButton(nowPartKind);
            bool isHave = false;
            string seqName;
            string itemSeq;
            for (int i = 0; i < ItemSeedInfo.listFortuneWheel.Count; i++)
            {
                seqName = ItemSeedInfo.listFortuneWheel[i].seqName;
                itemSeq = ItemSeedInfo.listFortuneWheel[i].itemSeq;
                CharPart tmpPart;
                //옷일 경우
                if (seqName == "item_seq")
                {
                    tmpPart = XMLManager.Instance.UsePartByItemSeq(itemSeq);

                    if (XMLManager.Instance.CheckIsHave("item_seq", tmpPart.charPartItem.seq))
                    {
                        GameObject obj = CreatePartButton(nowButtonMenu, tmpPart, false);
                        ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

                        itemTypeButtonUI.ChangeItemTypeImage(ItemType.spItem);
                        itemTypeButtonUI.ChangeButtonColor(Color.white);
                        obj.GetComponent<Image>().color = Color.white;
                        //하나라도 있으면 텍스트 삭제
                        isHave = true;
                    }
                }
                else
                {
                    //노답
                }
            }
            if (!isHave)
            {
                textViewText.text = LocalizationManager.Localize(StringInfo.TextViewWheelInfo);

                charactorView.SetActive(false);
                textView.SetActive(true);
                //없으면 생성안됨 = 컬렉션 클리어시에만 생성
            }
            else
            {
                textViewText.text = "";

                charactorView.SetActive(true);
                textView.SetActive(false);
            }
        }
        else
        {
            if (textView.activeSelf)
            {
                charactorView.SetActive(true);
                textView.SetActive(false);
            }
            CreateClearButton(nowPartKind);
            charPartType1Q.Clear();
            charPartType2Q.Clear();
            storeQ.Clear();
            specialQ.Clear();

            ContentObjectPool.SetIdx();

            //기본 버튼 생성, 구매아이템은 큐에넣기 
            for (int i = 0; i < thispartsDicKeys.Count; i++)
            {
                int idx = new int();
                idx = thispartsDicKeys[i];

                if (thispartsDic[idx].itemType == ItemType.storeItem)
                {
                    charPartType1Q.Enqueue(thispartsDic[idx]);
                    continue;
                }
                else if(thispartsDic[idx].itemType == ItemType.spItem)
                {
                    charPartType2Q.Enqueue(thispartsDic[idx]);
                    continue;
                }
                GameObject obj = CreatePartButton(nowButtonMenu, thispartsDic[idx], false);
                ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

                itemTypeButtonUI.ChangeItemTypeImage(ItemType.nomalItem);
                itemTypeButtonUI.ChangeButtonColor(Color.white);
                obj.GetComponent<Image>().color = Color.white;
            }
            
            //보유한 구매아이템 생성, 보유하지 않은 아이템은 큐에넣기 
            while (charPartType1Q.Count>0)
            {
                CharPart tmpPart = charPartType1Q.Dequeue();
                if (XMLManager.Instance.CheckIsHave("item_seq", tmpPart.charPartItem.seq))
                {
                    GameObject obj = CreatePartButton(nowButtonMenu, tmpPart, false);
                    ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

                    itemTypeButtonUI.ChangeItemTypeImage(ItemType.storeItem);
                    itemTypeButtonUI.ChangeButtonColor(Color.white);
                    itemTypeButtonUI.ChangeStoreColor(false);
                    obj.GetComponent<Image>().color = Color.white;
                }
                else
                {
                    storeQ.Enqueue(tmpPart);
                }
            }
            //보유한 스페셜아이템 생성, 보유하지 않은 아이템은 큐에 넣기
            while (charPartType2Q.Count > 0)
            {
                CharPart tmpPart = charPartType2Q.Dequeue();
                bool goBreak = false;
                for (int i = 0; i < CollectionSeed.collectionLimit; i++) { 
                    if (tmpPart.charPartItem.seq == CollectionSeed.collectionSetsDic[i].rewardSeq.itemSeq)
                    {
                        goBreak = true;
                    }
                }
                if (goBreak) continue;
                if (XMLManager.Instance.CheckIsHave("item_seq", tmpPart.charPartItem.seq))
                {
                    GameObject obj = CreatePartButton(nowButtonMenu, tmpPart, false);
                    ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

                    itemTypeButtonUI.ChangeItemTypeImage(ItemType.spItem);
                    itemTypeButtonUI.ChangeButtonColor(Color.white);
                    itemTypeButtonUI.ChangeStoreColor(false);
                    obj.GetComponent<Image>().color = Color.white;
                }
                else
                {
                    specialQ.Enqueue(tmpPart);
                }
            }
            //보유하지않은 구매아이템 생성 / 딤처리와 상점처리할것@@@@@@@@@@
            while (storeQ.Count > 0)
            {
                CharPart tmpPart = storeQ.Dequeue();
                GameObject obj = CreatePartButton(nowButtonMenu, tmpPart, true);
                ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

                itemTypeButtonUI.ChangeItemTypeImage(tmpPart.itemType);
                itemTypeButtonUI.ChangeButtonColor(Color.gray);
                itemTypeButtonUI.ChangeStoreColor(true);
                obj.GetComponent<Image>().color = Color.gray;
            }
            //보유하지 않은 스페셜 아이템은 생성되지 않는다

            //드레스에 슬립까지 생성하기, 슬립은 독자적인 레이어
            if (nowPartOrder == OrderType.Dress)
            {
                GameObject obj = CreateSlipButton();
                ItemTypeButtonUI itemTypeButtonUI = obj.GetComponent<ItemTypeButtonUI>();

                itemTypeButtonUI.ChangeItemTypeImage(ItemType.nomalItem);
                itemTypeButtonUI.ChangeButtonColor(Color.white);
                itemTypeButtonUI.ChangeStoreColor(false);
                obj.GetComponent<Image>().color = Color.white;
            }
        }
    }

    //삭제시 추가생성, 삭제 확인하며 삭제
    public void RemoveThisPart(KindType thisPart)
    {
        charPartSetting.CheckRemovePart(thisPart);
    }
    //기존 파트 존재 확인 및 추가생성, 삭제 확인하며 생성
    public void CheckThisPart(CharPart charPart)
    {
        charPartSetting.CheckCreatePart(charPart, (KindType)0);
    }
    //xml이 있는 상태에서 생성
    public void CreateThisPart(CharPart charPart)
    {
        charPartSetting.CreatePartNotXMLChange(charPart, (KindType)0);
    }

    //칼라피커 버튼 생성 
    public void CreateColorPickerButton()
    {
        GameObject PickerButton = Instantiate(prefabColorButton, bottomElementContentsRoot);
        PickerButton.tag = "OtherButton";
        PickerButton.transform.SetAsFirstSibling();

        PickerButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SetPaintCharPosition();
            //makingFriend.RemovePart(orderNum);
        });
    }

    //일반 몸 버튼 생성 
    public void CreateNomalBodyButton(CharPart charPart)
    {
        GameObject ClearButton = Instantiate(prefabPartButton, bottomElementContentsRoot);
        ClearButton.tag = "OtherButton";
        ClearButton.transform.SetAsFirstSibling();

        ClearButton.GetComponent<ItemTypeButtonUI>().ChangeButtonImage(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "Body_0"));

        ClearButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            UnSetPaintCharPosition();

            UnsetBack();
            CheckThisPart(charPart);
            if (isHand)
            {
                RemoveThisPart(HandMatch.handBackKind[0]);
                RemoveThisPart(HandMatch.handBackKind[1]);
                RemoveThisPart(HandMatch.handBackKind[2]);
                CheckThisPart(handPart);
            }
            //makingFriend.RemovePart(orderNum);
        });
    }

    //칼라몸 버튼 생성 
    public void CreateColorBodyButton(int colorNum, CharPart charPart)
    {
        GameObject ColorButton = Instantiate(prefabPartButton, bottomElementContentsRoot);
        ColorButton.tag = "OtherButton";
        ColorButton.name = colorNum.ToString();

        ColorButton.transform.SetAsFirstSibling();

        ItemTypeButtonUI partButtonUI = ColorButton.GetComponent<ItemTypeButtonUI>();
        partButtonUI.ChangeButtonImage(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "Body_1"));
        partButtonUI.ChangeButtonColor(ObjectInfo.listBodyColor[colorNum]);

        ColorButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SetColor(partButtonUI.buttonImage.color);
        });
    }

    //한부위 초기화 버튼 생성 
    public void CreateClearButton(KindType removeKind)
    {
        GameObject ClearButton = Instantiate(prefabPartButton, bottomElementContentsRoot);
        //ClearButton.transform.Find("Text").GetComponent<Text>().text = "삭제";
        ItemTypeButtonUI clearButtonUI = ClearButton.GetComponent<ItemTypeButtonUI>();

        clearButtonUI.ChangeButtonImage(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_remove_n"));
        clearButtonUI.ChangeButtonSize(132f);
        ClearButton.tag = "OtherButton";
        ClearButton.transform.SetAsFirstSibling();

        ClearButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            isPaint = false;
            UnsetBack();
            charPartSetting.CheckRemovePart(removeKind);
            isSp = false;
        });
    }

    //스페셜 초기화 버튼 생성
    public void CreateSPClearButton()
    {
        GameObject ClearButton = Instantiate(prefabPartButton, bottomElementContentsRoot);
        ItemTypeButtonUI clearButtonUI = ClearButton.GetComponent<ItemTypeButtonUI>();

        clearButtonUI.ChangeButtonImage(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "btn_remove_n"));
        clearButtonUI.ChangeButtonSize(132f);
        ClearButton.tag = "OtherButton";
        ClearButton.transform.SetAsFirstSibling();

        ClearButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            isPaint = false;
            UnsetBack();

            for (int i = 0; i < CollectionSeed.collectionLimit; i++)
            {
                string seqName = CollectionSeed.collectionSetsDic[i].rewardSeq.seqName;
                string itemSeq = CollectionSeed.collectionSetsDic[i].rewardSeq.itemSeq;
                CharPart tmpPart;
                //옷일 경우
                if (seqName == "item_seq")
                {
                    tmpPart = XMLManager.Instance.UsePartByItemSeq(itemSeq);
                    if (XMLManager.Instance.isPartByPartKind((int)tmpPart.charPartItem.kind.kindType))
                    {
                        charPartSetting.CheckRemovePart(tmpPart.charPartItem.kind.kindType);
                    }
                }
                //아닐 경우가 있으면 안됨 막아야함
                else
                {

                }
            }
            isSp = false;
        });
    }

    //슬립 버튼 생성
    public GameObject CreateSlipButton()
    {
        string seq = ParseItemSeq.MakeItemSeq((int)OrderType.Slip, 0);

        CharPartItem slipCharPartItem = new CharPartItem(seq, 0, (int)KindType.Slip, (int)OrderType.Slip);
        CharPart slipCharPart = new CharPart(slipCharPartItem, Color.white, "-1", 0, 0, 0, XMLManager.Instance.allPartPosDic[seq].x, XMLManager.Instance.allPartPosDic[seq].y);

        GameObject PartButton = ContentObjectPool.nowButton(prefabPartButton);
        PartButton.transform.SetAsLastSibling();
        PartButton.SetActive(true);
        PartButton.GetComponent<ItemTypeButtonUI>().ChangeButtonImage(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, ParseItemSeq.ItemSeqToStringSpName(seq)));
        PartButton.name = seq;

        PartButton.GetComponent<Button>().onClick.RemoveAllListeners();
        PartButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            string seqName = ParseItemSeq.SplitItemSeq1(slipCharPart.charPartItem.seq);
            string itemSeq = ParseItemSeq.SplitItemSeq2(slipCharPart.charPartItem.seq);
            if (!XMLManager.Instance.CheckIsHave("order_seq", "046"))//얘는 슬립아이템인데 드레스 카테고리로 팔기 때문에 조치
            {
                AlertAD(seqName, itemSeq);
                return;
            }

            UnSetPaintCharPosition();

            CheckThisPart(slipCharPart);
            isSp = true;
        });
        return PartButton;
    }

    //파트 생성 버튼 생성 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    public GameObject CreatePartButton(string nowButtonMenu, CharPart charPart, bool isCost) {
        GameObject PartButton = ContentObjectPool.nowButton(prefabPartButton);
        PartButton.transform.SetAsLastSibling();
        PartButton.SetActive(true);

        PartButton.GetComponent<ItemTypeButtonUI>().ChangeButtonImage(AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, ParseItemSeq.ItemSeqToStringSpName(charPart.charPartItem.seq)));
        PartButton.name = charPart.charPartItem.seq;

        PartButton.GetComponent<Button>().onClick.RemoveAllListeners();
        PartButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            if(!DoTweenUtil.isTweening)
            {
                if (!XMLManager.Instance.CheckIsHave("item_seq", charPart.charPartItem.seq))
                {

                    AlertAD("item_seq", charPart.charPartItem.seq);
                    return;
                }
                if (!InMakingManager.Instance.isTutorial)
                {
                    if (nowButtonMenu.Equals("BackHair") || nowButtonMenu.Equals("SideHair") || nowButtonMenu.Equals("FrontHair") || nowButtonMenu.Equals("Eyebrow"))
                    {
                        SetColorView();
                        UnsetBack();
                    }
                    else
                    {
                        UnSetPaintCharPosition();
                    }
                }
                CheckThisPart(charPart);
                isSp = true;
            }
        });
        return PartButton;
    }


    public void SetColor(Color color)
    {
        SetPartName();
        if (isSp)
        {
            if (!XMLManager.Instance.isPartByPartKind((int)CharPartOrder.OrderNumToKindNum((int)backPartOrder)))
            {
                SetBack();
            }
            else
            {
                SetRenderObject();
            }
            savedColor = color;
            SetRenColor(ren, savedColor);
            if (nowPartKind == KindType.Body)
            {
                if (isHand)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        if (handRenDic.ContainsKey(i))
                        {
                            SetRenColor(handRenDic[i], savedColor);
                        }
                    }
                    XMLManager.Instance.ChangeColorByPartKind((int)HandMatch.handBackKind[0], savedColor);
                    XMLManager.Instance.ChangeColorByPartKind((int)HandMatch.handBackKind[1], savedColor);
                    XMLManager.Instance.ChangeColorByPartKind((int)HandMatch.handBackKind[2], savedColor);
                }
            }
            XMLManager.Instance.ChangeColorByPartKind((int)nowPartKind - 1, savedColor);
        }
        else
        {
           //스프라이트가 없음
        }
    }


    public void SetPartName()
    {
        if (isSp)
        {
            switch (nowPartOrder)
            {
                case OrderType.Body:
                    backPartOrder = OrderType.BodyBack;
                    alterPartOrder = OrderType.BodyTrans;
                    break;
                case OrderType.BackHair:
                    backPartOrder = OrderType.BackHairBack;
                    alterPartOrder = OrderType.BackHairTrans;
                    break;
                case OrderType.SideHair:
                    backPartOrder = OrderType.SideHairBack;
                    alterPartOrder = OrderType.SideHairTrans;
                    break;
                case OrderType.FrontHair:
                    backPartOrder = OrderType.FrontHairBack;
                    alterPartOrder = OrderType.FrontHairTrans;
                    break;
                case OrderType.Eyebrow:
                    backPartOrder = OrderType.EyebrowBack;
                    alterPartOrder = OrderType.EyebrowTrans;
                    break;
            }
        }
    }

    #region 칼라 뷰
    public void SetBack()
    {
        if (isSp)
        {
            SetPartName();

            isBack = true;
            CharPart nowPart = XMLManager.Instance.SelectPartByPartKind((int)nowPartKind);
            //백 생성
            XMLManager.Instance.CreatePartByPartOrder((int)backPartOrder, nowPart.charPartItem.spNum);
            CreateThisPart(XMLManager.Instance.newTmpPart);

            //앞쪽은 트랜스로 교체
            XMLManager.Instance.CreatePartByPartOrder((int)alterPartOrder, nowPart.charPartItem.spNum);
            //eyebrow일 경우 없을 수도 있으니까 한 작업 추가
            //spNum이 맞지 않으면 newTmpPart는 null값을 리턴하므로

            switch (nowPartOrder)
            {
                default:
                    charPartSetting.CheckCreatePart(XMLManager.Instance.newTmpPart, (KindType)0);
                    break;
                case OrderType.Body:
                    charPartSetting.CheckCreatePart(XMLManager.Instance.newTmpPart, (KindType)0);
                    SetHandBack(KindType.Shirts);
                    SetHandBack(KindType.Outer);
                    SetHandBack(KindType.Dress);
                    break;
                case OrderType.Eyebrow:
                    if (XMLManager.Instance.newTmpPart == null)
                    {
                        makingFriend.charSimpleObjectPool.UnsetNowPart((int)CharPartOrder.OrderNumToKindNum((int)nowPartOrder));
                    }
                    else
                    {
                        charPartSetting.CheckCreatePart(XMLManager.Instance.newTmpPart, (KindType)0);
                    }
                    break;
            }

            SetRenderObject();
        }
        else {
            //"파트이름이 없음")
        }
    }

    public void SetHandBack(KindType parentKind)
    {
        if (isHand)
        {
            if (nowPartOrder == OrderType.BackHair || nowPartOrder == OrderType.SideHair || nowPartOrder == OrderType.FrontHair)
            {
                return;
            }
            //손의 spNum구해오기

            if (!XMLManager.Instance.isPartByPartKind((int)parentKind))
            {
                return;
            }
            if(parentKind == KindType.Shirts && charPartSetting.charPartsPrefabDic.ContainsKey((int)KindType.Outer))
            {
                if (charPartSetting.charPartsPrefabDic[(int)KindType.Outer].GetComponent<CharPartUse>().charPart.charPartItem.seq != "054115")
                {
                    return;
                }
            }

            if (!handRenDic.ContainsKey(HandMatch.kindNumToArrayNum(parentKind)))
            {
                SpriteRenderer handRen = new SpriteRenderer();
                handRenDic.Add(HandMatch.kindNumToArrayNum(parentKind), handRen);
            }
            CharPart parentPart = XMLManager.Instance.SelectPartByPartKind((int)parentKind);
            handPart = XMLManager.Instance.SelectPartByPartKind((int)HandMatch.handKind[HandMatch.kindNumToArrayNum(parentKind)]);
            if(handPart == null)
            {
                return;
            }
            Debug.Log(handPart);

            if (XMLManager.Instance.isPartByPartKind((int)HandMatch.handKind[HandMatch.kindNumToArrayNum(parentKind)]))
            {
                charPartSetting.CheckRemovePart(HandMatch.handKind[HandMatch.kindNumToArrayNum(parentKind)]);
            }
            if (XMLManager.Instance.isPartByPartKind((int)HandMatch.handBackKind[HandMatch.kindNumToArrayNum(parentKind)]))
            {
                charPartSetting.CheckRemovePart(HandMatch.handBackKind[HandMatch.kindNumToArrayNum(parentKind)]);
            }
            OrderType backType;
            OrderType transType;
            if(parentPart.handType == 1)
            {
                backType = HandMatch.uHandBackOrder[HandMatch.kindNumToArrayNum(parentKind)];
                transType = HandMatch.uHandTransOrder[HandMatch.kindNumToArrayNum(parentKind)];
            }
            else
            {
                backType = HandMatch.handBackOrder[HandMatch.kindNumToArrayNum(parentKind)];
                transType = HandMatch.handTransOrder[HandMatch.kindNumToArrayNum(parentKind)];
            }

            XMLManager.Instance.CreatePartByPartOrder((int)backType, handPart.charPartItem.spNum);
            CreateThisPart(XMLManager.Instance.newTmpPart);

            XMLManager.Instance.CreatePartByPartOrder((int)transType, handPart.charPartItem.spNum);
            CreateThisPart(XMLManager.Instance.newTmpPart);
        }
        else
        {
            //"손없음");
        }
    }

    public void SetRenderObject()
    {
        if (charPartSetting.charPartsPrefabDic.ContainsKey((int)CharPartOrder.OrderNumToKindNum((int)backPartOrder))){
            GameObject backPartObj = charPartSetting.charPartsPrefabDic[(int)CharPartOrder.OrderNumToKindNum((int)backPartOrder)];
            ren = backPartObj.GetComponent<SpriteRenderer>();
        }
        SetHandRenderObject();
        rgbPointerView.SetText();
    }

    public void SetHandRenderObject()
    {
        if(isHand)
        {
            if (nowPartOrder == OrderType.BackHair || nowPartOrder == OrderType.SideHair || nowPartOrder == OrderType.FrontHair || nowPartOrder == OrderType.Eyebrow)
            {
                return;
            }
            for (int i = 0; i < 3; i++)
            {
                if (charPartSetting.charPartsPrefabDic.ContainsKey((int)HandMatch.handBackKind[i]))
                {
                    GameObject handBackPartObj = charPartSetting.charPartsPrefabDic[(int)HandMatch.handBackKind[i]];

                    if (handRenDic.ContainsKey(i))
                    {
                        handRenDic[i] = handBackPartObj.GetComponent<SpriteRenderer>();
                    }
                    else
                    {
                        handRenDic.Add(i, handBackPartObj.GetComponent<SpriteRenderer>());
                    }
                }
            }
        }
    }

    //칼라피커를 한번 더 눌러서 칼라를 없앨때, 다른 몸통을 눌렀을때만 아예 색이 없어진다
    void UnsetBack()
    {
        if (isBack)
        {
            isPaint = false;
            isBack = false;

            KindType removePartKind = CharPartOrder.OrderNumToKindNum((int)backPartOrder);
            KindType tmpPartKind = 0;
            switch (removePartKind)
            {
                case KindType.BodyBack:
                    tmpPartKind = KindType.Body;
                    break;
                case KindType.BackHairBack:
                    tmpPartKind = KindType.BackHair;
                    break;
                case KindType.SideHairBack:
                    tmpPartKind = KindType.SideHair;
                    break;
                case KindType.FrontHairBack:
                    tmpPartKind = KindType.FrontHair;
                    break;
                case KindType.EyebrowBack:
                    tmpPartKind = KindType.Eyebrow;
                    break;
            }

            RemoveThisPart(removePartKind);
            RemoveThisPart(tmpPartKind);

            if (tmpPartKind == KindType.Body)
            {
                ren = null;
                handRenDic.Clear();
                if (isHand)
                {
                    RemoveThisPart(HandMatch.handBackKind[0]);
                    RemoveThisPart(HandMatch.handKind[0]);
                    RemoveThisPart(HandMatch.handBackKind[1]);
                    RemoveThisPart(HandMatch.handKind[1]);
                    RemoveThisPart(HandMatch.handBackKind[2]);
                    RemoveThisPart(HandMatch.handKind[2]);
                }
            }
        }
        else
        {
            //"back이 없음");
        }
    }

    public void SetRenColor(SpriteRenderer ren, Color color)
    {
        if (ren == null)
        {
            return;
        }
        ren.color = color;
        rgbPointerView.SetText();
    }

    //칼라피커
    public void SetColorByButton()
    {
        if (isSp)
        {
            if (!isBack)
            {
               
                SetBack();
            }
            else
            {
                SetRenderObject();
            }
            savedColor = EventSystem.current.currentSelectedGameObject.transform.GetChild(0).GetComponent<Image>().color;
            SetRenColor(ren, savedColor);
            if (nowPartKind == KindType.Body)
            {
                if (isHand)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        if (handRenDic.ContainsKey(i))
                        {
                            SetRenColor(handRenDic[i], savedColor);
                        }
                    }
                    XMLManager.Instance.ChangeColorByPartKind((int)HandMatch.handBackKind[0], savedColor);
                    XMLManager.Instance.ChangeColorByPartKind((int)HandMatch.handBackKind[1], savedColor);
                    XMLManager.Instance.ChangeColorByPartKind((int)HandMatch.handBackKind[2], savedColor);
                }
            }
            XMLManager.Instance.ChangeColorByPartKind((int)nowPartKind - 1, savedColor);
        }
        else
        {
            //"백이없음");
        }
    }

    public void SetColorView()
    {
        if (!colorView.activeSelf)
        {
            SetPaintCharPosition();
        }
    }

    //캔버스 키고 피커와 버튼 반대로 하는 기능 (버튼으로 사용시 칼라피커로 시작). 안씀
    //public void SetColorPickerToggle()
    //{
    //    if (!DoTweenUtil.isTweening)
    //    {
    //        if (!colorPicker.activeSelf)
    //        {
    //            //SetPaintCharPosition();
    //            //ColorButtonView.SetActive(false);
    //            DoTweenUtil.DoLocalMoveX(2f, 0.2f, InMakingManager.Instance.charTrRoot.transform);
    //            colorView.GetComponent<ColorView>().startColorPicker = true;
    //            StartPicker();
    //        }
    //        else
    //        {
    //            //UnSetPaintCharPosition();
    //            DoTweenUtil.DoLocalMoveX(0, 0.2f, InMakingManager.Instance.charTrRoot.transform);
    //            colorView.GetComponent<ColorView>().startColorPicker = false;
    //            StopPicker();
    //            colorView.SetActive(false);
    //        }
    //    }
    //}

    //칼라뷰 만들고 캐릭터 이동시키기
    public void SetPaintCharPosition()
    {
        if (!colorView.activeSelf)
        {
            DoTweenUtil.DoLocalMoveX(2f, 0.2f, InMakingManager.Instance.charTrRoot.transform);
            colorView.GetComponent<ColorView>().startColorPicker = false;
            colorView.SetActive(true);
        }
        colorView.GetComponent<ColorView>().ButtonInit();
        if (!colorButtonView.activeSelf && !colorPicker.activeSelf)
        {
            colorView.GetComponent<ColorView>().startColorPicker = true;
            StartPicker();
        }
    }

    //칼라뷰 없애고 캐릭터 이동시키기
    public void UnSetPaintCharPosition()
    {
        if (colorView.activeSelf)
        {
            DoTweenUtil.DoLocalMoveX(0, 0.2f, InMakingManager.Instance.charTrRoot.transform);
            colorView.GetComponent<ColorView>().startColorPicker = false;

            colorView.SetActive(false);
            colorPicker.SetActive(false);
        }
    }

    
    public void SetColorPickerToggle(bool isColorPicker)
    {
        if (!isColorPicker)
        {
            colorButtonView.SetActive(false);
            StartPicker();
        }
        else
        {
            StopPicker();
            colorButtonView.SetActive(true);
        }
    }

    public void StartPicker()
    {
        colorView.GetComponent<ColorView>().startColorPicker = true;
        colorView.SetActive(true);

        colorPicker.SetActive(true);
        CP = colorPicker.GetComponent<ColorPickerTriangle>();
        CP.SetNewColor(savedColor);
    }

    public void StopPicker()
    {
        colorView.GetComponent<ColorView>().startColorPicker = false;
        colorPicker.SetActive(false);
    }

    public void SavePaint()
    {
        if (isBack)
        {
            if (isPaint)
            {
                XMLManager.Instance.ChangeColorByPartKind((int)nowPartKind - 1, CP.TheColor);
                if (nowPartKind == KindType.Body)
                {
                    if (isHand)
                    {
                        XMLManager.Instance.ChangeColorByPartKind((int)HandMatch.handBackKind[0], CP.TheColor);
                        XMLManager.Instance.ChangeColorByPartKind((int)HandMatch.handBackKind[1], CP.TheColor);
                        XMLManager.Instance.ChangeColorByPartKind((int)HandMatch.handBackKind[2], CP.TheColor);
                    }
                }
                savedColor = CP.TheColor;
            }
            else
            {
                ////Debug.Log("isPaint아님");
            }
        }
        else
        {
            ////Debug.Log("back이 없음");
        }
    }

    public void StopPaint()
    {
        if (isPaint)
        {
            isPaint = false;
        }
        else
        {
            ////Debug.Log("backPartOrder=null");
        }
    }

    #endregion

    //광고뷰로 갈지 물어보지 않고 보내버리기
    public void AlertAD(string seqName, string itemSeq)
    {
        goADView.PopupGoADPanel(false, XMLManager.Instance.SelectBundleByItemSeq(seqName, itemSeq));
    }

    //카테고리 버튼 전체 인잇
    public void InitOrderTypeButton()
    {
        for (int i = 0; i < menuContent.childCount; i++)
        {
            menuContent.GetChild(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < menuContent.childCount; i++)
        {
            menuContent.GetChild(i).gameObject.SetActive(true);
        }
    }

    //상하의 레이어 토글 버튼
    public void OnPantsOverToggleButton()
    {
        charPartSetting.ChangePantsSibling();
    }

    #region 우측UI
    public void SaveMakedCharacter()
    {
        XMLManager.Instance.SaveMakedWEAR();
        GoMainScene();
    }

    public void GoMainScene()
    {
        if (InGameManager.Instance.inModify)
        {
            InGameManager.Instance.inModify = false;
        }
        SceneManager.LoadScene("MainScene");
    }

    public void SaveButtonClick()
    {
        alertView.Alert(string.Format(LocalizationManager.Localize(StringInfo.MakSave_Q), "\n"), new AlertViewOptions
        {
            cancelButtonDelegate = () =>
            {
                GoMainScene();
            },
            okButtonDelegate = () =>
            {
                // 저장 후 광고 띄우기 (튜토리얼 제외)
                //if (!InMakingManager.Instance.isTutorial)
                //{
                //    AdsManager.Instance.WatchAdWithExitMaking();
                //}

                int missionKind = MissionSeed.missionLevelDic[2].count[MissionSeed.missionDic[2].nowLevel];
                if (XMLManager.Instance.isPartByPartKind(missionKind)) {
                    XMLManager.Instance.SaveMissionValue(XMLManager.Instance.nodeSaveKind, "value", missionKind.ToString());
                }
                MissionSeed.CheckAllMission();

                SaveMakedCharacter();
            }
        });
    }

    public void OnClearButtonClick()
    {
        alertView.Alert(LocalizationManager.Localize(StringInfo.MakClearInfo), new AlertViewOptions
        {
            okButtonDelegate = () =>
            {
                XMLManager.Instance.SetMakedWEAR();
                InMakingManager.Instance.InitMakingCharacter();
            },
            cancelButtonDelegate = () =>
            {
            }
        });
    }
    #endregion

    #region 좌측UI
    public void OnStoreButtonClick() {
        storeView.goStorePanel.SetActive(true);
    }

    #endregion
}