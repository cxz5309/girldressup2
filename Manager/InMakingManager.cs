﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.U2D;
using Assets.SimpleLocalization;

public class InMakingManager : MonoBehaviour
{
    public static InMakingManager Instance;

    public Camera mainCamera;

    public MakingCanvas makingCanvas;
    public Friend makingFriend;
    public GameObject waterMark;
    public GameObject charTrRoot;

    public GameObject makingTutorialCanvas;

    //화면에서 터치된 오브젝트
    private Vector3 mousePositionWorld;
    static float DISTANCE = 10f;
    public GameObject hitGameObject;

    int ClickCount = 0;

    //튜토리얼
    public bool isTutorial;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        Debug.Log("Making Start Log");
        StopAllCoroutines();
    }

    private void Start()
    {
        if (!InGameManager.Instance.inModify)
        {
            XMLManager.Instance.SetMakedWEAR();
        }
        else
        {
            XMLManager.Instance.SetModifyWEAR("-1");
        }
        InitMakingCharacter();
        makingCanvas.CreateEachPartButton("Body");

        InitWaterMark(XMLManager.Instance.LoadUserHave_RmWaterMark());

        //튜토리얼
        if (InGameManager.Instance.firstDay && InGameManager.Instance.newToday)
        {
            if (!InGameManager.Instance.clearTutorial)
            {
                isTutorial = true;
                makingTutorialCanvas.SetActive(true);
            }
            else
            {
                isTutorial = false;
                makingTutorialCanvas.SetActive(false);
            }
        }
        else
        {
            isTutorial = false;
            makingTutorialCanvas.SetActive(false);
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ClickDown();
        }
        if (Input.GetMouseButtonUp(0))
        {
            ClickUp();
        }
        //back버튼 터치
#if UNITY_ANDROID

        if (!isTutorial)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (DisMissAll())
                {
                    return;
                }
                ClickCount++;
                if (!IsInvoking("DoubleClick"))
                    Invoke("DoubleClick", 0.5f);
            }
            else if (ClickCount == 2)
            {
                if (IsInvoking("DoubleClick"))
                {
                    QuitAlert();
                    ClickCount++;
                }
            }
        }
#else
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (DisMissAll())
                {
                    return;
                }
                ClickCount++;

                if (!IsInvoking("DoubleClick"))
                    Invoke("DoubleClick", 0.5f);
            }
            else if (ClickCount == 2)
            {
                if (IsInvoking("DoubleClick"))
                {
                    QuitAlert();
                    ClickCount++;
                }
            }
#endif
    }

    private void QuitAlert()
    {
        makingCanvas.alertView.Alert(LocalizationManager.Localize(StringInfo.QuitGame), new AlertViewOptions
        {
            cancelButtonDelegate = () =>
            {

            },
            okButtonDelegate = () =>
            {
                Application.Quit();
            }
        });
    }

    void DoubleClick()
    {
        ClickCount = 0;
    }

    public void InitMakingCharacter()
    {
        List<CharPart> list = XMLManager.Instance.listMakingPARTInit;
        makingFriend.CreateCharacter(list);
    }

    public void InitWaterMark(bool rmWaterMark)
    {
        if (rmWaterMark)
        {
            waterMark.gameObject.SetActive(false);
        }
        else
        {
            Vector3 vector3 = makingCanvas.bottomElementContentsRoot.transform.position;
            waterMark.transform.position = ObjectInfo.waterMarkPosMaking;

            waterMark.gameObject.SetActive(true);
        }
    }

    public void MakingSceneRefresh()
    {
        InitWaterMark(XMLManager.Instance.LoadUserHave_RmWaterMark());
        makingCanvas.InitOrderTypeButton();
        makingCanvas.CreateEachPartButton(CharPartOrder.EnumToString(makingCanvas.nowPartOrder));
    }

    #region 클릭
    //클릭되는게 없으면 tool지우기
    private void ClickDown()
    {
        mousePositionWorld = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, DISTANCE));
        RaycastHit2D hit = Physics2D.Raycast(mousePositionWorld, -Vector2.up);
        if (hit.collider == null)
        {
            hitGameObject = null;
        }
        else
        {
            hitGameObject = hit.collider.gameObject;
        }
    }

    void ClickUp()
    {
        //HitColliderCallBack();
    }
#endregion

    public bool DisMissAll()
    {
        //알림 있으면 알림부터 제거
        if (makingCanvas.alertView.goPanel.activeSelf)
        {
            makingCanvas.alertView.OnDismissButtonClick();
            return true;
        }
        if (makingCanvas.itemGetPopupUI.gameObject.activeSelf)
        {
            makingCanvas.itemGetPopupUI.DismissItemGetPopup();
            return true;
        }

        //나머지는 한꺼번에 제거  하지 말고 그냥 위에순서부터 하나씩 제거
        if (makingCanvas.settingView.goSettingPanel.activeSelf)
        {
            makingCanvas.settingView.DisMiss();
            return true;
        }
        if (makingCanvas.collectionView.collectionPanel.activeSelf)
        {
            makingCanvas.collectionView.Dismiss();
            return true;
        }
        if (makingCanvas.fortuneWheelView.goFortuneWheelPanel.activeSelf)
        {
            makingCanvas.fortuneWheelView.DisMiss();
            return true;
        }
        if (makingCanvas.dailyView.goDailyPanel.activeSelf)
        {
            makingCanvas.dailyView.DisMiss();
            return true;
        }
        if (makingCanvas.storeView.goStorePanel.activeSelf)
        {
            makingCanvas.storeView.DisMiss();
            return true;
        }
        if (makingCanvas.missionView.goMissionPanel.activeSelf)
        {
            makingCanvas.missionView.DisMiss();
            return true;
        }
        if (makingCanvas.goADView.goADPanel.activeSelf)
        {
            makingCanvas.goADView.DisMiss();
            return true;
        }

        if (makingCanvas.moreGameView.goMoreGamePanel.activeSelf)
        {
            makingCanvas.moreGameView.DisMiss();
            return true;
        }

        return false;
    }


    //public void HitColliderCallBack()
    //{
    //    if (hitGameObject.name != "RewardButtonGroup")
    //    {
    //        InGameManager.Instance.rewardButtonUI.CloseRewardContainer();
    //    }
    //}
}
