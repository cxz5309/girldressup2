using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.SimpleLocalization;
using UnityEngine.U2D;
using GoogleMobileAds.Api;
using GoogleMobileAds.Api.Mediation.UnityAds;

public class IntroCanvas : MonoBehaviour
{
    public Image StartButtonBackImg;
    public AlertView alertView;
    public Button StartButton;
    public AtlasLoader atlasLoader;
    public GameObject loadingPanel;

    int ClickCount = 0;
    public bool canStart;

    private void Start()
    {
        string language = PlayerPrefs.GetString(StringInfo.playerPrefsKeyList[7], "empty");
        if (language.Equals("empty"))
        {
            switch (Application.systemLanguage)
            {
                default:
                    language = "English";
                    break;
                case SystemLanguage.English:
                    language = "English";
                    break;
                case SystemLanguage.Korean:
                    language = "Korean";
                    break;
                case SystemLanguage.Japanese:
                    language = "Japanese";
                    break;
                //case SystemLanguage.ChineseTraditional:
                //    language = "ChineseTraditional";
                //    break;
                //case SystemLanguage.Portuguese:
                //    language = "Portuguese";
                //    break;
                //case SystemLanguage.Spanish:
                //    language = "Spanish";
                //    break;
                //case SystemLanguage.Russian:
                //    language = "Russian";
                //    break;
                
            }
        }
        PlayerPrefs.SetString(StringInfo.playerPrefsKeyList[7], language);
        LocalizationManager.Read();
        LocalizationManager.Language = language;

        //if (language.Equals("Korean"))
        //{
        //    imageLogo.sprite = Resources.Load<Sprite>("intro_logo_kr");
        //}
        //else if (language.Equals("Japanese"))
        //{
        //    imageLogo.sprite = Resources.Load<Sprite>("intro_logo_jp");
        //}
        //else
        //{
        //    imageLogo.sprite = Resources.Load<Sprite>("intro_logo_en");
        //}

        //SoundManager.Instance.Init(() => {

        //});
        AudioManager.Inst.PlayBGM("cute-and-catchy-happy-baby");

        AdsManager.Instance.InitAds();
    }

    private void Update()
    {
      
#if UNITY_ANDROID

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (DisMissAll())
            {
                return;
            }
            ClickCount++;

            if (!IsInvoking("DoubleClick"))
                Invoke("DoubleClick", 0.5f);
        }
        else if (ClickCount == 2)
        {
            if (IsInvoking("DoubleClick"))
            {
                QuitAlert();
                ClickCount++;
            }
        }
#else
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (DisMissAll())
            {
                return;
            }
            ClickCount++;

            if (!IsInvoking("DoubleClick"))
                Invoke("DoubleClick", 0.5f);
        }
        else if (ClickCount == 2)
        {
            if (IsInvoking("DoubleClick"))
            {
                QuitAlert();
                ClickCount++;
            }
        }
#endif
    }

    void DoubleClick()
    {
        ClickCount = 0;
    }

    private void QuitAlert()
    {
        alertView.Alert(LocalizationManager.Localize(StringInfo.QuitGame), new AlertViewOptions
        {
            cancelButtonDelegate = () =>
            {

            },
            okButtonDelegate = () =>
            {
                Application.Quit();
            }
        });
    }

    public void SetStart()
    {
        if (canStart)
        {
            coInit();
        }
    }

    public void coInit()
    {
        loadingPanel.SetActive(true);
        //1. xml 읽어오기
        XMLManager.Instance.InitXml(InitXmlCallback);
    }

    private void InitXmlCallback()
    {
        //2. 아틀라스 로드 => 성능 많이 차지함
        atlasLoader.LoadAtlas(LoadAtlasCallback);
    }

    private void LoadAtlasCallback()
    {
        //3. 게임 데이터 세팅
        InGameManager.Instance.LoadAllSetting();
        InGameManager.Instance.InGameManagerStart();
        InitIAP();
    }

    /// <summary>
    /// 인앱 초기화
    /// </summary>
    private void InitIAP()
    {
        IAPManager.Instance.Init((isSuccess) =>
        {
            InitAds();
        });
    }

    /// <summary>
    /// 광고 초기화
    /// </summary>
    private void InitAds()
    {
        MobileAds.Initialize(initStatus =>
        {
            // Unity Ads EU Consent and GDPR
            UnityAds.SetGDPRConsentMetaData(true);
            GoNextScene();
        });
    }

    public void GoNextScene()
    {
        if (InGameManager.Instance.firstDay)
        {
            SceneManager.LoadScene("MakingScene");
        }
        else
        {
            SceneManager.LoadScene("MainScene");
        }
    }

    public void OnMoreGamesClick()
    {
        Application.OpenURL(StringInfo.OpenURL);
    }


    public bool DisMissAll()
    {
        bool clear = false;

        //알림 있으면 알림부터 제거
        if (alertView.goPanel.activeSelf)
        {
            alertView.OnDismissButtonClick();
            clear = true;
        }

        return clear;
    }


    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}

