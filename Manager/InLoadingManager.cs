﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InLoadingManager : MonoBehaviour
{
    private void Start()
    {
        StopAllCoroutines();

        SetStart();
    }

    public void SetStart()
    {
        StartCoroutine(coInit());
    }

    public IEnumerator coInit()
    {
        XMLManager.Instance.LoadXml();
        yield return new WaitForSeconds(1f);
        InGameManager.Instance.LoadAllSetting();
        yield return new WaitForSeconds(1f);
        InGameManager.Instance.InGameManagerStart();
        yield return new WaitForSeconds(2f);
        GoNextScene();
        yield break;
    }

    public void GoNextScene()
    {
        SceneManager.LoadScene(InGameManager.Instance.loadingPrevScene);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
