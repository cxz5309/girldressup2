﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Serialization;
using UnityEngine.Android;
using System.Collections;

public class XMLManager : MonoBehaviour
{
    public static XMLManager Instance = null;

    //xml데이터 문서
    private XmlDocument data = new XmlDocument();
    private XmlDocument position = new XmlDocument();
    private XmlDocument partItem = new XmlDocument();

    private XmlDocument bgItem = new XmlDocument();
    private XmlDocument textItem = new XmlDocument();
    private XmlDocument object1Item = new XmlDocument();
    private XmlDocument object2Item = new XmlDocument();
    private XmlDocument animalItem = new XmlDocument();
    private XmlDocument bubbleItem = new XmlDocument();
    private XmlDocument orderButton = new XmlDocument();

    private XmlDocument user = new XmlDocument();
    private XmlDocument userHave = new XmlDocument();

    private XmlDocument itemSeed = new XmlDocument();
    private XmlDocument storeItem = new XmlDocument();
    private XmlDocument mission = new XmlDocument();
    private XmlDocument collection = new XmlDocument();


    //xml 노드의 루트 
    public XmlNode root;
    public XmlNode positionRoot;
    public XmlNode partItemRoot;

    public XmlNode userRoot;
    public XmlNode userHaveRoot;

    public XmlNode itemSeedRoot;
    public XmlNode storeItemRoot;

    public XmlNode bgItemRoot;
    public XmlNode textItemRoot;
    public XmlNode object1ItemRoot;
    public XmlNode object2ItemRoot;
    public XmlNode animalItemRoot;
    public XmlNode bubbleItemRoot;
    public XmlNode orderButtonRoot;
    public XmlNode missionRoot;
    public XmlNode missions;
    public XmlNode collectionRoot;
    public XmlNode collections;

    #region DataNode
    public XmlNode nodeALL_SLOT;
    public XmlNode nodeMAIN;

    public XmlNodeList listSLOT;
    #endregion

    #region UserNode
    XmlNode nodeDateTime;
    XmlNode nodeCoins;
    XmlNode nodeMission;
    XmlNode nodeMissions;
    XmlNode nodeCollections;
    XmlNode nodeHaveBundle;
    public XmlNode nodeWheelTimes;
    public XmlNode nodeSaveKind;
    public XmlNode nodePerchaseTimes;
    public XmlNode nodeSaveSlotTimes;
    public XmlNode nodeShowADTimes;
    public XmlNode nodeGetMissionRWTimes;
    public XmlNode nodeEvaluation;
    #endregion

    #region PartItemNode
    public Dictionary<string, CharPartItem> allPartItemDic = new Dictionary<string, CharPartItem>();
    #endregion

    #region PositionNode
    public Dictionary<string, CharPartItemPos> allPartPosDic = new Dictionary<string, CharPartItemPos>();
    #endregion

    #region Data
    public Dictionary<int, List<CharPart>> listAllMainWEAR = new Dictionary<int, List<CharPart>>();

    public Dictionary<int, Dictionary<int, ItemPart>> listAllObjDic = new Dictionary<int, Dictionary<int, ItemPart>>();
    public Dictionary<int, List<int>> listAllObjKeysDic = new Dictionary<int, List<int>>();

    public Dictionary<int, Dictionary<int, CharPart>> listAllPartDic = new Dictionary<int, Dictionary<int, CharPart>>();
    public Dictionary<int, List<int>> listAllPartKeysDic = new Dictionary<int, List<int>>();

    public List<CharTr> listMAIN_ObjTr = new List<CharTr>();

    public List<CharPart> listMakingCharPart = new List<CharPart>();
    public List<CharPart> listTmpCharPart = new List<CharPart>();
    public Dictionary<string, BubbleText> dicTmpBubble = new Dictionary<string, BubbleText>();
    public List<int> listTmpTrSeq = new List<int>();
    public CharTr tmpCharTr;

    public List<ItemPart> listTmpItemPart = new List<ItemPart>();
    string itemSeqName = null;

    public List<CharPart> listMakingPARTInit = new List<CharPart>();

    public CharPart newTmpPart;
    #endregion

    #region 스트링 값
    public string bodyInitSeq = "007000";
    public string underInitSeq = "009000";
    public string eyeLInitSeq = "013001";
    public string eyeRInitSeq = "014001";
    public string noseInitSeq = "015001";
    public string mouseInitSeq = "016099";

    string xmlWEAR = "WEAR";
    string xmlTR = "TR";

    string xSt = "x";
    string ySt = "y";
    string handSt = "hand";
    string itemTypeSt = "item_type";

    string object1SeqSt = "object1_seq";
    string object2SeqSt = "object2_seq";
    string animalSeqSt = "animal_seq";
    string testSeqSt = "text_seq";
    string bubbleSeqSt = "bubble_seq";

    string item_seqSt = "item_seq";
    string sprite_numSt = "sprite_num";
    string part_kindSt = "part_kind";
    string part_orderSt = "part_order";
    string part_color_rgbaSt = "part_color_rgba";
    string wear_seqSt = "wear_seq";
    string mission_seqSt = "mission_seq";
    string collection_seqSt = "collection_seq";
    string seqNameSt = "seq_name";
    string trSeqSt = "tr_seq";
    string usedSeqSt = "used_seq";
    string bgSeqSt = "bg_seq";
    string positionXSt = "position_x";
    string positionYSt = "position_y";
    string scaleSt = "scale";
    string trOrderSt = "tr_order";
    string isReversedSt = "is_reversed";
    string rowSt = "row";
    string isStoreSt = "is_store";

    // Resources/XML/TestItem.XML 파일.
    public string xmlFileName = "ChibiData";
    public string xmlPositionName = "PartItemPosition";
    public string xmlPartItemName = "PartItemData";
    public string xmlBGItemName = "BGItemData";
    public string xmlObject1ItemName = "Object1ItemData";
    public string xmlObject2ItemName = "Object2ItemData";
    public string xmlAnimalItemName = "AnimalItemData";
    public string xmlTextItemName = "TextItemData";
    public string xmlBubbleItemName = "BubbleItemData";
    public string xmlOrderButtonName = "OrderButtonData";
    public string xmlUserName = "User";
    public string xmlUserHaveName = "UserHave";
    public string xmlItemSeedName = "ItemSeedData";
    public string xmlStoreItemName = "StoreItemData";
    public string xmlMissionName = "MissionData";
    public string xmlCollecionName = "CollectionData";

    //저장해야하는 정보는 패스가 필요함
    string userDataPath;
    string userHaveDataPath;
    string gameDataPath;
    string positionPath;
    string partItemPath;
    string bgItemPath;
    string object1ItemPath;
    string object2ItemPath;
    string animalItemPath;
    string textItemPath;
    string bubbleItemPath;
    string itemSeedPath;
    string storeItemPath;
    string orderButtonPath;
    string missionPath;
    string collectionPath;
    #endregion

    bool isNew;
    StringBuilder stringBuilder = new StringBuilder();

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        //테스트때는 InGameManager쪽에서 실행, 실제 구동때는 IntroCanvas에(InGameManager도 캔버스 이전에 실행되기 때문)
        //InitEditorXml();
        
    }

    public void InitXml(Action callback)
    {
        StartCoroutine(CoInitXml(callback));
    }

    public void InitEditorXml()
    {
        isNew = PlayerPrefs.HasKey(StringInfo.playerPrefsKeyList[8]) ? true : false;

        GetAllPath();
        LoadXml();
        ReadEditorXml();
    }

    public void GetAllPath()
    {
        if (!isNew)
        {
            userDataPath = GetPath(xmlUserName, true);
            userHaveDataPath = GetPath(xmlUserHaveName, true);
            gameDataPath = GetPath(xmlFileName, true);
        }
        else
        {
            userDataPath = GetPath(xmlUserName, false);
            userHaveDataPath = GetPath(xmlUserHaveName, false);
            gameDataPath = GetPath(xmlFileName, false);
        }
        positionPath = GetPath(xmlPositionName, false);
        partItemPath = GetPath(xmlPartItemName, false);
        bgItemPath = GetPath(xmlBGItemName, false);
        object1ItemPath = GetPath(xmlObject1ItemName, false);
        object2ItemPath = GetPath(xmlObject2ItemName, false);
        animalItemPath = GetPath(xmlAnimalItemName, false);
        textItemPath = GetPath(xmlTextItemName, false);
        bubbleItemPath = GetPath(xmlBubbleItemName, false);
        itemSeedPath = GetPath(xmlItemSeedName, false);
        storeItemPath = GetPath(xmlStoreItemName, false);
        orderButtonPath = GetPath(xmlOrderButtonName, false);
        missionPath = GetPath(xmlMissionName, false);
        collectionPath = GetPath(xmlCollecionName, false);
    }

    public void LoadXml()
    {
        LoadXml(data, gameDataPath);
        LoadXml(user, userDataPath);
        LoadXml(userHave, userHaveDataPath);

        LoadXml(position, positionPath);
        LoadXml(partItem, partItemPath);

        LoadXml(itemSeed, itemSeedPath);
        LoadXml(storeItem, storeItemPath);
        LoadXml(orderButton, orderButtonPath);
        LoadXml(mission, missionPath);
        LoadXml(collection, collectionPath);

        LoadXml(bgItem, bgItemPath);
        LoadXml(object1Item, object1ItemPath);
        LoadXml(object2Item, object2ItemPath);
        LoadXml(animalItem, animalItemPath);
        LoadXml(textItem, textItemPath);
        LoadXml(bubbleItem, bubbleItemPath);

        AllocateXmlNodeRoot();
    }
    public IEnumerator CoInitXml(Action callback)
    {
        GetAllPath();
        yield return null;

        LoadXml();
        yield return null;

        StartCoroutine(CoReadAllXML(callback));
    }

    //public void ReadAllXML()
    //{
    //    StartCoroutine("CoLoadAllXML");
    //}

    public void ReadEditorXml()
    {
        LoadAllPartItem();
        LoadAllPositionItem();
        OrderType[] charPartOrderTypes = (OrderType[])Enum.GetValues(typeof(OrderType));
        for (int i = 0; i < charPartOrderTypes.Length; i++)
        {
            LoadCharPartItemByPart_Order((int)charPartOrderTypes[i]);
            //아이템의 번호를 이용하여 순서정하기
            if (listAllPartDic.ContainsKey(i))
            {
                listAllPartKeysDic[i] = new List<int>(listAllPartDic[i].Keys);
                listAllPartKeysDic[i].Sort();
            }
        }

        LoadAllObj();
        FriendType[] friendTypes = (FriendType[])Enum.GetValues(typeof(FriendType));
        //0은 캐릭터니까 제외
        for (int i = 1; i < friendTypes.Length; i++)
        {
            LoadCharPartItemByPart_Order((int)charPartOrderTypes[i]);
            //아이템의 번호를 이용하여 순서정하기
            if (listAllObjDic.ContainsKey(i))
            {
                listAllObjKeysDic[i] = new List<int>(listAllObjDic[i].Keys);
                listAllObjKeysDic[i].Sort();
            }
        }
    }

    public IEnumerator CoReadAllXML(Action callback)
    {
        LoadAllPartItem();
        yield return null;
        LoadAllPositionItem();
        yield return null;
        OrderType[] charPartOrderTypes = (OrderType[])Enum.GetValues(typeof(OrderType));
        for (int i = 0; i < charPartOrderTypes.Length; i++)
        {
            LoadCharPartItemByPart_Order((int)charPartOrderTypes[i]);
            //아이템의 번호를 이용하여 순서정하기
            if (listAllPartDic.ContainsKey(i))
            {
                listAllPartKeysDic[i] = new List<int>(listAllPartDic[i].Keys);
                listAllPartKeysDic[i].Sort();
            }
        }
        yield return null;


        LoadAllObj();
        yield return null;
        FriendType[] friendTypes = (FriendType[])Enum.GetValues(typeof(FriendType));
        //0은 캐릭터니까 제외
        for (int i = 1; i < friendTypes.Length; i++)
        {
            //아이템의 번호를 이용하여 순서정하기
            if (listAllObjDic.ContainsKey(i))
            {
                listAllObjKeysDic[i] = new List<int>(listAllObjDic[i].Keys);
                listAllObjKeysDic[i].Sort();
            }
        }

        callback();
    }

    //PartItemData.xml 모든 아이템 로드
    public void LoadAllPartItem()
    {
        CharPartItem tmpPartItem;
        string itemSeq;
        int spriteNum;
        int partKind;
        int partOrder;
        XmlNodeList listTmpNode = SelectAllCharPartItem();

        for (int i = 0; i < listTmpNode.Count; i++)
        {
            itemSeq = listTmpNode[i].Attributes[ item_seqSt].Value;
            spriteNum = int.Parse(listTmpNode[i].Attributes[ sprite_numSt].Value);
            partKind = int.Parse(listTmpNode[i].Attributes[ part_kindSt].Value);
            partOrder = int.Parse(listTmpNode[i].Attributes[ part_orderSt].Value);

            tmpPartItem = new CharPartItem(itemSeq, spriteNum, partKind, partOrder);
            allPartItemDic.Add(itemSeq, tmpPartItem);
        }
    }

    //PartItemPosition.xml 모든 아이템 로드
    public void LoadAllPositionItem()
    {
        CharPartItemPos tmpItemPos;
        string itemSeq;
        int x;
        int y;
        string hand = null;
        string itemType;

        XmlNodeList listTmpNode = SelectAllItemPosition();

        for (int i = 0; i < listTmpNode.Count; i++)
        {
            itemSeq = listTmpNode[i].Attributes[ item_seqSt].Value;
            x = int.Parse(listTmpNode[i].Attributes[xSt].Value);
            y = int.Parse(listTmpNode[i].Attributes[ySt].Value);
            hand = listTmpNode[i].Attributes[handSt].Value;
            itemType = listTmpNode[i].Attributes[itemTypeSt].Value;
            tmpItemPos = new CharPartItemPos(itemSeq, x, y, hand, itemType);
            allPartPosDic.Add(itemSeq, tmpItemPos);
        }
    }

    //메인의 모든 오브젝트 로드
    public void LoadAllObj()
    {
        XmlNodeList listMAIN_Obj = null;

        //0은 캐릭터니까 제외
        for (int i = 1; i < 7; i++)
        {
            Dictionary<int, ItemPart> OneMainObjDic = new Dictionary<int, ItemPart>();
            switch (i)
            {
                default:
                    Debug.LogError("groupSeq 잘못됨");
                    break;
                case 1:
                    itemSeqName =  bgSeqSt;
                    listMAIN_Obj = SelectAllBGItem();
                    break;
                case 2:
                    itemSeqName =  object1SeqSt;
                    listMAIN_Obj = SelectAllObject1Item();
                    break;
                case 3:
                    itemSeqName =  object2SeqSt;
                    listMAIN_Obj = SelectAllObject2Item();
                    break;
                case 4:
                    itemSeqName =  animalSeqSt;
                    listMAIN_Obj = SelectAllAnimalItem();
                    break;
                case 5:
                    itemSeqName =  bubbleSeqSt;
                    listMAIN_Obj = SelectAllBubbleItem();
                    break;
                case 6:
                    itemSeqName =  testSeqSt;
                    listMAIN_Obj = SelectAllTextItem();
                    break;
            }

            XmlNode nodeObj;
            ItemPart tmpItemPart;
            string itemSeq;
            string typeSeq;
            int itemType;

            for (int j = 0; j < listMAIN_Obj.Count; j++)
            {
                nodeObj = listMAIN_Obj[j];
                itemSeq = nodeObj.Attributes[ item_seqSt].Value;//넘버링

                typeSeq = nodeObj.Attributes[itemSeqName].Value;//고유이름
                itemType = int.Parse(nodeObj.Attributes[itemTypeSt].Value);//상점, 스페셜
                tmpItemPart = new ItemPart(itemSeq, itemSeqName, typeSeq, itemType);

                OneMainObjDic.Add(int.Parse(itemSeq), tmpItemPart);
            }

            if (!listAllObjDic.ContainsKey(i))
            {
                List<int> list = new List<int>();
                listAllObjDic.Add(i, OneMainObjDic);
                listAllObjKeysDic.Add(i, list);
            }
        }
    }
    //파트아이템xml에서 아이템 가져오고 파트아이템위치xml에서 위치 가져와서 listTmpCharPart로 넘겨주기
    //오더에 따른 아이템 분류
    public void LoadCharPartItemByPart_Order(int partOrder)
    {
        Dictionary<int, CharPart> listTmp = new Dictionary<int, CharPart>();

        CharPart tmpPart;
        CharPartItem tmpPartItem;
        Color color;

        XmlNodeList allPartByOrderNodeList = SelectAllPartItemByPart_OrderList(partOrder);
        XmlNode element;

        for(int i = 0; i < allPartByOrderNodeList.Count; i++)
        {
            element = allPartByOrderNodeList[i];

            string itemSeq = element.Attributes[ item_seqSt].Value;

            //PartItemData.xml에서 해당 아이템 찾기
            tmpPartItem = new CharPartItem(itemSeq, int.Parse(element.Attributes[ sprite_numSt].Value), int.Parse(element.Attributes[ part_kindSt].Value), int.Parse(element.Attributes[ part_orderSt].Value));

            //PartItemPosition.xml에서 해당 좌표 찾기
            string handSeq;
            int handType = 0;
            if (allPartPosDic[itemSeq].hand != null && allPartPosDic[itemSeq].hand != "")
            {
                handSeq = allPartPosDic[itemSeq].hand;
                if (handSeq[handSeq.Length - 1] == 'U')
                {
                    handType = 1;
                    handSeq = handSeq.TrimEnd('U');
                }
                else
                {
                    handType = 0;
                }
            }
            else
            {
                handSeq = "-1";
            }
            int pantType = 0;

            int type = 0;
            if (allPartPosDic[itemSeq].itemType != null && allPartPosDic[itemSeq].itemType != "")
            {
                type = int.Parse(allPartPosDic[itemSeq].itemType);
            }
            float tmpX = allPartPosDic[itemSeq].x;
            float tmpY = allPartPosDic[itemSeq].y;

            //색
            color = Color.white;
            tmpPart = new CharPart(tmpPartItem, color, handSeq, handType, pantType, type, tmpX, tmpY);
            listTmp.Add(int.Parse(element.Attributes[ sprite_numSt].Value), tmpPart);
        }

        if (!listAllPartDic.ContainsKey(partOrder))
        {
            List<int> list = new List<int>();
            listAllPartDic.Add(partOrder, listTmp);
            listAllPartKeysDic.Add(partOrder, list);
        }
    }

    /// <summary>
    /// 파일 경로 가져오기
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    private string GetPath(string fileName, bool isUserSaveFile)
    {
        string path;
#if UNITY_ANDROID
        path = Application.persistentDataPath + "/XML/" + fileName + ".xml";
        //path = "jar:file://" + Application.dataPath + "!/assets/" + "/XML/" + fileName + ".xml";
        DirectoryInfo directoryInfo = new DirectoryInfo(Application.persistentDataPath + "/XML");

        if (!directoryInfo.Exists) 
        {
            directoryInfo.Create();
        }

        if (!File.Exists(path))
        {
            //DebugManager.ScreenDebug("패스가 존재하지않습니다");
            //StartCoroutine(RemoveFile(fileName, path));

            StartCoroutine(CopyFile(fileName, path));
            //StartCoroutine(InitCopyFile(fileName));
        }
        else
        {
            //DebugManager.ScreenDebug("패스가 존재합니다??");
            if (!isUserSaveFile)
            {
                StartCoroutine(RemoveFile(fileName, path));
            }
            //StartCoroutine(RemoveFile(fileName, path));
            //StartCoroutine(InitCopyFile(fileName));
            // 업데이트
        }
#endif
#if UNITY_IOS
        path = Application.persistentDataPath + "/XML/" + fileName + ".xml";
        if (!File.Exists(path))
        {  
            //File.Copy( Application.dataPath + "/Raw/" + "XML/" + fileName + ".xml" , path);
        }
        else
        {
        }
#endif
#if UNITY_EDITOR
        //stringBuilder.Append().Append().Append().Append();
        //stringBuilder.ToString();
        path = Application.streamingAssetsPath + "/" + fileName + ".xml";
            if (!File.Exists(path))
            {
                //DebugManager.ScreenDebug("패스가 존재하지않습니다");

                //File.Copy(Application.streamingAssetsPath + "/XML/" + fileName + ".xml", path);
            }
            else
            {
                //DebugManager.ScreenDebug("패스가 존재합니다??");

                //StartCoroutine(CopyFile(fileName, path));               
            }
#endif
        //DebugManager.ScreenDebug(path);
        //StringBuilder stringBuilder = new StringBuilder();
        //DebugManager.dText = stringBuilder.Append("패스 : " + path).Append("\n").Append("원본 :").Append("jar:file://").Append(Application.dataPath).Append("!/assets/").Append("XML/").Append(fileName + ".xml\n").ToString();
        //DebugManager.ScreenDebug(DebugManager.dText);
        //Debug.Log(DebugManager.dText);
        return path;
    }



    private IEnumerator CopyFile(string fileName, string path)
    {
        stringBuilder.Clear();
        stringBuilder.Append("jar:file://").Append(Application.dataPath).Append("!/assets/").Append(fileName).Append(".xml");
        WWW loadDB = new WWW(stringBuilder.ToString());
        while (!loadDB.isDone) {
        }
        File.WriteAllBytes(path, loadDB.bytes);
        //DebugManager.ScreenDebug("복사 종료");
        yield return null;
    }


    private IEnumerator InitCopyFile(string fileName)
    {
        stringBuilder.Clear();
        stringBuilder.Append("jar:file://").Append(Application.dataPath).Append("!/assets/").Append("XML/").Append(fileName).Append(".xml");
        WWW loadDB = new WWW(stringBuilder.ToString());
        while (!loadDB.isDone)
        {
        }
        stringBuilder.Clear();
        stringBuilder.Append(Application.persistentDataPath).Append("/XML/").Append(fileName).Append(".xml");
        File.Delete(stringBuilder.ToString());
        yield return null;


        string outPath = Application.persistentDataPath + "/XML/" + fileName + ".xml";
        //string inPath = Application.dataPath + "/Resources/XML/"+ fileName + ".xml";
        //WWW loadDB = new WWW(inPath); 
        //while (!loadDB.isDone) { }

        TextAsset t = (TextAsset)Resources.Load("/XML/" + fileName + ".xml", typeof(TextAsset));
        yield return t;


        File.WriteAllBytes(outPath, t.bytes);
        yield return null;
    }

    private IEnumerator RemoveFile(string fileName, string path)
    {
        stringBuilder.Clear();
        stringBuilder.Append("jar:file://").Append(Application.dataPath).Append("!/assets/").Append("XML/").Append(fileName).Append(".xml");
        WWW loadDB = new WWW(stringBuilder.ToString());
        while (!loadDB.isDone) { }
        File.Delete(loadDB.ToString());
        File.Delete(path);
        //DebugManager.ScreenDebug("삭제 완료");
        StartCoroutine(CopyFile(fileName, path));
        yield return null;
    }

    //Load

    private void LoadXml(XmlDocument xml, string dataPath)
    {
        xml.Load(dataPath);
    }

    //private void LoadXmlFile(string _fileName)
    //{
    //    //string filePath = Application.persistentgameDataPath + "/" + _fileName + ".xml";
    //    //if (Application.platform == RuntimePlatform.Android)
    //    //{
    //    //    WWW wwwUrl = new WWW("jar:file://" + Application.gameDataPath + "!/assets/" + _fileName + ".xml");
    //    //    File.WriteAllBytes(filePath, wwwUrl.bytes);
    //    //}
    //    //StreamReader streamReader = new StreamReader(filePath);
    //    //data.Load(filePath);

    //    //TextAsset txtAsset = (TextAsset)Resources.Load("XML/" + _fileName);
    //    ////Debug.Log(txtAsset.text);
    //    data.Load(gameDataPath);
    //}

    public void AllocateXmlNodeRoot()
    {
        //데이터 xml
        root = data.DocumentElement;
        //////DebugManager.Instance.Screen//Debug(root.Name);

        nodeALL_SLOT = root.SelectSingleNode("ALL_SLOT");
        nodeMAIN = root.SelectSingleNode("MAIN");

        listSLOT = nodeALL_SLOT.SelectNodes("SLOT");

        //User xml
        userRoot = user.DocumentElement;

        nodeDateTime = userRoot.SelectSingleNode("DateTime");
        nodeCoins = userRoot.SelectSingleNode("Coins");
        nodeHaveBundle = userRoot.SelectSingleNode("HaveBundle");

        nodeMission = userRoot.SelectSingleNode("Mission");
        nodeMissions = userRoot.SelectSingleNode("Missions");
        nodeCollections = userRoot.SelectSingleNode("Collections");

        nodeWheelTimes = nodeMission.SelectSingleNode("WheelTimes");
        nodeSaveKind = nodeMission.SelectSingleNode("SaveKind");
        nodePerchaseTimes = nodeMission.SelectSingleNode("PerchaseTimes");
        nodeSaveSlotTimes = nodeMission.SelectSingleNode("SaveSlotTimes");
        nodeShowADTimes = nodeMission.SelectSingleNode("ShowADTimes");
        nodeGetMissionRWTimes = nodeMission.SelectSingleNode("GetMissionRWTimes");
        nodeEvaluation = nodeMission.SelectSingleNode("Evaluation");
        //UserHave xml
        userHaveRoot = userHave.DocumentElement;

        //position xml
        positionRoot = position.DocumentElement;

        //partItem xml
        partItemRoot = partItem.DocumentElement;

        //BGItem xml
        bgItemRoot = bgItem.DocumentElement;

        //Obj1Item xml
        object1ItemRoot = object1Item.DocumentElement;

        //Obj2Item xml
        object2ItemRoot = object2Item.DocumentElement;

        //AnimalItem xml
        animalItemRoot = animalItem.DocumentElement;

        //TextItem xml
        textItemRoot = textItem.DocumentElement;

        //BubbleItem xml
        bubbleItemRoot = bubbleItem.DocumentElement;

        //OrderButton xml
        orderButtonRoot = orderButton.DocumentElement;

        //ItemSeed xml
        itemSeedRoot = itemSeed.DocumentElement;

        //StoreItem xml
        storeItemRoot = storeItem.DocumentElement;

        //missionData xml
        missionRoot = mission.DocumentElement;
        missions = missionRoot.SelectSingleNode("Missions");

        //collectionData xml
        collectionRoot = collection.DocumentElement;
        collections = collectionRoot.SelectSingleNode("Collections");
    }

    //노드 찾기
    public XmlNode SelectNode(XmlNode parent, string nodeName, string att, string seq)
    {
        stringBuilder.Clear();
        stringBuilder.Append(nodeName).Append("[@").Append(att).Append("=").Append(seq).Append("]");
        XmlNode tmpNode = parent.SelectSingleNode(stringBuilder.ToString());
        if(tmpNode == null)
        {
            //Debug.Log("찾는 노드 없음");
        }
        return tmpNode;
    }

    //PartItemPosition.xml에서 노드 찾기
    public XmlNode SelectOneItemPosition(string itemSeq)
    {
        XmlNode tmpNode = SelectNode(positionRoot,  rowSt,  item_seqSt, itemSeq);
        return tmpNode;
    }

    //PartItemPosition.xml 모든 노드 리스트 
    public XmlNodeList SelectAllItemPosition()
    {
        XmlNodeList listTmpNode = positionRoot.ChildNodes;
        return listTmpNode;
    }

    //PartItemData.xml 모든 노드 리스트 
    public XmlNodeList SelectAllCharPartItem()
    {
        XmlNodeList listTmpNode = partItemRoot.ChildNodes;
        return listTmpNode;
    }


    //PartItemData.xml에서 노드 찾기 -> allPartItemDic 으로 대체됨
    public XmlNode SelectOneCharPartItem(string itemSeq)
    {
        
        XmlNode tmpNode = SelectNode(partItemRoot,  rowSt,  item_seqSt, itemSeq);
        return tmpNode;
    }

    public XmlNode SelectOneUserCoins(string coinSeq)
    {
        XmlNode tmpNode = SelectNode(nodeCoins, "HaveCoins", "coin_seq", coinSeq);
        return tmpNode;
    }

    //UserHave.xml에서 같은 Seq_name 리스트
    public XmlNodeList SelectHAVE_ITEMBySeq_name(string seqName)
    {
        stringBuilder.Clear();
        stringBuilder.Append("HAVE_ITEM").Append("[@seq_name='").Append(seqName).Append("']");
        XmlNodeList tmpNodeList = userHaveRoot.SelectNodes(stringBuilder.ToString());
        return tmpNodeList;
    }

    //UserHave.xml 같은 Seq_name 리스트에서 노드 찾기
    public XmlNode SelectUserHave(string seqName, string userSeq)
    {
        XmlNodeList listByOrder_seq = SelectHAVE_ITEMBySeq_name(seqName);
        XmlNode element;
        for(int i = 0; i < listByOrder_seq.Count; i++)
        {
            element = listByOrder_seq[i];
            if (element.Attributes[seqName].Value == userSeq)
            {
                return element;
            }
        }
        return null;
    }

    //UserHave.xml 전체 노드 찾기
    XmlNodeList SelectAllUserHaveItem()
    {
        XmlNodeList tmpNodeList = userHaveRoot.SelectNodes("HAVE_ITEM");
        return tmpNodeList;
    }

    public XmlNodeList SelectAllBundle() {
        XmlNodeList tmpNodeList = storeItemRoot.ChildNodes;
        return tmpNodeList;
    }

    //StoreItem.xml에서 번들 찾기
    public XmlNode SelectOneStoreBundle(string bundleSeq)
    {
        XmlNode tmpNode = SelectNode(storeItemRoot, "BUNDLE", "bundle_seq", bundleSeq);
        return tmpNode;
    }

    public XmlNodeList SelectAllBundleItem(string bundleSeq)
    {
        XmlNodeList tmpNodeList = null;

        try
        {
            tmpNodeList = SelectOneStoreBundle(bundleSeq).SelectNodes("HAVE_ITEM");
        }
        catch
        {

        }
        return tmpNodeList;
    }

    //아이템seq가지고 번들 이름 찾기
    public int SelectBundleByItemSeq(string seqName, string itemSeq)
    {
        Debug.Log(seqName);
        Debug.Log(itemSeq);
        XmlNodeList tmpNodeList = SelectAllBundle();
        
        for (int i = 0; i < tmpNodeList.Count; i++)
        {
            XmlNodeList tmpBundleList = tmpNodeList[i].SelectNodes("HAVE_ITEM");
            for (int j = 0; j < tmpBundleList.Count; j++)
            {
                if(CheckNodeIsItem(tmpBundleList[j], seqName, itemSeq))
                {
                    return i;
                }
            }
        }
        return -1;
    }

    
    //bgItemData.xml에서 노드 찾기
    XmlNode SelectOneBGItem(string itemSeq)
    {
        XmlNode tmpNode = SelectNode(bgItemRoot,  rowSt,  item_seqSt, itemSeq);
        return tmpNode;
    }
    //OrderButton.xml에서 노드 찾기
    XmlNode SelectOneOrderButton(string orderSeq)
    {
        XmlNode tmpNode = SelectNode(orderButtonRoot,  rowSt, "order_seq", orderSeq);
        return tmpNode;
    }

    public XmlNodeList SelectOrderButtonBySeq_name(string seqName)
    {
        stringBuilder.Clear();
        stringBuilder.Append("row").Append("[@seq_name='").Append(seqName).Append("']");
        XmlNodeList tmpNodeList = orderButtonRoot.SelectNodes(stringBuilder.ToString());
        return tmpNodeList;
    }

    //MissionData.xml 전체 노드 찾기
    public XmlNodeList SelectAllMission()
    {
        XmlNode tmpNode = missionRoot.SelectSingleNode("Missions");
        return tmpNode.SelectNodes("Mission");
    }

    //MissionData.xml 노드 찾기
    public XmlNode SelectOneMission(string missionSeq)
    {
        XmlNode tmpNode = SelectNode(missions, "Mission",  mission_seqSt, missionSeq);
        return tmpNode;
    }

    //CollectionData.xml 전체 노드 찾기
    public XmlNodeList SelectAllCollection()
    {
        XmlNode tmpNode = collectionRoot.SelectSingleNode("Collections");
        return tmpNode.SelectNodes("Collection");
    }

    //CollectionData.xml 노드 찾기
    public XmlNode SelectOneCollection(string collectionSeq)
    {
        XmlNode tmpNode = SelectNode(collections, "Collection",  collection_seqSt, collectionSeq);
        return tmpNode;
    }
    XmlNodeList SelectAllBGItem()
    {
        XmlNodeList tmpNodeList = bgItemRoot.SelectNodes( rowSt);
        return tmpNodeList;
    }
    XmlNodeList SelectAllObject1Item()
    {
        XmlNodeList tmpNodeList = object1ItemRoot.SelectNodes( rowSt);
        return tmpNodeList;
    }
    XmlNodeList SelectAllObject2Item()
    {
        XmlNodeList tmpNodeList = object2ItemRoot.SelectNodes( rowSt);
        return tmpNodeList;
    }
    XmlNodeList SelectAllAnimalItem()
    {
        XmlNodeList tmpNodeList = animalItemRoot.SelectNodes( rowSt);
        return tmpNodeList;
    }
    XmlNodeList SelectAllTextItem()
    {
        XmlNodeList tmpNodeList = textItemRoot.SelectNodes( rowSt);
        return tmpNodeList;
    }
    XmlNode SelectOneTextItem(string textSeq)
    {
        XmlNode tmpNode = SelectNode(textItemRoot,  rowSt, "text_seq", textSeq);
        return tmpNode;
    }
    XmlNodeList SelectAllBubbleItem()
    {
        XmlNodeList tmpNodeList = bubbleItemRoot.SelectNodes( rowSt);
        return tmpNodeList;
    }

    XmlNode SelectOneBubbleItem(string textSeq)
    {
        XmlNode tmpNode = SelectNode(bubbleItemRoot,  rowSt,  bubbleSeqSt, textSeq);
        return tmpNode;
    }
    public XmlNode SelectGroup(XmlNode targetParentNode, int groupSeq)
    {
        XmlNode tmpNode = SelectNode(targetParentNode, "ITEM_GROUP", "group_seq", groupSeq.ToString());
        return tmpNode;
    }

    public XmlNode SelectWear(XmlNode targetParentNode, string wearSeq)
    {
        XmlNode tmpNode = SelectNode(targetParentNode,  xmlWEAR,  wear_seqSt, wearSeq.ToString());
        return tmpNode;
    }

    public XmlNode SelectBubble(XmlNode targetParentNode, string bubbleSeq)
    {
        XmlNode tmpNode = SelectNode(targetParentNode, "BUBBLE_ITEM",  bubbleSeqSt, bubbleSeq.ToString());
        return tmpNode;
    }

    public Sprite SetSprite(int x)
    {
        XmlNode tmp = SelectNode(partItemRoot,  rowSt,  item_seqSt, x.ToString());
        CharPartItem tmpItem = new CharPartItem(tmp.Attributes[ item_seqSt].Value, int.Parse(tmp.Attributes[ sprite_numSt].Value), int.Parse(tmp.Attributes[ part_kindSt].Value), int.Parse(tmp.Attributes[ part_orderSt].Value));
        return AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllPartsAtlas, tmpItem.spriteName);
    }

    XmlNode SelectSlot(string slotSeq)
    {
        stringBuilder.Clear();
        stringBuilder.Append("SLOT[@slot_seq=").Append(slotSeq).Append("]");
        XmlNode tmpNode = nodeALL_SLOT.SelectSingleNode(stringBuilder.ToString());
        return tmpNode;
    }

#region User.xml
    //코인 로드(int)
    public int LoadUserCoins(string coinSeq)
    {
        return int.Parse(SelectOneUserCoins(coinSeq).Attributes["have"].Value);
    }
    //코인 추가
    public void SaveUserCoinsAdd(string coinSeq, int addCoins)
    {
        int prevCoins = LoadUserCoins(coinSeq);
        SelectOneUserCoins(coinSeq).Attributes["have"].Value = (prevCoins + addCoins).ToString();
        user.Save(userDataPath);
    }
    //코인 빼기
    public void SaveUserCoinsReduce(string coinSeq, int subCoins)
    {
        int prevCoins = LoadUserCoins(coinSeq);
        SelectOneUserCoins(coinSeq).Attributes["have"].Value = (prevCoins - subCoins).ToString();
        user.Save(userDataPath);
    }

    //지정한 날짜 세이브
    public void SaveUserDateTime(string dateTimeName, string dateTime)
    {
        XmlNode nodeSelectDateTime = nodeDateTime.SelectSingleNode(dateTimeName);

        nodeSelectDateTime.Attributes["time"].Value = dateTime;
        user.Save(userDataPath);
    }

    //지정한 날짜 로드
    public string LoadUserDateTime(string dateTimeName)
    {
        XmlNode nodeSelectDateTime = nodeDateTime.SelectSingleNode(dateTimeName);
        return nodeSelectDateTime.Attributes["time"].Value;
    }

    //이번 데일리 시드 받아오기
    public int LoadUserDateDailySeed()
    {
        XmlNode nodeDailySeed = nodeDateTime.SelectSingleNode("DailySeed");
        int seed = int.Parse(nodeDailySeed.Attributes["seed"].Value);
        return seed;
    }

    //이번 데일리 시드 변경하기
    public void SaveUserDateDailySeed(int seed)
    {
        XmlNode nodeDailySeed = nodeDateTime.SelectSingleNode("DailySeed");
        nodeDailySeed.Attributes["seed"].Value = seed.ToString();
        user.Save(userDataPath);
    }

    //이번 룰렛 시드 받아오기
    public int LoadUserDateWheelSeed()
    {
        XmlNode nodeWheelSeed = nodeDateTime.SelectSingleNode("FortuneWheelSeed");
        int seed = int.Parse(nodeWheelSeed.Attributes["seed"].Value);
        return seed;
    }

    //이번 룰렛 시드 변경하기
    public void SaveUserDateWheelSeed(int seed)
    {
        XmlNode nodeWheelSeed = nodeDateTime.SelectSingleNode("FortuneWheelSeed");
        nodeWheelSeed.Attributes["seed"].Value = seed.ToString();
        user.Save(userDataPath);
    }

    //보상 받은지 입력
    public void SaveUserGetReward(bool getReward)
    {
        XmlNode nodeGetReward = nodeDateTime.SelectSingleNode("GetReward");
        nodeGetReward.Attributes["getReward"].Value = getReward.ToString();
        user.Save(userDataPath);
    }

    //보상받은지 확인
    public bool LoadUserGetReward()
    {
        XmlNode nodeGetReward = nodeDateTime.SelectSingleNode("GetReward");
        bool getReward = bool.Parse(nodeGetReward.Attributes["getReward"].Value);
        return getReward;
    }
    //총접속날짜 저장
    public void SaveUserDayCount(int dayCount)
    {
        XmlNode nodeDayCount = nodeDateTime.SelectSingleNode("DayCount");
        nodeDayCount.Attributes["day_count"].Value = dayCount.ToString();
        user.Save(userDataPath);
    }
    //총접속날짜 로드
    public int LoadUserDayCount()
    {
        XmlNode nodeDayCount = nodeDateTime.SelectSingleNode("DayCount");
        int dayCount = int.Parse(nodeDayCount.Attributes["day_count"].Value);
        return dayCount;
    }

    //미션 통합 로드
    public float LoadMissionValue(XmlNode node)
    {
        float value = int.Parse(node.Attributes["value"].Value);
        return value;
    }

    public void SaveMissionValue(XmlNode node, string att, string val)
    {
        node.Attributes[att].Value = val;
        user.Save(userDataPath);
    }

    //무언가 횟수 +1
    public void SaveMissionValueUp(XmlNode node)
    {
        int value = int.Parse(node.Attributes["value"].Value);

        node.Attributes["value"].Value = (value + 1).ToString();
        user.Save(userDataPath);
    }

    ////휠 돌린 횟수 로드
    //public int LoadUserWheelTimes()
    //{
    //    int wheelTimes = int.Parse(nodeWheelTimes.Attributes["times"].Value);
    //    return wheelTimes;
    //}

    ////휠 돌린 횟수 +1
    //public void SaveUserWheelTimesUp()
    //{
    //    int wheelTimes = int.Parse(nodeWheelTimes.Attributes["times"].Value);

    //    nodeWheelTimes.Attributes["times"].Value = (wheelTimes + 1).ToString();
    //    user.Save(userDataPath);
    //}

    //Missions
    public XmlNodeList SelectAllUserMissions()
    {
        return nodeMissions.SelectNodes("Mission_Info");
    }

    public XmlNode SelectOneUserMissions(string missionSeq)
    {
        XmlNodeList tmpNodeList = SelectAllUserMissions();
        for (int i = 0; i < tmpNodeList.Count; i++)
        {
            if (tmpNodeList[i].Attributes["mission_seq"].Value == missionSeq)
            {
                return tmpNodeList[i];
            }
        }
        return null;
    }

    public void SaveMissionIsOn(string missionSeq, bool isOn)
    {

        XmlNode tmpNode = SelectOneUserMissions(missionSeq);
        tmpNode.Attributes["is_on"].Value = isOn.ToString();
        user.Save(userDataPath);
    }
    public void SaveMissionIsClear(string missionSeq, bool isClear)
    {
        XmlNode tmpNode = SelectOneUserMissions(missionSeq);
        tmpNode.Attributes["is_clear"].Value = isClear.ToString();
        user.Save(userDataPath);
    }

    public int LoadNowLevel(string missionSeq)
    {
        XmlNode tmpNode = SelectOneUserMissions(missionSeq);
        int nowLevel = int.Parse(tmpNode.Attributes["now_level"].Value);
        return nowLevel;
    }

    public void SaveMissionNowLevel(string missionSeq, int nowLevel)
    {
        XmlNode tmpNode = SelectOneUserMissions(missionSeq);
        tmpNode.Attributes["now_level"].Value = nowLevel.ToString();
        tmpNode.Attributes["is_clear"].Value = "False";
        user.Save(userDataPath);
    }
    //Collections
    public XmlNodeList SelectAllUserCollections()
    {
        return nodeCollections.SelectNodes("Collection_Info");
    }

    public XmlNode SelectOneUserCollections(string collectionSeq)
    {
        XmlNodeList tmpNodeList = SelectAllUserCollections();
        for (int i = 0; i < tmpNodeList.Count; i++)
        {
            if (tmpNodeList[i].Attributes["collection_seq"].Value == collectionSeq)
            {
                return tmpNodeList[i];
            }
        }
        return null;
    }

    public void SaveCollectionIsOn(string collectionSeq, bool isOn)
    {
        XmlNode tmpNode = SelectOneUserCollections(collectionSeq);
        tmpNode.Attributes["is_on"].Value = isOn.ToString();
        user.Save(userDataPath);
    }
    public void SaveCollectionIsClear(string collectionSeq, bool isClear)
    {
        XmlNode tmpNode = SelectOneUserCollections(collectionSeq);
        tmpNode.Attributes["is_clear"].Value = isClear.ToString();
        user.Save(userDataPath);
    }
    public void SaveCollectionLevelUp(string collectionSeq)
    {
        XmlNode tmpNode = SelectOneUserCollections(collectionSeq);
        int prevLevel = int.Parse(tmpNode.Attributes["level"].Value);
        prevLevel += 1;
        tmpNode.Attributes["level"].Value = prevLevel.ToString();
        user.Save(userDataPath);
    }


    //HaveBundle
    public List<int> LoadStoreBundleList()
    {
        List<int> tmpList = new List<int>();
        for (int i = 0; i < nodeHaveBundle.ChildNodes.Count;i++) {
            tmpList.Add(int.Parse(nodeHaveBundle.ChildNodes[i].Attributes["bundle_seq"].Value));
        }
        return tmpList;
    }

    public void SaveStoreBundleOne(string bundleSeq)
    {
        if (SelectNode(nodeHaveBundle, "BUNDLE", "bundle_seq", bundleSeq) == null)
        {
            XmlElement BUNDLE = userHave.CreateElement("BUNDLE");
            BUNDLE.SetAttribute("bundle_seq", bundleSeq);
            XmlNode xmlNode = nodeHaveBundle.OwnerDocument.ImportNode(BUNDLE, true);
            nodeHaveBundle.AppendChild(xmlNode);
            user.Save(userDataPath);
        }
    }

    #endregion

    #region UserHave.xml

    //모든 아이템 소유 검사는 이쪽에서
    public bool IsUserHaveItem(string seqName, string itemSeq)
    {
        XmlNode nodeTmp = SelectUserHave(seqName, itemSeq);

        if (nodeTmp != null)
        {
            return true;
        }

        if (seqName == "hair_seq")
        {
            nodeTmp = SelectUserHave( item_seqSt, ParseItemSeq.numToD3((int)OrderType.BackHair) + itemSeq);
            if (nodeTmp == null) 
            {
                return false;
            }
            nodeTmp = SelectUserHave( item_seqSt, ParseItemSeq.numToD3((int)OrderType.SideHair) + itemSeq);
            if (nodeTmp == null)
            {
                return false;
            }
            nodeTmp = SelectUserHave( item_seqSt, ParseItemSeq.numToD3((int)OrderType.FrontHair) + itemSeq);
            if (nodeTmp == null)
            {
                return false;
            }
            return true;
        }
        else if (seqName == "eyes_seq")
        {
            nodeTmp = SelectUserHave( item_seqSt, ParseItemSeq.numToD3((int)OrderType.EyeLeft) + itemSeq);
            if (nodeTmp == null)
            {
                return false;
            }
            nodeTmp = SelectUserHave( item_seqSt, ParseItemSeq.numToD3((int)OrderType.EyeRight) + itemSeq);
            if (nodeTmp == null)
            {
                return false;
            }
            return true;
        }

        if(seqName ==  item_seqSt && (int.Parse(ParseItemSeq.SplitItemSeq1(itemSeq)) == (int)OrderType.BackHair||
            int.Parse(ParseItemSeq.SplitItemSeq1(itemSeq)) == (int)OrderType.SideHair||
            int.Parse(ParseItemSeq.SplitItemSeq1(itemSeq)) == (int)OrderType.FrontHair))
        {
            nodeTmp = SelectUserHave("hair_seq", ParseItemSeq.SplitItemSeq2(itemSeq));
            if(nodeTmp != null)
            {
                return true;
            }
        }
        else if (seqName ==  item_seqSt && (int.Parse(ParseItemSeq.SplitItemSeq1(itemSeq)) == (int)OrderType.EyeLeft ||
            int.Parse(ParseItemSeq.SplitItemSeq1(itemSeq)) == (int)OrderType.EyeRight))
        {
            nodeTmp = SelectUserHave("eyes_seq", ParseItemSeq.SplitItemSeq2(itemSeq));
            if (nodeTmp != null)
            {
                return true;
            }
        }
        return false;
    }

    //소유 아이템 추가하기
    public void AddUserHaveItem(string seqName, string itemSeq, ItemGetRoot itemGetRoot)
    {
        XmlElement HAVE_ITEM = userHave.CreateElement("HAVE_ITEM");
        HAVE_ITEM.SetAttribute( seqNameSt, seqName);
        HAVE_ITEM.SetAttribute(seqName, itemSeq);
        HAVE_ITEM.SetAttribute(isStoreSt, (itemGetRoot == ItemGetRoot.store).ToString());

        userHaveRoot.AppendChild(HAVE_ITEM);
        userHave.Save(userHaveDataPath);
    }


    //전체 소유 아이템의 isStore 체크 후 true일 때 삭제하기
    public void IsStoreHaveItem()
    {
        XmlNodeList nodeTmpList = userHaveRoot.SelectNodes("HAVE_ITEM");
        for(int i = nodeTmpList.Count - 1; i >= 0; i--) {
            if (bool.Parse(nodeTmpList[i].Attributes[isStoreSt].Value))
            {
                userHaveRoot.RemoveChild(nodeTmpList[i]);
            }
        }
    }

    //워터마크
    public void SaveUserHave_RmWaterMark(bool rmWaterMark)
    {
        XmlNode nodeTmp = userHaveRoot.SelectSingleNode("WATER_MARK");
        nodeTmp.Attributes["rm_water_mark"].Value = rmWaterMark.ToString();
        userHave.Save(userHaveDataPath);
    }

    public bool LoadUserHave_RmWaterMark()
    {
        XmlNode nodeTmp = userHaveRoot.SelectSingleNode("WATER_MARK");
        bool rmWaterMark = bool.Parse(nodeTmp.Attributes["rm_water_mark"].Value);
        return rmWaterMark;
    }

    //광고 삭제
    public void SaveUserHave_RmAD(bool rmAD)
    {
        XmlNode nodeTmp = userHaveRoot.SelectSingleNode("AD");
        nodeTmp.Attributes["rm_ad"].Value = rmAD.ToString();
        userHave.Save(userHaveDataPath);
    }

    public bool LoadUserHave_RmAD()
    {
        XmlNode nodeTmp = userHaveRoot.SelectSingleNode("AD");
        bool rmAd = bool.Parse(nodeTmp.Attributes["rm_ad"].Value);
        return rmAd;
    }

    #endregion

    #region OrderButton.xml
    public bool IsCostOrderButton(string seqName, string itemSeq)
    {
        XmlNodeList listTmp = SelectOrderButtonBySeq_name(seqName);
        XmlNode nodeTmp = null;
        for(int i = 0; i < listTmp.Count; i++)
        {
            if(listTmp[i].Attributes[seqName].Value == itemSeq)
            {
                nodeTmp = listTmp[i];
            }
        }
        if(nodeTmp == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
#endregion

#region MissionData.xml
    public Dictionary<int, Mission> LoadAllMission()
    {
        XmlNodeList tmpMissionNodeList = SelectAllMission();
        XmlNodeList tmpUserNodeList = SelectAllUserMissions();

        Dictionary<int, Mission> tmpDic = new Dictionary<int, Mission>();
        Mission tmpMission;
        int seq;
        bool isOn;
        bool isClear;
        int nowLevel;
        int lastLevel;
        string rewardType;
        string reward;
        for (int i = 0; i < tmpMissionNodeList.Count; i++)
        {
            seq = int.Parse(tmpMissionNodeList[i].Attributes[ mission_seqSt].Value);

            //안맞으면 seq로 찾아야 할 수도 있음
            //user.xml에서 찾아야 하는 정보(가변)
            isOn = bool.Parse(tmpUserNodeList[i].Attributes["is_on"].Value);
            isClear = bool.Parse(tmpUserNodeList[i].Attributes["is_clear"].Value);
            nowLevel = int.Parse(tmpUserNodeList[i].Attributes["now_level"].Value);

            //mission.xml에서 찾아야 하는 정보(불변)
            lastLevel = int.Parse(tmpMissionNodeList[i].Attributes["last_level"].Value);
            rewardType = tmpMissionNodeList[i].Attributes["reward_type"].Value;
            reward = tmpMissionNodeList[i].Attributes["reward"].Value;
            tmpMission = new Mission(seq, isOn, isClear, nowLevel, lastLevel, rewardType, reward);
            tmpDic.Add(seq, tmpMission);
        }
        return tmpDic;
    }

    public Mission LoadOneMission(string missionSeq)
    {
        XmlNode tmpMissionNode = SelectOneMission(missionSeq);
        XmlNode tmpUserNode = SelectOneUserMissions(missionSeq);

        Mission tmpMission;
        int seq;
        bool isOn;
        bool isClear;
        int nowLevel;
        int lastLevel;
        string rewardType;
        string reward;

        seq = int.Parse(tmpMissionNode.Attributes[mission_seqSt].Value);

        //안맞으면 seq로 찾아야 할 수도 있음
        //user.xml에서 찾아야 하는 정보(가변)
        isOn = bool.Parse(tmpUserNode.Attributes["is_on"].Value);
        isClear = bool.Parse(tmpUserNode.Attributes["is_clear"].Value);
        nowLevel = int.Parse(tmpUserNode.Attributes["now_level"].Value);

        //mission.xml에서 찾아야 하는 정보(불변)
        lastLevel = int.Parse(tmpMissionNode.Attributes["last_level"].Value);
        rewardType = tmpMissionNode.Attributes["reward_type"].Value;
        reward = tmpMissionNode.Attributes["reward"].Value;
        tmpMission = new Mission(seq, isOn, isClear, nowLevel, lastLevel, rewardType, reward);

        return tmpMission;
    }
    public int LoadLastLevel(string missionSeq)
    {
        XmlNode tmpNode = SelectOneMission(missionSeq.ToString());
        int lastLevel = int.Parse(tmpNode.Attributes["last_level"].Value);
        return lastLevel;
    }

#endregion

#region collectionData.xml
    public Dictionary<int, CollectionSet> LoadAllCollections()
    {
        XmlNodeList tmpCollectionNodeList = SelectAllCollection();
        XmlNodeList tmpUserNodeList = SelectAllUserCollections();

        Dictionary<int, CollectionSet> tmpDic = new Dictionary<int, CollectionSet>();
        CollectionSet tmpCollection;
        int seq;
        bool isOn;
        bool isClear;
        int level;
        UserHave finalReward;
        
        //현재 3개의 컬렉션이 있음
        for (int i = 0; i < tmpCollectionNodeList.Count; i++)
        {
            seq = int.Parse(tmpCollectionNodeList[i].Attributes[collection_seqSt].Value);

            //가변
            isOn = bool.Parse(tmpUserNodeList[i].Attributes["is_on"].Value);
            isClear = bool.Parse(tmpUserNodeList[i].Attributes["is_clear"].Value);
            level = int.Parse(tmpUserNodeList[i].Attributes["level"].Value);

            tmpCollection = new CollectionSet(seq, isOn, isClear, level);

            //불변
            XmlNodeList listColPartNode = tmpCollectionNodeList[i].SelectNodes("CollectionItem");
            XmlNode nodeReward = tmpCollectionNodeList[i].SelectSingleNode("RewardItem");

            List<UserHave> itemColList = new List<UserHave>();
            Dictionary<UserHave, List<UserHave>> itemReqDic = new Dictionary<UserHave, List<UserHave>>();

            string seqNameC;
            string itemSeq;
            //불변
            for (int j = 0; j < listColPartNode.Count; j++)
            {
                seqNameC = listColPartNode[j].Attributes[ seqNameSt].Value;
                itemSeq = listColPartNode[j].Attributes[seqNameC].Value;

                UserHave userHaveC = new UserHave(seqNameC, itemSeq);
                itemColList.Add(userHaveC);

                XmlNodeList listReqPartNode = listColPartNode[j].SelectNodes("RequireItem");
                List<UserHave> itemReqList = new List<UserHave>();

                string seqNameR;
                string reqItem;
                for(int k = 0; k < listReqPartNode.Count; k++)
                {
                    seqNameR = listReqPartNode[k].Attributes[ seqNameSt].Value;
                    reqItem = listReqPartNode[k].Attributes[seqNameR].Value;
                    UserHave userHaveR = new UserHave(seqNameR, reqItem);
                    itemReqList.Add(userHaveR);
                }
                itemReqDic.Add(userHaveC, itemReqList);
            }

            string seqNameF;
            string rewardSeq;
            seqNameF = nodeReward.Attributes[seqNameSt].Value;
            rewardSeq = nodeReward.Attributes[seqNameF].Value;
            finalReward = new UserHave(seqNameF, rewardSeq);

            tmpCollection.SetColItemSeq(itemColList);
            tmpCollection.SetReqItemSeq(itemReqDic);
            tmpCollection.SetRewardSeq(finalReward);

            tmpDic.Add(seq, tmpCollection);
        }
        return tmpDic;
    }
    #endregion

    #region ItemSeed.xml
    public List<UserHave> LoadAllDailyItemSeed()
    {
        List<UserHave> listTmp = new List<UserHave>();
        XmlNode nodeDaily = itemSeedRoot.SelectSingleNode("DAILY_SEED");

        XmlNodeList listDaily = nodeDaily.ChildNodes;
        for (int i = 0; i < listDaily.Count; i++)
        {
            UserHave tmpHave = new UserHave(listDaily[i].Attributes[ seqNameSt].Value, listDaily[i].Attributes[ item_seqSt].Value);

            listTmp.Add(tmpHave);
        }

        return listTmp;
    }

    //데일리시드에 아이템 있는지 체크
    public bool IsDailyItemSeed(string seqName, string itemSeq)
    {
        XmlNode nodeDaily = itemSeedRoot.SelectSingleNode("DAILY_SEED");

        XmlNodeList listDaily = nodeDaily.ChildNodes;
        for (int i = 0; i < listDaily.Count; i++)
        {
            if(CheckNodeIsItem(listDaily[i], seqName, itemSeq))
            {
                return true;
            }
        }
        return false;
    }

    public List<UserHave> LoadAllWheelItemSeed()
    {
        List<UserHave> listTmp = new List<UserHave>();
        XmlNode nodeWheel = itemSeedRoot.SelectSingleNode("FORTUNE_WHEEL_SEED");

        XmlNodeList listWheel = nodeWheel.ChildNodes;
        for (int i = 0; i < listWheel.Count; i++)
        {
            UserHave tmpHave = new UserHave(listWheel[i].Attributes[ seqNameSt].Value, listWheel[i].Attributes[ item_seqSt].Value);

            listTmp.Add(tmpHave);
        }
        return listTmp;
    }

    //룰렛시드에 아이템 있는지 체크
    public bool IsWheelItemSeed(string seqName, string itemSeq)
    {
        XmlNode nodeWheel = itemSeedRoot.SelectSingleNode("FORTUNE_WHEEL_SEED");

        XmlNodeList listWheel = nodeWheel.ChildNodes;
        for (int i = 0; i < listWheel.Count; i++)
        {
            if (CheckNodeIsItem(listWheel[i], seqName, itemSeq))
            {
                return true;
            }
        }
        return false;
    }

    #endregion

    #region StoreItem.xml
    public List<UserHave> LoadAllBundleItem(string bundleSeq)
    {
        List<UserHave> tmpList = new List<UserHave>();
        XmlNodeList listBUNDLE_HAVE_ITEM = SelectAllBundleItem(bundleSeq);
        if (listBUNDLE_HAVE_ITEM != null)
        {
            for (int i = 0; i < listBUNDLE_HAVE_ITEM.Count; i++)
            {
                string seqName = listBUNDLE_HAVE_ITEM[i].Attributes[seqNameSt].Value;
                string itemSeq = listBUNDLE_HAVE_ITEM[i].Attributes[seqName].Value;
                UserHave userHave = new UserHave(seqName, itemSeq);
                tmpList.Add(userHave);
            }
        }
        return tmpList;
    }

    //번들의 워터마크 로드
    public bool LoadBundleRmWaterMark(string bundleSeq)
    {
        XmlNode nodeTmp = SelectOneStoreBundle(bundleSeq).SelectSingleNode("WATER_MARK");
        if(nodeTmp == null)
        {
            return false;
        }
        return bool.Parse(nodeTmp.Attributes["rm_water_mark"].Value);
    }
    //번들의 광고 삭제 로드
    public bool LoadBundleRmAd(string bundleSeq)
    {
        XmlNode nodeTmp = SelectOneStoreBundle(bundleSeq).SelectSingleNode("AD");
        if (nodeTmp == null)
        {
            return false;
        }
        return bool.Parse(nodeTmp.Attributes["rm_ad"].Value);
    }

    public int LoadBundleCoin(string bundleSeq)
    {
        int coin = int.Parse(SelectOneStoreBundle(bundleSeq).Attributes["coin"].Value);
        return coin;
    }
    public int LoadBundleCash(string bundleSeq)
    {
        int cash = int.Parse(SelectOneStoreBundle(bundleSeq).Attributes["cash"].Value);
        return cash;
    }
    public string LoadBundleProductId(string bundleSeq)
    {
        string productId = SelectOneStoreBundle(bundleSeq).Attributes["product_id"].Value;
        return productId;
    }
    #endregion

    //파트카인드로 파트아이템xml에서 아이템 가져오기
    public XmlNodeList SelectAllPartItemByPart_KindList(int partKind)
    {
        stringBuilder.Clear();
        stringBuilder.Append("row[@part_kind = '").Append(partKind).Append("']");

        XmlNodeList tmpNodeList = partItemRoot.SelectNodes(stringBuilder.ToString());
        return tmpNodeList;
    }

    //파트오더로 파트아이템xml에서 아이템 가져오기
    public XmlNodeList SelectAllPartItemByPart_OrderList(int partOrder)
    {
        stringBuilder.Clear();
        stringBuilder.Append("row[@part_order = '").Append(partOrder).Append("']");

        XmlNodeList tmpNodeList = partItemRoot.SelectNodes(stringBuilder.ToString());
        return tmpNodeList;
    }
    

    public void LoadOneSLOTCharWearXML(string slotSeq, string wearSeq)
    {
        listTmpCharPart.Clear();

        XmlNode nodeSLOT = SelectSlot(slotSeq);
        XmlNode nodeGROUP0 = SelectNode(nodeSLOT, "ITEM_GROUP", "group_seq", "0");
        XmlNodeList listWEAR = nodeGROUP0.SelectNodes( xmlWEAR);

        for (int j = 0; j < listWEAR.Count; j++)
        {
            Dictionary<string, CharPartItem> tmpItemDic = new Dictionary<string, CharPartItem>();
            Dictionary<string, Color> tmpColorDic = new Dictionary<string, Color>();

            if (listWEAR[j].Attributes[ wear_seqSt].Value == wearSeq)
            {
                XmlNodeList listPART = listWEAR[j].ChildNodes;
                XmlNode nodePART;
                for (int i = 0; i < listPART.Count; i++)
                {
                    nodePART = listPART[i];
                    //XmlNode nodeItem = SelectOneCharPartItem(nodePART.Attributes[ item_seqSt].Value);
                    CharPartItem tmpPartItem = allPartItemDic[nodePART.Attributes[ item_seqSt].Value];
                    CharPart tmpPart = UsePart(tmpPartItem);
                    tmpPart.color = ColorCodeUtil.htmlStringToColor(nodePART.Attributes[ part_color_rgbaSt].Value);
                    tmpPart.pantType = int.Parse(nodePART.Attributes["pant_type"].Value);
                    listTmpCharPart.Add(tmpPart);
                }
            }
        }
    }

    public void LoadAllSlotCharTrXML()
    {
        for (int i = 0; i < listSLOT.Count; i++)
        {
            XmlNodeList listITEM_GROUP = SelectSlot(i.ToString()).ChildNodes;
            CharTr tmpCharTr;

            for (int j = 0; j < listITEM_GROUP.Count; j++)
            {
                XmlNode nodeTR_CHAR = listITEM_GROUP[j].SelectSingleNode("TR_CHAR");

                List<CharTr> tmpListCharTr = new List<CharTr>();
                XmlNodeList listTR_SEQ = nodeTR_CHAR.ChildNodes;
                XmlNode element;
                for (int k = 0; k < listTR_SEQ.Count; k++)
                {
                    element = listTR_SEQ[k];
                    tmpCharTr = new CharTr(element.Attributes[ trSeqSt].Value, element.Attributes[ usedSeqSt].Value, new Vector3(float.Parse(element.Attributes[ positionXSt].Value), float.Parse(element.Attributes[ positionYSt].Value)), float.Parse(element.Attributes[ scaleSt].Value), int.Parse(element.Attributes[ trOrderSt].Value), bool.Parse(element.Attributes[ isReversedSt].Value));
                    tmpListCharTr.Add(tmpCharTr);
                }
            }
        }
     }

#region 메인씬
    //메인씬 로드 wear
    public void LoadAllMAIN_WEAR()
    {
        listAllMainWEAR.Clear();

        CharPart tmpPart;
        CharPartItem tmpPartItem;
        Color tmpColor;
        int pantType = 0;

        XmlNodeList listMAIN_WEAR = SelectGroup(nodeMAIN, 0).SelectNodes( xmlWEAR);
        XmlNode element;

        //Debug.Log("메인 그룹0 wear리스트" + listMAIN_WEAR.Count);
        for(int i=0;i<listMAIN_WEAR.Count;i++) {
            element = listMAIN_WEAR[i];

            if (element.Attributes[ wear_seqSt].Value.Equals("-1"))
            {
                continue;
            }
            XmlNodeList listPart = element.ChildNodes;
            XmlNode part;
            List<CharPart> oneWear = new List<CharPart>();
            for(int j=0;j<listPart.Count;j++)
            {
                part = listPart[j];
                string itemSeq = part.Attributes[ item_seqSt].Value;
                //PartItemData.xml에서 해당 아이템 찾기
                tmpPartItem = allPartItemDic[itemSeq];

                //PartItemPosition.xml에서 해당 좌표 찾기
                string handSeq;
                int handType = 0;
                if (allPartPosDic[itemSeq].hand != null && allPartPosDic[itemSeq].hand != "")
                {
                    handSeq = allPartPosDic[itemSeq].hand;
                    if (handSeq[handSeq.Length - 1] == 'U')
                    {
                        handType = 1;
                        handSeq = handSeq.TrimEnd('U');
                    }
                    else
                    {
                        handType = 0;
                    }
                }
                else
                {
                    ////Debug.Log("핸드없음");
                    handSeq = "-1";
                }

                int type = 0;
                if (allPartPosDic[itemSeq].itemType != null && allPartPosDic[itemSeq].itemType != "")
                {
                    type = int.Parse(allPartPosDic[itemSeq].itemType);
                }
                float tmpX = allPartPosDic[itemSeq].x;
                float tmpY = allPartPosDic[itemSeq].y;


                tmpColor = ColorCodeUtil.htmlStringToColor(part.Attributes[ part_color_rgbaSt].Value);
                pantType = int.Parse(part.Attributes["pant_type"].Value);

                tmpPart = new CharPart(tmpPartItem, tmpColor, handSeq, handType, pantType, type, tmpX, tmpY);
                oneWear.Add(tmpPart);
            }
            listAllMainWEAR.Add(int.Parse(element.Attributes[ wear_seqSt].Value), oneWear);
        }
    }
    //텍스트 이름 로드
    public string LoadOneText(string textSeq)
    {
        XmlNode tmpNode = SelectOneTextItem(textSeq);
        return tmpNode.Attributes[ item_seqSt].Value;
    }
    //텍스트 이름 로드
    public string LoadOneBubble(string bubbleSeq)
    {
        XmlNode tmpNode = SelectOneBubbleItem(bubbleSeq);
        return tmpNode.Attributes[ item_seqSt].Value;
    }

    //메인씬 특정 그룹 tr 로드 
    public void LoadAllMAINGROUPTr(int groupSeq)
    {
        listMAIN_ObjTr.Clear();

        XmlNode nodeMAINGROUP = SelectGroup(nodeMAIN, groupSeq);
        XmlNode nodeMAINTR_CHAR = nodeMAINGROUP.SelectSingleNode("TR_CHAR");

        XmlNode nodeMAINUsedWEAR;
        XmlNode nodeMAINUsedBG;

        CharTr tmpTr;

        XmlNodeList listMAINTRs = nodeMAINTR_CHAR.ChildNodes;
        XmlNode element;

        for(int i=0;i< listMAINTRs.Count;i++)
        {
            element = listMAINTRs[i];
            string used_seq = element.Attributes[ usedSeqSt].Value;
            //Debug.Log(used_seq);

            //같은 그룹에서 usedSeq로 해당 아이템 찾기
            switch (groupSeq)
            {
                case 0:
                    nodeMAINUsedWEAR = SelectNode(nodeMAINGROUP,  xmlWEAR,  wear_seqSt, used_seq);
                    break;
                case 1:
                    nodeMAINUsedBG = SelectNode(nodeMAINGROUP, "BG_ITEM",  bgSeqSt, used_seq);
                    break;
            }
            tmpTr = new CharTr(element.Attributes[ trSeqSt].Value, used_seq, new Vector3(float.Parse(element.Attributes[ positionXSt].Value), float.Parse(element.Attributes[ positionYSt].Value), 0), float.Parse(element.Attributes[ scaleSt].Value), int.Parse(element.Attributes[ trOrderSt].Value), bool.Parse(element.Attributes[ isReversedSt].Value));
            listMAIN_ObjTr.Add(tmpTr);
        }
    }
    //특정 슬로 특정 그룹 tr 로드 
    public void LoadAllSLOTGROUPTr(string slotSeq, int groupSeq)
    {
        listMAIN_ObjTr.Clear();

        XmlNode nodeSLOT = SelectSlot(slotSeq);
        XmlNode nodeGroup = SelectGroup(nodeSLOT, groupSeq);
        XmlNode nodeMAINTR_CHAR = nodeGroup.SelectSingleNode("TR_CHAR");

        XmlNode nodeMAINUsedWEAR;
        XmlNode nodeMAINUsedBG;

        CharTr tmpTr;

        XmlNodeList listMAINTRs = nodeMAINTR_CHAR.ChildNodes;
        XmlNode element;
        for(int i=0;i< listMAINTRs.Count;i++)
        {
            element = listMAINTRs[i];
            string used_seq = element.Attributes[ usedSeqSt].Value;
            //Debug.Log(used_seq);

            //같은 그룹에서 usedSeq로 해당 아이템 찾기
            switch (groupSeq)
            {
                case 0:
                    nodeMAINUsedWEAR = SelectNode(nodeGroup,  xmlWEAR,  wear_seqSt, used_seq);
                    break;
                case 1:
                    nodeMAINUsedBG = SelectNode(nodeGroup, "BG_ITEM",  bgSeqSt, used_seq);
                    break;
            }
            tmpTr = new CharTr(element.Attributes[ trSeqSt].Value, used_seq, new Vector3(float.Parse(element.Attributes[ positionXSt].Value), float.Parse(element.Attributes[ positionYSt].Value), 0), float.Parse(element.Attributes[ scaleSt].Value), int.Parse(element.Attributes[ trOrderSt].Value), bool.Parse(element.Attributes[ isReversedSt].Value));
            listMAIN_ObjTr.Add(tmpTr);
        }
    }
    //텍스트박스@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    //메인에 텍스트박스 만들기
    public void CreateOneBubbleMAIN(string bubbleSeq, string trSeq, string text)
    {
        XmlNode nodeMAINGROUP_four;
        nodeMAINGROUP_four = SelectGroup(nodeMAIN, 5);
        CreateOneBubbleXML(nodeMAINGROUP_four, bubbleSeq, trSeq, text);
    }

    //텍스트박스 만들기
    public void CreateOneBubbleXML(XmlNode parentGROUP, string bubbleSeq, string trSeq, string text)
    {
        //wear이 없다면 wear 생성
        XmlNode nodeBubble = SelectNode(parentGROUP, "BUBBLE_ITEM",  bubbleSeqSt, bubbleSeq.ToString());
        if (nodeBubble != null)
        {
            //Debug.Log("nodeWEAR 존재");
        }
        else
        {
            XmlElement newBUBBLE = data.CreateElement("BUBBLE_ITEM");
            newBUBBLE.SetAttribute( bubbleSeqSt, bubbleSeq);
            parentGROUP.AppendChild(newBUBBLE);
            nodeBubble = newBUBBLE;
        }

        XmlElement newTEXT = data.CreateElement("TEXT");
        newTEXT.SetAttribute( trSeqSt, trSeq.ToString());
        newTEXT.SetAttribute("text", text);
        newTEXT.SetAttribute("font_color", "0");
        newTEXT.SetAttribute("font_size", "0");
        nodeBubble.AppendChild(newTEXT);
        data.Save(gameDataPath);
    }

    //텍스트 생성할때 사용, 텍스트박스 하나 불러오기
    public void LoadOneMAIN_BUBBLE(string bubbleSeq)
    {
        dicTmpBubble.Clear();
        XmlNode nodeMAINGROUP_FOUR;
        nodeMAINGROUP_FOUR = SelectGroup(nodeMAIN, 5);
        XmlNode nodeMAINBUBBLE = SelectNode(nodeMAINGROUP_FOUR, "BUBBLE_ITEM",  bubbleSeqSt, bubbleSeq);

        XmlNodeList listMAINBUBBLE = nodeMAINBUBBLE.ChildNodes;
        XmlNode element;
        for (int i = 0; i < listMAINBUBBLE.Count; i++)
        {
            element = listMAINBUBBLE[i];

            string trSeq = element.Attributes[ trSeqSt].Value;
            string text = element.Attributes["text"].Value;
            int color = int.Parse(element.Attributes["font_color"].Value);
            int size = int.Parse(element.Attributes["font_size"].Value);
            BubbleText tmpBT = new BubbleText(bubbleSeq, trSeq, text, color, size);
            dicTmpBubble.Add(trSeq, tmpBT);
        }
    }

    //텍스트 입력
    public void SaveOneMAIN_BUBBLE(BubbleText bubbleText)
    {
        XmlNode nodeMAINGROUP_four = SelectGroup(nodeMAIN, 5);
        XmlNode nodeBubble = SelectNode(nodeMAINGROUP_four, "BUBBLE_ITEM",  bubbleSeqSt, bubbleText.bubbleSeq.ToString());
        XmlNode nodeText = SelectNode(nodeBubble, "TEXT",  trSeqSt, bubbleText.trSeq);
        nodeText.Attributes["text"].Value = bubbleText.text;
        nodeText.Attributes["font_color"].Value = bubbleText.fontColor.ToString();
        nodeText.Attributes["font_size"].Value = bubbleText.fontSize.ToString();

        data.Save(gameDataPath);


    }

    //메인의 텍스트 지우기
    public void RemoveOneMAIN_GROUP_Bubble(int groupSeq, string wearSeq, string trSeq)
    {
        XmlNode nodeGROUP = SelectGroup(nodeMAIN, groupSeq);
        XmlNode nodeBUBBLE = SelectBubble(nodeGROUP, wearSeq);
        XmlNode nodeTEXT = SelectNode(nodeBUBBLE, "TEXT",  trSeqSt, trSeq);
        nodeBUBBLE.RemoveChild(nodeTEXT);
        if(nodeBUBBLE.ChildNodes.Count == 0)
        {
            nodeGROUP.RemoveChild(nodeBUBBLE);
        }
        data.Save(gameDataPath);
    }

    //메인의 텍스트 전부 지우기
    public void RemoveAllMAIN_BUBBLE()
    {
        XmlNode nodeGROUP = SelectGroup(nodeMAIN, 5);
        XmlNodeList listBUBBLE = nodeGROUP.SelectNodes("BUBBLE_ITEM");
        for (int i = 0; i < listBUBBLE.Count; i++)
        {
            nodeGROUP.RemoveChild(listBUBBLE[i]);
        }
    }

    //텍스트//////////////////
    //public XmlNodeList LoadAllMAIN_TEXT(int groupSeq) {
    //    XmlNode nodeMAINGROUP = SelectGroup(nodeMAIN, groupSeq);
    //    XmlNodeList listTEXT_ITEM = nodeMAINGROUP.SelectNodes("TEXT_ITEM");
    //    return listTEXT_ITEM;
    //}
    //public XmlNode LoadOneMAIN_TEXT(int groupSeq, string usedSeq)
    //{
    //    XmlNode nodeMAINGROUP = SelectGroup(nodeMAIN, groupSeq);
    //    XmlNode nodeTEXT_ITEM = SelectNode(nodeMAINGROUP, "TEXT_ITEM", "obj_seq", usedSeq);
    //    return nodeTEXT_ITEM;
    //}
    //public TextObj LoadOneMAIN_TEXTObj(int groupSeq, string usedSeq)
    //{
    //    XmlNode tmpNode = LoadOneMAIN_TEXT(groupSeq, usedSeq);
    //    string text = tmpNode.Attributes["text"].Value;
    //    int fontStyle = int.Parse(tmpNode.Attributes["font_style"].Value);
    //    int fontSize = int.Parse(tmpNode.Attributes["font_size"].Value);
    //    int fontColor = int.Parse(tmpNode.Attributes["font_color"].Value);
    //    return new TextObj(int.Parse(usedSeq), text, fontStyle, fontSize, fontColor);
    //}
    //public int LoadMostObj_seqMAIN_TEXT(int groupSeq) {
    //    int max = 0;

    //    //Debug.Log(LoadAllMAIN_TEXT(groupSeq).Count);
    //    foreach (XmlNode element in LoadAllMAIN_TEXT(groupSeq))
    //    {
    //        if (max < int.Parse(element.Attributes["obj_seq"].Value))
    //            max = int.Parse(element.Attributes["obj_seq"].Value);
    //    }
    //    return max;
    //}

    //캐릭터 생성할때 사용, WEAR하나 불러오기
    public void LoadOneMAIN_WEAR(string wearSeq)
    {
        listTmpCharPart.Clear();
        XmlNode nodeMAINGROUP_ZERO;
        nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        XmlNode nodeMAINWEAR = SelectNode(nodeMAINGROUP_ZERO,  xmlWEAR,  wear_seqSt, wearSeq);

        CharPart tmpPart;
        CharPartItem tmpPartItem;
        Color tmpColor;
        int pantType;


        XmlNodeList listMAINWEAR_PARTS = nodeMAINWEAR.ChildNodes;
        XmlNode element;
        for(int i = 0; i < listMAINWEAR_PARTS.Count; i++)
        {
            element = listMAINWEAR_PARTS[i];

            string itemSeq = element.Attributes[ item_seqSt].Value;

            //PartItemData.xml에서 해당 아이템 찾기
            tmpPartItem = allPartItemDic[itemSeq];

            //PartItemPosition.xml에서 해당 좌표 찾기
            string handSeq;
            int handType = 0;
            if (allPartPosDic[itemSeq].hand != null && allPartPosDic[itemSeq].hand != "")
            {
                handSeq = allPartPosDic[itemSeq].hand;
                if (handSeq[handSeq.Length - 1] == 'U')
                {
                    handType = 1;
                    handSeq = handSeq.TrimEnd('U');
                }
                else
                {
                    handType = 0;
                }
            }
            else
            {
                ////Debug.Log("핸드없음");
                handSeq = "-1";
            }

            int type = 0;
            if (allPartPosDic[itemSeq].itemType != null && allPartPosDic[itemSeq].itemType != "")
            {
                type = int.Parse(allPartPosDic[itemSeq].itemType);
            }
            float tmpX = allPartPosDic[itemSeq].x;
            float tmpY = allPartPosDic[itemSeq].y;

            tmpColor = ColorCodeUtil.htmlStringToColor(element.Attributes[ part_color_rgbaSt].Value);
            pantType = int.Parse(element.Attributes["pant_type"].Value);

            tmpPart = new CharPart(tmpPartItem, tmpColor, handSeq, handType, pantType, type, tmpX, tmpY);
            listTmpCharPart.Add(tmpPart);
        }
    }

    //캐릭터 하나 있는지 없는지
    public bool IsOneMAINWEARByWearSeq(string wearSeq)
    {
        XmlNode nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);

        if (SelectWear(nodeMAINGROUP_ZERO, wearSeq) != null)
        {
            return true;
        }
        return false;
    }

    //캐릭터 하나의 이름 저장하기
    public void SaveMAIN_WEARname(string name, string wearSeq)
    {
        XmlNode nodeMAINGROUP_ZERO;
        nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        XmlNode tmpWear = SelectWear(nodeMAINGROUP_ZERO, wearSeq);
        if (tmpWear.Attributes["wear_name"].Value != null)
        {
            tmpWear.Attributes["wear_name"].Value = name;
        }
        else
        {
            //Debug.Log("wear_name attribute가 없다");
        }
        data.Save(gameDataPath);
    }
    //캐릭터 하나의 이름 불러오기
    public string LoadMAIN_WEARname(string wearSeq)
    {
        XmlNode nodeMAINGROUP_ZERO;
        nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        XmlNode tmpWear = SelectWear(nodeMAINGROUP_ZERO, wearSeq);
        if (tmpWear.Attributes["wear_name"].Value != null)
        {
            return tmpWear.Attributes["wear_name"].Value;
        }
        else
        {
            //Debug.Log("wear_name attribute가 없다");
            return null;
        }
    }

    //메인의 캐릭터(WEAR)지우기
    public void RemoveOneMAIN_GROUP_WEAR(int groupSeq, string wearSeq)
    {
        XmlNode nodeGROUP = SelectGroup(nodeMAIN, groupSeq);
        XmlNode nodeWEAR = SelectWear(nodeGROUP, wearSeq);
        nodeGROUP.RemoveChild(nodeWEAR);
        data.Save(gameDataPath);

    }
 
    public XmlNode SelectOneMAINGTOUPTrByTrSeq(int groupSeq, int trSeq)
    {
        XmlNode nodeMAINGROUP = SelectGroup(nodeMAIN, groupSeq);
        XmlNode nodeMAINTR_CHAR = nodeMAINGROUP.SelectSingleNode("TR_CHAR");
        XmlNode nodeMAINTR = SelectNode(nodeMAINTR_CHAR,  xmlTR,  trSeqSt, trSeq.ToString());
        return nodeMAINTR;
    }

    public bool IsOneMAINGROUPTrByTrSeq(int groupSeq, int trSeq)
    {
        if(SelectOneMAINGTOUPTrByTrSeq(groupSeq, trSeq) != null)
        {
            return true;
        }
        return false;
    }
    //tr이동@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    public void SetOneMAINGROUPTrByTrSeq(int groupSeq, int trSeq, string att, string value)
    {
        SelectOneMAINGTOUPTrByTrSeq(groupSeq, trSeq).Attributes[att].Value = value;
        data.Save(gameDataPath);
    }
    public XmlNode SelectOneSLOTGTOUPTrByTrSeq(string slotSeq, int groupSeq, int trSeq)
    {
        XmlNode nodeSLOT = SelectSlot(slotSeq);
        XmlNode nodeMAINGROUP = SelectGroup(nodeSLOT, groupSeq);
        XmlNode nodeMAINTR_CHAR = nodeMAINGROUP.SelectSingleNode("TR_CHAR");
        XmlNode nodeMAINTR = SelectNode(nodeMAINTR_CHAR,  xmlTR,  trSeqSt, trSeq.ToString());
        return nodeMAINTR;
    }

    public bool IsOneSLOTGROUPTrByTrSeq(string slotSeq, int groupSeq, int trSeq)
    {
        if (SelectOneSLOTGTOUPTrByTrSeq(slotSeq, groupSeq, trSeq) != null)
        {
            return true;
        }
        return false;
    }

    public void SetOneSLOTGROUPTrByTrSeq(string slotSeq, int groupSeq, int trSeq, string att, string value)
    {
        SelectOneSLOTGTOUPTrByTrSeq(slotSeq, groupSeq, trSeq).Attributes[att].Value = value;
        data.Save(gameDataPath);
    }
    //위치정보 생성
    public void CreateOneMAINGROUPTr(int groupSeq, string trSeq, string useqSeq, CharTr charTr)
    {
        //Debug.Log("groupSeq" + groupSeq);
        XmlNode nodeMAINGROUP = SelectGroup(nodeMAIN, groupSeq);
        XmlNode nodeMAINTR_CHAR = nodeMAINGROUP.SelectSingleNode("TR_CHAR");

        XmlElement newTR = data.CreateElement( xmlTR);
        newTR.SetAttribute( trSeqSt, trSeq);
        newTR.SetAttribute( usedSeqSt, useqSeq);
        newTR.SetAttribute( positionXSt, charTr.Position.x.ToString());
        newTR.SetAttribute( positionYSt, charTr.Position.y.ToString());
        newTR.SetAttribute( scaleSt, charTr.Scale.ToString());
        newTR.SetAttribute( trOrderSt, charTr.Order.ToString());
        newTR.SetAttribute( isReversedSt, charTr.IsReversed.ToString());

        nodeMAINTR_CHAR.AppendChild(newTR);
        data.Save(gameDataPath);
    }

    //trSeq로 tr하나 삭제
    public void RemoveOneMAINGROUPTrByTrSeq(int groupSeq, int trSeq)
    {
        XmlNode nodeMAINGROUP = SelectGroup(nodeMAIN, groupSeq);
        XmlNode nodeMAINTR_CHAR = nodeMAINGROUP.SelectSingleNode("TR_CHAR");
        XmlNode nodeTR = SelectNode(nodeMAINTR_CHAR,  xmlTR,  trSeqSt, trSeq.ToString());
        
        if (nodeTR != null)
        {
            if (groupSeq == 5)
            {
                string usedSeq;
                usedSeq = nodeTR.Attributes[ usedSeqSt].Value;
                RemoveOneMAIN_GROUP_Bubble(groupSeq, usedSeq, trSeq.ToString());
            }
            nodeMAINTR_CHAR.RemoveChild(nodeTR);
        }
        else
        {
            //Debug.Log("nodeTR is null");
        }
        data.Save(gameDataPath);
    }
    //같은 UsedSeq를 사용하는 메인의 모든 Tr선택 
    public void SelectSomeMAINGROUPTrByUsedSeq(int groupSeq, string usedSeq)
    {
        listTmpTrSeq.Clear();
        XmlNode nodeMAINGROUP = SelectGroup(nodeMAIN, groupSeq);
        XmlNode nodeMAINTR_CHAR = nodeMAINGROUP.SelectSingleNode("TR_CHAR");
        XmlNodeList listMAIN_TR = nodeMAINTR_CHAR.SelectNodes( xmlTR);
        for(int i = 0; i < listMAIN_TR.Count; i++)
        {
            if (listMAIN_TR[i].Attributes[ usedSeqSt].Value== usedSeq)
            {
                //Debug.Log("찾음" + usedSeq);
                //Debug.Log("찾은거 " + listMAIN_TR[i].Attributes[ trSeqSt].Value);

                listTmpTrSeq.Add(int.Parse(listMAIN_TR[i].Attributes[ trSeqSt].Value));
            }
        }
    }

    public void RemoveAllMAINGROUPTr(int groupSeq)
    {
        XmlNode nodeMAINGROUP = SelectGroup(nodeMAIN, groupSeq);
        XmlNode nodeMAINTR_CHAR = nodeMAINGROUP.SelectSingleNode("TR_CHAR");
        nodeMAINTR_CHAR.RemoveAll();
        if(groupSeq == 4)
        {
            RemoveAllMAIN_BUBBLE();
        }
        data.Save(gameDataPath);
    }
#endregion

#region 메이킹씬
    //메이킹씬 세팅 그룹_0 -> WEAR_-1 -> 700_Body_0 리턴
    public void SetMakedWEAR()
    {
        listMakingPARTInit.Clear();
        XmlNode nodeMAINGROUP_ZERO;
        nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        XmlNode nodeMAINWEARNegative = SelectNode(nodeMAINGROUP_ZERO,  xmlWEAR,  wear_seqSt, "-1");

        nodeMAINGROUP_ZERO.RemoveChild(nodeMAINWEARNegative);


        string handSeq = "-1";
        int handType = 0;
        int pantType = 0;
        int type = 0;
        Color tmpColor = Color.white;

        //string leftSeq = "013000";
        //string rightSeq = "014000";
        //string noseSeq = "015001";
        //string mouseSeq = "016099";
        //string fHairSeq = "021000";
        //string sHairSeq = "018000";
        //string bHairSeq = "004000";

        //시작시 세팅되는 캐릭터
        CreateOnePartXML(nodeMAINGROUP_ZERO, -1,  bodyInitSeq, 0, tmpColor);//몸
        CreateOnePartXML(nodeMAINGROUP_ZERO, -1,  underInitSeq, 0, tmpColor);//속옷
        //CreateOnePartXML(nodeMAINGROUP_ZERO, -1, leftSeq, 0, tmpColor);//왼눈
        //CreateOnePartXML(nodeMAINGROUP_ZERO, -1, rightSeq, 0, tmpColor);//오른눈
        //CreateOnePartXML(nodeMAINGROUP_ZERO, -1, noseSeq, 0, tmpColor);//코
        //CreateOnePartXML(nodeMAINGROUP_ZERO, -1, mouseSeq, 0, tmpColor);//입
        //CreateOnePartXML(nodeMAINGROUP_ZERO, -1, fHairSeq, 0, tmpColor);//앞머
        //CreateOnePartXML(nodeMAINGROUP_ZERO, -1, sHairSeq, 0, tmpColor);//옆머
        //CreateOnePartXML(nodeMAINGROUP_ZERO, -1, bHairSeq, 0, tmpColor);//뒷머

        
        CharPart tmpPart;
        CharPartItem tmpPartItem = new CharPartItem( bodyInitSeq, 0, (int)KindType.Body, (int)OrderType.Body);
        float tmpX = allPartPosDic[ bodyInitSeq].x;
        float tmpY = allPartPosDic[ bodyInitSeq].y;
        tmpPart = new CharPart(tmpPartItem, tmpColor, handSeq, handType, pantType, type, tmpX, tmpY);

        listMakingPARTInit.Add(tmpPart);

        tmpPartItem = new CharPartItem( underInitSeq, 0, (int)KindType.Underwear, (int)OrderType.Underwear);
        tmpX = allPartPosDic[ underInitSeq].x;
        tmpY = allPartPosDic[ underInitSeq].y;
        tmpPart = new CharPart(tmpPartItem, tmpColor, handSeq, handType, pantType, type, tmpX, tmpY);

        listMakingPARTInit.Add(tmpPart);

        //tmpPartItem = new CharPartItem(leftSeq, 0, (int)KindType.EyeLeft, (int)OrderType.EyeLeft);
        //tmpX = allPartPosDic[leftSeq].x;
        //tmpY = allPartPosDic[leftSeq].y;
        //tmpPart = new CharPart(tmpPartItem, tmpColor, handSeq, handType, pantType, type, tmpX, tmpY);

        //listMakingPARTInit.Add(tmpPart);

        //tmpPartItem = new CharPartItem(rightSeq, 0, (int)KindType.EyeRight, (int)OrderType.EyeRight);
        //tmpX = allPartPosDic[rightSeq].x;
        //tmpY = allPartPosDic[rightSeq].y;
        //tmpPart = new CharPart(tmpPartItem, tmpColor, handSeq, handType, pantType, type, tmpX, tmpY);

        //listMakingPARTInit.Add(tmpPart);

        //tmpPartItem = new CharPartItem(noseSeq, 1, (int)KindType.Nose, (int)OrderType.Nose);
        //tmpX = allPartPosDic[noseSeq].x;
        //tmpY = allPartPosDic[noseSeq].y;
        //tmpPart = new CharPart(tmpPartItem, tmpColor, handSeq, handType, pantType, type, tmpX, tmpY);

        //listMakingPARTInit.Add(tmpPart);

        //tmpPartItem = new CharPartItem(mouseSeq, 99, (int)KindType.Mouse, (int)OrderType.Mouse);
        //tmpX = allPartPosDic[mouseSeq].x;
        //tmpY = allPartPosDic[mouseSeq].y;
        //tmpPart = new CharPart(tmpPartItem, tmpColor, handSeq, handType, pantType, type, tmpX, tmpY);

        //listMakingPARTInit.Add(tmpPart);

        //tmpPartItem = new CharPartItem(fHairSeq, 0, (int)KindType.FrontHair, (int)OrderType.FrontHair);
        //tmpX = allPartPosDic[fHairSeq].x;
        //tmpY = allPartPosDic[fHairSeq].y;
        //tmpPart = new CharPart(tmpPartItem, tmpColor, handSeq, handType, pantType, type, tmpX, tmpY);

        //listMakingPARTInit.Add(tmpPart);

        //tmpPartItem = new CharPartItem(sHairSeq, 0, (int)KindType.SideHair, (int)OrderType.SideHair);
        //tmpX = allPartPosDic[sHairSeq].x;
        //tmpY = allPartPosDic[sHairSeq].y;
        //tmpPart = new CharPart(tmpPartItem, tmpColor, handSeq, handType, pantType, type, tmpX, tmpY);

        //listMakingPARTInit.Add(tmpPart);

        //tmpPartItem = new CharPartItem(bHairSeq, 0, (int)KindType.BackHair, (int)OrderType.BackHair);
        //tmpX = allPartPosDic[bHairSeq].x;
        //tmpY = allPartPosDic[bHairSeq].y;
        //tmpPart = new CharPart(tmpPartItem, tmpColor, handSeq, handType, pantType, type, tmpX, tmpY);

        listMakingPARTInit.Add(tmpPart);
    }
    //wearSeq로 메인의 WEAR 받아서 사용(listMakingPARTInit)
    public void SetModifyWEAR(string wearSeq)
    {
        listMakingPARTInit.Clear();
        XmlNode nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        XmlNode nodeMAINWEARNegative = SelectNode(nodeMAINGROUP_ZERO,  xmlWEAR,  wear_seqSt, wearSeq);
        XmlNodeList listAllPARTs = nodeMAINWEARNegative.ChildNodes;
        XmlNode element;
        for(int i=0;i< listAllPARTs.Count; i++){
            element = listAllPARTs[i];

            string itemSeq = element.Attributes[ item_seqSt].Value;
            CharPart tmpPart = UsePartByItemSeq(itemSeq);
            tmpPart.color = ColorCodeUtil.htmlStringToColor(element.Attributes[ part_color_rgbaSt].Value);
            tmpPart.pantType = int.Parse(element.Attributes["pant_type"].Value);
            listMakingPARTInit.Add(tmpPart);
        }
    }

    //메이킹씬 -> 메인씬 wear-1 -> 마지막 wear
    public void SaveMakedWEAR()
    {
        XmlNode nodeMAINGROUP_ZERO;
        nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        XmlNode nodeMAINWEARNegative = SelectNode(nodeMAINGROUP_ZERO,  xmlWEAR,  wear_seqSt, "-1");

        XmlNode tmpNode = nodeMAINWEARNegative.Clone();

        //-1 WEAR 복사하여 마지막 WEAR로 붙이기
        if (!InGameManager.Instance.inModify)
        {
            int max = -1;
            for (int i = 0; i < nodeMAINGROUP_ZERO.SelectNodes( xmlWEAR).Count; i++)
            {
                if (max < int.Parse(nodeMAINGROUP_ZERO.SelectNodes( xmlWEAR)[i].Attributes[ wear_seqSt].Value))
                    max = int.Parse(nodeMAINGROUP_ZERO.SelectNodes( xmlWEAR)[i].Attributes[ wear_seqSt].Value);
            }
            int wearSeq = max + 1;//-1번 WEAR빼야함
            InGameManager.Instance.modifyNum = wearSeq;
            XmlElement newNode = data.CreateElement( xmlWEAR);

            newNode.SetAttribute( wear_seqSt, wearSeq.ToString());
            newNode.SetAttribute("wear_name", "chibi");
            newNode.InnerXml = tmpNode.InnerXml;

            nodeMAINGROUP_ZERO.AppendChild(newNode);
            data.Save(gameDataPath);
        }
        else
        {
            LoadseqWEARToWEARNegative("-1", InGameManager.Instance.modifyNum.ToString());
        }
    }

    //MAINGROUP0 -> WEAR_-1 -> 특정 파트의 칼라 변경
    public void ChangeColorByPartKind(int partKind, Color color)
    {
        XmlNode nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        XmlNode nodeMAINWEARNegative = SelectNode(nodeMAINGROUP_ZERO,  xmlWEAR,  wear_seqSt, "-1");

        CharPartItem tmpPartItem;
        //모든 파트 탐색

        XmlNodeList listMAINWEAR_PARTS = nodeMAINWEARNegative.ChildNodes;
        XmlNode element;
        for (int i = 0; i < listMAINWEAR_PARTS.Count; i++)
        {
            element = listMAINWEAR_PARTS[i];
            //현재 아이템xml에서 파트정보 가져오기
            tmpPartItem = allPartItemDic[element.Attributes[ item_seqSt].Value];
            //원하는 파트인지 확인
            if (tmpPartItem.kindInt == partKind)
            {
                //Debug.Log(tmpNode.Attributes[ item_seqSt].Value);
                //맞다면 칼라 변경
                element.Attributes[ part_color_rgbaSt].Value = ColorCodeUtil.ColorToHtmlString(color);
                //이 파트의 아이템을 찾음
            }
        }
        data.Save(gameDataPath);
    }

    public void ChangePantTypeByPartKind(int partKind, int pantType)
    {
        XmlNode nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        XmlNode nodeMAINWEARNegative = SelectNode(nodeMAINGROUP_ZERO,  xmlWEAR,  wear_seqSt, "-1");

        CharPartItem tmpPartItem;

        XmlNodeList listMAINWEAR_PARTS = nodeMAINWEARNegative.ChildNodes;
        XmlNode element;
        for(int i = 0; i < listMAINWEAR_PARTS.Count; i++)
        {
            element = listMAINWEAR_PARTS[i];
            //현재 아이템xml에서 파트정보 가져오기
            //tmpNode = SelectOneCharPartItem(element.Attributes[ item_seqSt].Value);
            tmpPartItem = allPartItemDic[element.Attributes[ item_seqSt].Value];
            //원하는 파트인지 확인
            if (tmpPartItem.kindInt == partKind)
            {
                //맞다면 타입 변경
                element.Attributes["pant_type"].Value = pantType.ToString();
            }
        }
        data.Save(gameDataPath);
    }

    //파트만들기@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    public void CreateOnePartMain_Negative(string itemSeq)
    {
        newTmpPart = null;
        XmlNode nodeMAINGROUP_ZERO;
        nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        CreateOnePartXML(nodeMAINGROUP_ZERO, -1, itemSeq, 0, Color.white);
    }

    //특정 오더정보로 메이킹 파트 만들고 사용
    public void CreatePartByPartOrder(int partOrder, int spNum)
    {
        newTmpPart = null;
        XmlNode nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);

        if (listAllPartDic.ContainsKey(partOrder))
        {
            if (listAllPartDic[partOrder].ContainsKey(spNum))
            {
                newTmpPart = listAllPartDic[partOrder][spNum];
                CreateOnePartXML(nodeMAINGROUP_ZERO, -1, newTmpPart.charPartItem.seq, 0, Color.white);
            }
            else
            {
                Debug.Log("같은 스프라이트넘버를 가진 아이템이 없음");
            }
        }
    }

    //특정 오더정보로 메이킹 파트 만들지 않고 사용
    public void UsePartByPartOrder(int partOrder, int spNum)
    {
        newTmpPart = null;
        newTmpPart = listAllPartDic[partOrder][spNum];
    }

    //옷파트 하나 생성
    public void CreateOnePartXML(XmlNode parentGROUP, int wearSeq, string itemSeq, int pantType, Color color, string wearName = "")
    {
        //wear이 없다면 wear 생성
        XmlNode nodeWEAR = SelectNode(parentGROUP,  xmlWEAR,  wear_seqSt, wearSeq.ToString());
        if (nodeWEAR != null)
        {
            //Debug.Log("nodeWEAR 존재");
        }
        else
        {
            //Debug.Log("nodeWEAR 없음 새로 생성");
            ////Debug.GetComponent<Text>().text = "nodeWEAR 없음 새로 생성";

            XmlElement newWEAR = data.CreateElement( xmlWEAR);
            newWEAR.SetAttribute( wear_seqSt, wearSeq.ToString());
            newWEAR.SetAttribute("wear_name", wearName.ToString());
            parentGROUP.AppendChild(newWEAR);
            nodeWEAR = newWEAR;
        }

        XmlElement newPART = data.CreateElement("PART");
        newPART.SetAttribute( item_seqSt, itemSeq.ToString());
        newPART.SetAttribute( part_color_rgbaSt, ColorCodeUtil.ColorToHtmlString(color));
        newPART.SetAttribute("pant_type", pantType.ToString());
        nodeWEAR.AppendChild(newPART);
        data.Save(gameDataPath);
    }

    //파트아이템xml에서 아이템이 있는지 없는지 확인
    public bool IsCharPartItem(string itemSeq)
    {
        if (!allPartItemDic.ContainsKey(itemSeq))
        {
            return false;
        }
        return true;
    }

    //MAINGROUP_0 -> WEAR_-1 -> 같은 파트종류가 있을 경우 true
    public bool isPartByPartKind(int partKind)
    {
        XmlNode nodeMAINGROUP_ZERO;
        nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        XmlNode nodeMAINWEARNegative = SelectNode(nodeMAINGROUP_ZERO,  xmlWEAR,  wear_seqSt, "-1");

        XmlNodeList listMAINWEAR_PARTS = nodeMAINWEARNegative.ChildNodes;
        CharPartItem tmpPartItem;
        for (int i = 0; i < listMAINWEAR_PARTS.Count; i++)
        {
            tmpPartItem = allPartItemDic[listMAINWEAR_PARTS[i].Attributes[ item_seqSt].Value];
            if (tmpPartItem.kindInt == partKind)
            {
                return true;
            }
        }
        return false;
    }
    //손 만들 때 itemSeq string만들어서 CharPart만들어서 사용
    public CharPart UsePartByItemSeq(string itemSeq)
    {
        CharPartItem tmpPartItem;
        CharPart tmpPart;
        Color color;
        //Debug.Log(itemSeq);
        //PartItemData.xml에서 아이템 찾기
        tmpPartItem = allPartItemDic[itemSeq];
        //PartItemPosition.xml에서 해당 좌표 찾기
        string handSeq;
        int handType = 0;
        if (allPartPosDic[itemSeq].hand != null && allPartPosDic[itemSeq].hand != "")
        {
            handSeq = allPartPosDic[itemSeq].hand;
            if (handSeq[handSeq.Length - 1] == 'U')
            {
                handType = 1;
                handSeq = handSeq.TrimEnd('U');
            }
            else
            {
                handType = 0;
            }
        }
        else
        {
            ////Debug.Log("핸드없음");
            handSeq = "-1";
        }
        int pantType = 0;

        int type = 0;
        if (allPartPosDic[itemSeq].itemType != null && allPartPosDic[itemSeq].itemType != "")
        {
            type = int.Parse(allPartPosDic[itemSeq].itemType);
        }
        float tmpX = allPartPosDic[itemSeq].x;
        float tmpY = allPartPosDic[itemSeq].y;


        color = Color.white;

        tmpPart = new CharPart(tmpPartItem, color, handSeq, handType, pantType, type, tmpX, tmpY);
        return tmpPart;
    }
    
    //특정 파트정보로 메이킹파트 선택하여 사용
    public CharPart SelectPartByPartKind(int partKind)
    {
        XmlNode nodeMAINGROUP_ZERO;
        nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        XmlNode nodeMAINWEARNegative = SelectNode(nodeMAINGROUP_ZERO,  xmlWEAR,  wear_seqSt, "-1");
        XmlNodeList listMAINWEAR_PARTS = nodeMAINWEARNegative.ChildNodes;

        CharPart tmpPart = null;
        CharPartItem tmpPartItem;

        for (int i = 0; i < listMAINWEAR_PARTS.Count; i++)
        {
            tmpPartItem = allPartItemDic[listMAINWEAR_PARTS[i].Attributes[ item_seqSt].Value];

            if (tmpPartItem.kindInt == partKind)
            {
                //Debug.Log("같은 파트 종류가 있다");
                tmpPart = UsePart(tmpPartItem);
                tmpPart.color = ColorCodeUtil.htmlStringToColor(listMAINWEAR_PARTS[i].Attributes[ part_color_rgbaSt].Value);
                tmpPart.pantType = int.Parse(listMAINWEAR_PARTS[i].Attributes["pant_type"].Value);
            }
        }
        if (tmpPart == null)
        {
            //Debug.Log("같은 파트 종류가 없다 null반환");
        }
        return tmpPart;
    }

    //특정 파트정보로 메이킹파트 삭제
    public void RemovePartByPartKind(int partKind)
    {
        XmlNode nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);
        XmlNode nodeMAINWEARNegative = SelectNode(nodeMAINGROUP_ZERO,  xmlWEAR,  wear_seqSt, "-1");
        XmlNodeList listMAINWEAR_PARTS = nodeMAINWEARNegative.ChildNodes;

        CharPartItem tmpPartItem;

        for (int i = 0; i < listMAINWEAR_PARTS.Count; i++)
        {
            tmpPartItem = allPartItemDic[listMAINWEAR_PARTS[i].Attributes[ item_seqSt].Value];

            if (tmpPartItem.kindInt == partKind)
            {
                //Debug.Log("같은 파트 종류가 있다");

                nodeMAINWEARNegative.RemoveChild(listMAINWEAR_PARTS[i]);
            }
        }
        data.Save(gameDataPath);
    }
    
    //특정 파트노드를 CharPart로 변환
    public CharPart UsePartByNode(XmlNode targetNode)
    {
        CharPart tmpPart = null;
        CharPartItem tmpPartItem;
        Color color;

        string itemSeq = targetNode.Attributes[ item_seqSt].Value;

        //PartItemData.xml에서 해당 아이템 찾기
        int spNum = allPartItemDic[itemSeq].spNum;
        int partKind = allPartItemDic[itemSeq].kindInt;
        int partOrder = allPartItemDic[itemSeq].orderInt;

        tmpPartItem = new CharPartItem(itemSeq, spNum, partKind, partOrder);

        //PartItemPosition.xml에서 해당 좌표 찾기
        string handSeq;
        int handType = 0;
        if (allPartPosDic[itemSeq].hand != null && allPartPosDic[itemSeq].hand != "")
        {
            handSeq = allPartPosDic[itemSeq].hand;
            if (handSeq[handSeq.Length - 1] == 'U')
            {
                handType = 1;
                handSeq = handSeq.TrimEnd('U');
            }
            else
            {
                handType = 0;
            }
        }
        else
        {
            handSeq = "-1";
        }
        int pantType = 0;

        int type = 0;
        if (allPartPosDic[itemSeq].itemType != null && allPartPosDic[itemSeq].itemType != "")
        {
            type = int.Parse(allPartPosDic[itemSeq].itemType);
        }
        float tmpX = allPartPosDic[itemSeq].x;
        float tmpY = allPartPosDic[itemSeq].y;

        //색
        color = Color.white;

        tmpPart = new CharPart(tmpPartItem, color, handSeq, handType, pantType, type, tmpX, tmpY);
      
        return tmpPart;
    }

    //
    public CharPart UsePart(CharPartItem tmpPartItem)
    {
        CharPart tmpPart = null;
        Color color;

        string itemSeq = tmpPartItem.seq;

        //PartItemPosition.xml에서 해당 좌표 찾기
        string handSeq;
        int handType = 0;
        if (allPartPosDic[itemSeq].hand != null && allPartPosDic[itemSeq].hand != "")
        {
            handSeq = allPartPosDic[itemSeq].hand;
            if (handSeq[handSeq.Length - 1] == 'U')
            {
                handType = 1;
                handSeq = handSeq.TrimEnd('U');
            }
            else
            {
                handType = 0;
            }
        }
        else
        {
            ////Debug.Log("핸드없음");
            handSeq = "-1";
        }
        int pantType = 0;

        int type = 0;
        if (allPartPosDic[itemSeq].itemType != null && allPartPosDic[itemSeq].itemType != "")
        {
            type = int.Parse(allPartPosDic[itemSeq].itemType);
        }
        float tmpX = allPartPosDic[itemSeq].x;
        float tmpY = allPartPosDic[itemSeq].y;

        //색
        color = Color.white;

        tmpPart = new CharPart(tmpPartItem, color, handSeq, handType, pantType, type, tmpX, tmpY);

        return tmpPart;
    }

    public CharTr UseTrByNode(XmlNode targetNode)
    {
        CharTr tmpTr;

        string trSeq = targetNode.Attributes[ trSeqSt].Value;
        string usedSeq = targetNode.Attributes[ usedSeqSt].Value;
        Vector3 vector3 = new Vector3(float.Parse(targetNode.Attributes[ positionXSt].Value), float.Parse(targetNode.Attributes[ positionYSt].Value), 0);
        float scale = float.Parse(targetNode.Attributes[ trOrderSt].Value);
        int order = int.Parse(targetNode.Attributes[ trOrderSt].Value);
        bool isReversed = bool.Parse(targetNode.Attributes[ isReversedSt].Value);

        tmpTr = new CharTr(trSeq, usedSeq, vector3, scale, order, isReversed);
        return tmpTr;
    }

#endregion

    public void CloneXML(XmlNode prevNode, XmlNode parentNode, string name, string setSeq, int seq)
    {
        XmlNode tmpNode = prevNode.Clone();
        XmlElement newNode = data.CreateElement(name);
        newNode.SetAttribute(setSeq, seq.ToString());
        newNode.InnerXml = tmpNode.InnerXml;

        parentNode.AppendChild(newNode);

        data.Save(gameDataPath);
    }

    //슬롯 존재 확인 리턴
    public bool isSelectSlot(string slotSeq)
    {
        if (SelectSlot(slotSeq) != null)
        {
            //Debug.Log("슬롯 존재 확인");
            return true;
        }
        return false;
    }

    //슬롯에 저장하기
    public void SaveMainToSlotXML(string slotSeq)
    {
        RemoveOneSlotXML(slotSeq);

        XmlNode tmpNode = nodeMAIN.Clone();
        XmlElement newNode = data.CreateElement("SLOT");
        newNode.SetAttribute("slot_seq", slotSeq.ToString());
        int slotSeqPlus = int.Parse(slotSeq) + 1;
        newNode.SetAttribute("slot_name", "Slot " + slotSeqPlus.ToString());
        newNode.InnerXml = tmpNode.InnerXml;

        nodeALL_SLOT.AppendChild(newNode);

        //////DebugManager.Instance.Screen//Debug("gameDataPath" + gameDataPath);
        data.Save(gameDataPath);
        ////DebugManager.Screen//Debug(newNode.Name.ToString());
    }
    //슬롯에서 불러오기
    public bool LoadSlotToMain(int slotSeq)
    {
        XmlNode nodeSLOT = SelectSlot(slotSeq.ToString());
        if (nodeSLOT == null)
        {
            //Debug.Log(false);
            return false;
        }

        nodeMAIN.InnerXml = nodeSLOT.InnerXml;
        data.Save(gameDataPath);

        //Debug.Log(true);
        return true;
    }
    //슬롯 하나 삭제
    public void RemoveOneSlotXML(string slotSeq)
    {
        XmlNode nodeSLOT = SelectSlot(slotSeq);
        if (nodeSLOT != null)
        {
            nodeALL_SLOT.RemoveChild(nodeSLOT);
        }
        else
        {
            //Debug.Log("nodeSLOT is null");
        }
        data.Save(gameDataPath);
    }
    //슬롯 이름 불러오기
    public string LoadSlotName(string slotSeq)
    {
        XmlNode nodeSLOT = SelectSlot(slotSeq);
        return nodeSLOT.Attributes["slot_name"].Value;
    }
    //슬롯 이름 저장하기 
    public void SaveSlotName(string slotSeq, string name)
    {
        XmlNode nodeSLOT = SelectSlot(slotSeq);
        nodeSLOT.Attributes["slot_name"].Value = name;
    }

    //MAIN_GROUP_0 내에서 WEAR 붙여넣기
    public void LoadseqWEARToWEARNegative(string fromWearSeq, string toWearSeq)
    {
        XmlNode nodeMAINGROUP_ZERO;
        nodeMAINGROUP_ZERO = SelectGroup(nodeMAIN, 0);

        XmlNode from = SelectWear(nodeMAINGROUP_ZERO, fromWearSeq);
        XmlNode to = SelectWear(nodeMAINGROUP_ZERO, toWearSeq);

        to.InnerXml = from.InnerXml;
        data.Save(gameDataPath);
    }

    //옷 하나 삭제
    public void RemoveOnePartXML(XmlNode targetParentNode, int wearSeq, int itemSeq)
    {
        XmlNode nodeITEM_GROUP0 = targetParentNode.FirstChild;

        stringBuilder.Clear();
        stringBuilder.Append("WEAR[@wear_seq='").Append(wearSeq).Append("']");
        XmlNode nodeWEAR = nodeITEM_GROUP0.SelectSingleNode(stringBuilder.ToString());

        stringBuilder.Clear();
        stringBuilder.Append("PART[@item_seq='").Append(itemSeq).Append("']");
        XmlNode nodePART = nodeWEAR.SelectSingleNode(stringBuilder.ToString());

        if (nodePART != null)
        {
            nodeWEAR.RemoveChild(nodePART);
        }
        else
        {
            //Debug.Log("nodePART is null");
        }
        data.Save(gameDataPath);
    }


    //캐릭터 하나 추가
    public void CreateOneWearXML(XmlNode targetParentNode, int wearSeq, Friend friend)
    {
        XmlNode nodeITEM_GROUP0 = targetParentNode.FirstChild;

        XmlElement newWEAR = data.CreateElement( xmlWEAR);
        nodeITEM_GROUP0.AppendChild(newWEAR);
       
        data.Save(gameDataPath);
    }
    //위치정보 하나 삭제

    //trSeq라는 이름을 가진 위치정보 삭제
    public void RemoveOneTrSeqXML(XmlNode targetParentNode, int group, int slotSeq, int trSeq)
    {
        XmlNode nodeITEM_GROUP_group = SelectNode(targetParentNode, "ITEM_GROUP", "group_seq", group.ToString());
        XmlNode nodeTR_CHAR = nodeITEM_GROUP_group.SelectSingleNode("TR_CHAR");

        XmlNode nodeTR = SelectNode(nodeTR_CHAR,  xmlTR,  trSeqSt, trSeq.ToString());
        if (nodeTR != null)
        {
            nodeTR_CHAR.RemoveChild(nodeTR);
        }
        else
        {
            //Debug.Log("nodeTR is null");
        }
        data.Save(gameDataPath);
        ////DebugManager.Screen//Debug(nodeTR_CHAR.Name.ToString());
    }

    //아이템을 가지고 있는지 완벽하게 확인한다
    //IsUserHaveItem()과 대치하도록 한다
    //버그 아주아주 많을 예정..
    public bool CheckIsHave(string seqName, string itemSeq)
    {
        bool returnFalseValue = false;
        CharPart tmpCharPart = null;
        ItemPart tmpItemPart = null;

        string orderSt = null;
        string spNumSt = null;
        int order = 0;
        int spNum = 0;
        int key;

        switch (seqName)
        {
            default:
                // hair_seq, eyes_seq등 나머지
                break;
            //버튼타입부터 거른다
            case "order_seq":
            case "obj_type_seq":
                if (IsCostOrderButton(seqName, itemSeq))
                {
                    if (IsUserHaveItem(seqName, itemSeq))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            //파트 아이템일경우
            case "hair_seq":
            case "eyes_seq":
            case "item_set":
                spNum = int.Parse(itemSeq);
                break;
            case "item_seq":
                orderSt = ParseItemSeq.SplitItemSeq1(itemSeq);
                spNumSt = ParseItemSeq.SplitItemSeq2(itemSeq);

                order = int.Parse(orderSt);
                spNum = int.Parse(spNumSt);
                if (listAllPartDic.ContainsKey(order))
                {
                    if (listAllPartDic[order].ContainsKey(spNum))
                    {
                        tmpCharPart = listAllPartDic[order][spNum];
                    }
                    else
                    {
                        Debug.LogError("아이템을 찾지 못함" + seqName + itemSeq);
                    }
                }
                else
                {
                    Debug.LogError("아이템을 찾지 못함" + seqName + itemSeq);
                }
                break;
                //오브젝트 아이템일 경우
            case "bg_seq":
                for (int i = 0; i < listAllObjKeysDic[(int)FriendType.BG].Count; i++)
                {
                    key = listAllObjKeysDic[(int)FriendType.BG][i];

                    if (listAllObjDic[(int)FriendType.BG][key].typeSeq == itemSeq)
                    {
                        tmpItemPart = listAllObjDic[(int)FriendType.BG][key];
                        break;
                    }
                }
                break;
            case "object1_seq":
                for (int i = 0; i < listAllObjKeysDic[(int)FriendType.Object1].Count; i++)
                {
                    key = listAllObjKeysDic[(int)FriendType.Object1][i];

                    if (listAllObjDic[(int)FriendType.Object1][key].typeSeq == itemSeq)
                    {
                        tmpItemPart = listAllObjDic[(int)FriendType.Object1][key];
                        break;
                    }
                }
                break;
            case "object2_seq":
                for (int i = 0; i < listAllObjKeysDic[(int)FriendType.Object2].Count; i++)
                {
                    key = listAllObjKeysDic[(int)FriendType.Object2][i];

                    if (listAllObjDic[(int)FriendType.Object2][key].typeSeq == itemSeq)
                    {
                        tmpItemPart = listAllObjDic[(int)FriendType.Object2][key];
                        break;
                    }
                }
                break;
            case "animal_seq":
                for (int i = 0; i < listAllObjKeysDic[(int)FriendType.Animal].Count; i++)
                {
                    key = listAllObjKeysDic[(int)FriendType.Animal][i];

                    if (listAllObjDic[(int)FriendType.Animal][key].typeSeq == itemSeq)
                    {
                        tmpItemPart = listAllObjDic[(int)FriendType.Animal][key];
                        break;
                    }
                }
                break;
            case "text_seq":
                for (int i = 0; i < listAllObjKeysDic[(int)FriendType.Text].Count; i++)
                {
                    key = listAllObjKeysDic[(int)FriendType.Text][i];

                    if (listAllObjDic[(int)FriendType.Text][key].typeSeq == itemSeq)
                    {
                        tmpItemPart = listAllObjDic[(int)FriendType.Text][key];
                        break;
                    }
                }
                break;
        }
        if (tmpItemPart == null && tmpCharPart == null)
        {
            Debug.Log("아이템x 맞으면 버그" + seqName + itemSeq);
        }
        else
        {
            //해금되는 조건 0. 아이템이 무료이다
            if (seqName == "item_seq")
            {
                if (tmpCharPart.itemType == ItemType.nomalItem)
                {
                    //해금되는 조건 3. 상위 버튼이 무료이거나 해금되어있다
                    if (!IsCostOrderButton("order_seq", orderSt))
                    {
                        return true;
                    }
                    else
                    {
                        if (IsUserHaveItem("order_seq", orderSt))
                        {
                            return true;
                        }
                    }
                }
            }
            else
            {
                if (tmpItemPart.itemType == ItemType.nomalItem)
                {
                    if (!IsCostOrderButton("obj_type_seq", itemSeq))
                    {
                        return true;
                    }
                    else
                    {
                        if (IsUserHaveItem("obj_type_seq", itemSeq))
                        {
                            return true;
                        }
                    }
                }
            }
        }
        //해금되는 조건 1. 요구하는 아이템(세트)을 가지고 있는다
        if (IsUserHaveItem(seqName, itemSeq))
        {
            return true;
        }
        //해금되는 조건 1-1. 대치되는 세트를 가지고 있는다
        if (tmpCharPart != null)
        {
            switch ((OrderType)order)
            {
                case OrderType.BackHair:
                case OrderType.SideHair:
                case OrderType.FrontHair:
                    if (IsUserHaveItem("hair_seq", spNumSt))
                    {
                        return true;
                    }
                    break;
                case OrderType.EyeLeft:
                case OrderType.EyeRight:
                    if (IsUserHaveItem("eyes_seq", spNumSt))
                    {
                        return true;
                    }
                    break;
            }
            //아이템셋에 캐릭터 파트가 있을경우
            for (int i = 0; i < CollectionSeed.colItemSetUH.Count; i++)
            {
                for (int j = 0; j < CollectionSeed.colItemSetUH[i].Count; j++)
                {
                    if ("item_seq" == CollectionSeed.colItemSetUH[i][j].seqName &&
                        tmpCharPart.charPartItem.seq == CollectionSeed.colItemSetUH[i][j].itemSeq)
                    {
                        if (IsUserHaveItem("item_set", i.ToString()))
                        {
                            return true;
                        }
                    }
                }
            }
        }
        else if(tmpItemPart != null)
        {
            //아이템셋에 오브젝트가 있을경우

            for (int i = 0; i < CollectionSeed.colItemSetUH.Count; i++)
            {
                for (int j = 0; j < CollectionSeed.colItemSetUH[i].Count; j++)
                {
                    if (tmpItemPart.seqName == CollectionSeed.colItemSetUH[i][j].seqName &&
                        tmpItemPart.typeSeq == CollectionSeed.colItemSetUH[i][j].itemSeq)
                    {
                        if (IsUserHaveItem("item_set", i.ToString()))
                        {
                            return true;
                        }
                    }
                }
            }
        }

        int newOrder = 0;
        CharPart newCharPart = null;
        bool isNoCost = false;
        int count = 0;
        //해금되는 조건 2. 요구하는 세트의 아이템을 전부 가지고 있는다||무료이다
        switch (seqName) {
            case "hair_seq":
                for (int i = 0; i < 3; i++)
                {
                    switch (i)
                    {
                        case 0:
                            newOrder = (int)OrderType.BackHair;
                            break;
                        case 1:
                            newOrder = (int)OrderType.SideHair;
                            break;
                        case 2:
                            newOrder = (int)OrderType.FrontHair;
                            break;
                    }
                    if (listAllPartDic.ContainsKey(newOrder))
                    {
                        if (listAllPartDic[newOrder].ContainsKey(spNum))
                        {
                            newCharPart = listAllPartDic[newOrder][spNum];
                        }
                    }
                    if (newCharPart != null)
                    {
                        if (newCharPart.itemType == ItemType.nomalItem)
                        {
                            isNoCost = true;
                        }
                        if (IsUserHaveItem("item_seq", ParseItemSeq.MakeItemSeq(newOrder, spNum)) || isNoCost)
                        {
                            count++;
                        }
                    }
                }
                if(count == 3)
                {
                    return true;
                }
                break;
            case "eyes_seq":
                for (int i = 0; i < 2; i++)
                {
                    switch (i)
                    {
                        case 0:
                            newOrder = (int)OrderType.EyeLeft;
                            break;
                        case 1:
                            newOrder = (int)OrderType.EyeRight;
                            break;
                    }
                    if (listAllPartDic.ContainsKey(newOrder))
                    {
                        if (listAllPartDic[newOrder].ContainsKey(spNum))
                        {
                            newCharPart = listAllPartDic[newOrder][spNum];
                        }
                    }
                    if (newCharPart != null)
                    {
                        if (newCharPart.itemType == ItemType.nomalItem)
                        {
                            isNoCost = true;
                        }
                        if (IsUserHaveItem("item_seq", ParseItemSeq.MakeItemSeq(newOrder, spNum)) || isNoCost)
                        {
                            count++;
                        }
                    }
                }
                if (count == 2)
                {
                    return true;
                }
                break;
            case "item_set":
                int countMax = CollectionSeed.colItemSetUH[spNum].Count;
                for (int j = 0; j < countMax; j++)
                {
                    if (IsUserHaveItem(CollectionSeed.colItemSetUH[spNum][j].seqName, CollectionSeed.colItemSetUH[spNum][j].itemSeq))
                    {
                        count++;
                    }
                }
                if(count == countMax)
                {
                    return true;
                }
                break;
        }
        return returnFalseValue;
    }


    //아이템 seq == 특정 노드 아이템인지 확인하기
    public bool CheckNodeIsItem(XmlNode tmpNode, string seqName, string itemSeq)
    {
        string firSt = "";
        string secSt = "";

        //그냥 가지고 있는지
        if (tmpNode.Attributes["seq_name"].Value == seqName)
        {
            if (tmpNode.Attributes[tmpNode.Attributes["seq_name"].Value].Value == itemSeq)
            {
                return true;
            }
        }

        //없으면 특수한 이름 확인
        if (seqName == "item_seq")
        {
            firSt = ParseItemSeq.SplitItemSeq1(itemSeq);
            secSt = ParseItemSeq.SplitItemSeq2(itemSeq);
        }

        if (tmpNode.Attributes["seq_name"].Value == "order_seq")
        {
            if (seqName == "item_seq" && tmpNode.Attributes["order_seq"].Value == firSt)
            {
                return true;
            }
        }
        if (firSt == "013" || firSt == "014")
        {
            if (tmpNode.Attributes["seq_name"].Value == "eyes_seq")
            {
                if (tmpNode.Attributes["eyes_seq"].Value == secSt)
                {
                    return true;
                }
            }
        }
        if (firSt == "004" || firSt == "018" || firSt == "021")
        {
            if (tmpNode.Attributes["seq_name"].Value == "hair_seq")
            {
                if (tmpNode.Attributes["hair_seq"].Value == secSt)
                {
                    return true;
                }
            }
        }
        return false;
    }


    //string -> Vector3
    public Vector3 StringToVector3(string sVector)
    {
        if (sVector.StartsWith("(") && sVector.EndsWith(")"))
        {
            sVector = sVector.Substring(1, sVector.Length - 2);
        }

        string[] sArray = sVector.Split(',');

        Vector3 result = new Vector3(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]));

        return result;
    }
}
