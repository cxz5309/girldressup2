﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleObjectPool : MonoBehaviour
{
    public Dictionary<int, GameObject> indexObjDic = new Dictionary<int, GameObject>();

    int idx = 0;
    
    public GameObject nowButton(GameObject prefabGameObject)
    {
        int i = idx;
        idx++;
        if (indexObjDic.ContainsKey(i))
        {
            return indexObjDic[i];
        }
        else
        {
            GameObject indexObj = Instantiate(prefabGameObject, transform);
            indexObjDic.Add(i, indexObj);
            return indexObj;
        }
    }

    public void SetIdx()
    {
        idx = 0;
    }

    private void OnDisable()
    {
        indexObjDic.Clear();
    }
}
