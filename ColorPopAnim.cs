﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPopAnim : MonoBehaviour
{
    public void ColorPop()
    {
        InGameManager.Instance.StartEffect(4, new Vector3(0,1,0));
    }
}
