﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Assets.SimpleLocalization;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

public class BubbleText
{
    public string bubbleSeq;
    public string trSeq;
    public string text;
    public int fontColor;
    public int fontSize;
    public bool isStart;

    public BubbleText(string bubbleSeq, string trSeq, string text, int fontColor, int fontSize)
    {
        this.bubbleSeq = bubbleSeq;
        this.trSeq = trSeq;
        this.text = text;
        this.fontColor = fontColor;
        this.fontSize = fontSize;
        if(text == LocalizationManager.Localize(StringInfo.defaultText) || (text.Length == 0 && text.TakeWhile(char.IsWhiteSpace).Count() == 0))
        {
            isStart = true;
        }
        else
        {
            isStart = false;
        }
    }
}

public class TMPObject : MonoBehaviour
{
    public TMP_FontAsset korean;
    public TMP_FontAsset japanese;

    private Material koreanMaterial;
    private Material japaneseMaterial;
    public TMP_Text tmp_text;

    private TouchScreenKeyboard touchScreenKeyboard;
    public TextMeshPro textMeshPro;
    public BoxCollider2D col;

    //public GameObject deemSpriteRect;

    public BubbleText bubbleText;

    public string newBubbleSeq;
    public string newTrSeq;
    string newText = null;
    string prevText = null;

    public bool cantOpen;
    public bool isOpen;
    enum ButtonState
    {
        Input, OK, Cancel
    }
    enum StartState
    {
        isStart, notStart
    }
    ButtonState buttonState;
    StartState startState;
    StartState prevStartState;
    bool space = false;
    private void Start()
    {
        LocalizationManager.Localize(prevText = StringInfo.defaultText);
    }

    private void Update()
    {
        //ResizeCollider();
        if (InMainManager.Instance.canvasState == InMainManager.CanvasState.mainCanvas)
        {
            if (Input.GetMouseButtonUp(0))
            {
                //레이어마스크 8번 Friend를 무시하여 각 콜라이더가 동시에 반응하도록 한다
                Vector2 touchPosition = InMainManager.Instance.mainCamera.ScreenToWorldPoint(Input.mousePosition);
                Ray2D ray = new Ray2D(touchPosition, Vector2.zero);
                int layerMask = 1 << 8;

                layerMask = ~layerMask;
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 1, layerMask);

                if (hit.collider != null)
                {
                    if (hit.collider == col)
                    {
                        MouseUp();
                    }
                }
            }
        }
        if (isOpen)
        {
            if (TouchScreenKeyboard.isSupported)
            {
                //Debug.Log("Length" + newText.Length);
                //Debug.Log("text.Count(Char.IsWhiteSpace)" + touchScreenKeyboard.text.Count(char.IsWhiteSpace));
                //Debug.Log("text.TakeWhile(char.IsWhiteSpace).Count()" + touchScreenKeyboard.text.TakeWhile(char.IsWhiteSpace).Count());
                //Debug.Log("text.Split().Length" + touchScreenKeyboard.text.Split().Length);
                //Debug.Log("text.Count(s => s == ' ')" + touchScreenKeyboard.text.Count(s => s == ' '));
                newText = touchScreenKeyboard.text;

                if (touchScreenKeyboard.status == TouchScreenKeyboard.Status.Done)
                {
                    SetButtonState(true);
                    isOpen = false;
                }
                if (touchScreenKeyboard.status == TouchScreenKeyboard.Status.Canceled)
                {
                    SetButtonState(false);
                    //deemSpriteRect.SetActive(false);
                    isOpen = false;
                }
                if (touchScreenKeyboard.status == TouchScreenKeyboard.Status.LostFocus)
                {
                    SetButtonState(false);
                    //deemSpriteRect.SetActive(false);
                    isOpen = false;
                }
                SetText();
            }
        }
    }

     public void Init(bool isNew)
    {
        transform.parent.GetComponent<Drag>().tmpObj = this;
        LocalizationText();
        //로드 아닌 최소 생성
        if (isNew)
        {
            startState = StartState.isStart;

            bubbleText = new BubbleText(newBubbleSeq, newTrSeq, LocalizationManager.Localize(StringInfo.defaultText), PlayerPrefs.GetInt(StringInfo.playerPrefsKeyList[4]), 60);
            newText = LocalizationManager.Localize(StringInfo.defaultText);
            tmp_text.fontSizeMax = 60/*ObjectInfo.arrayTextSize[bubbleText.fontSize]*/;
        }
        else
        {
            SetStartState(bubbleText.isStart);
            //InMainManager.LoadText에서 BubbleText 생성
            newText = bubbleText.text;
            tmp_text.fontSizeMax = 60/*ObjectInfo.arrayTextSize[bubbleText.fontSize]*/;
        }

        prevText = newText;
        prevStartState = startState;
        buttonState = ButtonState.Input;
        SetText();
    }

    //내부 콜라이더 클릭시
    private void MouseUp()
    {
        //UI뒤에있을때 선택불가
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        Debug.Log(results.Count);
        if (results.Count > 0)
        {
            Debug.Log("behind ui");
            return;
        }

        if (cantOpen)
        {
            Debug.Log("cant open");
            return;
        }
        OpenKeyBoard();
    }

    public void SetText()
    {
        if (startState == StartState.isStart)
        {
            if (newText != LocalizationManager.Localize(StringInfo.defaultText))
            {
                startState = StartState.notStart;
            }
        }
        if (newText.Replace(" ", "").Length == 0)
        {
            if (newText.TakeWhile(char.IsWhiteSpace).Count() == 0)
            {
                if (space == false)
                {
                    startState = StartState.isStart;
                }
                else
                {
                    startState = StartState.notStart;
                }
            }
            else 
            {
                space = true;
                startState = StartState.notStart;
            }
        }
        else
        {
            space = false;
        }

        switch (buttonState)
        {
            case ButtonState.OK:
            case ButtonState.Input:
                if (startState == StartState.isStart)
                {
                    textMeshPro.SetText(LocalizationManager.Localize(StringInfo.defaultText));
                    textMeshPro.color = Color.gray;
                }
                else
                {
                    textMeshPro.color = ObjectInfo.arrayTextColor[bubbleText.fontColor];
                    
                    if (space)
                    {
                        newText = " ";
                    }
                    textMeshPro.SetText(newText);
                }
                break;
            case ButtonState.Cancel:
                newText = prevText;
                if (prevStartState == StartState.isStart)
                {
                    startState = StartState.isStart;
                    textMeshPro.SetText(LocalizationManager.Localize(StringInfo.defaultText));
                    textMeshPro.color = Color.gray;
                }
                else
                {
                    textMeshPro.color = ObjectInfo.arrayTextColor[bubbleText.fontColor];
                    textMeshPro.SetText(prevText);
                }
                break;
        }
        
        ResizeCollider();
        bubbleText.text = newText;
        XMLManager.Instance.SaveOneMAIN_BUBBLE(bubbleText);
    }
    public void SetButtonState(bool isOk)
    {
        if (isOk)
        {
            buttonState = ButtonState.OK;
        }
        else
        {
            buttonState = ButtonState.Cancel;
        }
    }
    public void SetStartState(bool isStart)
    {
        if (isStart)
        {
            startState = StartState.isStart;
        }
        else
        {
            startState = StartState.notStart;
        }
    }

    public void OpenKeyBoard()
    {
        Debug.Log("Open KeyBoard");
        isOpen = true;

        prevText = newText;
        prevStartState = startState;
        buttonState = ButtonState.Input;
        space = false;
        if (startState == StartState.isStart)
        {
            touchScreenKeyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, true, true, false, false, LocalizationManager.Localize(StringInfo.defaultText));
        }
        else
        {
            touchScreenKeyboard = TouchScreenKeyboard.Open(newText, TouchScreenKeyboardType.Default, true, true, false, false, LocalizationManager.Localize(StringInfo.defaultText));
        }
    }

    public void ResizeCollider()
    {
        StartCoroutine(CoResizeCollider());
    }

    IEnumerator CoResizeCollider()
    {
        yield return null;
        float resizeX;
        float resizeY;
        float max = 0;
        for (int i = 0; i < tmp_text.textInfo.lineCount; i++)
        {
            if(max < tmp_text.textInfo.characterInfo[tmp_text.textInfo.lineInfo[i].lastCharacterIndex].xAdvance)
            {
                max = tmp_text.textInfo.characterInfo[tmp_text.textInfo.lineInfo[i].lastCharacterIndex].xAdvance;
            }
        }
        resizeX = max * 2;
        resizeY = ((textMeshPro.fontSize / 8)* tmp_text.textInfo.lineCount + 1) + (tmp_text.textInfo.lineCount * 0.5f);

        if (resizeX > textMeshPro.rectTransform.sizeDelta.x)
        {
            resizeX = textMeshPro.rectTransform.sizeDelta.x;
        }
        if (resizeY > textMeshPro.rectTransform.sizeDelta.y)
        {
            resizeY = textMeshPro.rectTransform.sizeDelta.y;
        }

        if (resizeX < 10)
        {
            resizeX = 10;
        }
        if (resizeY < 10)
        {
            resizeY = 10;
        }
        col.size = new Vector2(resizeX, resizeY);
    }

    public void LocalizationText()
    {
        switch (LocalizationManager.Language)
        {
            case "Korean":
                tmp_text.font = korean;
                koreanMaterial = korean.material;
                tmp_text.fontSharedMaterial = koreanMaterial;
                break;
            case "English":
                tmp_text.font = japanese;
                japaneseMaterial = japanese.material;
                tmp_text.fontSharedMaterial = japaneseMaterial;
                break;
            case "Japanese":
                tmp_text.font = japanese;
                japaneseMaterial = japanese.material;
                tmp_text.fontSharedMaterial = japaneseMaterial;
                break;
        }
    }
    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
