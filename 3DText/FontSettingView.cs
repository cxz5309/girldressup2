﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FontSettingView : MonoBehaviour
{
    public GameObject goFontSettingPanel;
    public GameObject[] checkColorImg = new GameObject[12];
    //public GameObject[] checkSizeImg = new GameObject[3];

    private GameObject currentObj;

    public VerticalViewUI verticalViewUI;

    private int prevColBtn;
    private int prevSizeBtn;


    private void Update()
    {
        if (goFontSettingPanel.activeSelf)
        {
            if (Input.GetMouseButtonUp(0))
            {
                if (EventSystem.current.currentSelectedGameObject != null)
                {
                    currentObj = EventSystem.current.currentSelectedGameObject;
                    if (!currentObj.CompareTag("FontSettingGroup") &&
                        !currentObj.CompareTag("DismissButton"))
                    {
                        DisMiss();
                    }
                }
                else
                {
                    DisMiss();
                }
            }
        }
    }

    private void OnEnable()
    {
        int colorNum = PlayerPrefs.GetInt(StringInfo.playerPrefsKeyList[4]);
        //int sizeNum = PlayerPrefs.GetInt("FontSize");
        prevColBtn = colorNum;
        //prevSizeBtn = sizeNum;

        checkColorImg[colorNum].GetComponent<Image>().color = Color.gray;
        //switchSizeBtn(sizeNum, true);
    }

    public void OnColorButtonClick(int i)
    {
        PlayerPrefs.SetInt(StringInfo.playerPrefsKeyList[4], i);
        PlayerPrefs.Save();

        if(i!= prevColBtn)
        {
            checkColorImg[prevColBtn].GetComponent<Image>().color = Color.white;
            checkColorImg[i].GetComponent<Image>().color = Color.gray;

            ItemTypeButtonUI itemTypeButtonUI = verticalViewUI.textSettingButton.GetComponent<ItemTypeButtonUI>();
            Color color = ObjectInfo.arrayTextColor[i];
            itemTypeButtonUI.ChangeButtonColor(color);
            itemTypeButtonUI.SetButtonImage();
            itemTypeButtonUI.UnsetbuttonBackImage();
            prevColBtn = i;
        }
    }
    //public void OnSizeButtonClick(int i)
    //{
    //    PlayerPrefs.SetInt("FontSize", i);
    //    PlayerPrefs.Save();

    //    if (i != prevSizeBtn)
    //    {
    //        switchSizeBtn(prevSizeBtn, false);
    //        switchSizeBtn(i, true);
    //        prevSizeBtn = i;
    //    }
    //}

    //void switchSizeBtn(int sizeBtnNum, bool press)
    //{
    //    switch (sizeBtnNum)
    //    {
    //        case 0:
    //            if (press)
    //            {
    //                checkSizeImg[sizeBtnNum].GetComponent<Image>().sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "txt_sz_btn01_p");
    //            }
    //            else
    //            {
    //                checkSizeImg[sizeBtnNum].GetComponent<Image>().sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "txt_sz_btn01_n");
    //            }
    //            break;
    //        case 1:
    //            if (press)
    //            {
    //                checkSizeImg[sizeBtnNum].GetComponent<Image>().sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "txt_sz_btn02_p");
    //            }
    //            else
    //            {
    //                checkSizeImg[sizeBtnNum].GetComponent<Image>().sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "txt_sz_btn02_n");
    //            }
    //            break;
    //        case 2:
    //            if (press)
    //            {
    //                checkSizeImg[sizeBtnNum].GetComponent<Image>().sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "txt_sz_btn03_p");
    //            }
    //            else
    //            {
    //                checkSizeImg[sizeBtnNum].GetComponent<Image>().sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "txt_sz_btn03_n");
    //            }
    //            break;
    //    }
    //}

    public void DisMiss()
    {
        goFontSettingPanel.SetActive(false);
    }
}
