﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColiderLineRender : MonoBehaviour
{
    public BoxCollider2D col;
    public LineRenderer[] lines = new LineRenderer[4];
    float resizeX;
    float resizeY;

    Vector2[] resizePos = new Vector2[4];

    void Update()
    {
        resizeX = col.size.x;
        resizeY = col.size.y;
        //꼭지점 정해주기
        resizePos[0] = new Vector2(-resizeX, resizeY)/2;
        resizePos[1] = new Vector2(resizeX, resizeY)/2;
        resizePos[2] = new Vector2(resizeX, -resizeY)/2;
        resizePos[3] = new Vector2(-resizeX, -resizeY)/2;
        //라인위치
        for (int i = 0; i < 4; i++)
        {
            lines[i].SetPosition(0, resizePos[(i) % 4]);
            lines[i].SetPosition(1, resizePos[(i + 1) % 4]);
            lines[i].SetPosition(0, resizePos[(i) % 4]);
            lines[i].SetPosition(1, resizePos[(i + 1) % 4]);
        }
    }
}
