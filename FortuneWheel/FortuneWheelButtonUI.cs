﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class FortuneWheelButtonUI : MonoBehaviour
{
    public Image coolDownImage;
    public GameObject FortuneWheelPanel;
    public Text secontText;

    StringBuilder stringBuilder = new StringBuilder();

    private void Start()
    {
        if (InGameManager.Instance.wheelUpdateTime >= ObjectInfo.ITEM_COOL_TIME)
        {
            coolDownImage.fillAmount = 1f;
            secontText.text = StringInfo.GO;
            InGameManager.Instance.wheelUpdateTime = ObjectInfo.ITEM_COOL_TIME;
            InGameManager.Instance.canSlider = true;
        }
        else
        {
            InGameManager.Instance.canSlider = false;
        }
    }

    private void Update()
    {
        if (InGameManager.Instance != null)
        {
            if (!InGameManager.Instance.canSlider)
            {
                if (InGameManager.Instance.wheelUpdateTime >= ObjectInfo.ITEM_COOL_TIME)
                {
                    coolDownImage.fillAmount = 1f;
                    secontText.text = StringInfo.GO;
                    InGameManager.Instance.canSlider = true;
                    return;
                }
                coolDownImage.fillAmount = Mathf.SmoothStep(0, 100, InGameManager.Instance.wheelUpdateTime / ObjectInfo.ITEM_COOL_TIME) * 0.01f;

                //stringBuilder.Clear();
                //stringBuilder.Append(min.ToString("D2")).Append(" : ").Append(second.ToString("D2"));

                secontText.text = string.Format(StringInfo.WheelTimeFormat, InGameManager.Instance.wheelMin, InGameManager.Instance.wheelSecond);
            }
        }
    }

    public void OnFortuneWheelClick()
    {
        FortuneWheelPanel.SetActive(true);
    }

    public void InitCooldownStart()
    {
        InGameManager.Instance.canSlider = false;
        InGameManager.Instance.wheelStartTime = Time.realtimeSinceStartup;
        InGameManager.Instance.wheelUpdateTime = 0;//업데이트 되자마자 조건 충족하여 canSlider true 되는 문제해결
    }
    public void InitCooldownEnd()
    {
        InGameManager.Instance.canSlider = true;
    }
}
