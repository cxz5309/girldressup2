﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.SimpleLocalization;
using System.Text;
using UnityEngine.SceneManagement;

public class FortuneWheelView : MonoBehaviour
{
    public GameObject goFortuneWheelPanel;
    public MkeyFW.WheelController wheelController;
    public Mkey.CoinProcAnim coinProcAnim;
    public SpriteRenderer sectorZeroItemSprite;
    public GameObject sectorZeroCoinSprite;
    public SpriteRenderer sectorFourItemSprite;
    public GameObject sectorFourCoinSprite;

    public MkeyFW.SpinButton spinButton;
    public MkeyFW.PointerBehavior pointerBehavior;
    public Image coolDownImg;
    public Text coolDownText;

    public FortuneWheelButtonUI fortunewheelButtonUI;
    public ItemGetPopupUI itemGetPopup;
    public AlertView alertView;
    public SAlertViewUI sAlertView;

    public int selSector;
    public int coin;
    public int adVal = 1;
    public string seqName;
    public string seq;
    public bool isMoving;

    StringBuilder stringBuilder = new StringBuilder();

    private void OnEnable()
    {
        DoTweenUtil.DoPopupOpen(30f, 82f, .4f, wheelController.transform);
        for(int i=0;i< coinProcAnim.coinsL.Count; i++)
        {
            Destroy(coinProcAnim.coinsL[i]);
        }
        wheelController.Reel.transform.localRotation = Quaternion.Euler(0, 0, 22f);
        wheelController.currSector = 0;
        //wheelController.InitSpin();
    }

    private void Start()
    {
        SetZeroItem();

        if (InGameManager.Instance.wheelUpdateTime >= ObjectInfo.ITEM_COOL_TIME)
        {
            coolDownText.text = LocalizationManager.Localize(StringInfo.StartWheelButton);
            coolDownImg.fillAmount = 1f;
        }
        else
        {
            //stringBuilder.Clear();
            //stringBuilder.Append(min.ToString("D2")).Append(" : ").Append(second.ToString("D2"));
            //coolDownText.text = stringBuilder.ToString();

            coolDownText.text = string.Format(StringInfo.WheelTimeFormat, InGameManager.Instance.wheelMin, InGameManager.Instance.wheelSecond);
        }
    }

    private void Update()
    {
        if (InGameManager.Instance != null)
        {
            if (InGameManager.Instance.wheelUpdateTime >= ObjectInfo.ITEM_COOL_TIME)
            {
                coolDownText.text = LocalizationManager.Localize(StringInfo.StartWheelButton);

                coolDownImg.fillAmount = 1f;
                return;
            }
            else
            {
                coolDownImg.fillAmount = Mathf.SmoothStep(0, 100, InGameManager.Instance.wheelUpdateTime / ObjectInfo.ITEM_COOL_TIME) * 0.01f;

                //stringBuilder.Clear();
                //stringBuilder.Append(min.ToString("D2")).Append(" : ").Append(second.ToString("D2"));
                //coolDownText.text = stringBuilder.ToString();

                coolDownText.text = string.Format(StringInfo.WheelTimeFormat, InGameManager.Instance.wheelMin, InGameManager.Instance.wheelSecond);
            }
        }
    }

    public void SetZeroItem()
    {
        if (ItemSeedInfo.nowSeed1 == -1)
        {
            sectorZeroItemSprite.gameObject.SetActive(false);
            sectorZeroCoinSprite.gameObject.SetActive(true);
        }
        else
        {
            sectorZeroItemSprite.gameObject.SetActive(true);
            sectorZeroCoinSprite.gameObject.SetActive(false);
            sectorZeroItemSprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllPartsAtlas, ParseItemSeq.ItemSeqToStringSpName(ItemSeedInfo.listFortuneWheel[ItemSeedInfo.nowSeed1].itemSeq));
        }
        if (ItemSeedInfo.nowSeed2 == -1)
        {
            sectorFourItemSprite.gameObject.SetActive(false);
            sectorFourCoinSprite.gameObject.SetActive(true);
        }
        else
        {
            sectorFourItemSprite.gameObject.SetActive(true);
            sectorFourCoinSprite.gameObject.SetActive(false);
            sectorFourItemSprite.sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllPartsAtlas, ParseItemSeq.ItemSeqToStringSpName(ItemSeedInfo.listFortuneWheel[ItemSeedInfo.nowSeed2].itemSeq));
        }
    }

    //룰렛 패널 끌 경우
    public void DisMiss()
    {
        if (!DoTweenUtil.isTweening)
        {
            //스핀멈추고 라이트 멈추고 쿨타임 초기화한다
            if (wheelController.isSpin)
            {
                wheelController.CancelSpin();
                wheelController.CancelLight();
                wheelController.CancelInvoke();
                InitCooldownEnd();
            }
            coinProcAnim.CancelInvoke();
            //소리도 멈춘다
            AudioManager.Inst.StopAllSFX();
            pointerBehavior.isPlaying = false;
            //애니메이션도 삭제한다
            for (int i = 0; i < coinProcAnim.coinsL.Count; i++)
            {
                Destroy(coinProcAnim.coinsL[i]);
            }
            goFortuneWheelPanel.SetActive(false);
        }
    }

    public void SetCoin(int coins)
    {
        coin = coins * adVal;
    }


    //룰렛이 멈췄을때
    public void GetSectorAndGetSelItem(int selSector)
    {
        if (selSector == 0)
        {
            if (ItemSeedInfo.nowSeed1 == -1)
            {
                GetCoin();

                itemGetPopup.SetStartAction(() =>
                {
                    SetItemGetPopup(coin.ToString() + " " + LocalizationManager.Localize(StringInfo.COIN) + " " + LocalizationManager.Localize(StringInfo.GET_IT), SetCoinSprite(selSector));
                    InGameManager.Instance.StartEffect(5, itemGetPopup.coinStartPos.position);
                });

                itemGetPopup.SetOnClickAction(() =>
                {
                    AudioManager.Inst.PlaySFX("gaining-bonus-points-cut2");
                    InGameManager.Instance.StartEffect(3, itemGetPopup.coinStartPos.position);
                });
            }
            else
            {
                seqName = ItemSeedInfo.listFortuneWheel[ItemSeedInfo.nowSeed1].seqName;
                seq = ItemSeedInfo.listFortuneWheel[ItemSeedInfo.nowSeed1].itemSeq;
                string spName = ParseItemSeq.ItemSeqToStringSpName(seq);

                Sprite sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, spName);

                GetItem();

                itemGetPopup.SetStartAction(() =>
                {
                    SetItemGetPopup(LocalizationManager.Localize(StringInfo.ITEM) + " " + LocalizationManager.Localize(StringInfo.GET_IT), sprite);
                    InGameManager.Instance.StartEffect(4, itemGetPopup.coinStartPos.position);
                });

                itemGetPopup.SetOnClickAction(() =>
                {
                });
            }
        }
        else if (selSector == 4)
        {
            if (ItemSeedInfo.nowSeed2 == -1)
            {
                GetCoin();

                itemGetPopup.SetStartAction(() =>
                {
                    SetItemGetPopup(coin.ToString() + " " + LocalizationManager.Localize(StringInfo.COIN) + " " + LocalizationManager.Localize(StringInfo.GET_IT), SetCoinSprite(selSector));
                    InGameManager.Instance.StartEffect(5, itemGetPopup.coinStartPos.position);
                });
                itemGetPopup.SetOnClickAction(() =>
                {
                    AudioManager.Inst.PlaySFX("gaining-bonus-points-cut2");
                    InGameManager.Instance.StartEffect(3, itemGetPopup.coinStartPos.position);
                });
            }
            else
            {
                seqName = ItemSeedInfo.listFortuneWheel[ItemSeedInfo.nowSeed2].seqName;
                seq = ItemSeedInfo.listFortuneWheel[ItemSeedInfo.nowSeed2].itemSeq;
                string spName = ParseItemSeq.ItemSeqToStringSpName(seq);

                Sprite sprite = AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, spName);

                GetItem();

                itemGetPopup.SetStartAction(() =>
                {
                    SetItemGetPopup(LocalizationManager.Localize(StringInfo.ITEM) + " " + LocalizationManager.Localize(StringInfo.GET_IT), sprite);
                    InGameManager.Instance.StartEffect(4, itemGetPopup.coinStartPos.position);
                });
                itemGetPopup.SetOnClickAction(() =>
                {
                });
            }
        }
        else
        {
            GetCoin();

            itemGetPopup.SetStartAction(() =>
            {
                SetItemGetPopup(coin.ToString() + " " + LocalizationManager.Localize(StringInfo.COIN) + " " + LocalizationManager.Localize(StringInfo.GET_IT), SetCoinSprite(selSector));
                InGameManager.Instance.StartEffect(5, itemGetPopup.coinStartPos.position);
            });
            itemGetPopup.SetOnClickAction(() =>
            {
                AudioManager.Inst.PlaySFX("gaining-bonus-points-cut2");
                InGameManager.Instance.StartEffect(3, itemGetPopup.coinStartPos.position);
            });
        }
        InitCooldownStart();
        XMLManager.Instance.SaveMissionValueUp(XMLManager.Instance.nodeWheelTimes);
        ItemSeedInfo.SetRandomWheelSeed();
        SetZeroItem();

        MissionSeed.CheckAllMission();

        itemGetPopup.gameObject.SetActive(true);
    }

    public Sprite SetCoinSprite(int selSector)
    {
        switch (selSector)
        {
            default:
                return AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "coin_01");
            case 0:
                return AtlasInfo.GetAtlasNotNumbering(AtlasInfo.AllUIAtlas, "coin_04");
        }
    }

    public void GetCoin()
    {
        InGameManager.Instance.AddCoins(coin);
    }

    public void GetItem()
    {
        InGameManager.Instance.AddItem(seqName, seq, ItemGetRoot.wheel);//itemGetType 0 : 구매, 1 : 데일리, 2 : 룰렛,  3: 그 외
        InGameManager.Instance.InitItemGet();
    }

    public void SetItemGetPopup(string text, Sprite sprite)
    {
        itemGetPopup.SetText(text);
        if (sprite != null)
        {
            itemGetPopup.SetImage(sprite);
        }
    }

    public void InitCooldownStart()
    {
        fortunewheelButtonUI.InitCooldownStart();
    }
    public void InitCooldownEnd()
    {
        fortunewheelButtonUI.InitCooldownEnd();
    }

    public void SetWheelInteractable()
    {
        if (!InGameManager.Instance.canSlider)
        {
            spinButton.interactable = false;
        }
        else
        {
            spinButton.interactable = true;
        }
    }

    public void AlertCooldown()
    {
        sAlertView.SAlert(LocalizationManager.Localize(StringInfo.WheelADInfo));
    }
}
